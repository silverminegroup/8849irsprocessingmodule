using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace IRSIApplication
{
   public interface IMethods
    {
       string SendSubmission(ref DataTable dt);
       string GetSingleAcknowledgements(ref DataRow dr);
       string GetBulkAcknowledgements(ref string count); 
       string Logout();
       string Login();

       bool SendMail(string pTo, string pCC, string bCC, string pSubject, string pBody);
       
       DataTable GetLatestRecords(ref string clsRP);
       DataTable Search(ref string clsRP);
       int UpdateMailSentStatus(ref string clsRP);
       string CheckAuthentication(ref string clsRP);
       DataSet DisplaySubmissionData(ref string clsRP);
       DataSet NotifyUser(ref string clsRP);
       string LockRecord(ref string clsRP);                
       int UnLockRecord(ref string clsRP);
       int BlockRecord(ref string clsRP);
    }
}
