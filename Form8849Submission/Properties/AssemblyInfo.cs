﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("eForm8849 Submission")]
[assembly: AssemblyDescription("eForm8849 Submission Module")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Silvermine Group")]
[assembly: AssemblyProduct("eForm8849 IRS Authorized Submissions")]
[assembly: AssemblyCopyright("Copyright © Silvermine Group 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("df81a1cb-e140-4062-9d8e-44f52fc0c40b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
