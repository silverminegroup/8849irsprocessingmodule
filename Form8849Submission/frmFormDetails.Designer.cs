namespace Form8849Sub
{
    partial class frmFormDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlSubmissionInfo = new System.Windows.Forms.TabControl();
            this.tabBusinessInfo = new System.Windows.Forms.TabPage();
            this.lblBusinessType = new System.Windows.Forms.Label();
            this.lblBusinessPhone = new System.Windows.Forms.Label();
            this.lblBusinessName = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblTPPhone = new System.Windows.Forms.Label();
            this.lblTPPIN = new System.Windows.Forms.Label();
            this.lblTPName = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblBusinessModifiedOn = new System.Windows.Forms.Label();
            this.lblBusinessCreatedOn = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblPreparerZip = new System.Windows.Forms.Label();
            this.lblPreparerAdd = new System.Windows.Forms.Label();
            this.lblPreparerCountry = new System.Windows.Forms.Label();
            this.lblPreparerState = new System.Windows.Forms.Label();
            this.lblPreparerCity = new System.Windows.Forms.Label();
            this.lblPreparerEmail = new System.Windows.Forms.Label();
            this.lblPreparerPh = new System.Windows.Forms.Label();
            this.lblPreparerPhoneLocation = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.lblPreparerName = new System.Windows.Forms.Label();
            this.lblPreparerSSNPOrPTIN_No = new System.Windows.Forms.Label();
            this.lblPreparerSelfEmployed = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.lblPreparerSSNOrPTIN = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblOfficerName = new System.Windows.Forms.Label();
            this.lblOfficerPhone = new System.Windows.Forms.Label();
            this.lblOfficerPIN = new System.Windows.Forms.Label();
            this.lblOfficerTitle = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblBusinessAdd = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblBusinessZip = new System.Windows.Forms.Label();
            this.lblBusinessCountry = new System.Windows.Forms.Label();
            this.lblBusinessState = new System.Windows.Forms.Label();
            this.lblBusinessCity = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.tabFormInfo = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tabTaxableVeh = new System.Windows.Forms.TabPage();
            this.dgvVehicles_taxable = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabSuspendedVeh = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvVehicles_Suspended = new System.Windows.Forms.DataGridView();
            this.tabCreditVeh = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvVehicles_Credit = new System.Windows.Forms.DataGridView();
            this.tabSoldSuspendedVeh = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvVehicles_SoldSuspended = new System.Windows.Forms.DataGridView();
            this.tabMileageExceededVeh = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvVehicles_MileageExceeded = new System.Windows.Forms.DataGridView();
            this.tabTGWIncreasedVeh = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvVehicles_WeightIncreased = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblLstName = new System.Windows.Forms.Label();
            this.lblReferral = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblMidName = new System.Windows.Forms.Label();
            this.lblSignupIP = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblFstName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblCreatedOn = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFilingStatus = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControlSubmissionInfo.SuspendLayout();
            this.tabBusinessInfo.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabFormInfo.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabTaxableVeh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_taxable)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabSuspendedVeh.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_Suspended)).BeginInit();
            this.tabCreditVeh.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_Credit)).BeginInit();
            this.tabSoldSuspendedVeh.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_SoldSuspended)).BeginInit();
            this.tabMileageExceededVeh.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_MileageExceeded)).BeginInit();
            this.tabTGWIncreasedVeh.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_WeightIncreased)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlSubmissionInfo
            // 
            this.tabControlSubmissionInfo.Controls.Add(this.tabBusinessInfo);
            this.tabControlSubmissionInfo.Controls.Add(this.tabFormInfo);
            this.tabControlSubmissionInfo.Controls.Add(this.tabTaxableVeh);
            this.tabControlSubmissionInfo.Controls.Add(this.tabSuspendedVeh);
            this.tabControlSubmissionInfo.Controls.Add(this.tabCreditVeh);
            this.tabControlSubmissionInfo.Controls.Add(this.tabSoldSuspendedVeh);
            this.tabControlSubmissionInfo.Controls.Add(this.tabMileageExceededVeh);
            this.tabControlSubmissionInfo.Controls.Add(this.tabTGWIncreasedVeh);
            this.tabControlSubmissionInfo.Location = new System.Drawing.Point(-3, 112);
            this.tabControlSubmissionInfo.Name = "tabControlSubmissionInfo";
            this.tabControlSubmissionInfo.Padding = new System.Drawing.Point(15, 9);
            this.tabControlSubmissionInfo.SelectedIndex = 0;
            this.tabControlSubmissionInfo.Size = new System.Drawing.Size(1041, 414);
            this.tabControlSubmissionInfo.TabIndex = 0;
            // 
            // tabBusinessInfo
            // 
            this.tabBusinessInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabBusinessInfo.Controls.Add(this.lblBusinessType);
            this.tabBusinessInfo.Controls.Add(this.lblBusinessPhone);
            this.tabBusinessInfo.Controls.Add(this.lblBusinessName);
            this.tabBusinessInfo.Controls.Add(this.groupBox4);
            this.tabBusinessInfo.Controls.Add(this.groupBox2);
            this.tabBusinessInfo.Controls.Add(this.groupBox6);
            this.tabBusinessInfo.Controls.Add(this.groupBox3);
            this.tabBusinessInfo.Controls.Add(this.groupBox1);
            this.tabBusinessInfo.Controls.Add(this.label22);
            this.tabBusinessInfo.Controls.Add(this.label21);
            this.tabBusinessInfo.Controls.Add(this.label20);
            this.tabBusinessInfo.Controls.Add(this.panel9);
            this.tabBusinessInfo.Location = new System.Drawing.Point(4, 34);
            this.tabBusinessInfo.Margin = new System.Windows.Forms.Padding(0);
            this.tabBusinessInfo.Name = "tabBusinessInfo";
            this.tabBusinessInfo.Size = new System.Drawing.Size(1033, 376);
            this.tabBusinessInfo.TabIndex = 0;
            this.tabBusinessInfo.Text = "Business Information";
            // 
            // lblBusinessType
            // 
            this.lblBusinessType.AutoSize = true;
            this.lblBusinessType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessType.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessType.Location = new System.Drawing.Point(811, 53);
            this.lblBusinessType.Name = "lblBusinessType";
            this.lblBusinessType.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessType.TabIndex = 31;
            this.lblBusinessType.Text = "--------";
            // 
            // lblBusinessPhone
            // 
            this.lblBusinessPhone.AutoSize = true;
            this.lblBusinessPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessPhone.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessPhone.Location = new System.Drawing.Point(594, 53);
            this.lblBusinessPhone.Name = "lblBusinessPhone";
            this.lblBusinessPhone.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessPhone.TabIndex = 30;
            this.lblBusinessPhone.Text = "--------";
            // 
            // lblBusinessName
            // 
            this.lblBusinessName.AutoSize = true;
            this.lblBusinessName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessName.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessName.Location = new System.Drawing.Point(140, 53);
            this.lblBusinessName.Name = "lblBusinessName";
            this.lblBusinessName.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessName.TabIndex = 29;
            this.lblBusinessName.Text = "--------";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox4.Controls.Add(this.lblTPPhone);
            this.groupBox4.Controls.Add(this.lblTPPIN);
            this.groupBox4.Controls.Add(this.lblTPName);
            this.groupBox4.Controls.Add(this.label50);
            this.groupBox4.Controls.Add(this.label52);
            this.groupBox4.Controls.Add(this.label53);
            this.groupBox4.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox4.Location = new System.Drawing.Point(466, 301);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(557, 67);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Third Party Assignee";
            // 
            // lblTPPhone
            // 
            this.lblTPPhone.AutoSize = true;
            this.lblTPPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTPPhone.ForeColor = System.Drawing.Color.Blue;
            this.lblTPPhone.Location = new System.Drawing.Point(345, 26);
            this.lblTPPhone.Name = "lblTPPhone";
            this.lblTPPhone.Size = new System.Drawing.Size(48, 17);
            this.lblTPPhone.TabIndex = 22;
            this.lblTPPhone.Text = "--------";
            // 
            // lblTPPIN
            // 
            this.lblTPPIN.AutoSize = true;
            this.lblTPPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTPPIN.ForeColor = System.Drawing.Color.Blue;
            this.lblTPPIN.Location = new System.Drawing.Point(495, 26);
            this.lblTPPIN.Name = "lblTPPIN";
            this.lblTPPIN.Size = new System.Drawing.Size(48, 17);
            this.lblTPPIN.TabIndex = 21;
            this.lblTPPIN.Text = "--------";
            // 
            // lblTPName
            // 
            this.lblTPName.AutoSize = true;
            this.lblTPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTPName.ForeColor = System.Drawing.Color.Blue;
            this.lblTPName.Location = new System.Drawing.Point(127, 27);
            this.lblTPName.Name = "lblTPName";
            this.lblTPName.Size = new System.Drawing.Size(48, 17);
            this.lblTPName.TabIndex = 20;
            this.lblTPName.Text = "--------";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(21, 27);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(49, 17);
            this.label50.TabIndex = 11;
            this.label50.Text = "Name:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(466, 26);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 17);
            this.label52.TabIndex = 10;
            this.label52.Text = "PIN:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(284, 26);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(53, 17);
            this.label53.TabIndex = 9;
            this.label53.Text = "Phone:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblBusinessModifiedOn);
            this.groupBox2.Controls.Add(this.lblBusinessCreatedOn);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox2.Location = new System.Drawing.Point(18, 301);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(442, 67);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Timestamp";
            // 
            // lblBusinessModifiedOn
            // 
            this.lblBusinessModifiedOn.AutoSize = true;
            this.lblBusinessModifiedOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessModifiedOn.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessModifiedOn.Location = new System.Drawing.Point(297, 26);
            this.lblBusinessModifiedOn.Name = "lblBusinessModifiedOn";
            this.lblBusinessModifiedOn.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessModifiedOn.TabIndex = 26;
            this.lblBusinessModifiedOn.Text = "--------";
            // 
            // lblBusinessCreatedOn
            // 
            this.lblBusinessCreatedOn.AutoSize = true;
            this.lblBusinessCreatedOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessCreatedOn.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessCreatedOn.Location = new System.Drawing.Point(78, 26);
            this.lblBusinessCreatedOn.Name = "lblBusinessCreatedOn";
            this.lblBusinessCreatedOn.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessCreatedOn.TabIndex = 25;
            this.lblBusinessCreatedOn.Text = "--------";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(18, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(62, 17);
            this.label28.TabIndex = 13;
            this.label28.Text = "Created:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(240, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 17);
            this.label27.TabIndex = 12;
            this.label27.Text = "Modified:";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox6.Controls.Add(this.lblPreparerZip);
            this.groupBox6.Controls.Add(this.lblPreparerAdd);
            this.groupBox6.Controls.Add(this.lblPreparerCountry);
            this.groupBox6.Controls.Add(this.lblPreparerState);
            this.groupBox6.Controls.Add(this.lblPreparerCity);
            this.groupBox6.Controls.Add(this.lblPreparerEmail);
            this.groupBox6.Controls.Add(this.lblPreparerPh);
            this.groupBox6.Controls.Add(this.lblPreparerPhoneLocation);
            this.groupBox6.Controls.Add(this.label86);
            this.groupBox6.Controls.Add(this.label85);
            this.groupBox6.Controls.Add(this.label84);
            this.groupBox6.Controls.Add(this.label83);
            this.groupBox6.Controls.Add(this.label80);
            this.groupBox6.Controls.Add(this.label81);
            this.groupBox6.Controls.Add(this.label82);
            this.groupBox6.Controls.Add(this.lblPreparerName);
            this.groupBox6.Controls.Add(this.lblPreparerSSNPOrPTIN_No);
            this.groupBox6.Controls.Add(this.lblPreparerSelfEmployed);
            this.groupBox6.Controls.Add(this.label74);
            this.groupBox6.Controls.Add(this.label75);
            this.groupBox6.Controls.Add(this.lblPreparerSSNOrPTIN);
            this.groupBox6.Controls.Add(this.label78);
            this.groupBox6.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox6.Location = new System.Drawing.Point(466, 99);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(557, 196);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Paid Preparer";
            // 
            // lblPreparerZip
            // 
            this.lblPreparerZip.AutoSize = true;
            this.lblPreparerZip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerZip.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerZip.Location = new System.Drawing.Point(345, 132);
            this.lblPreparerZip.Name = "lblPreparerZip";
            this.lblPreparerZip.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerZip.TabIndex = 43;
            this.lblPreparerZip.Text = "--------";
            // 
            // lblPreparerAdd
            // 
            this.lblPreparerAdd.AutoSize = true;
            this.lblPreparerAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerAdd.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerAdd.Location = new System.Drawing.Point(345, 24);
            this.lblPreparerAdd.Name = "lblPreparerAdd";
            this.lblPreparerAdd.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerAdd.TabIndex = 42;
            this.lblPreparerAdd.Text = "--------";
            // 
            // lblPreparerCountry
            // 
            this.lblPreparerCountry.AutoSize = true;
            this.lblPreparerCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerCountry.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerCountry.Location = new System.Drawing.Point(345, 105);
            this.lblPreparerCountry.Name = "lblPreparerCountry";
            this.lblPreparerCountry.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerCountry.TabIndex = 41;
            this.lblPreparerCountry.Text = "--------";
            // 
            // lblPreparerState
            // 
            this.lblPreparerState.AutoSize = true;
            this.lblPreparerState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerState.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerState.Location = new System.Drawing.Point(345, 78);
            this.lblPreparerState.Name = "lblPreparerState";
            this.lblPreparerState.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerState.TabIndex = 40;
            this.lblPreparerState.Text = "--------";
            // 
            // lblPreparerCity
            // 
            this.lblPreparerCity.AutoSize = true;
            this.lblPreparerCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerCity.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerCity.Location = new System.Drawing.Point(345, 51);
            this.lblPreparerCity.Name = "lblPreparerCity";
            this.lblPreparerCity.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerCity.TabIndex = 39;
            this.lblPreparerCity.Text = "--------";
            // 
            // lblPreparerEmail
            // 
            this.lblPreparerEmail.AutoSize = true;
            this.lblPreparerEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerEmail.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerEmail.Location = new System.Drawing.Point(128, 159);
            this.lblPreparerEmail.Name = "lblPreparerEmail";
            this.lblPreparerEmail.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerEmail.TabIndex = 38;
            this.lblPreparerEmail.Text = "--------";
            // 
            // lblPreparerPh
            // 
            this.lblPreparerPh.AutoSize = true;
            this.lblPreparerPh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerPh.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerPh.Location = new System.Drawing.Point(128, 132);
            this.lblPreparerPh.Name = "lblPreparerPh";
            this.lblPreparerPh.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerPh.TabIndex = 37;
            this.lblPreparerPh.Text = "--------";
            // 
            // lblPreparerPhoneLocation
            // 
            this.lblPreparerPhoneLocation.AutoSize = true;
            this.lblPreparerPhoneLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerPhoneLocation.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerPhoneLocation.Location = new System.Drawing.Point(128, 105);
            this.lblPreparerPhoneLocation.Name = "lblPreparerPhoneLocation";
            this.lblPreparerPhoneLocation.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerPhoneLocation.TabIndex = 36;
            this.lblPreparerPhoneLocation.Text = "--------";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.Transparent;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(22, 105);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(111, 17);
            this.label86.TabIndex = 31;
            this.label86.Text = "Phone Location:";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.Color.Transparent;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(22, 132);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(53, 17);
            this.label85.TabIndex = 30;
            this.label85.Text = "Phone:";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.Color.Transparent;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(22, 159);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(46, 17);
            this.label84.TabIndex = 29;
            this.label84.Text = "Email:";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.Transparent;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(284, 105);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(61, 17);
            this.label83.TabIndex = 28;
            this.label83.Text = "Country:";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(284, 24);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(64, 17);
            this.label80.TabIndex = 27;
            this.label80.Text = "Address:";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.Color.Transparent;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(284, 51);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(35, 17);
            this.label81.TabIndex = 26;
            this.label81.Text = "City:";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.BackColor = System.Drawing.Color.Transparent;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(284, 78);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(45, 17);
            this.label82.TabIndex = 25;
            this.label82.Text = "State:";
            // 
            // lblPreparerName
            // 
            this.lblPreparerName.AutoSize = true;
            this.lblPreparerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerName.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerName.Location = new System.Drawing.Point(127, 24);
            this.lblPreparerName.Name = "lblPreparerName";
            this.lblPreparerName.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerName.TabIndex = 23;
            this.lblPreparerName.Text = "--------";
            // 
            // lblPreparerEIN
            // 
            this.lblPreparerSSNPOrPTIN_No.AutoSize = true;
            this.lblPreparerSSNPOrPTIN_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerSSNPOrPTIN_No.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerSSNPOrPTIN_No.Location = new System.Drawing.Point(127, 78);
            this.lblPreparerSSNPOrPTIN_No.Name = "lblPreparerEIN";
            this.lblPreparerSSNPOrPTIN_No.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerSSNPOrPTIN_No.TabIndex = 22;
            this.lblPreparerSSNPOrPTIN_No.Text = "--------";
            // 
            // lblPreparerSelfEmployed
            // 
            this.lblPreparerSelfEmployed.AutoSize = true;
            this.lblPreparerSelfEmployed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerSelfEmployed.ForeColor = System.Drawing.Color.Blue;
            this.lblPreparerSelfEmployed.Location = new System.Drawing.Point(127, 51);
            this.lblPreparerSelfEmployed.Name = "lblPreparerSelfEmployed";
            this.lblPreparerSelfEmployed.Size = new System.Drawing.Size(48, 17);
            this.lblPreparerSelfEmployed.TabIndex = 20;
            this.lblPreparerSelfEmployed.Text = "--------";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(21, 51);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(102, 17);
            this.label74.TabIndex = 11;
            this.label74.Text = "Self Employed:";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(21, 24);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(49, 17);
            this.label75.TabIndex = 14;
            this.label75.Text = "Name:";
            // 
            // lblPreparerSSNOrPTIN
            // 
            this.lblPreparerSSNOrPTIN.AutoSize = true;
            this.lblPreparerSSNOrPTIN.BackColor = System.Drawing.Color.Transparent;
            this.lblPreparerSSNOrPTIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparerSSNOrPTIN.ForeColor = System.Drawing.Color.Black;
            this.lblPreparerSSNOrPTIN.Location = new System.Drawing.Point(21, 78);
            this.lblPreparerSSNOrPTIN.Name = "lblPreparerSSNOrPTIN";
            this.lblPreparerSSNOrPTIN.Size = new System.Drawing.Size(97, 17);
            this.lblPreparerSSNOrPTIN.TabIndex = 9;
            this.lblPreparerSSNOrPTIN.Text = "SSN/PTIN No:";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(284, 132);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(32, 17);
            this.label78.TabIndex = 8;
            this.label78.Text = "Zip:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblOfficerName);
            this.groupBox3.Controls.Add(this.lblOfficerPhone);
            this.groupBox3.Controls.Add(this.lblOfficerPIN);
            this.groupBox3.Controls.Add(this.lblOfficerTitle);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox3.Location = new System.Drawing.Point(18, 99);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(442, 89);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Officer Details";
            // 
            // lblOfficerName
            // 
            this.lblOfficerName.AutoSize = true;
            this.lblOfficerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficerName.ForeColor = System.Drawing.Color.Blue;
            this.lblOfficerName.Location = new System.Drawing.Point(76, 24);
            this.lblOfficerName.Name = "lblOfficerName";
            this.lblOfficerName.Size = new System.Drawing.Size(48, 17);
            this.lblOfficerName.TabIndex = 28;
            this.lblOfficerName.Text = "--------";
            // 
            // lblOfficerPhone
            // 
            this.lblOfficerPhone.AutoSize = true;
            this.lblOfficerPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficerPhone.ForeColor = System.Drawing.Color.Blue;
            this.lblOfficerPhone.Location = new System.Drawing.Point(297, 51);
            this.lblOfficerPhone.Name = "lblOfficerPhone";
            this.lblOfficerPhone.Size = new System.Drawing.Size(48, 17);
            this.lblOfficerPhone.TabIndex = 29;
            this.lblOfficerPhone.Text = "--------";
            // 
            // lblOfficerPIN
            // 
            this.lblOfficerPIN.AutoSize = true;
            this.lblOfficerPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficerPIN.ForeColor = System.Drawing.Color.Blue;
            this.lblOfficerPIN.Location = new System.Drawing.Point(76, 51);
            this.lblOfficerPIN.Name = "lblOfficerPIN";
            this.lblOfficerPIN.Size = new System.Drawing.Size(48, 17);
            this.lblOfficerPIN.TabIndex = 27;
            this.lblOfficerPIN.Text = "--------";
            // 
            // lblOfficerTitle
            // 
            this.lblOfficerTitle.AutoSize = true;
            this.lblOfficerTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOfficerTitle.ForeColor = System.Drawing.Color.Blue;
            this.lblOfficerTitle.Location = new System.Drawing.Point(297, 25);
            this.lblOfficerTitle.Name = "lblOfficerTitle";
            this.lblOfficerTitle.Size = new System.Drawing.Size(48, 17);
            this.lblOfficerTitle.TabIndex = 26;
            this.lblOfficerTitle.Text = "--------";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(18, 24);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(49, 17);
            this.label37.TabIndex = 17;
            this.label37.Text = "Name:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(240, 51);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 17);
            this.label38.TabIndex = 18;
            this.label38.Text = "Phone:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(18, 51);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(34, 17);
            this.label39.TabIndex = 19;
            this.label39.Text = "PIN:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(240, 25);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(39, 17);
            this.label40.TabIndex = 20;
            this.label40.Text = "Title:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.lblBusinessAdd);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.lblBusinessZip);
            this.groupBox1.Controls.Add(this.lblBusinessCountry);
            this.groupBox1.Controls.Add(this.lblBusinessState);
            this.groupBox1.Controls.Add(this.lblBusinessCity);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox1.Location = new System.Drawing.Point(18, 194);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 101);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Business Address";
            // 
            // lblBusinessAdd
            // 
            this.lblBusinessAdd.AutoSize = true;
            this.lblBusinessAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessAdd.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessAdd.Location = new System.Drawing.Point(76, 21);
            this.lblBusinessAdd.Name = "lblBusinessAdd";
            this.lblBusinessAdd.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessAdd.TabIndex = 20;
            this.lblBusinessAdd.Text = "--------";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(18, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 17);
            this.label26.TabIndex = 11;
            this.label26.Text = "Address:";
            // 
            // lblBusinessZip
            // 
            this.lblBusinessZip.AutoSize = true;
            this.lblBusinessZip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessZip.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessZip.Location = new System.Drawing.Point(297, 69);
            this.lblBusinessZip.Name = "lblBusinessZip";
            this.lblBusinessZip.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessZip.TabIndex = 24;
            this.lblBusinessZip.Text = "--------";
            // 
            // lblBusinessCountry
            // 
            this.lblBusinessCountry.AutoSize = true;
            this.lblBusinessCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessCountry.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessCountry.Location = new System.Drawing.Point(297, 45);
            this.lblBusinessCountry.Name = "lblBusinessCountry";
            this.lblBusinessCountry.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessCountry.TabIndex = 23;
            this.lblBusinessCountry.Text = "--------";
            // 
            // lblBusinessState
            // 
            this.lblBusinessState.AutoSize = true;
            this.lblBusinessState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessState.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessState.Location = new System.Drawing.Point(77, 69);
            this.lblBusinessState.Name = "lblBusinessState";
            this.lblBusinessState.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessState.TabIndex = 22;
            this.lblBusinessState.Text = "--------";
            // 
            // lblBusinessCity
            // 
            this.lblBusinessCity.AutoSize = true;
            this.lblBusinessCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessCity.ForeColor = System.Drawing.Color.Blue;
            this.lblBusinessCity.Location = new System.Drawing.Point(77, 45);
            this.lblBusinessCity.Name = "lblBusinessCity";
            this.lblBusinessCity.Size = new System.Drawing.Size(48, 17);
            this.lblBusinessCity.TabIndex = 21;
            this.lblBusinessCity.Text = "--------";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(240, 45);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 17);
            this.label29.TabIndex = 14;
            this.label29.Text = "Country:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(18, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 17);
            this.label25.TabIndex = 10;
            this.label25.Text = "City:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(18, 69);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 17);
            this.label24.TabIndex = 9;
            this.label24.Text = "State:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(240, 69);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 17);
            this.label23.TabIndex = 8;
            this.label23.Text = "Zip:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(488, 53);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 17);
            this.label22.TabIndex = 7;
            this.label22.Text = "Phone:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(24, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(110, 17);
            this.label21.TabIndex = 6;
            this.label21.Text = "Business Name:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(750, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 17);
            this.label20.TabIndex = 5;
            this.label20.Text = "Type:";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label7);
            this.panel9.Location = new System.Drawing.Point(-1, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1035, 30);
            this.panel9.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Business Profile Information for this submission";
            // 
            // tabFormInfo
            // 
            this.tabFormInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabFormInfo.Controls.Add(this.label16);
            this.tabFormInfo.Controls.Add(this.panel10);
            this.tabFormInfo.Controls.Add(this.groupBox5);
            this.tabFormInfo.Controls.Add(this.label64);
            this.tabFormInfo.Controls.Add(this.label63);
            this.tabFormInfo.Controls.Add(this.label62);
            this.tabFormInfo.Controls.Add(this.label61);
            this.tabFormInfo.Controls.Add(this.label60);
            this.tabFormInfo.Controls.Add(this.label59);
            this.tabFormInfo.Controls.Add(this.label58);
            this.tabFormInfo.Controls.Add(this.label57);
            this.tabFormInfo.Controls.Add(this.label56);
            this.tabFormInfo.Controls.Add(this.label55);
            this.tabFormInfo.Location = new System.Drawing.Point(4, 34);
            this.tabFormInfo.Name = "tabFormInfo";
            this.tabFormInfo.Size = new System.Drawing.Size(1033, 376);
            this.tabFormInfo.TabIndex = 7;
            this.tabFormInfo.Text = "2290 Form Info";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label79);
            this.panel10.Location = new System.Drawing.Point(-1, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1035, 30);
            this.panel10.TabIndex = 44;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(3, 8);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(234, 13);
            this.label79.TabIndex = 0;
            this.label79.Text = "General Information of this form 2290 submission";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.label68);
            this.groupBox5.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox5.Location = new System.Drawing.Point(682, 270);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(339, 81);
            this.groupBox5.TabIndex = 43;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Timestamp";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Blue;
            this.label65.Location = new System.Drawing.Point(105, 50);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(48, 17);
            this.label65.TabIndex = 26;
            this.label65.Text = "--------";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Blue;
            this.label66.Location = new System.Drawing.Point(105, 26);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(48, 17);
            this.label66.TabIndex = 25;
            this.label66.Text = "--------";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(18, 26);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(85, 17);
            this.label67.TabIndex = 13;
            this.label67.Text = "Created On:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(18, 50);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(88, 17);
            this.label68.TabIndex = 12;
            this.label68.Text = "Modified On:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(353, 164);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(116, 17);
            this.label64.TabIndex = 42;
            this.label64.Text = "Consent Signed?";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(27, 164);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(106, 17);
            this.label63.TabIndex = 41;
            this.label63.Text = "Payment Mode:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(27, 132);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(76, 17);
            this.label62.TabIndex = 40;
            this.label62.Text = "Credit Tax:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(27, 103);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(101, 17);
            this.label61.TabIndex = 39;
            this.label61.Text = "Additional Tax:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(27, 75);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(71, 17);
            this.label60.TabIndex = 38;
            this.label60.Text = "Total Tax:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(679, 75);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(53, 17);
            this.label59.TabIndex = 37;
            this.label59.Text = "Period:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(679, 103);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(119, 17);
            this.label58.TabIndex = 36;
            this.label58.Text = "First Used Month:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(353, 132);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(107, 17);
            this.label57.TabIndex = 35;
            this.label57.Text = "Is Final Return?";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(353, 103);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(121, 17);
            this.label56.TabIndex = 34;
            this.label56.Text = "Is VIN Correction?";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(353, 75);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(139, 17);
            this.label55.TabIndex = 33;
            this.label55.Text = "Is Address Change? ";
            // 
            // tabTaxableVeh
            // 
            this.tabTaxableVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabTaxableVeh.Controls.Add(this.dgvVehicles_taxable);
            this.tabTaxableVeh.Controls.Add(this.panel3);
            this.tabTaxableVeh.Location = new System.Drawing.Point(4, 34);
            this.tabTaxableVeh.Name = "tabTaxableVeh";
            this.tabTaxableVeh.Padding = new System.Windows.Forms.Padding(3);
            this.tabTaxableVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabTaxableVeh.TabIndex = 1;
            this.tabTaxableVeh.Text = "Taxable Vehicles";
            // 
            // dgvVehicles_taxable
            // 
            this.dgvVehicles_taxable.AllowUserToAddRows = false;
            this.dgvVehicles_taxable.AllowUserToDeleteRows = false;
            this.dgvVehicles_taxable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_taxable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_taxable.Location = new System.Drawing.Point(0, 29);
            this.dgvVehicles_taxable.Name = "dgvVehicles_taxable";
            this.dgvVehicles_taxable.ReadOnly = true;
            this.dgvVehicles_taxable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVehicles_taxable.ShowEditingIcon = false;
            this.dgvVehicles_taxable.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_taxable.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(-1, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1035, 30);
            this.panel3.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "All vehicles which are subject to tax in the current year";
            // 
            // tabSuspendedVeh
            // 
            this.tabSuspendedVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabSuspendedVeh.Controls.Add(this.panel5);
            this.tabSuspendedVeh.Controls.Add(this.dgvVehicles_Suspended);
            this.tabSuspendedVeh.Location = new System.Drawing.Point(4, 34);
            this.tabSuspendedVeh.Name = "tabSuspendedVeh";
            this.tabSuspendedVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabSuspendedVeh.TabIndex = 3;
            this.tabSuspendedVeh.Text = "Suspended Vehicles";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(-1, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1035, 30);
            this.panel5.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(302, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "All vehicles which are subject to suspension in the current year";
            // 
            // dgvVehicles_Suspended
            // 
            this.dgvVehicles_Suspended.AllowUserToAddRows = false;
            this.dgvVehicles_Suspended.AllowUserToDeleteRows = false;
            this.dgvVehicles_Suspended.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_Suspended.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_Suspended.Location = new System.Drawing.Point(0, 29);
            this.dgvVehicles_Suspended.Name = "dgvVehicles_Suspended";
            this.dgvVehicles_Suspended.ReadOnly = true;
            this.dgvVehicles_Suspended.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_Suspended.TabIndex = 5;
            // 
            // tabCreditVeh
            // 
            this.tabCreditVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabCreditVeh.Controls.Add(this.panel4);
            this.tabCreditVeh.Controls.Add(this.dgvVehicles_Credit);
            this.tabCreditVeh.Location = new System.Drawing.Point(4, 34);
            this.tabCreditVeh.Name = "tabCreditVeh";
            this.tabCreditVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabCreditVeh.TabIndex = 2;
            this.tabCreditVeh.Text = "Credit Vehicles";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(-1, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1035, 30);
            this.panel4.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(275, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "All vehicles which are subject to credit in the current year";
            // 
            // dgvVehicles_Credit
            // 
            this.dgvVehicles_Credit.AllowUserToAddRows = false;
            this.dgvVehicles_Credit.AllowUserToDeleteRows = false;
            this.dgvVehicles_Credit.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_Credit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_Credit.Location = new System.Drawing.Point(0, 25);
            this.dgvVehicles_Credit.Name = "dgvVehicles_Credit";
            this.dgvVehicles_Credit.ReadOnly = true;
            this.dgvVehicles_Credit.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_Credit.TabIndex = 4;
            // 
            // tabSoldSuspendedVeh
            // 
            this.tabSoldSuspendedVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabSoldSuspendedVeh.Controls.Add(this.panel6);
            this.tabSoldSuspendedVeh.Controls.Add(this.dgvVehicles_SoldSuspended);
            this.tabSoldSuspendedVeh.Location = new System.Drawing.Point(4, 34);
            this.tabSoldSuspendedVeh.Name = "tabSoldSuspendedVeh";
            this.tabSoldSuspendedVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabSoldSuspendedVeh.TabIndex = 4;
            this.tabSoldSuspendedVeh.Text = "Sold Suspended Vehicles";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label4);
            this.panel6.Location = new System.Drawing.Point(-1, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1035, 30);
            this.panel6.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(263, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "All vehicles which are subject to tax in the current year";
            // 
            // dgvVehicles_SoldSuspended
            // 
            this.dgvVehicles_SoldSuspended.AllowUserToAddRows = false;
            this.dgvVehicles_SoldSuspended.AllowUserToDeleteRows = false;
            this.dgvVehicles_SoldSuspended.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_SoldSuspended.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_SoldSuspended.Location = new System.Drawing.Point(0, 29);
            this.dgvVehicles_SoldSuspended.Name = "dgvVehicles_SoldSuspended";
            this.dgvVehicles_SoldSuspended.ReadOnly = true;
            this.dgvVehicles_SoldSuspended.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_SoldSuspended.TabIndex = 5;
            // 
            // tabMileageExceededVeh
            // 
            this.tabMileageExceededVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabMileageExceededVeh.Controls.Add(this.panel8);
            this.tabMileageExceededVeh.Controls.Add(this.dgvVehicles_MileageExceeded);
            this.tabMileageExceededVeh.Location = new System.Drawing.Point(4, 34);
            this.tabMileageExceededVeh.Name = "tabMileageExceededVeh";
            this.tabMileageExceededVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabMileageExceededVeh.TabIndex = 6;
            this.tabMileageExceededVeh.Text = "Mileage Exceeded Vehicles";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label6);
            this.panel8.Location = new System.Drawing.Point(-1, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1035, 30);
            this.panel8.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(263, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "All vehicles which are subject to tax in the current year";
            // 
            // dgvVehicles_MileageExceeded
            // 
            this.dgvVehicles_MileageExceeded.AllowUserToAddRows = false;
            this.dgvVehicles_MileageExceeded.AllowUserToDeleteRows = false;
            this.dgvVehicles_MileageExceeded.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_MileageExceeded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_MileageExceeded.Location = new System.Drawing.Point(0, 29);
            this.dgvVehicles_MileageExceeded.Name = "dgvVehicles_MileageExceeded";
            this.dgvVehicles_MileageExceeded.ReadOnly = true;
            this.dgvVehicles_MileageExceeded.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_MileageExceeded.TabIndex = 5;
            // 
            // tabTGWIncreasedVeh
            // 
            this.tabTGWIncreasedVeh.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabTGWIncreasedVeh.Controls.Add(this.panel7);
            this.tabTGWIncreasedVeh.Controls.Add(this.dgvVehicles_WeightIncreased);
            this.tabTGWIncreasedVeh.Location = new System.Drawing.Point(4, 34);
            this.tabTGWIncreasedVeh.Name = "tabTGWIncreasedVeh";
            this.tabTGWIncreasedVeh.Size = new System.Drawing.Size(1033, 376);
            this.tabTGWIncreasedVeh.TabIndex = 5;
            this.tabTGWIncreasedVeh.Text = "Weight Increased Vehicles";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label5);
            this.panel7.Location = new System.Drawing.Point(-1, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1035, 30);
            this.panel7.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(263, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "All vehicles which are subject to tax in the current year";
            // 
            // dgvVehicles_WeightIncreased
            // 
            this.dgvVehicles_WeightIncreased.AllowUserToAddRows = false;
            this.dgvVehicles_WeightIncreased.AllowUserToDeleteRows = false;
            this.dgvVehicles_WeightIncreased.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVehicles_WeightIncreased.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVehicles_WeightIncreased.Location = new System.Drawing.Point(0, 29);
            this.dgvVehicles_WeightIncreased.Name = "dgvVehicles_WeightIncreased";
            this.dgvVehicles_WeightIncreased.ReadOnly = true;
            this.dgvVehicles_WeightIncreased.Size = new System.Drawing.Size(558, 347);
            this.dgvVehicles_WeightIncreased.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblStatus);
            this.panel1.Controls.Add(this.lblPhone);
            this.panel1.Controls.Add(this.lblLstName);
            this.panel1.Controls.Add(this.lblReferral);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lblMidName);
            this.panel1.Controls.Add(this.lblSignupIP);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.lblFstName);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lblCategory);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lblCreatedOn);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1036, 106);
            this.panel1.TabIndex = 1;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblStatus.Location = new System.Drawing.Point(854, 14);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(105, 17);
            this.lblStatus.TabIndex = 26;
            this.lblStatus.Text = "USER STATUS";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.ForeColor = System.Drawing.Color.Cyan;
            this.lblPhone.Location = new System.Drawing.Point(595, 41);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(48, 17);
            this.lblPhone.TabIndex = 25;
            this.lblPhone.Text = "--------";
            // 
            // lblLstName
            // 
            this.lblLstName.AutoSize = true;
            this.lblLstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblLstName.Location = new System.Drawing.Point(354, 70);
            this.lblLstName.Name = "lblLstName";
            this.lblLstName.Size = new System.Drawing.Size(48, 17);
            this.lblLstName.TabIndex = 24;
            this.lblLstName.Text = "--------";
            // 
            // lblReferral
            // 
            this.lblReferral.AutoSize = true;
            this.lblReferral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblReferral.Location = new System.Drawing.Point(595, 70);
            this.lblReferral.Name = "lblReferral";
            this.lblReferral.Size = new System.Drawing.Size(48, 17);
            this.lblReferral.TabIndex = 21;
            this.lblReferral.Text = "--------";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(259, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 17);
            this.label12.TabIndex = 14;
            this.label12.Text = "Last Name:";
            // 
            // lblMidName
            // 
            this.lblMidName.AutoSize = true;
            this.lblMidName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMidName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblMidName.Location = new System.Drawing.Point(354, 41);
            this.lblMidName.Name = "lblMidName";
            this.lblMidName.Size = new System.Drawing.Size(48, 17);
            this.lblMidName.TabIndex = 22;
            this.lblMidName.Text = "--------";
            // 
            // lblSignupIP
            // 
            this.lblSignupIP.AutoSize = true;
            this.lblSignupIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignupIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblSignupIP.Location = new System.Drawing.Point(97, 70);
            this.lblSignupIP.Name = "lblSignupIP";
            this.lblSignupIP.Size = new System.Drawing.Size(48, 17);
            this.lblSignupIP.TabIndex = 20;
            this.lblSignupIP.Text = "--------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(259, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Middle Name:";
            // 
            // lblFstName
            // 
            this.lblFstName.AutoSize = true;
            this.lblFstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblFstName.Location = new System.Drawing.Point(353, 14);
            this.lblFstName.Name = "lblFstName";
            this.lblFstName.Size = new System.Drawing.Size(48, 17);
            this.lblFstName.TabIndex = 19;
            this.lblFstName.Text = "--------";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.Cyan;
            this.lblEmail.Location = new System.Drawing.Point(595, 10);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(48, 17);
            this.lblEmail.TabIndex = 23;
            this.lblEmail.Text = "--------";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(259, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "First Name:";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblCategory.Location = new System.Drawing.Point(97, 41);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(48, 17);
            this.lblCategory.TabIndex = 18;
            this.lblCategory.Text = "--------";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(535, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 17);
            this.label14.TabIndex = 15;
            this.label14.Text = "Phone:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(16, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 17);
            this.label15.TabIndex = 16;
            this.label15.Text = "Signup IP:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(535, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 17);
            this.label17.TabIndex = 17;
            this.label17.Text = "Email:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(16, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 17);
            this.label18.TabIndex = 12;
            this.label18.Text = "Category:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(535, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 17);
            this.label13.TabIndex = 7;
            this.label13.Text = "Referral:";
            // 
            // lblCreatedOn
            // 
            this.lblCreatedOn.AutoSize = true;
            this.lblCreatedOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatedOn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblCreatedOn.Location = new System.Drawing.Point(946, 70);
            this.lblCreatedOn.Name = "lblCreatedOn";
            this.lblCreatedOn.Size = new System.Drawing.Size(48, 17);
            this.lblCreatedOn.TabIndex = 3;
            this.lblCreatedOn.Text = "--------";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblUserName.Location = new System.Drawing.Point(97, 14);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(48, 17);
            this.lblUserName.TabIndex = 2;
            this.lblUserName.Text = "--------";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Location = new System.Drawing.Point(854, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Sign Up Date:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(16, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "User Name:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.lblFilingStatus);
            this.panel2.Controls.Add(this.btnSubmit);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Location = new System.Drawing.Point(0, 525);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1036, 41);
            this.panel2.TabIndex = 2;
            // 
            // lblFilingStatus
            // 
            this.lblFilingStatus.AutoSize = true;
            this.lblFilingStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblFilingStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilingStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblFilingStatus.Location = new System.Drawing.Point(711, 11);
            this.lblFilingStatus.Name = "lblFilingStatus";
            this.lblFilingStatus.Size = new System.Drawing.Size(110, 17);
            this.lblFilingStatus.TabIndex = 27;
            this.lblFilingStatus.Text = "FILING STATUS";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Location = new System.Drawing.Point(866, 9);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 1;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(947, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(679, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 17);
            this.label16.TabIndex = 45;
            this.label16.Text = "IRS Payment:";
            // 
            // frmFormDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.ClientSize = new System.Drawing.Size(1036, 566);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControlSubmissionInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmFormDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form 2290 Details";
            this.Load += new System.EventHandler(this.frmFormDetails_Load);
            this.tabControlSubmissionInfo.ResumeLayout(false);
            this.tabBusinessInfo.ResumeLayout(false);
            this.tabBusinessInfo.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tabFormInfo.ResumeLayout(false);
            this.tabFormInfo.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabTaxableVeh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_taxable)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabSuspendedVeh.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_Suspended)).EndInit();
            this.tabCreditVeh.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_Credit)).EndInit();
            this.tabSoldSuspendedVeh.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_SoldSuspended)).EndInit();
            this.tabMileageExceededVeh.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_MileageExceeded)).EndInit();
            this.tabTGWIncreasedVeh.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVehicles_WeightIncreased)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlSubmissionInfo;
        private System.Windows.Forms.TabPage tabTaxableVeh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tabCreditVeh;
        private System.Windows.Forms.TabPage tabSuspendedVeh;
        private System.Windows.Forms.TabPage tabSoldSuspendedVeh;
        private System.Windows.Forms.TabPage tabTGWIncreasedVeh;
        private System.Windows.Forms.TabPage tabMileageExceededVeh;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCreatedOn;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridView dgvVehicles_taxable;
        private System.Windows.Forms.DataGridView dgvVehicles_Credit;
        private System.Windows.Forms.DataGridView dgvVehicles_Suspended;
        private System.Windows.Forms.DataGridView dgvVehicles_SoldSuspended;
        private System.Windows.Forms.DataGridView dgvVehicles_WeightIncreased;
        private System.Windows.Forms.DataGridView dgvVehicles_MileageExceeded;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblReferral;
        private System.Windows.Forms.Label lblSignupIP;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblLstName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblMidName;
        private System.Windows.Forms.Label lblFstName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabFormInfo;
        private System.Windows.Forms.TabPage tabBusinessInfo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblOfficerPhone;
        private System.Windows.Forms.Label lblOfficerName;
        private System.Windows.Forms.Label lblOfficerPIN;
        private System.Windows.Forms.Label lblOfficerTitle;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblBusinessZip;
        private System.Windows.Forms.Label lblBusinessCountry;
        private System.Windows.Forms.Label lblBusinessState;
        private System.Windows.Forms.Label lblBusinessCity;
        private System.Windows.Forms.Label lblBusinessAdd;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblPreparerZip;
        private System.Windows.Forms.Label lblPreparerAdd;
        private System.Windows.Forms.Label lblPreparerCountry;
        private System.Windows.Forms.Label lblPreparerState;
        private System.Windows.Forms.Label lblPreparerCity;
        private System.Windows.Forms.Label lblPreparerEmail;
        private System.Windows.Forms.Label lblPreparerPh;
        private System.Windows.Forms.Label lblPreparerPhoneLocation;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label lblPreparerName;
        private System.Windows.Forms.Label lblPreparerSSNPOrPTIN_No;
        private System.Windows.Forms.Label lblPreparerSelfEmployed;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label lblPreparerSSNOrPTIN;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblTPPhone;
        private System.Windows.Forms.Label lblTPPIN;
        private System.Windows.Forms.Label lblTPName;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblBusinessModifiedOn;
        private System.Windows.Forms.Label lblBusinessCreatedOn;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblBusinessType;
        private System.Windows.Forms.Label lblBusinessPhone;
        private System.Windows.Forms.Label lblBusinessName;
        private System.Windows.Forms.Label lblFilingStatus;
        private System.Windows.Forms.Label label16;
    }
}

