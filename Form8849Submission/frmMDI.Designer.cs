namespace Form8849Sub
{
    partial class frmMDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMDI));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtn_PendingSubmission = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_PendingAck = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Accepted = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Rejected = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Schedule1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_submissionMail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_eMail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Contact = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_reports = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_User = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Search = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_ipAddress = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_Settings = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusUserName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusCategory = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtn_PendingSubmission,
            this.toolStripSeparator6,
            this.tsbtn_PendingAck,
            this.toolStripSeparator7,
            this.tsbtn_Accepted,
            this.toolStripSeparator8,
            this.tsbtn_Rejected,
            this.toolStripSeparator9,
            this.tsbtn_Schedule1,
            this.toolStripSeparator10,
            this.tsbtn_submissionMail,
            this.toolStripSeparator11,
            this.toolStripSeparator13,
            this.toolStripSeparator14,
            this.toolStripSeparator15,
            this.tsbtn_eMail,
            this.toolStripSeparator1,
            this.tsbtn_Contact,
            this.toolStripSeparator2,
            this.tsbtn_reports,
            this.toolStripSeparator5,
            this.tsbtn_User,
            this.toolStripSeparator3,
            this.tsbtn_Search,
            this.toolStripSeparator4,
            this.tsbtn_ipAddress,
            this.toolStripSeparator12,
            this.tsbtn_Settings});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(66, 496);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbtn_PendingSubmission
            // 
            this.tsbtn_PendingSubmission.BackColor = System.Drawing.Color.Maroon;
            this.tsbtn_PendingSubmission.ForeColor = System.Drawing.Color.White;
            this.tsbtn_PendingSubmission.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_PendingSubmission.Image")));
            this.tsbtn_PendingSubmission.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_PendingSubmission.Name = "tsbtn_PendingSubmission";
            this.tsbtn_PendingSubmission.Size = new System.Drawing.Size(63, 59);
            this.tsbtn_PendingSubmission.Text = "Pendings";
            this.tsbtn_PendingSubmission.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtn_PendingSubmission.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtn_PendingSubmission.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_PendingSubmission.ToolTipText = "Pending for submitting to IRS";
            this.tsbtn_PendingSubmission.Click += new System.EventHandler(this.tsbtn_Pending_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_PendingAck
            // 
            this.tsbtn_PendingAck.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tsbtn_PendingAck.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_PendingAck.Image")));
            this.tsbtn_PendingAck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_PendingAck.Name = "tsbtn_PendingAck";
            this.tsbtn_PendingAck.Size = new System.Drawing.Size(63, 59);
            this.tsbtn_PendingAck.Text = "Ack Failed";
            this.tsbtn_PendingAck.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtn_PendingAck.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtn_PendingAck.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_PendingAck.ToolTipText = "Submitted: Acknowledgement Needed";
            this.tsbtn_PendingAck.Click += new System.EventHandler(this.tsbtn_PendingAck_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Accepted
            // 
            this.tsbtn_Accepted.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tsbtn_Accepted.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Accepted.Image")));
            this.tsbtn_Accepted.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Accepted.Name = "tsbtn_Accepted";
            this.tsbtn_Accepted.Size = new System.Drawing.Size(63, 59);
            this.tsbtn_Accepted.Text = "Accepted";
            this.tsbtn_Accepted.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtn_Accepted.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtn_Accepted.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Accepted.ToolTipText = "Accepted: Pending Schedule1";
            this.tsbtn_Accepted.Visible = false;
            this.tsbtn_Accepted.Click += new System.EventHandler(this.tsbtn_Accepted_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Rejected
            // 
            this.tsbtn_Rejected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tsbtn_Rejected.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Rejected.Image")));
            this.tsbtn_Rejected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Rejected.Name = "tsbtn_Rejected";
            this.tsbtn_Rejected.Size = new System.Drawing.Size(63, 59);
            this.tsbtn_Rejected.Text = "Rejected";
            this.tsbtn_Rejected.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtn_Rejected.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtn_Rejected.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Rejected.ToolTipText = "Rejected by IRS";
            this.tsbtn_Rejected.Click += new System.EventHandler(this.tsbtn_Rejected_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Schedule1
            // 
            this.tsbtn_Schedule1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tsbtn_Schedule1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtn_Schedule1.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Schedule1.Image")));
            this.tsbtn_Schedule1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Schedule1.Name = "tsbtn_Schedule1";
            this.tsbtn_Schedule1.Size = new System.Drawing.Size(63, 19);
            this.tsbtn_Schedule1.Text = "Sch 1";
            this.tsbtn_Schedule1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtn_Schedule1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtn_Schedule1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Schedule1.ToolTipText = "Schedule1 Retrieved";
            this.tsbtn_Schedule1.Visible = false;
            this.tsbtn_Schedule1.Click += new System.EventHandler(this.tsbtn_Schedule1_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_submissionMail
            // 
            this.tsbtn_submissionMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.tsbtn_submissionMail.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_submissionMail.Image")));
            this.tsbtn_submissionMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_submissionMail.Name = "tsbtn_submissionMail";
            this.tsbtn_submissionMail.Size = new System.Drawing.Size(63, 59);
            this.tsbtn_submissionMail.Text = "Email";
            this.tsbtn_submissionMail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_submissionMail.ToolTipText = "Send mail to customer \r\nregarding accepted or rejected \r\nsubmissions";
            this.tsbtn_submissionMail.Click += new System.EventHandler(this.tsbtn_submissionMail_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(63, 6);
            this.toolStripSeparator11.Visible = false;
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(63, 6);
            this.toolStripSeparator13.Visible = false;
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(63, 6);
            this.toolStripSeparator14.Visible = false;
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_eMail
            // 
            this.tsbtn_eMail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_eMail.Enabled = false;
            this.tsbtn_eMail.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_eMail.Image")));
            this.tsbtn_eMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_eMail.Name = "tsbtn_eMail";
            this.tsbtn_eMail.Size = new System.Drawing.Size(63, 44);
            this.tsbtn_eMail.ToolTipText = "Compose Mail";
            this.tsbtn_eMail.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Contact
            // 
            this.tsbtn_Contact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.tsbtn_Contact.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtn_Contact.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Contact.Image")));
            this.tsbtn_Contact.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Contact.Name = "tsbtn_Contact";
            this.tsbtn_Contact.Size = new System.Drawing.Size(63, 19);
            this.tsbtn_Contact.Text = "Fax";
            this.tsbtn_Contact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtn_Contact.ToolTipText = "Contact Information";
            this.tsbtn_Contact.Click += new System.EventHandler(this.tsbtn_Contact_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_reports
            // 
            this.tsbtn_reports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_reports.Enabled = false;
            this.tsbtn_reports.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_reports.Image")));
            this.tsbtn_reports.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_reports.Name = "tsbtn_reports";
            this.tsbtn_reports.Size = new System.Drawing.Size(44, 44);
            this.tsbtn_reports.Text = "toolStripButton3";
            this.tsbtn_reports.ToolTipText = "Reports";
            this.tsbtn_reports.Visible = false;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_User
            // 
            this.tsbtn_User.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_User.Enabled = false;
            this.tsbtn_User.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_User.Image")));
            this.tsbtn_User.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_User.Name = "tsbtn_User";
            this.tsbtn_User.Size = new System.Drawing.Size(44, 44);
            this.tsbtn_User.Text = "toolStripButton1";
            this.tsbtn_User.ToolTipText = "User Management";
            this.tsbtn_User.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Search
            // 
            this.tsbtn_Search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_Search.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Search.Image")));
            this.tsbtn_Search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Search.Name = "tsbtn_Search";
            this.tsbtn_Search.Size = new System.Drawing.Size(44, 44);
            this.tsbtn_Search.Text = "toolStripButton6";
            this.tsbtn_Search.ToolTipText = "Search";
            this.tsbtn_Search.Visible = false;
            this.tsbtn_Search.Click += new System.EventHandler(this.tsbtn_Search_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_ipAddress
            // 
            this.tsbtn_ipAddress.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_ipAddress.Enabled = false;
            this.tsbtn_ipAddress.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_ipAddress.Image")));
            this.tsbtn_ipAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_ipAddress.Name = "tsbtn_ipAddress";
            this.tsbtn_ipAddress.Size = new System.Drawing.Size(44, 44);
            this.tsbtn_ipAddress.Text = "toolStripButton1";
            this.tsbtn_ipAddress.ToolTipText = "Block IPs";
            this.tsbtn_ipAddress.Visible = false;
            this.tsbtn_ipAddress.Click += new System.EventHandler(this.tsbtn_ipAddress_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(63, 6);
            // 
            // tsbtn_Settings
            // 
            this.tsbtn_Settings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_Settings.Enabled = false;
            this.tsbtn_Settings.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_Settings.Image")));
            this.tsbtn_Settings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_Settings.Name = "tsbtn_Settings";
            this.tsbtn_Settings.Size = new System.Drawing.Size(44, 44);
            this.tsbtn_Settings.Text = "toolStripButton1";
            this.tsbtn_Settings.ToolTipText = "Settings";
            this.tsbtn_Settings.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusUserName,
            this.toolStripStatusCategory,
            this.toolStripStatusLoginTime});
            this.statusStrip1.Location = new System.Drawing.Point(66, 474);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1071, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusUserName
            // 
            this.toolStripStatusUserName.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusUserName.Name = "toolStripStatusUserName";
            this.toolStripStatusUserName.Size = new System.Drawing.Size(65, 17);
            this.toolStripStatusUserName.Text = "User Name";
            // 
            // toolStripStatusCategory
            // 
            this.toolStripStatusCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.toolStripStatusCategory.Name = "toolStripStatusCategory";
            this.toolStripStatusCategory.Size = new System.Drawing.Size(81, 17);
            this.toolStripStatusCategory.Text = "User Category";
            // 
            // toolStripStatusLoginTime
            // 
            this.toolStripStatusLoginTime.Name = "toolStripStatusLoginTime";
            this.toolStripStatusLoginTime.Size = new System.Drawing.Size(75, 17);
            this.toolStripStatusLoginTime.Text = "Logged In At";
            // 
            // frmMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1137, 496);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.IsMdiContainer = true;
            this.Name = "frmMDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "eForm2290 Administration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMDI_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtn_User;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusUserName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusCategory;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLoginTime;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtn_eMail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbtn_reports;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbtn_Contact;
        private System.Windows.Forms.ToolStripButton tsbtn_PendingSubmission;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbtn_Search;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbtn_ipAddress;
        private System.Windows.Forms.ToolStripButton tsbtn_PendingAck;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tsbtn_Accepted;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tsbtn_Rejected;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton tsbtn_Schedule1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton tsbtn_Settings;
        private System.Windows.Forms.ToolStripButton tsbtn_submissionMail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;



    }
}