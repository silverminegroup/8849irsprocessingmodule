﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using IRSLibrary;
using System.Collections;
using IRSLibrary.Utilities;
using System.Diagnostics;
using eFormSMSSender;
using eFormFaxSender;
using System.Text.RegularExpressions;

namespace Form8849Sub
{
    public partial class Auto8849Trans : Form
    {
        static Timer objTimer_Submission = new Timer();
        static Timer AutoTransTime = new Timer();


        string BulkFileName = "";
        static int Wr_Log;
        #region Variables

        public static DataSet mds_FullDetails = new DataSet();

        eForm8849Operations dbOperation = new eForm8849Operations();

        static DataTable mdt_US = new DataTable();

        static DataTable mdt_user_reg = new DataTable();
        static DataTable mdt_profile = new DataTable();
        static DataTable mdt_tax8849 = new DataTable();
        static DataTable mdt_vehicle = new DataTable();
        static DataTable mdt_irs_paymnet = new DataTable();
        static DataTable mdt_paid_preparer = new DataTable();
        static DataTable mdt_third_party_designee = new DataTable();
        static DataTable mdt_AnnualTaxCalMaster = new DataTable();
        static DataTable mdt_PartialTaxCalMaster = new DataTable();
        static DataTable mdt_TaxComputaion = new DataTable();

        #endregion

        public Auto8849Trans()
        {
            InitializeComponent();
        }

        private void AutoTrans_Load(object sender, EventArgs e)
        {
            this.Text = "AutoTrans 8849 .... v" + Application.ProductVersion;
            LoadTaxChart();

        }

        private void LoadTaxChart()
        {
            try
            {
                mdt_AnnualTaxCalMaster = dbOperation.Select_Data("usp_Get_8849AnnualTaxChartTemp", "", "").Tables[0];
                mdt_PartialTaxCalMaster = dbOperation.Select_Data("usp_Get_8849PartialTaxChartTemp", "", "").Tables[0];
            }
            catch { }
            finally /* Vishwa Added For Auto Start */
            {
                AutosubLog_Write("Inside LoadTaxChart Now Starting the Ticker.");
                //MessageBox.Show("Now trigerring the First Timer");
                AutoTransTime.Enabled = true;
                AutoTransTime.Interval = 10000; //After 10 seconds trigger the event.
                AutoTransTime.Tick += new EventHandler(btnSubmission_Click);
                AutoTransTime.Start();
                AutosubLog_Write("Inside LoadTaxChart Trigerred the Timer.");
            }
        }

        void objTimer_Submission_Tick(object sender, EventArgs e)
        {
            lblStatus_submission.Text = "working";

            try
            {
                AutosubLog_Write("inside timer tick event");
                objTimer_Submission.Enabled = false;
                AutosubLog_Write("timer enabled = false");

                if (IsHolidayToday())
                    return;

                AutosubLog_Write("holiday checking over");

                GetPendingSubmissionList();

                AutosubLog_Write("GetPendingSubmissionList over");
                BulkFileName = @"C:\" + "Bulk8849-" + DateTime.Now.ToString("yyyyMMddFFFF") + ".zip";
                AutosubLog_Write("bulk file name created");

                CreateBulkZip();

                AutosubLog_Write("CreateBulkZip over");

                try
                {
                    Transmit2IRS();
                }
                catch (Exception ex)
                {
                    AutosubLog_Write("Transmit To IRS Caught Error,  Error Details: " + ex.Message.ToString());// ex.InnerException.ToString());
                }

                AutosubLog_Write("Transmit2IRS over");

                GetAck();

                AutosubLog_Write("GetAck over");

                GetSch6();

                AutosubLog_Write("GetSch6 over");

                SendEmail();

                AutosubLog_Write("SendEmail over");

                objTimer_Submission.Enabled = true;

                AutosubLog_Write("timer enabled = true");
                AutosubLog_Write("leaving timer tick event");
            }
            catch (Exception ex)
            {
                objTimer_Submission.Enabled = true;
            }

            lblStatus_submission.Text = "waiting....";
        }

        private void GetPendingSubmissionList()
        {
            try
            {
                DataSet ldsResultSet = dbOperation.GetPendingSubmissionList("N");
                mdt_US = ldsResultSet.Tables[0];
            }
            catch
            {
            }
        }

        private void CreateBulkZip()
        {
            AutosubLog_Write("CreateBulkZip entered");
            try
            {

                AutosubLog_Write("try 0");

                Entity.Loginstatus = "True";

                AutosubLog_Write("loginstatus = true");

                ZipFile TransmissionZip = ZipFile.Create(BulkFileName);

                AutosubLog_Write("TransmissionZip object created");

                try
                {
                    AutosubLog_Write("try 1");

                    for (int i = 0; i < mdt_US.Rows.Count; i++)
                    {
                        AutosubLog_Write("inside for loop " + i.ToString());

                        #region bulk file for one reference
                        try
                        {
                            AutosubLog_Write("try 2");

                            DataRow ldr = mdt_US.Rows[i];

                            AutosubLog_Write("datarow created ldr");

                            #region Get data into tables

                            mds_FullDetails = dbOperation.GetAllSubmissionData(
                                 ldr["FormId"].ToString(),
                                 ldr["UserId"].ToString(),
                                 ldr["TaxId"].ToString());

                            mdt_user_reg = mds_FullDetails.Tables[0];
                            mdt_profile = mds_FullDetails.Tables[1];
                            mdt_tax8849 = mds_FullDetails.Tables[2];
                            mdt_vehicle = mds_FullDetails.Tables[3];
                            mdt_irs_paymnet = mds_FullDetails.Tables[4];
                            mdt_paid_preparer = mds_FullDetails.Tables[5];
                            mdt_third_party_designee = mds_FullDetails.Tables[6];

                            #endregion

                            AutosubLog_Write("Got data into tables");

                            #region validation

                            if (mdt_vehicle.Rows.Count == 0)
                            {
                                AutosubLog_Write("Ref# " + ldr["ReferenceNumber"].ToString() + " NO Vehicle Information Present, Contact Admin.");
                                System.IO.StreamWriter fileEr =
                                    new System.IO.StreamWriter(@"C:\Live Submissions\8849 Log\ERROR\ERRORS_FOUND" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt", true);
                                fileEr.WriteLine(TransmissionZip.Name.Replace("C:\\", "").Replace(".zip", "") + " :::> " + ldr["ReferenceNumber"].ToString() + " :::> Vehicle Not Present");
                                fileEr.Close();
                                continue;
                            }

                            if (mdt_irs_paymnet.Rows.Count > 0)
                            {
                                DateTime dtWithdrawalDate = Convert.ToDateTime(mdt_irs_paymnet.Rows[0]["ip_withdrawal_date"]);
                                DateTime ServerDate = dbOperation.GetServerDate();

                                if (IsWeekend(ServerDate))
                                {
                                    continue;
                                }
                                if (dtWithdrawalDate.Date < ServerDate.Date)
                                {
                                    mdt_irs_paymnet.Rows[0]["ip_withdrawal_date"] = ServerDate.ToString();
                                    mdt_irs_paymnet.AcceptChanges();
                                }
                            }

                            #endregion

                            AutosubLog_Write("validation done");

                            #region formatting

                            foreach (DataRow dr in mdt_vehicle.Rows)
                                dr["vd_VIN"] = Convert.ToString(dr["vd_VIN"]).ToUpper();

                            clsReturnValues rv = new clsReturnValues();

                            char status = 'N';
                            switch (ldr["Status"].ToString())
                            {
                                case "AF": status = 'S'; break;
                                case "IA": status = 'A'; break;
                                case "IR": status = 'X'; break;
                                case "RS": status = 'C'; break;
                                case "SA": status = 'N'; break;
                                case "SB": status = 'X'; break;
                                case "SR": status = 'R'; break;
                                case "SS": status = 'N'; break;
                                case "US": status = 'N'; break;
                            }

                            #endregion

                            AutosubLog_Write("formatting done");

                            string refNo = ldr["ReferenceNumber"].ToString();

                            AutosubLog_Write("got reference number");

                            string subid = "";
                            try
                            {
                                subid = CreateOneZip(refNo, ldr["FormId"].ToString(), status);
                            }
                            catch (Exception ex)
                            {
                                AutosubLog_Write("Catch CreateOneZip : " + ldr["FormId"].ToString() + ex.InnerException.ToString());
                                continue;
                            }
                            AutosubLog_Write("got submission id " + subid);
                            // New Added 25 Aug 2020 
                            if (subid == "-1" || subid.Length < 3) //If Submission id Length less than 3, it is error
                            {
                                AutosubLog_Write("Error Submission ID INVALID  : " + subid + " CreateOneZip : " + ldr["FormId"].ToString());
                                continue;
                            }
                           
                            try
                            {
                                AutosubLog_Write("try 3");

                                System.IO.StreamWriter file =
                                    new System.IO.StreamWriter(@"C:\Live Submissions\8849 Log\RefSubmission " + DateTime.Today.ToString("yyyy-MM-dd") + ".txt", true);

                                file.WriteLine(
                                    DateTime.Now.ToString("HH:mm:ss:fff") + " " +
                                    TransmissionZip.Name.Replace("C:\\", "").Replace(".zip", "").ToUpper() + " :::> " +
                                    refNo + " :::> " + subid);

                                file.Close();

                                AutosubLog_Write(refNo + " :: " + subid);
                            }
                            catch (Exception ex)
                            {
                                AutosubLog_Write("Catch 3 :" + ex.InnerException.ToString());
                            }
                            try
                            {
                                //update mdt_US with subid
                                AutosubLog_Write("try 4");
                                mdt_US.Select("ReferenceNumber='" + refNo + "'")[0]["SubmissionID"] = subid;
                                AutosubLog_Write("row selected in mdt_US: " + refNo + " : " + subid);
                                AutosubLog_Write("mdt_US.Rows.Count=" + mdt_US.Rows.Count.ToString());
                            }
                            catch (Exception ex)
                            {
                                AutosubLog_Write("Catch 4 :" + ex.InnerException.ToString());
                            }

                            AutosubLog_Write(subid + ".zip is getting added into TransmissionZip");
                            TransmissionZip.BeginUpdate();
                            TransmissionZip.Add(@"C:\" + subid + ".zip", CompressionMethod.Stored);
                            TransmissionZip.CommitUpdate();

                            AutosubLog_Write("TransmissionZip object committed");

                            File.Delete(@"C:\" + subid + ".zip");

                            AutosubLog_Write("File Deleted " + subid);
                        }
                        catch (Exception ex)
                        {
                            AutosubLog_Write("Catch 2: " + ex.InnerException.ToString());
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    AutosubLog_Write("Catch 1: " + ex.InnerException.ToString());
                }

                AutosubLog_Write("TransmissionZip.Count= " + TransmissionZip.Count.ToString());
                if (TransmissionZip.Count == 0)
                {
                    TransmissionZip.Close();
                    File.Delete(BulkFileName);
                }
                else
                    TransmissionZip.Close();

            }
            catch (Exception ex)
            {
                AutosubLog_Write("catch 0: " + ex.InnerException.ToString());
            }
        }

        // First start here... 
        private string CreateOneZip(string refNo, string formID, char status)
        {
            AutosubLog_Write(" CreateOneZip Enter");
            clsReturnValues rv = new clsReturnValues();

            try
            {
                AutosubLog_Write("CreateOneZip Enter");
                #region Variables declaration
                bool IsAddressChange, IsAmendedReturn, IsVINCorrection, IsFinalReturn, IsConsentToDiscloseYes, IsNotSubjectToTaxChecked;
                IsAddressChange = IsAmendedReturn = IsVINCorrection = IsFinalReturn = IsConsentToDiscloseYes = IsNotSubjectToTaxChecked = false;

                //int TaxEndMonth;
                string TaxEndMonth;
                decimal ClaimAmt;
                System.DateTime ClaimDate;
                System.DateTime LatestClaimDt;

                string AmendedMonth, TaxYear, First_Used_Date, SplContdDesc;
                SplContdDesc = TaxYear = AmendedMonth = First_Used_Date = "";
                TaxYear = "";

                string Filer_Name, Filer_NameControl, Filer_EIN, Filer_AddressLine1, Filer_City, Filer_State, Filer_Foreign_Country, Filer_ZIPCode;
                Filer_Name = Filer_NameControl = Filer_EIN = Filer_AddressLine1 = Filer_City = Filer_State = Filer_Foreign_Country = Filer_ZIPCode = "";

                string Officer_Name, Officer_Phone, Officer_PIN, Officer_Title;
                Officer_Name = Officer_Phone = Officer_PIN = Officer_Title = "";

                string CreditRefNo;
                CreditRefNo = "365";
                LatestClaimDt = Convert.ToDateTime("2020-06-30");

                string ThirdPartyDesigne_Name, ThirdPartyDesigne_Phone, ThirdPartyDesigne_PIN;
                ThirdPartyDesigne_Name = ThirdPartyDesigne_Phone = ThirdPartyDesigne_PIN = "";

                string Preparer_Firm, Preparer_EIN, Preparer_City, Preparer_State, Preparer_Zip, Preparer_Country, Preparer_Address, Preparer_ForeignPhone, Preparer_Phone, Preparer_Email, Preparer_Name, Preparer_PTIN_SSN;
                Preparer_Firm = Preparer_EIN = Preparer_City = Preparer_State = Preparer_Zip = Preparer_Country = Preparer_Address = Preparer_ForeignPhone = Preparer_Phone = Preparer_Email = Preparer_Name = Preparer_PTIN_SSN = "";

                bool IsPreparerHasPTIN, IsPreparerSelfEmployed;
                IsPreparerHasPTIN = IsPreparerSelfEmployed = false;

                string paymentType, Payment_Acc_No, Payment_Acc_Type, Payment_ReqPayDate, Payment_RoutingTransitNo, Payment_Txpyer_Ph;
                paymentType = Payment_Acc_No = Payment_Acc_Type = Payment_ReqPayDate = Payment_RoutingTransitNo = Payment_Txpyer_Ph = "";

                DataSet dsSoldVehicles, dsLowMileageVehicles, dsTaxOverVehicles;

                DataSet dsPriorYearMileageExceededVehicles, dsTGWIncreaseWorksheet, dsTaxComputation;
                string soldSuspendedVehicles, TotalVehicles;
                soldSuspendedVehicles = TotalVehicles = "";

                string LossType, VehicleExp, VehVIN;
                LossType = VehicleExp = VehVIN ="";


                decimal TaxFromTaxComputation, AdditionalTaxAmount, CreditAmount;
                TaxFromTaxComputation = AdditionalTaxAmount = CreditAmount = 0;

                DataTable ldtSoldVehicles, ldtLowMileageVehicles, ldtTaxOverVehicles;

                #endregion

                #region Value assignment

                #region Tax8849 values
                IsAddressChange = Convert.ToBoolean(mdt_tax8849.Rows[0]["ttd_IsAddressChange"]);
                IsFinalReturn = Convert.ToBoolean(mdt_tax8849.Rows[0]["ttd_IsFinalReturn"]);
                //
                //TaxEndMonth = Convert.ToInt16(mdt_tax8849.Rows[0]["ttd_TaxEndMonth"]);
                TaxEndMonth = Convert.ToString(mdt_tax8849.Rows[0]["ttd_TaxEndMonth"]);
                ClaimAmt = Convert.ToDecimal(mdt_tax8849.Rows[0]["tld_Claim_amt"]);
                ClaimDate = Convert.ToDateTime(mdt_tax8849.Rows[0]["tld_Claim_date"]);

                #endregion

                #region Profile Values

                Filer_Name = Convert.ToString(mdt_profile.Rows[0]["pd_business_name"]) + " " +
                    Convert.ToString(mdt_profile.Rows[0]["pd_fst_name"]) + " " +
                    Convert.ToString(mdt_profile.Rows[0]["pd_last_name"]);
                Filer_Name = Filer_Name.Trim();
                Filer_NameControl = getNameControl(Filer_Name);
                Random Randnum = new Random();
                string CrdRefNo = Randnum.Next(Randnum.Next(120,990), 1000).ToString();
                try
                {    
                    //CreditRefNo = Filer_NameControl.Substring(0, 1) + CrdRefNo.Substring(1, 2);
                    CreditRefNo = Filer_NameControl.Substring(0, 1) + CreditRefNo.Substring(1, 2); 
                }
                catch (Exception ex)
                {
                    CreditRefNo = "365";
                }
                Randnum = null; 

                Filer_EIN = Convert.ToString(mdt_profile.Rows[0]["pd_tax_id"]);
                Filer_AddressLine1 = Convert.ToString(mdt_profile.Rows[0]["pd_address"]);
                Filer_AddressLine1 = Regex.Replace(Filer_AddressLine1, @"\s+", " ").Trim(); // Newly Added CAN TEST
                Filer_City = Convert.ToString(mdt_profile.Rows[0]["pd_city"]);
                Filer_ZIPCode = Convert.ToString(mdt_profile.Rows[0]["pd_zipcode"]);
                Filer_Foreign_Country = Convert.ToString(mdt_profile.Rows[0]["pd_frn_cntry"]);

                Filer_State = Convert.ToString(mdt_profile.Rows[0]["pd_state"]);

                Officer_Name = Convert.ToString(mdt_profile.Rows[0]["pd_sig_name"]);
                Officer_Phone = Convert.ToString(mdt_profile.Rows[0]["pd_sig_phone"]);

                Officer_PIN = Convert.ToString(mdt_profile.Rows[0]["pd_sig_pin"]);
                Officer_Title = Convert.ToString(mdt_profile.Rows[0]["pd_sig_title"]);

                #endregion

                #region Third Party Designee

                if (mdt_third_party_designee.Rows.Count > 0)
                {
                    ThirdPartyDesigne_Name = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_name"]);
                    ThirdPartyDesigne_Phone = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_ph"]);
                    ThirdPartyDesigne_PIN = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_pin"]);
                }
                else
                {
                    ThirdPartyDesigne_Name = "";
                    ThirdPartyDesigne_Phone = "";
                    ThirdPartyDesigne_PIN = "";
                }

                #endregion

                #region paid Preparer

                if (mdt_paid_preparer.Rows.Count > 0)
                {
                    Preparer_Name = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_name"]);
                    Preparer_PTIN_SSN = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_ssn_ptin"]);
                    Preparer_Email = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_email"]);
                    Preparer_Address = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_address"]);
                    IsPreparerSelfEmployed = Convert.ToBoolean(mdt_paid_preparer.Rows[0]["ppd_IsSelfEmployed"]);

                    if (Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_ssn_ptin_type"]) == "PTIN")
                        IsPreparerHasPTIN = true;
                    else
                        IsPreparerHasPTIN = false;

                    if (Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone_Type"]) == "Foreign")
                    {
                        Preparer_ForeignPhone = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone"]);
                        Preparer_Phone = "";
                    }
                    else
                    {
                        Preparer_Phone = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone"]);
                        Preparer_ForeignPhone = "";
                    }

                    if (!IsPreparerSelfEmployed)
                    {
                        Preparer_Firm = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_name"]);
                        Preparer_EIN = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_EIN"]);
                        Preparer_City = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_city"]);

                        Preparer_Zip = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_zip"]);
                        Preparer_Country = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_country"]);

                        if (Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_state"]).Length > 0) //if (Preparer_Country != "")
                            Preparer_State = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_state"]);
                        else
                           Preparer_State = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_zip"]);
                    }
                }
                else
                {
                    Preparer_Name = "";
                    Preparer_PTIN_SSN = "";
                    Preparer_Email = "";
                    Preparer_Address = "";
                    IsPreparerSelfEmployed = false;
                    IsPreparerHasPTIN = false;
                    Preparer_Phone = "";
                    Preparer_ForeignPhone = "";
                    Preparer_Firm = "";
                    Preparer_EIN = "";
                    Preparer_City = "";
                    Preparer_Zip = "";
                    Preparer_Country = "";
                    Preparer_State = "";
                }

                #endregion

                #region IRS Payment
                // This can be Ignored - But at later time 
                if (mdt_tax8849.Rows.Count > 0)
                {
                    /*
                    if (Convert.ToString(mdt_tax8849.Rows[0]["ttd_Payment_Mode"]) == "EFTPS"
                        || Convert.ToString(mdt_tax8849.Rows[0]["ttd_Payment_Mode"]) == "CMO")
                    {
                        paymentType = "EFTPS";
                        Payment_Acc_No = "";
                        Payment_Acc_Type = ""; Payment_ReqPayDate = "";
                        Payment_RoutingTransitNo = "";
                        Payment_Txpyer_Ph = "";
                    }
                    else if (Convert.ToString(mdt_tax8849.Rows[0]["ttd_Payment_Mode"]) == "CDC")
                    {
                        paymentType = "CDC";//CreditDebitCard
                        Payment_Acc_No = "";
                        Payment_Acc_Type = ""; Payment_ReqPayDate = "";
                        Payment_RoutingTransitNo = "";
                        Payment_Txpyer_Ph = "";
                    }
                    else if (Convert.ToString(mdt_tax8849.Rows[0]["ttd_Payment_Mode"]) == "EFW")
                    {
                        paymentType = "DirectDebit";
                        Payment_Acc_No = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_bank_account_no"]);
                        Payment_Acc_Type = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_payment_type"]);

                        Payment_ReqPayDate = DateTime.Today.ToString("MM-dd-yyyy");

                        Payment_RoutingTransitNo = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_bank_routing_no"]);
                        Payment_Txpyer_Ph = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_taxpayer_phone"]);
                    }
                    else
                     * */
                    {
                        Payment_Acc_No = "";
                        Payment_Acc_Type = ""; Payment_ReqPayDate = "";
                        Payment_RoutingTransitNo = "";
                        Payment_Txpyer_Ph = "";
                    }
                }
                #endregion

                #region Vehciles

                

                ldtSoldVehicles = Create_Vehicle_8849Sold();
                ldtLowMileageVehicles = Create_Vehicle_8849Low();
                ldtTaxOverVehicles = Create_Vehicle_8849Over();

                foreach (DataRow ldr in mdt_vehicle.Rows)
                {
                    DataRow dr;
                    switch (Convert.ToString(ldr["vd_Type"]))
                    {
                        case "S":
                            dr = ldtSoldVehicles.NewRow();
                            dr["VIN"] = ldr["vd_VIN"];
                            VehVIN = Convert.ToString(ldr["vd_VIN"]).Trim();
                            dr["IsLogging"] = ldr["vd_IsLogging"];
                            dr["SoldTo"] = ldr["vd_SoldTo"];
                            dr["SoldEmail"] = ldr["vd_email"];
                            dr["LossType"] = ldr["vd_losstype"];// Stolen , Sold, Destroyed 
                            LossType = Convert.ToString(ldr["vd_losstype"]).Trim();
                            dr["TaxAmount"] = ldr["vd_tax_amt"];
                            dr["TaxYear"] = ldr["vd_tax_year"];
                            ClaimDate = Convert.ToDateTime(ldr["vd_overpayment_date"]);
                            TaxYear = Convert.ToString(ldr["vd_tax_year"]);
                            VehicleExp = Convert.ToString(ldr["vd_Explanation"]);
                            ldtSoldVehicles.Rows.Add(dr);
                            break;
                        case "L":
                            dr = ldtLowMileageVehicles.NewRow();
                            dr["VIN"] = ldr["vd_VIN"];
                            VehVIN = Convert.ToString(ldr["vd_VIN"]);
                            dr["IsLogging"] = ldr["vd_IsLogging"];
                            dr["LossType"] = ldr["vd_losstype"];
                            LossType = Convert.ToString(ldr["vd_losstype"]).Trim();
                            dr["TaxAmount"] = ldr["vd_tax_amt"];
                            dr["TaxYear"] = ldr["vd_tax_year"];
                            TaxYear = Convert.ToString(ldr["vd_tax_year"]);
                            VehicleExp = Convert.ToString(ldr["vd_Explanation"]);
                            ClaimDate = Convert.ToDateTime(ldr["vd_overpayment_date"]);
                            ldtLowMileageVehicles.Rows.Add(dr);
                            break;
                        case "O":
                            dr = ldtTaxOverVehicles.NewRow();
                            dr["VIN"] = ldr["vd_VIN"];
                            VehVIN = Convert.ToString(ldr["vd_VIN"]);
                            dr["IsLogging"] = ldr["vd_IsLogging"];
                            dr["LossType"] = ldr["vd_losstype"];
                            LossType = Convert.ToString(ldr["vd_losstype"]).Trim();
                            dr["OverPaymentDate"] = ldr["vd_overpayment_date"];
                            dr["TaxAmount"] = ldr["vd_tax_amt"];
                            dr["TaxYear"] = ldr["vd_tax_year"];
                            ClaimDate = Convert.ToDateTime(ldr["vd_overpayment_date"]);
                            TaxYear = Convert.ToString(ldr["vd_tax_year"]);
                            VehicleExp = Convert.ToString(ldr["vd_Explanation"]);
                            ldtTaxOverVehicles.Rows.Add(dr);
                            break;
                    }
                }

                dsSoldVehicles = new DataSet();
                dsSoldVehicles.Tables.Add(ldtSoldVehicles);

                dsLowMileageVehicles = new DataSet();
                dsLowMileageVehicles.Tables.Add(ldtLowMileageVehicles);
                
                dsTaxOverVehicles = new DataSet();
                dsTaxOverVehicles.Tables.Add(ldtTaxOverVehicles);

                
                if (TaxYear == "2018") {  //TY2018 = 2018-2019
                    TaxYear = "2019";
                    LatestClaimDt = Convert.ToDateTime("2019-06-30");
                }
                if (TaxYear == "2019") { //TY2019 = 2019-2020
                    TaxYear = "2020";
                    LatestClaimDt = Convert.ToDateTime("2020-06-30");
                }
                if(TaxYear == "2021") {
                    LatestClaimDt = Convert.ToDateTime("2020-06-30");
                }

                Hashtable ht_Month = new Hashtable();
                ht_Month.Add("01", "Jan");
                ht_Month.Add("02", "Feb");
                ht_Month.Add("03", "Mar");
                ht_Month.Add("04", "Apr");
                ht_Month.Add("05", "May");
                ht_Month.Add("06", "Jun");
                ht_Month.Add("07", "Jul");
                ht_Month.Add("08", "Aug");
                ht_Month.Add("09", "Sep");
                ht_Month.Add("10", "Oct");
                ht_Month.Add("11", "Nov");
                ht_Month.Add("12", "Dec");

                switch (TaxEndMonth)
                {
                    case "7": TaxEndMonth = "07"; break;
                    case "8": TaxEndMonth = "08"; break;
                    case "9": TaxEndMonth = "09"; break;
                    case "1": TaxEndMonth = "01"; break;
                    case "2": TaxEndMonth = "02"; break;
                    case "3": TaxEndMonth = "03"; break;
                    case "4": TaxEndMonth = "04"; break;
                    case "5": TaxEndMonth = "05"; break;
                    case "6": TaxEndMonth = "06"; break;
                }
                TaxEndMonth = "--" + TaxEndMonth;
                try
                {
                   
                }
                catch (Exception e)
                {
                    AutosubLog_Write("Error Occurred Vehicles. Exception =" + e.Message.ToString());
                }
 
                #endregion

                AutosubLog_Write("CreateOneZip Create Submission XML");
                #endregion

                #region Create Submission.xml
                VehicleExp = VehicleExp.Trim();
                VehicleExp = VehicleExp.Replace("  ", " ");

                //Enable below during testing.... 
                //SplContdDesc = "Special condition description";
                //CreditRefNo = "365";
                //IsPreparerSelfEmployed = true;

                Return8849Attachment objReturn8849Attachment = new Return8849Attachment(TaxYear);

                if (TaxYear == "2021")
                {    

                    objReturn8849Attachment.createReturnXml_8849_2021(TaxEndMonth, TaxYear, "", "EF8849",
                        IsPreparerSelfEmployed, Preparer_Firm, Preparer_EIN, Preparer_City, Preparer_State, Preparer_Zip,
                        Preparer_Country, Preparer_Address, Preparer_ForeignPhone, Preparer_Phone, Preparer_Email, Preparer_Name,
                        Preparer_PTIN_SSN, IsPreparerHasPTIN, Filer_AddressLine1, Filer_City, Filer_EIN, Filer_Foreign_Country,
                        Filer_Name, Filer_NameControl, Filer_State, Filer_ZIPCode, Officer_Name, Officer_Phone, Officer_PIN,
                        Officer_Title, Preparer_Email,
                        ClaimAmt, ClaimDate, LossType, VehVIN, VehicleExp, CreditRefNo, LatestClaimDt, SplContdDesc);

                }else if (TaxYear == "2020" || TaxYear == "2018" || TaxYear == "2019") {   //2021 New Changes

                    objReturn8849Attachment.createReturnXml_8849_2020(TaxEndMonth, TaxYear, "", "EF8849",
                        IsPreparerSelfEmployed, Preparer_Firm, Preparer_EIN, Preparer_City, Preparer_State, Preparer_Zip, 
                        Preparer_Country, Preparer_Address,Preparer_ForeignPhone, Preparer_Phone, Preparer_Email, Preparer_Name, 
                        Preparer_PTIN_SSN, IsPreparerHasPTIN, Filer_AddressLine1, Filer_City, Filer_EIN, Filer_Foreign_Country, 
                        Filer_Name, Filer_NameControl, Filer_State, Filer_ZIPCode, Officer_Name, Officer_Phone, Officer_PIN, 
                        Officer_Title, Preparer_Email,
                        ClaimAmt, ClaimDate, LossType, VehVIN, VehicleExp, CreditRefNo, LatestClaimDt, SplContdDesc);

                }

                else
                {
                    AutosubLog_Write("Invalid Year or Year not available to be processed. Tax Year :" + TaxYear);
                    return (-1).ToString();
                }

                #endregion
                AutosubLog_Write("CreateOneZip Completed Submission XML");

                AutosubLog_Write("CreateOneZip Create Manifest XML");
                #region Create Manifest.xml

                /* Vishwa 2015 */
                int temp_TaxYr = 0;
                temp_TaxYr = Convert.ToInt32(TaxYear);
                DateTime TaxPeriodEndDate = Convert.ToDateTime("06-30-" + TaxYear);
                if (temp_TaxYr > 0)
                {
                    temp_TaxYr = temp_TaxYr + 1;
                    TaxPeriodEndDate = Convert.ToDateTime("06-30-" + temp_TaxYr.ToString());
                }

                DateTime TaxPeriodBeginDate = Convert.ToDateTime("07-01-" + TaxYear);
                //DateTime TaxPeriodEndDate = Convert.ToDateTime("06-30-" + TaxYear);

                string subid = objReturn8849Attachment.createManifestXMLFile(TaxYear, TaxPeriodEndDate, TaxPeriodBeginDate, Filer_EIN, "8849");

                #endregion

                ZipFile SubmissionZip = ZipFile.Create(@"C:\" + subid + ".zip");

                SubmissionZip.BeginUpdate();
                SubmissionZip.Add(@"C:\manifest\manifest.xml");
                SubmissionZip.Add(@"C:\xml\Submission.xml");
                SubmissionZip.CommitUpdate();
                SubmissionZip.Close();
                AutosubLog_Write("CreateOneZip Completed Manifest XML");
                try
                { File.Delete(@"C:\manifest\manifest.xml"); }
                catch { }
                try { File.Delete(@"C:\xml\Submission.xml"); }
                catch { }
                try { Directory.Delete(@"C:\manifest"); }
                catch { }
                try { Directory.Delete(@"C:\xml"); }
                catch { }
                AutosubLog_Write("CreateOneZip Exit");
                return subid;
            }
            catch (Exception Ex)
            {
                AutosubLog_Write("CreateOneZip: Caught Exception -" + Ex.Message.ToString());
                AutosubLog_Write("Ref# " + refNo + ", formID# " + formID + ", Error Found Contact Admin.");
                try
                {
                    // Now send email, so that admin is aware and ensure it is blocked too untill further notice.
                    string eSubj = "eForm8849 ADMIN: Error processing Submission REF# " + refNo;
                    string eBody = "";
                    eBody = "<p style='font-family: calibri; font-size: 18px'>" +
                            "Dear Vishwanath / Noufaz, <br /><br />" +
                            "ERROR OCCURRED! Please BLOCK the REF# <span style='background-color:yellow;'> " + refNo + "</span>, Until further notice. <br />" +
                            "<br/>ERROR DETAILS : <br/>" +
                            "Caught Exception - <span style='background-color:yellow;'>" + Ex.Message.ToString() + " </span> <br/>" +
                            "<br /> The Submission will be automatically blocked, please verify. <br /> " +
                            " Sincerely, <br />" +
                            " eForm8849 ADMIN Team <br /> " +
                            " </p>";
                    bool mailSent = dbOperation.SendSubmissionMailRejection("vishwanathpm@silverminegroup.com,nanda@silverminegroup.com",
                        "", "", eSubj, eBody);

                    // Block the submission after sending an email.
                    //DataSet lds = dbOperation.Select_Data("usp_Sub8849_stopSubmission", "refNo", refNo);

                    string fields = "@refNo";
                    string values = refNo;
                    dbOperation.Update_Data("usp_Sub8849_stopSubmission", fields, values);

                    if (mailSent)
                    {
                        AutosubLog_Write("CreateOneZip: Successfully SENT Email to : vishwanathpm@silverminegroup.com;nanda@silverminegroup.com");
                    }
                    else
                    {
                        AutosubLog_Write("CreateOneZip: FAILED TO SEND EMAIL, BLOCK THE REF#: " + refNo);
                    }


                }
                catch
                {
                    AutosubLog_Write("CreateOneZip: Caught Error while BLOCKING the submission, For Reference REF#: " + refNo);
                }
                throw;
            }
        }

        private int ToInt32(string TaxYear)
        {
            throw new NotImplementedException();
        }

        private void Transmit2IRS()
        {
            string fileName = BulkFileName;
            fileName = fileName.Replace(".zip", "");

            Stack SubIds = Get_SubmissionIds_fromBulkZip(fileName);

            BaseMethods objBaseMethods = new BaseMethods();
            string returnMsg = objBaseMethods.BulkTransmission(fileName, SubIds);

            if (returnMsg.ToUpper() == "SUCCESS")
            {
                // Vishwa, after tesing if (Variables.TestIndicator == "P")
                {
                    string fields = "@sd_ref_no,@sd_form_id,@sd_submission_ID,@sd_submission_status,@sd_mail_status,@sd_rejection_err_codes";

                    for (int i = 0; i < mdt_US.Rows.Count; i++)
                    {
                        DataRow ldr = mdt_US.Rows[i];

                        if (Convert.ToString(ldr["SubmissionID"]) == "")
                            continue;

                        string values = ldr["ReferenceNumber"].ToString() + "," + ldr["FormId"].ToString() + "," + Convert.ToString(ldr["SubmissionID"]) + "," + "AF" + "," + "N" + "," + null;
                        dbOperation.Update_Data("usp_Sub8849_Update_Submission", fields, values);
                    }
                }
            }
        }

        private Stack Get_SubmissionIds_fromBulkZip(string fileName)
        {
            string targetDirectory = fileName + ".zip";
            ZipInputStream inStream = new ZipInputStream(File.OpenRead(targetDirectory));

            Stack SubIds = new Stack();
            try
            {
                while (true)
                {
                    ZipEntry objZipEntry = inStream.GetNextEntry();

                    if (objZipEntry == null)
                        break;

                    SubIds.Push(objZipEntry.Name.Replace(".zip", ""));
                }
            }
            catch { }
            return SubIds;
        }

        #region VehiclesStructure

        private DataTable Create_Vehicle_8849Sold()
        {
            DataTable ldtVehiclesStrSold = new DataTable("Vehicle8849Sold");
            ldtVehiclesStrSold.Columns.Add(new DataColumn("VIN"));
            ldtVehiclesStrSold.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtVehiclesStrSold.Columns.Add(new DataColumn("SoldTo"));
            ldtVehiclesStrSold.Columns.Add(new DataColumn("SoldEmail"));
            ldtVehiclesStrSold.Columns.Add(new DataColumn("LossType")); // Stolen , Sold, Destroyed 
            ldtVehiclesStrSold.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            ldtVehiclesStrSold.Columns.Add(new DataColumn("TaxYear"));
            return ldtVehiclesStrSold;
        }

        private DataTable Create_Vehicle_8849Low()
        {
            DataTable ldtVehiclesStrLow = new DataTable("Vehicle8849Low");
            ldtVehiclesStrLow.Columns.Add(new DataColumn("VIN"));
            ldtVehiclesStrLow.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtVehiclesStrLow.Columns.Add(new DataColumn("LossType")); //VD_LossType
            ldtVehiclesStrLow.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            ldtVehiclesStrLow.Columns.Add(new DataColumn("TaxYear"));
            return ldtVehiclesStrLow;
        }

        private DataTable Create_Vehicle_8849Over()
        {
            DataTable ldtVehiclesStrOver = new DataTable("Vehicle8849Over");
            ldtVehiclesStrOver.Columns.Add(new DataColumn("VIN"));
            ldtVehiclesStrOver.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtVehiclesStrOver.Columns.Add(new DataColumn("LossType")); //VD_LossType
            ldtVehiclesStrOver.Columns.Add(new DataColumn("OverPaymentDate")); //vd_overpayment_date
            ldtVehiclesStrOver.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            ldtVehiclesStrOver.Columns.Add(new DataColumn("TaxYear"));
            return ldtVehiclesStrOver;
        }
        

        private decimal calculateTaxAmt_TaxableVehicle(bool IsLogging, char WeightCategory, string FirstUsedMonth)
        {
            decimal taxAmount = 0.00M;

            DataTable ldt_TaxCalculation;

            if (FirstUsedMonth != "07")
            {
                if (mdt_PartialTaxCalMaster == null)
                    LoadTaxChart();
                else if (mdt_PartialTaxCalMaster.Rows.Count == 0)
                    LoadTaxChart();
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                switch (FirstUsedMonth)
                {
                    case "01": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jan"]); break;
                    case "02": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Feb"]); break;
                    case "03": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Mar"]); break;
                    case "04": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Apr"]); break;
                    case "05": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["May"]); break;
                    case "06": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jun"]); break;
                    case "08": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Aug"]); break;
                    case "09": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Sep"]); break;
                    case "10": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Oct"]); break;
                    case "11": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Nov"]); break;
                    case "12": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Dec"]); break;
                }
            }
            else
            {
                if (mdt_AnnualTaxCalMaster == null)
                    LoadTaxChart();
                else if (mdt_AnnualTaxCalMaster.Rows.Count == 0)
                    LoadTaxChart();

                ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                int rowIndex = ldt_TaxCalculation.DefaultView.Find(WeightCategory);

                if (IsLogging)
                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["logging"]);
                else
                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["NotLogging"]);
            }

            return taxAmount;
        }
        private Int32 calculateTaxAmt_CreditVehicle(bool IsLogging, char WeightCategory, string EffectiveMonth, string reason)
        {
            EffectiveMonth = Convert.ToDateTime(EffectiveMonth).ToString("MMM");
            EffectiveMonth = EffectiveMonth.ToUpper();
            decimal taxAmount = 0.00M;

            DataTable ldt_TaxCalculation;

            if (EffectiveMonth != "JUN" && reason != "Mileage Not Exceeded")
            {
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                switch (EffectiveMonth.ToUpper())
                {
                    case "JAN": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Feb"]); break;
                    case "FEB": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Mar"]); break;
                    case "MAR": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Apr"]); break;
                    case "APR": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["May"]); break;
                    case "MAY": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jun"]); break;
                    case "JUL": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Aug"]); break;
                    case "AUG": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Sep"]); break;
                    case "SEP": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Oct"]); break;
                    case "OCT": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Nov"]); break;
                    case "NOV": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Dec"]); break;
                    case "DEC": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jan"]); break;
                }
            }
            else
            {
                if (EffectiveMonth.ToUpper() == "JUL")
                {
                    ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                    ldt_TaxCalculation.DefaultView.Sort = "Category";
                    int rowIndex = ldt_TaxCalculation.DefaultView.Find(WeightCategory);

                    if (IsLogging)
                        taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["logging"]);
                    else
                        taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["NotLogging"]);
                }
                else
                {
                    ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                    ldt_TaxCalculation.DefaultView.Sort = "Category";
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);
                }
            }
            return Convert.ToInt32(Math.Round(taxAmount));
        }
        private decimal calculateTaxAmt_TGWIncreasedVehicle(char NewWeightCategory, char OldWeightCategory, string EffectiveMonth, bool IsLogging)
        {
            decimal taxAmount = 0.00M;
            decimal taxDiff = 0;

            DataTable ldt_TaxCalculation;

            if (EffectiveMonth == "07")
            {
                ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "'";

                if (IsLogging)
                    taxDiff = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                else
                    taxDiff = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);

                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "'";

                if (IsLogging)
                    taxAmount = taxDiff - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                else
                    taxAmount = taxDiff - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);

            }
            else
            {
                switch (EffectiveMonth)
                {
                    case "08": EffectiveMonth = "AUG"; break;
                    case "09": EffectiveMonth = "SEP"; break;
                    case "10": EffectiveMonth = "OCT"; break;
                    case "11": EffectiveMonth = "NOV"; break;
                    case "12": EffectiveMonth = "DEC"; break;
                    case "01": EffectiveMonth = "JAN"; break;
                    case "02": EffectiveMonth = "FEB"; break;
                    case "03": EffectiveMonth = "MAR"; break;
                    case "04": EffectiveMonth = "APR"; break;
                    case "05": EffectiveMonth = "MAY"; break;
                    case "06": EffectiveMonth = "JUN"; break;
                }
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";

                if (IsLogging)
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "' AND Logging='" + true + "'";
                else
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "' AND Logging='" + false + "'";

                taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

                if (IsLogging)
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "' AND Logging='" + true + "'";
                else
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "' AND Logging='" + false + "'";

                taxAmount = taxAmount - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

            }

            return Convert.ToDecimal(taxAmount.ToString("N2"));
        }
        private DataTable Create_Tax_Computation_Structure()
        {
            DataTable mdt_TaxComputaion = new DataTable("TaxComputation");
            mdt_TaxComputaion.Columns.Add(new DataColumn("Category"));
            mdt_TaxComputaion.PrimaryKey = new DataColumn[] { mdt_TaxComputaion.Columns["Category"] };
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Tax_Amount", typeof(decimal)));
            return mdt_TaxComputaion;

        }

        #endregion

        public static bool IsWeekend(DateTime dt)
        {
            if ((dt.DayOfWeek == DayOfWeek.Sunday) || (dt.DayOfWeek == DayOfWeek.Saturday))
                return true;
            else
                return false;
        }

        private string getNameControl(string Filer_Name)
        {
            if (Filer_Name.Length < 4)
                return Filer_Name.ToUpper().Trim();

            int i = 0;
            string nameStr = "";
            char[] name = new char[4];

            if (Filer_Name.StartsWith("The "))
                Filer_Name = Filer_Name.Remove(0, 4);
            foreach (char c in Filer_Name)
            {
                if (c != ' ')
                {
                    if (c == 39)
                        continue;

                    name[i] = c;

                    if (++i == 4)
                        break;
                }
            }
            /* Vishwa 2016 added
             When the ShortLenght of Business is less than 3 Char. It as appending "space" which is &#X0; 
             DP Rule 'MeF Process Error IDP Rule' aborted processing.
             Failed to parse XML attachment 
              '1.635935462269117101@example.org/06434620160742773648.zip/xml/Submission.xml':
              Invalid character entity reference: &#x0;
            */
            for (i = 1; i < 4; i++)
            {
                if (name[i] == '\0')
                {
                    name[i] = ' ';
                }
            }
            nameStr = new string(name).Trim();
            return nameStr.ToUpper();
        }

        private void btnSubmission_Click(object sender, EventArgs e)
        {
            AutoTransTime.Enabled = false;
            Wr_Log = 1;
            if (btnSubmission.Text == "Start")
            {
                btnSubmission.Text = "Stop";
                btnSubmission.BackColor = Color.Green;

                objTimer_Submission.Enabled = true;
                objTimer_Submission.Interval = Convert.ToInt16(nud_submission.Value) * 1000;
                objTimer_Submission.Tick += new EventHandler(objTimer_Submission_Tick);
                objTimer_Submission.Start();

            }
            else if (btnSubmission.Text == "Stop")
            {
                objTimer_Submission.Enabled = false;
                btnSubmission.Text = "Start";
                BaseMethods objBase = new BaseMethods();
                string val = objBase._Logout();
                btnSubmission.BackColor = Color.Red;
            }
        }

        private void GetAckSch1()
        {
            BaseMethods objBaseMethods = new BaseMethods();

            string bulkFileName = BulkFileName.Replace(".zip", "").Trim();

            string fileName = BulkFileName.Trim();
            fileName = fileName.Replace(".zip", "");

            Stack SubIds = Get_SubmissionIds_fromBulkZip(fileName);

            string returnMsg = objBaseMethods.Get_BulkAcks(SubIds, fileName);

            if (returnMsg.ToUpper() == "SUCCESS")
            {
                // Vishwa, after tesing if (Variables.TestIndicator == "P")
                {
                    string statusVal = "";

                    string ackFileName = fileName + "-Ack.zip";
                    BaseMethods bm = new BaseMethods();
                    DataSet ds = bm.UnzipFiles(ackFileName.Replace("C:\\", ""));

                    string ErrorList = "";
                    string node = "";
                    if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                        node = "AcceptanceStatusTxt";
                    else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                        node = "FilingStatus";


                    if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                        statusVal = "IA";
                    else
                    {
                        statusVal = "IR";
                        if (ds.Tables.Contains("Error"))
                        {
                            foreach (DataRow dr in ds.Tables["Error"].Rows)
                            {
                                if (ErrorList.Length > 0)
                                    ErrorList += "," + dr["RuleNumber"].ToString();
                                else
                                    ErrorList += dr["RuleNumber"].ToString();
                            }
                            ErrorList = ErrorList.Trim();
                        }
                    }

                    string fields = "@sd_ref_no,@sd_submission_status,@sd_rejection_err_codes";

                    for (int i = 0; i < mdt_US.Rows.Count; i++)
                    {
                        if (ErrorList.Length == 0)
                            ErrorList = null;

                        DataRow ldr = mdt_US.Rows[i];
                        string values = ldr["ReferenceNumber"].ToString() + "," + statusVal + "," + ErrorList;
                        dbOperation.Update_Data("usp_Sub8849_Update_Submission", fields, values);
                    }
                }
            }

            returnMsg = objBaseMethods.Get_BulkSchedule1s(SubIds, fileName);
        }

        private void GetAck()
        {
            BaseMethods objBaseMethods = new BaseMethods();

            DataSet ldsResultSet = dbOperation.GetPendingSubmissionList("S");

            foreach (DataRow ldr in ldsResultSet.Tables[0].Rows)
            {
                try
                {
                    string refNo = ldr["ReferenceNumber"].ToString();
                    string subid = ldr["SubmissionID"].ToString();

                    string returnMsg = objBaseMethods._GetAck(subid);

                    // Vishwa, after tesing if (Variables.TestIndicator == "P")
                    {
                        string statusVal = "";
                        if (objBaseMethods.ackFile_Status == "") continue;
                        //DataSet ds = objBaseMethods.UnzipFiles(subid + "-Ack.zip");
                        DataSet ds = objBaseMethods.readXMLFiles(objBaseMethods.ackFile_Status);

                        string ErrorList = "";
                        string ErrorMessages = "";
                        string node = "";
                        if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                            node = "AcceptanceStatusTxt";
                        else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                            node = "FilingStatus";

                        if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                            statusVal = "IA";
                        else
                        {
                            statusVal = "SR";
                            if (ds.Tables.Contains("ValidationErrorGrp"))
                            {
                                foreach (DataRow dr in ds.Tables["ValidationErrorGrp"].Rows)
                                {
                                    if (ErrorList.Length > 0)
                                        ErrorList += "," + dr["RuleNum"].ToString();
                                    else
                                        ErrorList += dr["RuleNum"].ToString();

                                    if (ErrorMessages.Length > 0)
                                        ErrorMessages += ";;;;" + dr["ErrorMessageTxt"].ToString();
                                    else
                                        ErrorMessages += dr["ErrorMessageTxt"].ToString();
                                }
                                ErrorMessages.Replace("'", "");

                                ErrorList = ErrorList.Trim();
                            }
                        }

                        string fields = "@sd_ref_no,@sd_submission_status,@sd_rejection_err_codes,@sd_rejection_err_msgs";
                        if (ErrorList.Length == 0)
                            ErrorList = null;

                        string values = refNo + "," + statusVal + "," + ErrorList + "," + ErrorMessages;
                        dbOperation.Update_Data("usp_Sub8849_Update_AckStatus_1", fields, values);

                        MoveFiles(subid, refNo);
                    }

                }
                catch { continue; }
            }
        }

        private void GetSch6()
        {
            BaseMethods objBaseMethods = new BaseMethods();

            DataSet ldsResultSet = dbOperation.GetPendingSubmissionList("A");

            foreach (DataRow ldr in ldsResultSet.Tables[0].Rows)
            {
                try
                {
                    string refNo = ldr["ReferenceNumber"].ToString();
                    string subid = ldr["SubmissionID"].ToString();

                    string returnMsg = "SUCCESS"; // NO Schedule 6 for FORM 8849 // 
                    ///objBaseMethods._GetSchedule1(subid);

                    if (returnMsg.ToUpper() == "SUCCESS")
                    {
                        #region Update DB
                        // Vishwa, after tesing if (Variables.TestIndicator == "P")
                        {
                            byte[] schedule1 = null;

                            int output = dbOperation.Update_Schedule1(refNo, schedule1);

                            string fields = "@sd_ref_no,@sd_submission_status,@sd_rejection_err_codes";
                            string values = refNo + "," + "IA" + "," + null;
                            dbOperation.Update_Data("[usp_Sub8849_Update_AckStatus]", fields, values);

                            // MoveFiles(subid, refNo); // Nothing to move now

                            DataSet lds = dbOperation.Select_Data("usp_Get_FreeServiceContact_ByRefNo8849", "refNo", refNo);

                            #region SEND SMS
                            try
                            {
                                if (Convert.ToString(lds.Tables[0].Rows[0]["mobile"]).Trim().Length == 10)
                                {

                                    SMSService.SendSingleSMS
                                        ("eForm8849"
                                        , Convert.ToString(lds.Tables[0].Rows[0]["mobile"].ToString().Trim())
                                        , "eForm8849.com"
                                        , "Congratulations! IRS has accepted your FORM 8849 returns. Please visit https://www.eForm2290.com to download a copy of Schedule 6", true);

                                }
                            }
                            catch { }
                            #endregion

                            // NO FAX 

                            //#region SEND FAX 
                            //try
                            //{
                            //    byte[] Schedule1 = null;
                            //    DataTable dt = new DataTable();
                            //    DBStorage db = new DBStorage();

                            //    dt = dbOperation.Select_Data("select S.submission_id, F.schedule1 FROM C_Submissions S  " +
                            //                                    "LEFT JOIN [8849_Form_Binary] F ON S.FK_8849F_key=F.FK_8849F_key " +
                            //                                    "WHERE S.ref_no='" + refNo + "'").Tables[0];
                            //    if (dt.Rows.Count > 0)
                            //    {
                            //        Schedule1 = ((byte[])dt.Rows[0][1]);
                            //        string tran_id = FAXService.SendFaxDocument(Convert.ToString(lds.Tables[0].Rows[0]["fax"]), Schedule1, "pdf");

                            //        if (tran_id.Length > 0)
                            //        {
                            //            bool statusUpdated = dbOperation.Update_FaxSentStatusAfterSubmission(Convert.ToString(refNo), tran_id);
                            //        }
                            //    }
                            //}
                            //catch { }
                            //#endregion

                        }
                        #endregion
                    }
                }
                catch { continue; }
            }
        }

        private void SendEmail()
        {
            // Vishwa, after tesing if (Variables.TestIndicator == "P")
            {
                BaseMethods objBaseMethods = new BaseMethods();

                DataSet ldsResultSet = dbOperation.GetPendingMailingList();

                foreach (DataRow ldr in ldsResultSet.Tables[0].Rows)
                {
                    try
                    {
                        string support = "support@eform2290.com";
                        string error = "";
                        bool mailSent = dbOperation.SendSubmissionMailAcceptance(
                            Convert.ToString(ldr["From"]).Substring(Convert.ToString(ldr["From"]).Length - 4, 4),
                            Convert.ToString(ldr["FirstName"]), Convert.ToString(ldr["Email"]),
                            Convert.ToString(ldr["ReferenceNumber"]), support, out error);

                        if (error.Length > 0)
                            continue;

                        if (mailSent)
                        {
                            string error1 = "";
                            bool statusUpdated = dbOperation.Update_MailSentStatusAfterSubmission(Convert.ToString(ldr["ReferenceNumber"]), out error1);

                            if (error1.Length > 0)
                                continue;
                        }
                    }
                    catch { continue; }
                }
            }
        }

        public void MoveFiles(string SubmissionId, string ReferenceNumber)
        {
            try
            {
                if (!Directory.Exists("C:\\Live Submissions\\8849\\" + ReferenceNumber))
                    Directory.CreateDirectory("C:\\Live Submissions\\8849\\" + ReferenceNumber);

                string SubmissionFileName = "", ReturnSubmissionFileName = "", GetAckFileName = "", GetSchedule1FileName = "",
                    PathToSave = "C:\\Live Submissions\\8849\\" + ReferenceNumber + "\\",
                    BulkToSave = "C:\\Live Submissions\\8849\\Bulk\\";

                SubmissionFileName = SubmissionId + ".zip";
                ReturnSubmissionFileName = SubmissionId + "-Return.zip";
                GetAckFileName = SubmissionId + "-Ack.zip";
                GetSchedule1FileName = SubmissionId + "-Sch1.zip";

                try
                {
                    if (File.Exists(BulkFileName))
                        File.Move(BulkFileName, BulkToSave + BulkFileName.Replace("C:\\", ""));
                }
                catch { }
                try
                {
                    if (File.Exists(BulkFileName.Replace(".zip", "-Return.zip")))
                        File.Move(BulkFileName.Replace(".zip", "-Return.zip"), BulkToSave + BulkFileName.Replace("C:\\", "").Replace(".zip", "-Return.zip"));
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + SubmissionFileName))
                        File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + ReturnSubmissionFileName))
                        File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + GetAckFileName))
                        File.Move("C:\\" + GetAckFileName, PathToSave + GetAckFileName);
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + GetSchedule1FileName))
                        File.Move("C:\\" + GetSchedule1FileName, PathToSave + GetSchedule1FileName);
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + SubmissionId + ".pdf"))
                    {
                        File.Move("C:\\" + SubmissionId + ".pdf", PathToSave + SubmissionId + ".pdf");
                        try { DeleteDirectory("C:\\" + SubmissionId + "-Sch1"); }
                        catch { }

                    }
                }
                catch { }
                try
                {
                    if (File.Exists("C:\\" + SubmissionFileName))
                        File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                }
                catch { }

            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
            }
        }

        public void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }

        private bool IsHolidayToday()
        {
            try
            {
                //Vishwa, If weekend donot process. 
                // only this week we process on weekends 9Jan2016 and 10Jan2016 
                //if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                //    return true;

                string text = System.IO.File.ReadAllText(@"FederalHoliday.txt");
                if (text.Contains(DateTime.Today.ToString("yyyy-MM-dd")))
                    return true;
            }
            catch { }

            return false;
        }

        public static void AutosubLog_Write(string pMsg)
        {
            if (Wr_Log != 0)
            {
                try
                {
                    System.IO.StreamWriter file =
                                       new System.IO.StreamWriter(@"C:\Live Submissions\8849 Log\LOG-AUTOSUB-" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt", true);
                    file.WriteLine(
                        DateTime.Now.ToString("HH:mm:ss:ffff") + " :::: " + pMsg);
                    file.Close();

                }
                catch
                {

                }
            }

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            BaseMethods objBaseMethods = new BaseMethods();
            MessageBox.Show(objBaseMethods._Logout1());
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            BaseMethods objBaseMethods = new BaseMethods();
            MessageBox.Show(objBaseMethods._Login());
        }
    }
}
