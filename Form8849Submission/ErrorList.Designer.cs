namespace Form8849Sub
{
    partial class frmErrorList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grvErrorList = new System.Windows.Forms.DataGridView();
            this.RuleDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grvErrorList)).BeginInit();
            this.SuspendLayout();
            // 
            // grvErrorList
            // 
            this.grvErrorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvErrorList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RuleDescription});
            this.grvErrorList.Location = new System.Drawing.Point(2, 2);
            this.grvErrorList.Name = "grvErrorList";
            this.grvErrorList.ReadOnly = true;
            this.grvErrorList.Size = new System.Drawing.Size(498, 338);
            this.grvErrorList.TabIndex = 0;
            // 
            // RuleDescription
            // 
            this.RuleDescription.DataPropertyName = "RuleDescription";
            this.RuleDescription.HeaderText = "RuleDescription";
            this.RuleDescription.Name = "RuleDescription";
            this.RuleDescription.ReadOnly = true;
            this.RuleDescription.Width = 1000;
            // 
            // frmErrorList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 342);
            this.Controls.Add(this.grvErrorList);
            this.MaximizeBox = false;
            this.Name = "frmErrorList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ErrorList";
            ((System.ComponentModel.ISupportInitialize)(this.grvErrorList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grvErrorList;
        private System.Windows.Forms.DataGridViewTextBoxColumn RuleDescription;
    }
}