namespace Form8849Sub
{
    partial class frmSubmissionsList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvSubmissionList = new System.Windows.Forms.DataGridView();
            this.chkbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ReferenceNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FromPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BusinessName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubmissionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reason = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAllowResubmission = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.lblToalPending = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer_clock = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubmissionList)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvSubmissionList
            // 
            this.dgvSubmissionList.AllowUserToAddRows = false;
            this.dgvSubmissionList.AllowUserToDeleteRows = false;
            this.dgvSubmissionList.AllowUserToOrderColumns = true;
            this.dgvSubmissionList.AllowUserToResizeRows = false;
            this.dgvSubmissionList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSubmissionList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSubmissionList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvSubmissionList.ColumnHeadersHeight = 24;
            this.dgvSubmissionList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSubmissionList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkbox,
            this.ReferenceNumber,
            this.TaxId,
            this.FromPeriod,
            this.ToPeriod,
            this.BusinessName,
            this.Status,
            this.FormId,
            this.UserId,
            this.SubmissionID,
            this.Reason,
            this.Email,
            this.FirstName});
            this.dgvSubmissionList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvSubmissionList.Location = new System.Drawing.Point(0, 59);
            this.dgvSubmissionList.Margin = new System.Windows.Forms.Padding(2);
            this.dgvSubmissionList.Name = "dgvSubmissionList";
            this.dgvSubmissionList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvSubmissionList.RowHeadersVisible = false;
            this.dgvSubmissionList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubmissionList.Size = new System.Drawing.Size(963, 428);
            this.dgvSubmissionList.TabIndex = 0;
            this.dgvSubmissionList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubmissionList_CellClick);
            // 
            // chkbox
            // 
            this.chkbox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.chkbox.FillWeight = 39.59391F;
            this.chkbox.Frozen = true;
            this.chkbox.HeaderText = "Select";
            this.chkbox.Name = "chkbox";
            this.chkbox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chkbox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chkbox.Width = 50;
            // 
            // ReferenceNumber
            // 
            this.ReferenceNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ReferenceNumber.DataPropertyName = "ReferenceNumber";
            this.ReferenceNumber.FillWeight = 112.0812F;
            this.ReferenceNumber.HeaderText = "Reference No";
            this.ReferenceNumber.Name = "ReferenceNumber";
            this.ReferenceNumber.ReadOnly = true;
            this.ReferenceNumber.Width = 141;
            // 
            // TaxId
            // 
            this.TaxId.DataPropertyName = "TaxId";
            this.TaxId.HeaderText = "Tax ID";
            this.TaxId.Name = "TaxId";
            this.TaxId.ReadOnly = true;
            // 
            // FromPeriod
            // 
            this.FromPeriod.DataPropertyName = "FromPeriod";
            this.FromPeriod.HeaderText = "Period From";
            this.FromPeriod.Name = "FromPeriod";
            this.FromPeriod.ReadOnly = true;
            // 
            // ToPeriod
            // 
            this.ToPeriod.DataPropertyName = "ToPeriod";
            this.ToPeriod.HeaderText = "PeriodTo";
            this.ToPeriod.Name = "ToPeriod";
            this.ToPeriod.ReadOnly = true;
            // 
            // BusinessName
            // 
            this.BusinessName.DataPropertyName = "BusinessName";
            this.BusinessName.HeaderText = "Filer Name";
            this.BusinessName.Name = "BusinessName";
            this.BusinessName.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            // 
            // FormId
            // 
            this.FormId.DataPropertyName = "FormId";
            this.FormId.HeaderText = "Form ID";
            this.FormId.Name = "FormId";
            this.FormId.ReadOnly = true;
            this.FormId.Visible = false;
            // 
            // UserId
            // 
            this.UserId.DataPropertyName = "UserId";
            this.UserId.HeaderText = "User ID";
            this.UserId.Name = "UserId";
            this.UserId.ReadOnly = true;
            this.UserId.Visible = false;
            // 
            // SubmissionID
            // 
            this.SubmissionID.DataPropertyName = "SubmissionID";
            this.SubmissionID.HeaderText = "Submission ID";
            this.SubmissionID.Name = "SubmissionID";
            this.SubmissionID.ReadOnly = true;
            // 
            // Reason
            // 
            this.Reason.DataPropertyName = "Reason";
            this.Reason.HeaderText = "Reason";
            this.Reason.Name = "Reason";
            this.Reason.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Reason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Reason.Text = "Check";
            this.Reason.Visible = false;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.Visible = false;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "FirstName";
            this.FirstName.Name = "FirstName";
            this.FirstName.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.btnAllowResubmission);
            this.panel2.Controls.Add(this.btnExit);
            this.panel2.Controls.Add(this.btnOpen);
            this.panel2.Controls.Add(this.btnSelectAll);
            this.panel2.Controls.Add(this.btnClearAll);
            this.panel2.Controls.Add(this.btnSubmit);
            this.panel2.Controls.Add(this.btnRefresh);
            this.panel2.Location = new System.Drawing.Point(-1, 483);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 42);
            this.panel2.TabIndex = 3;
            // 
            // btnAllowResubmission
            // 
            this.btnAllowResubmission.Location = new System.Drawing.Point(749, 5);
            this.btnAllowResubmission.Margin = new System.Windows.Forms.Padding(2);
            this.btnAllowResubmission.Name = "btnAllowResubmission";
            this.btnAllowResubmission.Size = new System.Drawing.Size(79, 32);
            this.btnAllowResubmission.TabIndex = 6;
            this.btnAllowResubmission.Text = "Notify User";
            this.btnAllowResubmission.UseVisualStyleBackColor = true;
            this.btnAllowResubmission.Click += new System.EventHandler(this.btnAllowResubmission_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(300, 5);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(68, 32);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Clos&e";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(228, 5);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(2);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(68, 32);
            this.btnOpen.TabIndex = 4;
            this.btnOpen.Text = "&Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(12, 5);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(68, 32);
            this.btnSelectAll.TabIndex = 3;
            this.btnSelectAll.Text = "Select &All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(84, 5);
            this.btnClearAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(68, 32);
            this.btnClearAll.TabIndex = 2;
            this.btnClearAll.Text = "&Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(832, 5);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(2);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(113, 32);
            this.btnSubmit.TabIndex = 1;
            this.btnSubmit.Text = "Submit to IRS";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(156, 5);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(68, 32);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "&Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.lblHeading);
            this.panel1.Controls.Add(this.lblDateTime);
            this.panel1.Controls.Add(this.lblToalPending);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 57);
            this.panel1.TabIndex = 4;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Arial Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblHeading.Location = new System.Drawing.Point(10, 15);
            this.lblHeading.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(241, 28);
            this.lblHeading.TabIndex = 3;
            this.lblHeading.Text = "Pending Submissions";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDateTime.Location = new System.Drawing.Point(804, 0);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(148, 17);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "Today\'s date and time";
            // 
            // lblToalPending
            // 
            this.lblToalPending.AutoSize = true;
            this.lblToalPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToalPending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblToalPending.Location = new System.Drawing.Point(913, 31);
            this.lblToalPending.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblToalPending.Name = "lblToalPending";
            this.lblToalPending.Size = new System.Drawing.Size(39, 20);
            this.lblToalPending.TabIndex = 1;
            this.lblToalPending.Text = "-----";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(843, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Count:";
            // 
            // timer_clock
            // 
            this.timer_clock.Tick += new System.EventHandler(this.timer_clock_Tick);
            // 
            // frmSubmissionsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 525);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgvSubmissionList);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "frmSubmissionsList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pending Submissions";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSubmissionsList_FormClosing);
            this.Load += new System.EventHandler(this.frmSubmissionsList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubmissionList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSubmissionList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblToalPending;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Timer timer_clock;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Button btnAllowResubmission;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReferenceNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxId;
        private System.Windows.Forms.DataGridViewTextBoxColumn FromPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn ToPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn BusinessName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormId;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubmissionID;
        private System.Windows.Forms.DataGridViewButtonColumn Reason;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
    }
}