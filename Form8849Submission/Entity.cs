using System;
using System.Collections.Generic;
using System.Text;

namespace Form8849Sub
{
    class Entity
    {
        public static string Username=null;
        public static string UserCategory = null;
        private string FormId;
        private static string LoginStatus;
        public string Formid
        {
            get { return FormId; }
            set { FormId = value; }
        }
        public static string Loginstatus
        {
            get { return LoginStatus; }
            set { LoginStatus = value; }
        }
    }
}
