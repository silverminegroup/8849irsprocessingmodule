using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using API;

namespace Form8849Sub
{
    public partial class frmFormDetails : Form
    {
        private static frmFormDetails childForm;

        public static frmFormDetails CurrentInstance
        {
            get
            {
                if (childForm == null)
                    childForm = new frmFormDetails();
                return childForm;
            }
        }

        public string FormId = "";
        public string UserId = "";
        public string TaxId = "";
        DataSet mds_FullDetails;
        DataTable mdt_user_reg;
        DataTable mdt_profile;
        DataTable mdt_tax2290;
        DataTable mdt_vehicle;
        DataTable mdt_irs_paymnet;
        DataTable mdt_paid_preparer;
        DataTable mdt_third_party_designee;

        DataTable ldtTaxableVehicles;
        DataTable ldtSuspendedVehicles;
        DataTable ldtNotPresentVehicles;
        DataTable ldtSoldSuspendedVehicles;
        DataTable ldtExceedLimitVehicles;
        DataTable ldtTGWIncreasedVehicles;

        eForm8849Operations dbOperation = new eForm8849Operations();

        public frmFormDetails()
        {
            InitializeComponent();
        }

        private void frmFormDetails_Load(object sender, EventArgs e)
        {
            mds_FullDetails = dbOperation.GetAllSubmissionData(FormId, UserId, TaxId);

            mdt_user_reg = mds_FullDetails.Tables[0];
            mdt_profile = mds_FullDetails.Tables[1];
            mdt_tax2290 = mds_FullDetails.Tables[2];
            mdt_vehicle = mds_FullDetails.Tables[3];
            mdt_irs_paymnet = mds_FullDetails.Tables[4];
            mdt_paid_preparer = mds_FullDetails.Tables[5];
            mdt_third_party_designee = mds_FullDetails.Tables[6];

            load_UserDetails();
            load_BusinessProfile();
            load_ThirdPartyDesignee();
            load_PaidPreparer();
            load_vehicles();
        }

        private void load_vehicles()
        {
            ldtTaxableVehicles = Create_Taxable_Vehicle_Structure();
            ldtSuspendedVehicles = Create_Suspended_Vehicle_Structure();
            ldtNotPresentVehicles = Create_Sold_Destroyed_Stolen_Vehicle_Structure();
            ldtSoldSuspendedVehicles = Create_Sold_SuspendedVehicle_Structure();
            ldtExceedLimitVehicles = Create_ExceedLimit_Vehicle_Structure();
            ldtTGWIncreasedVehicles = Create_TGWIncreased_Vehicle_Structure();
            foreach (DataRow ldr in mdt_vehicle.Rows)
            {
                DataRow dr;
                switch (Convert.ToString(ldr["vd_Type"]))
                {
                    case "T":
                        dr = ldtTaxableVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["WeightCategory"] = ldr["vd_Weight_Category_current"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_TaxableVehicle(
                            Convert.ToBoolean(ldr["vd_IsLogging"]),
                            Convert.ToChar(ldr["vd_Weight_Category_current"]),
                            Convert.ToString(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]));
                        ldtTaxableVehicles.Rows.Add(dr);
                        break;
                    case "C":
                        dr = ldtNotPresentVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["WeightCategory"] = ldr["vd_Weight_Category_current"];
                        dr["Reason"] = ldr["vd_credit_reason"];
                        dr["EffectiveDate"] = ldr["vd_effective_date"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_CreditVehicle(
                            Convert.ToBoolean(ldr["vd_IsLogging"]),
                            Convert.ToChar(ldr["vd_Weight_Category_current"]),
                            Convert.ToString(ldr["vd_effective_date"]),
                            Convert.ToString(ldr["vd_credit_reason"]));
                        ldtNotPresentVehicles.Rows.Add(dr);
                        break;
                    case "S":
                        dr = ldtSuspendedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["IsAgricultural"] = ldr["vd_IsAgriculture"];
                        ldtSuspendedVehicles.Rows.Add(dr);
                        break;
                    case "D":
                        dr = ldtSoldSuspendedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["Buyer"] = ldr["vd_buyer_name"];
                        dr["DateSold"] = ldr["vd_effective_date"];
                        ldtSoldSuspendedVehicles.Rows.Add(dr);
                        break;
                    case "M":
                        dr = ldtExceedLimitVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        ldtExceedLimitVehicles.Rows.Add(dr);
                        break;
                    case "W":
                        dr = ldtTGWIncreasedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["IncreasedMonthValue"] = Convert.ToDateTime(ldr["vd_effective_date"]).ToString("MM");
                        dr["WeightCategoryNew"] = ldr["vd_weight_category_current"];
                        dr["WeightCategoryOld"] = ldr["vd_weight_category_old"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_TGWIncreasedVehicle(
                            Convert.ToChar(ldr["vd_weight_category_current"]),
                            Convert.ToChar(ldr["vd_weight_category_old"]),
                            Convert.ToString(dr["IncreasedMonthValue"]),
                            Convert.ToBoolean(ldr["vd_IsLogging"]));
                        ldtTGWIncreasedVehicles.Rows.Add(dr);
                        break;
                }
            }


            dgvVehicles_taxable.DataSource = ldtTaxableVehicles;
            dgvVehicles_Suspended.DataSource = ldtSuspendedVehicles;
            dgvVehicles_SoldSuspended.DataSource = ldtSoldSuspendedVehicles;
            dgvVehicles_Credit.DataSource = ldtNotPresentVehicles;
            dgvVehicles_WeightIncreased.DataSource = ldtTGWIncreasedVehicles;
            dgvVehicles_MileageExceeded.DataSource = ldtExceedLimitVehicles;
        }

        #region VehiclesStructure
        private DataTable Create_Taxable_Vehicle_Structure()
        {
            DataTable ldtTaxableVehicles = new DataTable("TaxableVehicle");
            ldtTaxableVehicles.Columns.Add(new DataColumn("VIN"));
            ldtTaxableVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtTaxableVehicles.Columns.Add(new DataColumn("WeightCategory"));
            ldtTaxableVehicles.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            return ldtTaxableVehicles;
        }
        private DataTable Create_Suspended_Vehicle_Structure()
        {
            DataTable ldtSuspendedVehicles = new DataTable("SuspendedVehicle");
            ldtSuspendedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtSuspendedVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtSuspendedVehicles.Columns.Add(new DataColumn("IsAgricultural", typeof(Boolean)));
            return ldtSuspendedVehicles;
        }
        private DataTable Create_Sold_Destroyed_Stolen_Vehicle_Structure()
        {
            DataTable ldtNotPresentVehicles = new DataTable("NotPresentVehicle");
            ldtNotPresentVehicles.Columns.Add(new DataColumn("VIN"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("WeightCategory"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("Reason"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("EffectiveDate"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("TaxAmount"));
            return ldtNotPresentVehicles;
        }
        private DataTable Create_Sold_SuspendedVehicle_Structure()
        {
            DataTable ldtSoldSuspendedVehicles = new DataTable("SoldSuspendedVehicle");
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("Buyer"));
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("DateSold"));
            return ldtSoldSuspendedVehicles;
        }
        private DataTable Create_ExceedLimit_Vehicle_Structure()
        {
            DataTable ldtExceedLimitVehicles = new DataTable("ExceedLimitVehicle");
            ldtExceedLimitVehicles.Columns.Add(new DataColumn("VIN"));
            return ldtExceedLimitVehicles;
        }
        private DataTable Create_TGWIncreased_Vehicle_Structure()
        {
            DataTable ldtTGWIncreasedVehicles = new DataTable("TGWIncreasedVehicle");
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("IncreasedMonthValue"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("WeightCategoryNew"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("WeightCategoryOld"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            return ldtTGWIncreasedVehicles;
        }
        private decimal calculateTaxAmt_TaxableVehicle(bool IsLogging, char WeightCategory, string FirstUsedMonth)
        {
            decimal taxAmount = 0.00M;

          
            return taxAmount;
        }
        private decimal calculateTaxAmt_CreditVehicle(bool IsLogging, char WeightCategory, string EffectiveMonth, string reason)
        {
            EffectiveMonth = EffectiveMonth.ToUpper();
            decimal taxAmount = 0.00M;

          

            return taxAmount;
        }
        private decimal calculateTaxAmt_TGWIncreasedVehicle(char NewWeightCategory, char OldWeightCategory, string EffectiveMonth, bool IsLogging)
        {
            decimal taxAmount = 0.00M;
            decimal taxDiff = 0;

            DataTable ldt_TaxCalculation;
           

            return Convert.ToDecimal(taxAmount.ToString("N2"));
        }

        private DataTable Create_Tax_Computation_Structure()
        {
            DataTable mdt_TaxComputaion = new DataTable("TaxComputation");
            mdt_TaxComputaion.Columns.Add(new DataColumn("Category"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Tax_Amount", typeof(decimal)));
            return mdt_TaxComputaion;

        }
        #endregion

        private void load_UserDetails()
        {
            try
            {
                lblCategory.Text = mdt_user_reg.Rows[0]["t2ef_usr_category"].ToString();

                lblEmail.Text = mdt_user_reg.Rows[0]["t2ef_email"].ToString();
                lblPhone.Text = mdt_user_reg.Rows[0]["ur_phone_number"].ToString();

                lblFstName.Text = mdt_user_reg.Rows[0]["ur_fst_name"].ToString();
                lblLstName.Text = mdt_user_reg.Rows[0]["ur_last_name"].ToString();
                lblMidName.Text = mdt_user_reg.Rows[0]["ur_mid_name"].ToString();

                lblReferral.Text = mdt_user_reg.Rows[0]["ur_referal_source"].ToString();
                lblSignupIP.Text = mdt_user_reg.Rows[0]["ur_ip_address"].ToString();

                lblCreatedOn.Text = Convert.ToDateTime(mdt_user_reg.Rows[0]["cren_dt"]).ToString("yyyy-MMM-dd");

                if ((mdt_user_reg.Rows[0]["t2ef_status"]).ToString() == "A")
                    lblStatus.Visible = false;
                else
                    lblStatus.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void load_BusinessProfile()
        {
            try
            {
                lblBusinessName.Text = Convert.ToString(mdt_profile.Rows[0]["pd_business_name"]) +
                    " " + Convert.ToString(mdt_profile.Rows[0]["pd_fst_name"]) +
                    " " + Convert.ToString(mdt_profile.Rows[0]["pd_last_name"]);
                lblBusinessName.Text = lblBusinessName.Text.Trim();
                lblBusinessPhone.Text = mdt_profile.Rows[0]["pd_phone"].ToString();
                lblBusinessType.Text = mdt_profile.Rows[0]["pd_business_type"].ToString();

                lblOfficerName.Text = mdt_profile.Rows[0]["pd_sig_name"].ToString();
                lblOfficerPhone.Text = mdt_profile.Rows[0]["pd_sig_phone"].ToString();
                lblOfficerPIN.Text = Crypt.Decrypt(mdt_profile.Rows[0]["pd_sig_pin"].ToString());
                lblOfficerTitle.Text = mdt_profile.Rows[0]["pd_sig_title"].ToString();

                lblBusinessAdd.Text = mdt_profile.Rows[0]["pd_address"].ToString();
                lblBusinessCity.Text = mdt_profile.Rows[0]["pd_city"].ToString();
                if (Convert.ToString(mdt_profile.Rows[0]["pd_frn_cntry"]).Length > 0)
                    lblBusinessCountry.Text = mdt_profile.Rows[0]["pd_frn_cntry"].ToString();
                else
                    lblBusinessCountry.Text = "US";
                lblBusinessState.Text = mdt_profile.Rows[0]["pd_state"].ToString();
                lblBusinessZip.Text = mdt_profile.Rows[0]["pd_zipcode"].ToString();

                lblBusinessCreatedOn.Text = mdt_profile.Rows[0]["cren_dt"].ToString();
                lblBusinessModifiedOn.Text = mdt_profile.Rows[0]["last_updt_on"].ToString();
            }
            catch (Exception ex)
            {
            }

        }

        private void load_PaidPreparer()
        {
            try
            {
                lblPreparerSelfEmployed.Text = mdt_paid_preparer.Rows[0]["ppd_IsSelfEmployed"].ToString();

                lblPreparerName.Text = mdt_paid_preparer.Rows[0]["ppd_Name"].ToString();
                lblPreparerAdd.Text = mdt_paid_preparer.Rows[0]["ppd_address"].ToString();
                lblPreparerCity.Text = mdt_paid_preparer.Rows[0]["ppd_city"].ToString();
                lblPreparerState.Text = mdt_paid_preparer.Rows[0]["ppd_state"].ToString();
                lblPreparerCountry.Text = mdt_paid_preparer.Rows[0]["ppd_country"].ToString();

                lblPreparerEmail.Text = mdt_paid_preparer.Rows[0][""].ToString();

                lblPreparerPh.Text = mdt_paid_preparer.Rows[0][""].ToString();
                lblPreparerPhoneLocation.Text = mdt_paid_preparer.Rows[0][""].ToString();

                lblPreparerSSNOrPTIN.Text = mdt_paid_preparer.Rows[0]["ppd_SSN_PTIN_Type"].ToString() + ":";
                lblPreparerSSNPOrPTIN_No.Text = Crypt.Decrypt(mdt_paid_preparer.Rows[0]["ppd_SSN_PTIN"].ToString());
                lblPreparerZip.Text = mdt_paid_preparer.Rows[0][""].ToString();
            }
            catch (Exception ex)
            { }
        }

        private void load_ThirdPartyDesignee()
        {
            try
            {
                lblTPName.Text = mdt_third_party_designee.Rows[0]["tpd_designee_name"].ToString(); ;
                lblTPPhone.Text = mdt_third_party_designee.Rows[0]["tpd_designee_PH"].ToString(); ;
                lblTPPIN.Text = Crypt.Decrypt(mdt_third_party_designee.Rows[0]["tpd_designee_PIN"].ToString());
            }
            catch (Exception ex)
            {
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            GC.Collect();
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function Not Present", "SORRY!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

    }
}