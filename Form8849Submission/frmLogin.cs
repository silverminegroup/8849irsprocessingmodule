using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Form8849Sub
{
    public partial class frmLogin : Form
    {
        eForm8849Operations dbOperation = new eForm8849Operations();

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {

            DataTable dt = dbOperation.Select_Data("usp_sub2290_get_AdminUsers", "@email,@pwd", txtUserName.Text.Trim() + "," + txtPassword.Text.Trim()).Tables[0];
            if (dt.Rows.Count > 0)
            {
                Entity.Username = dt.Rows[0][0].ToString().ToUpper();
                Entity.UserCategory = dt.Rows[0][1].ToString().ToUpper(); 
                frmMDI MDI = new frmMDI();
                MDI.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Invalid Login Credentials!");
            }
            
        }
    }
}