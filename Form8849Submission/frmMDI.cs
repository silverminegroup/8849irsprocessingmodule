using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Form8849Sub
{
    public partial class frmMDI : Form
    {
        public frmMDI()
        {
            InitializeComponent();
            statusStrip1.Items["toolStripStatusUserName"].Text = Entity.Username;
            statusStrip1.Items["toolStripStatusCategory"].Text = Entity.UserCategory;
            statusStrip1.Items["toolStripStatusLoginTime"].Text = DateTime.Now.ToString();
        }

        private void tsbtn_Pending_Click(object sender, EventArgs e)
        {
            frmSubmissionsList.FORM_CATEGORY = "N";
            frmSubmissionsList frm = new frmSubmissionsList();            
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_ipAddress_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch { }
        }

        private void tsbtn_PendingAck_Click(object sender, EventArgs e)
        {
            frmSubmissionsList.FORM_CATEGORY = "S";
            frmSubmissionsList frm = new frmSubmissionsList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_Accepted_Click(object sender, EventArgs e)
        {
            frmSubmissionsList.FORM_CATEGORY = "A";
            frmSubmissionsList frm = new frmSubmissionsList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_Rejected_Click(object sender, EventArgs e)
        {
            frmSubmissionsList.FORM_CATEGORY = "R";
            frmSubmissionsList frm = new frmSubmissionsList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_Schedule1_Click(object sender, EventArgs e)
        {
            frmSubmissionsList.FORM_CATEGORY = "C";
            frmSubmissionsList frm = new frmSubmissionsList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_submissionMail_Click(object sender, EventArgs e)
        {
            frmSubmissionMailing frm = new frmSubmissionMailing();
            frm.MdiParent = this;
            frm.Show();
        }

        private void frmMDI_FormClosed(object sender, FormClosedEventArgs e)
        {

            if (Entity.Loginstatus == "True")
            {
                Cursor.Current = Cursors.WaitCursor;
                Cursor.Current = Cursors.Arrow;
            }
            GC.Collect();
            Application.Exit();
        }

        private void tsbtn_Contact_Click(object sender, EventArgs e)
        {
            frmSubmissionFAX frm = new frmSubmissionFAX();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbtn_Search_Click(object sender, EventArgs e)
        {
            IRSLibrary.BaseMethods BM = new IRSLibrary.BaseMethods();
            string count = "100";
            BM._GetNewAcks(ref count);
        }
    }
}