using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using System.IO;
using IRSLibrary;
using eFormSMSSender;

namespace Form8849Sub
{

    public partial class frmSubmissionsList : Form
    {
        public static string FORM_CATEGORY;

        private static frmSubmissionsList childForm;

        public static frmSubmissionsList CurrentInstance
        {
            get
            {
                if (childForm == null)
                    childForm = new frmSubmissionsList();
                return childForm;
            }
        }

        public string NewAmount;

        public static DataSet mds_FullDetails = new DataSet();

        eForm8849Operations dbOperation = new eForm8849Operations();

        static DataTable mdt_user_reg = new DataTable();
        static DataTable mdt_profile = new DataTable();
        static DataTable mdt_tax2290 = new DataTable();
        static DataTable mdt_vehicle = new DataTable();
        static DataTable mdt_irs_paymnet = new DataTable();
        static DataTable mdt_paid_preparer = new DataTable();
        static DataTable mdt_third_party_designee = new DataTable();
        static DataTable mdt_AnnualTaxCalMaster = new DataTable();
        static DataTable mdt_PartialTaxCalMaster = new DataTable();
        static DataTable mdt_TaxComputaion = new DataTable();

        public frmSubmissionsList()
        {
            InitializeComponent();
        }

        private void frmSubmissionsList_Load(object sender, EventArgs e)
        {
            try
            {
                if (FORM_CATEGORY == null)
                    FORM_CATEGORY = "N";

                switch (FORM_CATEGORY)
                {
                    case "N": this.Text = "Submissions";
                        lblHeading.Text = "Pending Submission List"; btnAllowResubmission.Visible = false; break;
                    case "S": this.Text = this.Text = "Submissions";
                        lblHeading.Text = "Submitted efiles: waiting for acknowledgement"; btnSubmit.Text = "Get Ack"; btnAllowResubmission.Visible = false; break;
                    case "A": this.Text = this.Text = "Submissions";
                        lblHeading.Text = "Accepted Submissions: waiting for Schedule1"; btnSubmit.Text = "Get Schedule1"; btnAllowResubmission.Visible = false; break;
                    case "R": this.Text = this.Text = "Submissions";
                        lblHeading.Text = "Rejected Submission List"; 
                        btnSubmit.Text="Resubmit"; btnSubmit.Enabled = true; btnAllowResubmission.Visible = true; break;
                    case "C": this.Text = this.Text = "Submissions";
                        lblHeading.Text = "Completed efiles: Schedule1 retrieved"; btnAllowResubmission.Visible = false; btnSubmit.Enabled = false; break;
                }
                lblToalPending.Text = dgvSubmissionList.Rows.Count.ToString();
                mdt_AnnualTaxCalMaster = dbOperation.Select_Data("usp_Get_2290AnnualTaxChartTemp", "", "").Tables[0];
                mdt_PartialTaxCalMaster = dbOperation.Select_Data("usp_Get_2290PartialTaxChartTemp", "", "").Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer_clock_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }

        private string getNameControl(string Filer_Name)
        {
            if (Filer_Name.Length < 4)
                return Filer_Name.ToUpper();

            int i = 0;
            char[] name = new char[4];

            if (Filer_Name.StartsWith("The "))
                Filer_Name = Filer_Name.Remove(0, 4);
            foreach (char c in Filer_Name)
            {
                if (c != ' ')
                {
                    if (c == 39)
                        continue;

                    name[i] = c;

                    if (++i == 4)
                        break;
                }
            }
            return new string(name).ToUpper();
        }

        private clsReturnValues sendSingleSubmissionToIRS(string refNo, string subID, string formID, char status)
        {
            clsReturnValues rv = new clsReturnValues();

            #region Variables declaration
            bool IsAddressChange, IsAmendedReturn, IsVINCorrection, IsFinalReturn, IsConsentToDiscloseYes, IsNotSubjectToTaxChecked;
            IsAddressChange = IsAmendedReturn = IsVINCorrection = IsFinalReturn = IsConsentToDiscloseYes = IsNotSubjectToTaxChecked = false;

            string AmendedMonth, TaxYear, First_Used_Date;
            AmendedMonth = TaxYear = First_Used_Date = "";

            string Filer_Name, Filer_NameControl, Filer_EIN, Filer_AddressLine1, Filer_City, Filer_State, Filer_Foreign_Country, Filer_ZIPCode;
            Filer_Name = Filer_NameControl = Filer_EIN = Filer_AddressLine1 = Filer_City = Filer_State = Filer_Foreign_Country = Filer_ZIPCode = "";

            string Officer_Name, Officer_Phone, Officer_PIN, Officer_Title;
            Officer_Name = Officer_Phone = Officer_PIN = Officer_Title = "";

            string ThirdPartyDesigne_Name, ThirdPartyDesigne_Phone, ThirdPartyDesigne_PIN;
            ThirdPartyDesigne_Name = ThirdPartyDesigne_Phone = ThirdPartyDesigne_PIN = "";

            string Preparer_Firm, Preparer_EIN, Preparer_City, Preparer_State, Preparer_Zip, Preparer_Country, Preparer_Address, Preparer_ForeignPhone, Preparer_Phone, Preparer_Email, Preparer_Name, Preparer_PTIN_SSN;
             Preparer_Firm= Preparer_EIN= Preparer_City= Preparer_State= Preparer_Zip= Preparer_Country=Preparer_Address = Preparer_ForeignPhone = Preparer_Phone = Preparer_Email = Preparer_Name = Preparer_PTIN_SSN = "";

            bool IsPreparerHasPTIN, IsPreparerSelfEmployed;
            IsPreparerHasPTIN = IsPreparerSelfEmployed = false;

            string paymentType, Payment_Acc_No, Payment_Acc_Type, Payment_ReqPayDate, Payment_RoutingTransitNo, Payment_Txpyer_Ph;
            paymentType = Payment_Acc_No = Payment_Acc_Type = Payment_ReqPayDate = Payment_RoutingTransitNo = Payment_Txpyer_Ph = "";

            DataSet dsTaxableVehicles, dsCreditVehicles, dsSuspendedVehicles, dsSoldSuspendedVehicles;
            DataSet dsPriorYearMileageExceededVehicles, dsTGWIncreaseWorksheet, dsTaxComputation;
            string soldSuspendedVehicles, TotalVehicles;
            soldSuspendedVehicles = TotalVehicles = "";

            decimal TaxFromTaxComputation, AdditionalTaxAmount, CreditAmount;
            TaxFromTaxComputation = AdditionalTaxAmount = CreditAmount = 0;

            DataTable ldtTaxableVehicles, ldtSuspendedVehicles, ldtNotPresentVehicles,
                ldtSoldSuspendedVehicles, ldtExceedLimitVehicles, ldtTGWIncreasedVehicles, ldtTaxComputation;

            #endregion

            #region Tax2290 values
            IsAddressChange = Convert.ToBoolean(mdt_tax2290.Rows[0]["ttd_IsAddressChange"]);
            IsAmendedReturn = Convert.ToBoolean(mdt_tax2290.Rows[0]["ttd_IsAmendedReturn"]);
            try
            {
                IsVINCorrection = Convert.ToBoolean(mdt_tax2290.Rows[0]["ttd_IsVINCorrection"]);
            }
            catch
            {
                IsVINCorrection = false;
            }

            IsFinalReturn = Convert.ToBoolean(mdt_tax2290.Rows[0]["ttd_IsFinalReturn"]);

            if (Convert.ToString(mdt_tax2290.Rows[0]["ttd_Consent_Tag"]) != "")
                IsConsentToDiscloseYes = Convert.ToBoolean(mdt_tax2290.Rows[0]["ttd_Consent_Tag"]);
            else
                IsConsentToDiscloseYes = Convert.ToBoolean(0);

            AmendedMonth = "--"+Convert.ToString(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]);

            TaxYear = Convert.ToString(mdt_tax2290.Rows[0]["tld_FromPeriod"]);
            TaxYear = TaxYear.Substring(TaxYear.Length - 4, 4);

            int mon = Convert.ToInt16(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]);
            if (mon >= 7)
                First_Used_Date = TaxYear + "-" +
                    Convert.ToString(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]);
            else if (mon < 7)
                First_Used_Date = Convert.ToString(Convert.ToInt16(TaxYear) + 1) + "-" +
                    Convert.ToString(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]);

            try
            {
                TaxFromTaxComputation = Convert.ToDecimal(mdt_tax2290.Rows[0]["tld_Total_Tax"]);
            }
            catch
            {
                TaxFromTaxComputation = 0;
            }
            try
            {
                AdditionalTaxAmount = Convert.ToDecimal(mdt_tax2290.Rows[0]["tld_Additional_Tax"]);
            }
            catch
            {
                AdditionalTaxAmount = 0;
            }
            try
            {
                CreditAmount = Convert.ToDecimal(mdt_tax2290.Rows[0]["tld_Credit_Amount"]);
            }
            catch
            {
                CreditAmount = 0;
            }
            #endregion

            #region Profile Values

            Filer_Name = Convert.ToString(mdt_profile.Rows[0]["pd_business_name"]) + " " +
                Convert.ToString(mdt_profile.Rows[0]["pd_fst_name"]) + " " +
                Convert.ToString(mdt_profile.Rows[0]["pd_last_name"]);
            Filer_Name = Filer_Name.Trim();
            Filer_NameControl = getNameControl(Filer_Name);

            Filer_EIN = Convert.ToString(mdt_profile.Rows[0]["pd_tax_id"]);


            Filer_AddressLine1 = Convert.ToString(mdt_profile.Rows[0]["pd_address"]);
            Filer_City = Convert.ToString(mdt_profile.Rows[0]["pd_city"]);
            Filer_ZIPCode = Convert.ToString(mdt_profile.Rows[0]["pd_zipcode"]);
            Filer_Foreign_Country = Convert.ToString(mdt_profile.Rows[0]["pd_frn_cntry"]);

            Filer_State = Convert.ToString(mdt_profile.Rows[0]["pd_state"]);

            Officer_Name = Convert.ToString(mdt_profile.Rows[0]["pd_sig_name"]);
            Officer_Phone = Convert.ToString(mdt_profile.Rows[0]["pd_sig_phone"]);

            Officer_PIN = Convert.ToString(mdt_profile.Rows[0]["pd_sig_pin"]);
            Officer_Title = Convert.ToString(mdt_profile.Rows[0]["pd_sig_title"]);

            #endregion

            #region Third Party Designee

            if (mdt_third_party_designee.Rows.Count > 0)
            {
                ThirdPartyDesigne_Name = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_name"]);
                ThirdPartyDesigne_Phone = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_ph"]);
                ThirdPartyDesigne_PIN = Convert.ToString(mdt_third_party_designee.Rows[0]["tpd_designee_pin"]);
            }
            else
            {
                ThirdPartyDesigne_Name = "";
                ThirdPartyDesigne_Phone = "";
                ThirdPartyDesigne_PIN = "";
            }

            #endregion

            #region paid Preparer

            if (mdt_paid_preparer.Rows.Count > 0)
            {
                Preparer_Name = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_name"]);
                Preparer_PTIN_SSN = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_ssn_ptin"]);
                Preparer_Email = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_email"]);
                Preparer_Address = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_address"]);
                IsPreparerSelfEmployed = Convert.ToBoolean(mdt_paid_preparer.Rows[0]["ppd_IsSelfEmployed"]);

                if (Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_ssn_ptin_type"]) == "PTIN")
                    IsPreparerHasPTIN = true;
                else
                    IsPreparerHasPTIN = false;

                if (Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone_Type"]) == "Foreign")
                {
                    Preparer_ForeignPhone = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone"]);
                    Preparer_Phone = "";
                }
                else
                {
                    Preparer_Phone = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_phone"]);
                    Preparer_ForeignPhone = "";
                }

                if (!IsPreparerSelfEmployed)
                {
                    Preparer_Firm = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_name"]);
                    Preparer_EIN = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_EIN"]);
                    Preparer_City = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_city"]);
                   
                    Preparer_Zip = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_zip"]);
                    Preparer_Country = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_country"]);

                    if(Preparer_Country!="")
                         Preparer_State = Convert.ToString(mdt_paid_preparer.Rows[0]["ppd_state"]);
                    else
                         Preparer_State = Convert.ToString(mdt_paid_preparer.Rows[0]["zip_st_cd"]);
                }
            }
            else
            {
                Preparer_Name = "";
                Preparer_PTIN_SSN = "";
                Preparer_Email = "";
                Preparer_Address = "";
                IsPreparerSelfEmployed = false;
                IsPreparerHasPTIN = false;
                Preparer_Phone = "";
                Preparer_ForeignPhone = "";
                Preparer_Firm = "";
                Preparer_EIN = "";
                Preparer_City = "";
                Preparer_Zip = "";
                Preparer_Country = "";
                Preparer_State = "";
            }

            #endregion

            #region IRS Payment

            if (Convert.ToString(mdt_tax2290.Rows[0]["ttd_Payment_Mode"]) == "EFTPS")
            {
                paymentType = "EFTPS";
                Payment_Acc_No = "";
                Payment_Acc_Type = ""; Payment_ReqPayDate = "";
                Payment_RoutingTransitNo = "";
                Payment_Txpyer_Ph = "";
            }
            else if (Convert.ToString(mdt_tax2290.Rows[0]["ttd_Payment_Mode"]) == "DirectDebit")
            {
                paymentType = "DirectDebit";
                Payment_Acc_No = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_bank_account_no"]);
                Payment_Acc_Type = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_payment_type"]);
                
                Payment_ReqPayDate = DateTime.Today.ToString("MM-dd-yyyy");

                Payment_RoutingTransitNo = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_bank_routing_no"]);
                Payment_Txpyer_Ph = Convert.ToString(mdt_irs_paymnet.Rows[0]["ip_taxpayer_phone"]);
            }
            else
            {
                Payment_Acc_No = "";
                Payment_Acc_Type = ""; Payment_ReqPayDate = "";
                Payment_RoutingTransitNo = "";
                Payment_Txpyer_Ph = "";
            }
           
            #endregion

            #region Vehciles

            ldtTaxableVehicles = Create_Taxable_Vehicle_Structure();
            ldtSuspendedVehicles = Create_Suspended_Vehicle_Structure();
            ldtNotPresentVehicles = Create_Sold_Destroyed_Stolen_Vehicle_Structure();
            ldtSoldSuspendedVehicles = Create_Sold_SuspendedVehicle_Structure();
            ldtExceedLimitVehicles = Create_ExceedLimit_Vehicle_Structure();
            ldtTGWIncreasedVehicles = Create_TGWIncreased_Vehicle_Structure();
            ldtTaxComputation = Create_Tax_Computation_Structure();
            soldSuspendedVehicles = "";
            foreach (DataRow ldr in mdt_vehicle.Rows)
            {
                DataRow dr;
                switch (Convert.ToString(ldr["vd_Type"]))
                {
                    case "T":
                        dr = ldtTaxableVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["WeightCategory"] = ldr["vd_Weight_Category_current"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_TaxableVehicle(
                            Convert.ToBoolean(ldr["vd_IsLogging"]),
                            Convert.ToChar(ldr["vd_Weight_Category_current"]),
                            Convert.ToString(mdt_tax2290.Rows[0]["ttd_FirstUsedMonth"]));
                        ldtTaxableVehicles.Rows.Add(dr);
                        break;
                    case "C":
                        dr = ldtNotPresentVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["WeightCategory"] = ldr["vd_Weight_Category_current"];
                        dr["Reason"] = ldr["vd_credit_reason"];
                        dr["EffectiveDate"] = ldr["vd_effective_date"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_CreditVehicle(
                            Convert.ToBoolean(ldr["vd_IsLogging"]),
                            Convert.ToChar(ldr["vd_Weight_Category_current"]),
                            Convert.ToString(ldr["vd_effective_date"]),
                            Convert.ToString(ldr["vd_credit_reason"]));
                        ldtNotPresentVehicles.Rows.Add(dr);
                        break;
                    case "S":
                        dr = ldtSuspendedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["IsAgricultural"] = ldr["vd_IsAgriculture"];
                        ldtSuspendedVehicles.Rows.Add(dr);
                        break;
                    case "D":
                        dr = ldtSoldSuspendedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["Buyer"] = ldr["vd_buyer_name"];
                        dr["DateSold"] = ldr["vd_effective_date"];
                        ldtSoldSuspendedVehicles.Rows.Add(dr);
                        soldSuspendedVehicles += Convert.ToString(ldr["vd_VIN"]);
                        break;
                    case "M":
                        dr = ldtExceedLimitVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        ldtExceedLimitVehicles.Rows.Add(dr);
                        break;
                    case "W":
                        dr = ldtTGWIncreasedVehicles.NewRow();
                        dr["VIN"] = ldr["vd_VIN"];
                        dr["IsLogging"] = ldr["vd_IsLogging"];
                        dr["IncreasedMonthValue"] = Convert.ToDateTime(ldr["vd_effective_date"]).ToString("MM");
                        dr["WeightCategoryNew"] = ldr["vd_weight_category_current"];
                        dr["WeightCategoryOld"] = ldr["vd_weight_category_old"];
                        dr["TaxAmount"] =
                            calculateTaxAmt_TGWIncreasedVehicle(
                            Convert.ToChar(ldr["vd_weight_category_current"]),
                            Convert.ToChar(ldr["vd_weight_category_old"]),
                            Convert.ToString(dr["IncreasedMonthValue"]),
                            Convert.ToBoolean(ldr["vd_IsLogging"]));

                        

                        DataTable ldt_TaxCalculation;


                        if (dr["IncreasedMonthValue"].ToString() == "07")
                        {
                            ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                            ldt_TaxCalculation.DefaultView.Sort = "Category";
                            ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryNew"]) + "'";

                            if (Convert.ToBoolean(dr["IsLogging"]))
                                dr["NewTaxAmount"] = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                            else
                                dr["NewTaxAmount"] = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);

                            ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryOld"]) + "'";

                            if (Convert.ToBoolean(dr["IsLogging"]))
                                dr["PreviousTaxAmount"] = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                            else
                                dr["PreviousTaxAmount"] = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);
                        }
                        else
                        {
                            string EffectiveMonth = dr["IncreasedMonthValue"].ToString();
                            switch (EffectiveMonth)
                            {
                                case "08": EffectiveMonth = "AUG"; break;
                                case "09": EffectiveMonth = "SEP"; break;
                                case "10": EffectiveMonth = "OCT"; break;
                                case "11": EffectiveMonth = "NOV"; break;
                                case "12": EffectiveMonth = "DEC"; break;
                                case "01": EffectiveMonth = "JAN"; break;
                                case "02": EffectiveMonth = "FEB"; break;
                                case "03": EffectiveMonth = "MAR"; break;
                                case "04": EffectiveMonth = "APR"; break;
                                case "05": EffectiveMonth = "MAY"; break;
                                case "06": EffectiveMonth = "JUN"; break;
                            }
                            ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                            ldt_TaxCalculation.DefaultView.Sort = "Category";

                            if (Convert.ToBoolean(dr["IsLogging"]))
                                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryNew"]) + "' AND Logging='" + true + "'";
                            else
                                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryNew"]) + "' AND Logging='" + false + "'";

                            dr["NewTaxAmount"] = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

                            if (Convert.ToBoolean(dr["IsLogging"]))
                                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryOld"]) + "' AND Logging='" + true + "'";
                            else
                                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + Convert.ToString(dr["WeightCategoryOld"]) + "' AND Logging='" + false + "'";

                            dr["PreviousTaxAmount"] =  Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

                        }

                        dr["IncreasedMonthValue"] = "--" + Convert.ToDateTime(ldr["vd_effective_date"]).ToString("MM");
                        ldtTGWIncreasedVehicles.Rows.Add(dr);
                        break;
                }
            }
            dsTaxableVehicles = new DataSet();
            dsTaxableVehicles.Tables.Add(ldtTaxableVehicles);
            dsSuspendedVehicles = new DataSet();
            dsSuspendedVehicles.Tables.Add(ldtSuspendedVehicles);
            dsSoldSuspendedVehicles = new DataSet();
            dsSoldSuspendedVehicles.Tables.Add(ldtSoldSuspendedVehicles);
            dsCreditVehicles = new DataSet();
            dsCreditVehicles.Tables.Add(ldtNotPresentVehicles);
            dsPriorYearMileageExceededVehicles = new DataSet();
            dsPriorYearMileageExceededVehicles.Tables.Add(ldtExceedLimitVehicles);
            dsTGWIncreaseWorksheet = new DataSet();
            dsTGWIncreaseWorksheet.Tables.Add(ldtTGWIncreasedVehicles);
            dsTaxComputation = new DataSet();
            dsTaxComputation.Tables.Add(ldtTaxComputation);

            if (ldtExceedLimitVehicles.Rows.Count > 0)
                IsNotSubjectToTaxChecked = true;
            else
                IsNotSubjectToTaxChecked = false;

            Hashtable ht_Month = new Hashtable();
            ht_Month.Add("01", "Jan");
            ht_Month.Add("02", "Feb");
            ht_Month.Add("03", "Mar");
            ht_Month.Add("04", "Apr");
            ht_Month.Add("05", "May");
            ht_Month.Add("06", "Jun");
            ht_Month.Add("07", "Jul");
            ht_Month.Add("08", "Aug");
            ht_Month.Add("09", "Sep");
            ht_Month.Add("10", "Oct");
            ht_Month.Add("11", "Nov");
            ht_Month.Add("12", "Dec");

            string month = mdt_tax2290.Rows[0]["ttd_firstUsedMonth"].ToString();
            month = ht_Month[month].ToString();

            foreach (DataRow ldr_Taxable in ldtTaxableVehicles.Rows)
            {
                string category = Convert.ToString(ldr_Taxable["WeightCategory"]);
                bool isLogging = Convert.ToBoolean(ldr_Taxable["IsLogging"]);

                mdt_PartialTaxCalMaster.DefaultView.Sort = "category";
                mdt_PartialTaxCalMaster.DefaultView.RowFilter = "category='" + category + "' and logging=" + isLogging;

                mdt_AnnualTaxCalMaster.DefaultView.Sort = "category";
                mdt_AnnualTaxCalMaster.DefaultView.RowFilter = "category='" + category + "'";

                ldtTaxComputation.DefaultView.Sort = "category";
                ldtTaxComputation.DefaultView.RowFilter = "category='" + category + "'";
                DataRow ldr_taxComputation;
                if (ldtTaxComputation.DefaultView.Count > 0)
                {
                    ldr_taxComputation = ldtTaxComputation.Rows.Find(category);

                    if (isLogging)
                    {
                        if (month != "Jul")
                        {
                            ldr_taxComputation["Logging_Partial_Tax"] =
                                mdt_PartialTaxCalMaster.DefaultView.ToTable().Rows[0][month];
                        }
                        else
                        {
                            ldr_taxComputation["Logging_Partial_Tax"] =
                               mdt_AnnualTaxCalMaster.DefaultView.ToTable().Rows[0]["logging"];
                        }
                        if (ldr_taxComputation.IsNull("Logging_Vehicle_Count"))
                            ldr_taxComputation["Logging_Vehicle_Count"] = 1;
                        else
                            ldr_taxComputation["Logging_Vehicle_Count"]
                                = Convert.ToInt16(ldr_taxComputation["Logging_Vehicle_Count"]) + 1;

                        ldr_taxComputation["Tax_Amount"] =
                            Convert.ToDecimal(ldr_taxComputation["Tax_Amount"]) +
                            Convert.ToDecimal(ldr_taxComputation["Logging_Partial_Tax"]);
                    }
                    else
                    {
                        if (month != "Jul")
                        {
                            ldr_taxComputation["Non_Logging_Partial_Tax"] =
                                mdt_PartialTaxCalMaster.DefaultView.ToTable().Rows[0][month];
                        }
                        else
                        {
                            ldr_taxComputation["Non_Logging_Partial_Tax"] =
                               mdt_AnnualTaxCalMaster.DefaultView.ToTable().Rows[0]["notlogging"];
                        }

                        if (ldr_taxComputation.IsNull("Non_Logging_Vehicle_Count"))
                            ldr_taxComputation["Non_Logging_Vehicle_Count"] = 1;
                        else
                            ldr_taxComputation["Non_Logging_Vehicle_Count"] =
                                Convert.ToInt16(ldr_taxComputation["Non_Logging_Vehicle_Count"]) + 1;

                        ldr_taxComputation["Tax_Amount"] = 
                            Convert.ToDecimal(ldr_taxComputation["Tax_Amount"]) +
                            Convert.ToDecimal(ldr_taxComputation["Non_Logging_Partial_Tax"]);
                    }

                }
                else
                {
                    ldr_taxComputation = ldtTaxComputation.NewRow();
                    ldr_taxComputation["Category"] = category;

                    if (isLogging)
                    {
                        if (month != "Jul")
                        {
                            ldr_taxComputation["Logging_Partial_Tax"] =
                                mdt_PartialTaxCalMaster.DefaultView[0][month];
                        }
                        else
                        {
                            ldr_taxComputation["Logging_Partial_Tax"] =
                                mdt_AnnualTaxCalMaster.DefaultView.ToTable().Rows[0]["logging"];
                        }
                        ldr_taxComputation["Logging_Vehicle_Count"] = 1;
                        ldr_taxComputation["Tax_Amount"] = Convert.ToDecimal(ldr_taxComputation["Logging_Partial_Tax"]);
                    }
                    else
                    {
                        if (month != "Jul")
                        {
                            ldr_taxComputation["Non_Logging_Partial_Tax"] =
                                mdt_PartialTaxCalMaster.DefaultView[0][month];
                        }
                        else
                        {
                            ldr_taxComputation["Non_Logging_Partial_Tax"] =
                                mdt_AnnualTaxCalMaster.DefaultView.ToTable().Rows[0]["notlogging"];
                        }
                        ldr_taxComputation["Non_Logging_Vehicle_Count"] = 1;
                        ldr_taxComputation["Tax_Amount"] = Convert.ToDecimal(ldr_taxComputation["Non_Logging_Partial_Tax"]);
                    }

                    ldtTaxComputation.Rows.Add(ldr_taxComputation);
                }
            }

            foreach (DataRow ldr_TGWI in ldtTGWIncreasedVehicles.Rows)
            {
                string category = Convert.ToString(ldr_TGWI["WeightCategoryNew"]);
                bool isLogging = Convert.ToBoolean(ldr_TGWI["IsLogging"]);

                DataRow ldr_taxComputation;
                if (ldtTaxComputation.DefaultView.Count > 0)
                {
                    ldr_taxComputation = ldtTaxComputation.Rows.Find(category);

                    if (isLogging)
                    {
                        ldr_taxComputation["Logging_Partial_Tax"] = ldr_TGWI["TaxAmount"];

                        ldr_taxComputation["Logging_Vehicle_Count"]
                            = Convert.ToInt16(ldr_taxComputation["Logging_Vehicle_Count"]) + 1;

                        ldr_taxComputation["Tax_Amount"] =
                            Convert.ToDecimal(ldr_taxComputation["Tax_Amount"]) +
                            Convert.ToDecimal(ldr_taxComputation["Logging_Partial_Tax"]);
                    }
                    else
                    {
                        ldr_taxComputation["Non_Logging_Partial_Tax"] = ldr_TGWI["TaxAmount"];

                        ldr_taxComputation["Non_Logging_Vehicle_Count"] =
                            Convert.ToInt16(ldr_taxComputation["Non_Logging_Vehicle_Count"]) + 1;

                        ldr_taxComputation["Tax_Amount"] =
                            Convert.ToDecimal(ldr_taxComputation["Tax_Amount"]) +
                            Convert.ToDecimal(ldr_taxComputation["Non_Logging_Partial_Tax"]);
                    }
                }
                else
                {
                    ldr_taxComputation = ldtTaxComputation.NewRow();
                    ldr_taxComputation["Category"] = category;

                    if (isLogging)
                    {
                        ldr_taxComputation["Logging_Partial_Tax"] = ldr_TGWI["TaxAmount"];
                        ldr_taxComputation["Logging_Vehicle_Count"] = 1;
                        ldr_taxComputation["Tax_Amount"] = Convert.ToDecimal(ldr_taxComputation["Logging_Partial_Tax"]);
                    }
                    else
                    {
                        ldr_taxComputation["Non_Logging_Partial_Tax"] = ldr_TGWI["TaxAmount"];
                        ldr_taxComputation["Non_Logging_Vehicle_Count"] = 1;
                        ldr_taxComputation["Tax_Amount"] = Convert.ToDecimal(ldr_taxComputation["Non_Logging_Partial_Tax"]);
                    }

                    ldtTaxComputation.Rows.Add(ldr_taxComputation);
                }
            }
             
            TotalVehicles = Convert.ToString(ldtTaxableVehicles.Rows.Count + ldtTGWIncreasedVehicles.Rows.Count);

            #endregion

            IRSLibrary.Return8849Attachment sendsubmission = new IRSLibrary.Return8849Attachment(TaxYear);
            rv = sendsubmission.SEND_APPLICATION_TO_IRS(
                refNo, subID, formID, status,
            #region parameter
                IsAddressChange,
                IsAmendedReturn,
                IsVINCorrection,
                IsFinalReturn,
                IsNotSubjectToTaxChecked,
                IsConsentToDiscloseYes,
                AmendedMonth,
                TaxYear,
                First_Used_Date,
                Filer_AddressLine1,
                Filer_City,
                Filer_EIN,
                Filer_Foreign_Country,
                Filer_Name,
                Filer_NameControl,
                Filer_State,
                Filer_ZIPCode,
                Officer_Name,
                Officer_Phone,
                Officer_PIN,
                Officer_Title,
                "",
                ThirdPartyDesigne_Name,
                ThirdPartyDesigne_Phone,
                ThirdPartyDesigne_PIN,
                Preparer_Firm, 
                Preparer_EIN, 
                Preparer_City, 
                Preparer_State, 
                Preparer_Zip, 
                Preparer_Country,
                Preparer_Address,
                Preparer_ForeignPhone,
                Preparer_Phone,
                Preparer_Email,
                Preparer_Name,
                Preparer_PTIN_SSN,
                IsPreparerHasPTIN,
                IsPreparerSelfEmployed,
                paymentType,
                Payment_Acc_No,
                Payment_Acc_Type,
                Payment_ReqPayDate,
                Payment_RoutingTransitNo,
                Payment_Txpyer_Ph,
                dsTaxableVehicles,
                dsCreditVehicles,
                dsSuspendedVehicles,
                dsSoldSuspendedVehicles,
                dsPriorYearMileageExceededVehicles,
                dsTGWIncreaseWorksheet,
                dsTaxComputation,
                soldSuspendedVehicles,
                TaxFromTaxComputation,
                AdditionalTaxAmount,
                CreditAmount,
                TotalVehicles
            #endregion
);
            return rv;
           
        }

        #region VehiclesStructure
        private DataTable Create_Taxable_Vehicle_Structure()
        {
            DataTable ldtTaxableVehicles = new DataTable("TaxableVehicle");
            ldtTaxableVehicles.Columns.Add(new DataColumn("VIN"));
            ldtTaxableVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtTaxableVehicles.Columns.Add(new DataColumn("WeightCategory"));
            ldtTaxableVehicles.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            return ldtTaxableVehicles;
        }
        private DataTable Create_Suspended_Vehicle_Structure()
        {
            DataTable ldtSuspendedVehicles = new DataTable("SuspendedVehicle");
            ldtSuspendedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtSuspendedVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtSuspendedVehicles.Columns.Add(new DataColumn("IsAgricultural", typeof(Boolean)));
            return ldtSuspendedVehicles;
        }
        private DataTable Create_Sold_Destroyed_Stolen_Vehicle_Structure()
        {
            DataTable ldtNotPresentVehicles = new DataTable("NotPresentVehicle");
            ldtNotPresentVehicles.Columns.Add(new DataColumn("VIN"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("WeightCategory"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("Reason"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("EffectiveDate"));
            ldtNotPresentVehicles.Columns.Add(new DataColumn("TaxAmount"));
            return ldtNotPresentVehicles;
        }
        private DataTable Create_Sold_SuspendedVehicle_Structure()
        {
            DataTable ldtSoldSuspendedVehicles = new DataTable("SoldSuspendedVehicle");
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("Buyer"));
            ldtSoldSuspendedVehicles.Columns.Add(new DataColumn("DateSold"));
            return ldtSoldSuspendedVehicles;
        }
        private DataTable Create_ExceedLimit_Vehicle_Structure()
        {
            DataTable ldtExceedLimitVehicles = new DataTable("ExceedLimitVehicle");
            ldtExceedLimitVehicles.Columns.Add(new DataColumn("VIN"));
            return ldtExceedLimitVehicles;
        }
        private DataTable Create_TGWIncreased_Vehicle_Structure()
        {
            DataTable ldtTGWIncreasedVehicles = new DataTable("TGWIncreasedVehicle");
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("VIN"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("IsLogging", typeof(Boolean)));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("IncreasedMonthValue"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("WeightCategoryNew"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("WeightCategoryOld"));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("TaxAmount", typeof(Decimal)));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("NewTaxAmount", typeof(Decimal)));
            ldtTGWIncreasedVehicles.Columns.Add(new DataColumn("PreviousTaxAmount", typeof(Decimal)));
            return ldtTGWIncreasedVehicles;
        }
        private decimal calculateTaxAmt_TaxableVehicle(bool IsLogging, char WeightCategory, string FirstUsedMonth)
        {
            decimal taxAmount = 0.00M;

            DataTable ldt_TaxCalculation;

            if (FirstUsedMonth != "07")
            {
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                switch (FirstUsedMonth)
                {
                    case "01": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jan"]); break;
                    case "02": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Feb"]); break;
                    case "03": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Mar"]); break;
                    case "04": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Apr"]); break;
                    case "05": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["May"]); break;
                    case "06": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jun"]); break;
                    case "08": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Aug"]); break;
                    case "09": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Sep"]); break;
                    case "10": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Oct"]); break;
                    case "11": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Nov"]); break;
                    case "12": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Dec"]); break;
                }
            }
            else
            {
                ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                int rowIndex = ldt_TaxCalculation.DefaultView.Find(WeightCategory);

                if (IsLogging)
                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["logging"]);
                else
                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["NotLogging"]);
            }

            return taxAmount;
        }
        private Int32 calculateTaxAmt_CreditVehicle(bool IsLogging, char WeightCategory, string EffectiveMonth, string reason)
        {
            EffectiveMonth = Convert.ToDateTime(EffectiveMonth).ToString("MMM");
            EffectiveMonth = EffectiveMonth.ToUpper();
            decimal taxAmount = 0.00M;

            DataTable ldt_TaxCalculation;

            if (EffectiveMonth != "JUN" && reason != "Mileage Not Exceeded")
            {
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                switch (EffectiveMonth.ToUpper())
                {
                    case "JAN": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Feb"]); break;
                    case "FEB": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Mar"]); break;
                    case "MAR": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Apr"]); break;
                    case "APR": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["May"]); break;
                    case "MAY": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jun"]); break;
                    case "JUL": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Aug"]); break;
                    case "AUG": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Sep"]); break;
                    case "SEP": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Oct"]); break;
                    case "OCT": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Nov"]); break;
                    case "NOV": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Dec"]); break;
                    case "DEC": taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["Jan"]); break;
                }
            }
            else
            {
                if (EffectiveMonth.ToUpper() == "JUL")
                {
                    ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                    ldt_TaxCalculation.DefaultView.Sort = "Category";
                    int rowIndex = ldt_TaxCalculation.DefaultView.Find(WeightCategory);

                    if (IsLogging)
                        taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["logging"]);
                    else
                        taxAmount = Convert.ToDecimal(ldt_TaxCalculation.Rows[rowIndex]["NotLogging"]);
                }
                else
                {
                    ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                    ldt_TaxCalculation.DefaultView.Sort = "Category";
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + WeightCategory + "' AND Logging='" + IsLogging + "'";

                    taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);
                }
            }
             return Convert.ToInt32(Math.Round(taxAmount));
        }
        private decimal calculateTaxAmt_TGWIncreasedVehicle(char NewWeightCategory, char OldWeightCategory, string EffectiveMonth, bool IsLogging)
        {
            decimal taxAmount = 0.00M;
            decimal taxDiff = 0;

            DataTable ldt_TaxCalculation;

            if (EffectiveMonth == "07")
            {
                ldt_TaxCalculation = mdt_AnnualTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";
                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "'";

                if (IsLogging)
                    taxDiff = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                else
                    taxDiff = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);

                ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "'";

                if (IsLogging)
                    taxAmount = taxDiff - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["logging"]);
                else
                    taxAmount = taxDiff - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0]["NotLogging"]);

            }
            else
            {
                switch (EffectiveMonth)
                {
                    case "08": EffectiveMonth = "AUG"; break;
                    case "09": EffectiveMonth = "SEP"; break;
                    case "10": EffectiveMonth = "OCT"; break;
                    case "11": EffectiveMonth = "NOV"; break;
                    case "12": EffectiveMonth = "DEC"; break;
                    case "01": EffectiveMonth = "JAN"; break;
                    case "02": EffectiveMonth = "FEB"; break;
                    case "03": EffectiveMonth = "MAR"; break;
                    case "04": EffectiveMonth = "APR"; break;
                    case "05": EffectiveMonth = "MAY"; break;
                    case "06": EffectiveMonth = "JUN"; break;
                }
                ldt_TaxCalculation = mdt_PartialTaxCalMaster.Copy();
                ldt_TaxCalculation.DefaultView.Sort = "Category";

                if (IsLogging)
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "' AND Logging='" + true + "'";
                else
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + NewWeightCategory + "' AND Logging='" + false + "'";

                taxAmount = Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

                if (IsLogging)
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "' AND Logging='" + true + "'";
                else
                    ldt_TaxCalculation.DefaultView.RowFilter = "Category='" + OldWeightCategory + "' AND Logging='" + false + "'";

                taxAmount = taxAmount - Convert.ToDecimal(ldt_TaxCalculation.DefaultView[0][EffectiveMonth]);

            }

            return Convert.ToDecimal(taxAmount.ToString("N2"));

        }

        private DataTable Create_Tax_Computation_Structure()
        {
            DataTable mdt_TaxComputaion = new DataTable("TaxComputation");
            mdt_TaxComputaion.Columns.Add(new DataColumn("Category"));
            mdt_TaxComputaion.PrimaryKey = new DataColumn[] {mdt_TaxComputaion.Columns["Category"] };
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Partial_Tax"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Non_Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Logging_Vehicle_Count"));
            mdt_TaxComputaion.Columns.Add(new DataColumn("Tax_Amount",typeof(decimal)));
            return mdt_TaxComputaion;

        }
        #endregion

        private void sendBulkSubmissionToIRS()
        {
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvSubmissionList.Rows)
                    dgvr.Cells[0].Value = true;
            }
            catch (Exception ex) 
            { 
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); 
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvSubmissionList.Rows)
                    dgvr.Cells[0].Value = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                getPendingSubmissionList();
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void bindGrid(DataTable dt)
        {
            dgvSubmissionList.AutoGenerateColumns = false;
            dgvSubmissionList.DataSource = dt;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                frmFormDetails obj_frmFormDetails =frmFormDetails.CurrentInstance;
               
                 foreach (DataGridViewRow dgvr in dgvSubmissionList.Rows)
                {
                    if (dgvr.Cells[0].Value == null)
                        continue;
                    if ((bool)dgvr.Cells[0].Value == false)
                        continue;
                    else
                    {
                        obj_frmFormDetails.FormId = dgvr.Cells["FormId"].Value.ToString();
                        obj_frmFormDetails.UserId = dgvr.Cells["UserId"].Value.ToString();
                        obj_frmFormDetails.TaxId = dgvr.Cells["TaxId"].Value.ToString();
                        break;
                    }
                }
                
                obj_frmFormDetails.ShowDialog();
            }
            catch { }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Entity.Loginstatus = "True";
                Cursor.Current = Cursors.WaitCursor;
                if (dgvSubmissionList.Rows.Count == 0)
                {
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (MessageBox.Show("Do you really want to submit this form to IRS?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for(int i=0;i<dgvSubmissionList.Rows.Count;i++)
                    {
                        DataGridViewRow dgvr = dgvSubmissionList.Rows[i];
                        try
                        {
                            if ((bool)dgvr.Cells[0].Value == false)
                                continue;
                        }
                        catch { continue; }

                        mds_FullDetails = dbOperation.GetAllSubmissionData(
                             dgvr.Cells["FormId"].Value.ToString(),
                             dgvr.Cells["UserId"].Value.ToString(),
                             dgvr.Cells["TaxId"].Value.ToString());

                        mdt_user_reg = mds_FullDetails.Tables[0];
                        mdt_profile = mds_FullDetails.Tables[1];
                        mdt_tax2290 = mds_FullDetails.Tables[2];
                        mdt_vehicle = mds_FullDetails.Tables[3];
                        mdt_irs_paymnet = mds_FullDetails.Tables[4];
                        mdt_paid_preparer = mds_FullDetails.Tables[5];
                        mdt_third_party_designee = mds_FullDetails.Tables[6];

                        if (mdt_vehicle.Rows.Count == 0)
                        {
                            MessageBox.Show("Vehicle Information is not present.");
                            return;
                        }

                        if (mdt_irs_paymnet.Rows.Count > 0)
                        { 
                            DateTime dtWithdrawalDate=Convert.ToDateTime(mdt_irs_paymnet.Rows[0]["ip_withdrawal_date"]);
                            DateTime ServerDate = dbOperation.GetServerDate();

                            if (IsWeekend(ServerDate))
                            {
                                MessageBox.Show("Error.");
                                return;
                            }
                            if (dtWithdrawalDate.Date < ServerDate.Date)
                            {
                                    mdt_irs_paymnet.Rows[0]["ip_withdrawal_date"] = ServerDate.ToString();
                                    mdt_irs_paymnet.AcceptChanges();
                            }
                        }

                        foreach (DataRow ldr in mdt_vehicle.Rows)
                            ldr["vd_VIN"] = Convert.ToString(ldr["vd_VIN"]).ToUpper();

                        clsReturnValues rv = new clsReturnValues();

                        char status = 'N';
                        switch (dgvr.Cells["Status"].Value.ToString())
                        {
                                case "AF":status='S';break;
                                case "IA":status='A';break;
                                case "IR":status='X';break;
                                case "RS":status='C';break;
                                case "SA":status='N';break;
                                case "SB":status='X';break;
                                case "SR":status='R';break;
                                case "SS":status='N';break;
                                case "US":status='N';break;
                        }

                        rv = sendSingleSubmissionToIRS(
                            dgvr.Cells["ReferenceNumber"].Value.ToString(),
                           Convert.ToString(dgvr.Cells["SubmissionID"].Value),
                            dgvr.Cells["FormId"].Value.ToString(),
                            status
                            );


                        #region PRODUCTION ONLY ----- update the submission result in database
                        
                        if (rv.submissionIndicator == "P")
                        {
                            if (rv.status.Length > 0 && rv.status != "N" && rv.status != "S")
                            {
                                string fields = "@sd_ref_no,@sd_form_id,@sd_submission_ID,@sd_submission_status,@sd_mail_status,@sd_rejection_err_codes";
                                string rejectionCodes = "";

                                if (rv.status == "R")
                                    rejectionCodes = rv.returnMsg;

                                string values = dgvr.Cells["ReferenceNumber"].Value.ToString() + "," +
                                    dgvr.Cells["FormId"].Value.ToString() + "," +
                                    rv.submissionID + "," + rv.status + "," + "N" + "," + rejectionCodes;

                                dbOperation.Update_Data("usp_Sub2290_Update_Submission", fields, values);

                                if (rv.status == "C")
                                {
                                  int output=  dbOperation.Update_Schedule1(dgvr.Cells["ReferenceNumber"].Value.ToString(), rv.schedule1);

                                  if (Convert.ToString(mdt_profile.Rows[0]["pd_mobile4sms"]) != "0")
                                  {
                                      try
                                      {
                                          if(Convert.ToString(mdt_profile.Rows[0]["pd_mobile4sms"]).Length>0)
                                          SMSService.SendSingleSMS("eForm2290", Convert.ToString(mdt_profile.Rows[0]["pd_mobile4sms"]), "eForm2290.com", "Congratulations!! IRS has accepted your 2290 Return. Visit eForm2290.com to download Schedule 1", true);
                                      }
                                      catch { }
                                  }
                                }
                               
                                if (rv.status != Convert.ToString(dgvr.Cells["Status"].Value))
                                {
                                    dgvSubmissionList.Rows.Remove(dgvr);
                                    i--;
                                }
                            }
                            if (rv.returnMsg.Contains("Login Failed"))
                                MessageBox.Show("IRS Login Failed! Please Try Again Later.");
                            else if(rv.status == "S")
                                MessageBox.Show("Acknowledgement Failed! Please Try Again Later.");
                        }

                        #endregion
                    }
                }
                Cursor.Current = Cursors.Arrow;
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void getPendingSubmissionList()
        {
            try
            {
                if (FORM_CATEGORY == "R")
                    dgvSubmissionList.Columns["Reason"].Visible = true;
                else
                    dgvSubmissionList.Columns["Reason"].Visible = false;

                DataSet ldsResultSet = dbOperation.GetPendingSubmissionList(FORM_CATEGORY);
                DataTable ldt = ldsResultSet.Tables[0];
                if (ldt.Rows.Count > 0)
                {
                    bindGrid(ldt);
                    lblToalPending.Text = ldt.Rows.Count.ToString();
                }
                else
                {
                    dgvSubmissionList.DataSource = null;
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblToalPending.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmSubmissionsList_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                IRSLibrary.BaseMethods BM = new BaseMethods();
                BM._Logout();
            }
            catch { }
        }

        private void btnAllowResubmission_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (dgvSubmissionList.Rows.Count == 0)
                {
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                if (MessageBox.Show("Do you really want to Allow This Customer To Resubmit?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < dgvSubmissionList.Rows.Count; i++)
                    {
                        DataGridViewRow dgvr = dgvSubmissionList.Rows[i];
                        try
                        {
                            if ((bool)dgvr.Cells[0].Value == false)
                                continue;
                        }
                        catch { continue; }

                        if (dgvr.Cells["Status"].Value.ToString() == "SR")
                        {
                            DataTable dt = dbOperation.Select_Data("usp_Sub2290_Get_Error_RulesNumbers", "@FormId", dgvr.Cells["FormID"].Value.ToString()).Tables[0];

                            bool F2290026 = false;

                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["RuleDescription"].ToString() == "This return is already accepted by IRS.")
                                {
                                    F2290026 = true;
                                }
                            }

                            if (!F2290026)
                            {
                                dbOperation.Select_Data("usp_Sub2290_AllowUserResubmission", "@UserId,@FormId", dgvr.Cells["UserID"].Value.ToString() + ',' + dgvr.Cells["FormID"].Value.ToString());
                            }
                            else
                            {
                                dbOperation.Select_Data("usp_Sub2290_Update_RejectFlag2IR", "@FormId", dgvr.Cells["FormID"].Value.ToString());

                                if (Convert.ToString(mdt_profile.Rows[0]["pd_mobile4sms"]) != "0")
                                {
                                    SMSService.SendSingleSMS("eForm2290", Convert.ToString(mdt_profile.Rows[0]["pd_mobile4sms"]), "2290", "IRS has rejected your 2290 Return. Please visit eForm2290.com for details.",true);
                                }
                            }

                            string profilename = dgvr.Cells["BusinessName"].Value.ToString();
                            string firstname = Convert.ToString(dgvr.Cells["FirstName"].Value);
                            string Email = Convert.ToString(dgvr.Cells["Email"].Value);
                            bool mailSent = SendMail_Rejection(profilename, firstname, Email, dt, F2290026);
                            
                            if (mailSent)
                            {
                                bool statusUpdated = dbOperation.Update_MailSentStatusAfterSubmission(Convert.ToString(dgvr.Cells["ReferenceNumber"].Value));

                                if (statusUpdated)
                                {
                                    dgvSubmissionList.Rows.Remove(dgvr);
                                    i--;
                                }
                            }
                        }
                    }
                }
                Cursor.Current = Cursors.Arrow;
            }
            catch { }
        }
        private bool SendMail_Rejection(string ProfileName, string Name, string Email, DataTable dtRejectionReasons, bool F2290026)
        {
            try
            {
                string subject="",body="";
                System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
                Name = textInfo.ToTitleCase(Name.ToLower());            

                if (F2290026)
                {
                    subject = ProfileName+" Submission Details";
                    body = "Dear " + Name + ":<br/><br/>";
                    body += "Your 2290 e-file for '" + ProfileName + "' has been rejected by IRS due the following reasons" + "<br/>";
                    body += "<ul>";
                    body += "<li>" + "This return is already accepted by IRS." + "</li>";
                    body += "</ul>";
                    body += "Please contact IRS on the following numbers to know more." + "<br/><br/>";
                    body += "United States:	866-699-4096 (toll free)<br/>Canada or Mexico:	859-669-5733 (not toll free)" + "<br/><br/>";
                    body += "If you have questions or need further assistance, ";
                    body += "please reply to this email." + "<br/><br/>";
                    body += "Thank you for your business." + "<br/><br/>";
                    body += "--<br/>With Regards,<br/>";
                    body += "eForm2290 Support Team<br/>";
                    body += "www.eForm2290.com<br/>";
                    body += "";
                }
                else
                {
                    subject = "Please resubmit your return for " + ProfileName ;

                    body = "Dear " + Name + ":<br/><br/>";
                    body += "Your 2290 e-file for '" + ProfileName + "' has been rejected by IRS due to following reasons" + "<br/>";

                    body += "<ul>";

                    foreach (DataRow dr in dtRejectionReasons.Rows)
                    {
                        body += "<li>" + dr["RuleDescription"].ToString() + "</li>";
                    }
                    body += "</ul>";

                    body += "Please resubmit your return with correct values. ";
                    body += "We will not collect payment for your next e-filing." + "<br/><br/>";
                    body += "If you have questions or need further assistance, ";
                    body += "please email to our support team." + "<br/><br/>";
                    body += "Thank you for your business." + "<br/><br/>";
                    body += "--<br/>With Regards,<br/>";
                    body += "eForm2290 Support Team<br/>";
                    body += "www.eForm2290.com<br/>";
                    body += "";
                            
                }
              
                 return dbOperation.SendSubmissionMailRejection(Email, "", "support@eform2290.com", subject, body);
            }
            catch
            {
                return false;
            }
        }

        private void dgvSubmissionList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 10)
                {
                    frmErrorList errorlist = new frmErrorList(dgvSubmissionList["FormID", e.RowIndex].Value.ToString());
                    errorlist.ShowDialog();
                }
            }
            catch
            {

            }

        }

        public static bool IsWeekend(DateTime dt)
        {
            if ((dt.DayOfWeek == DayOfWeek.Sunday) || (dt.DayOfWeek == DayOfWeek.Saturday))
                return true;
            else
                return false;
        }
    }
}
