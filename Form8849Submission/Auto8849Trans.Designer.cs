﻿namespace Form8849Sub
{
    partial class Auto8849Trans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmission = new System.Windows.Forms.Button();
            this.nud_submission = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.lblStatus_submission = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_submission)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSubmission
            // 
            this.btnSubmission.BackColor = System.Drawing.Color.Red;
            this.btnSubmission.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmission.ForeColor = System.Drawing.Color.White;
            this.btnSubmission.Location = new System.Drawing.Point(346, 40);
            this.btnSubmission.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSubmission.Name = "btnSubmission";
            this.btnSubmission.Size = new System.Drawing.Size(77, 30);
            this.btnSubmission.TabIndex = 25;
            this.btnSubmission.Text = "Start";
            this.btnSubmission.UseVisualStyleBackColor = false;
            this.btnSubmission.Click += new System.EventHandler(this.btnSubmission_Click);
            // 
            // nud_submission
            // 
            this.nud_submission.Location = new System.Drawing.Point(133, 43);
            this.nud_submission.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nud_submission.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.nud_submission.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nud_submission.Name = "nud_submission";
            this.nud_submission.Size = new System.Drawing.Size(66, 22);
            this.nud_submission.TabIndex = 27;
            this.nud_submission.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(204, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "seconds";
            // 
            // lblStatus_submission
            // 
            this.lblStatus_submission.AutoSize = true;
            this.lblStatus_submission.ForeColor = System.Drawing.Color.Red;
            this.lblStatus_submission.Location = new System.Drawing.Point(280, 46);
            this.lblStatus_submission.Name = "lblStatus_submission";
            this.lblStatus_submission.Size = new System.Drawing.Size(58, 17);
            this.lblStatus_submission.TabIndex = 14;
            this.lblStatus_submission.Text = "stopped";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Submission";
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(0, 71);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(63, 23);
            this.btnLogout.TabIndex = 29;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(0, 0);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(63, 23);
            this.btnLogin.TabIndex = 30;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // Auto8849Trans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(427, 94);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnSubmission);
            this.Controls.Add(this.nud_submission);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblStatus_submission);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Auto8849Trans";
            this.Opacity = 0.98D;
            this.Text = "AutoTrans 8849 Module";
            this.Load += new System.EventHandler(this.AutoTrans_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nud_submission)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSubmission;
        private System.Windows.Forms.NumericUpDown nud_submission;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblStatus_submission;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnLogin;

    }
}