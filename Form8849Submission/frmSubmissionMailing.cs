using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Form8849Sub
{
    public partial class frmSubmissionMailing : Form
    {
        eForm8849Operations dbOperation = new eForm8849Operations();


        public frmSubmissionMailing()
        {
            InitializeComponent();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                getPendingMailingList();
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void getPendingMailingList()
        {
            try
            {
                DataSet ldsResultSet = dbOperation.GetPendingMailingList();
                DataTable ldt = ldsResultSet.Tables[0];
                if (ldt.Rows.Count > 0)
                {
                    bindGrid(ldt);
                    lblToalPending.Text = ldt.Rows.Count.ToString();
                }
                else
                {
                    dgvMailingList.DataSource = null;
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblToalPending.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void bindGrid(DataTable dt)
        {
            dgvMailingList.AutoGenerateColumns = false;
            dgvMailingList.DataSource = dt;
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvMailingList.Rows)
                    dgvr.Cells[0].Value = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvMailingList.Rows)
                    dgvr.Cells[0].Value = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                frmFormDetails obj_frmFormDetails = frmFormDetails.CurrentInstance;

                foreach (DataGridViewRow dgvr in dgvMailingList.Rows)
                {
                    if (dgvr.Cells[0].Value == null)
                        continue;
                    if ((bool)dgvr.Cells[0].Value == false)
                        continue;
                    else
                    {
                        obj_frmFormDetails.FormId = dgvr.Cells["FormId"].Value.ToString();
                        obj_frmFormDetails.UserId = dgvr.Cells["UserId"].Value.ToString();
                        obj_frmFormDetails.TaxId = dgvr.Cells["TaxId"].Value.ToString();
                        break;
                    }
                }

                obj_frmFormDetails.Parent = this.Parent;
                obj_frmFormDetails.ShowDialog();
            }
            catch { }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvMailingList.Rows.Count == 0)
                {
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (MessageBox.Show("Do you really want to send mail?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < dgvMailingList.Rows.Count; i++)
                    {
                        DataGridViewRow dgvr = dgvMailingList.Rows[i];
                        try
                        {
                            if ((bool)dgvr.Cells[0].Value == false)
                                continue;
                        }
                        catch { continue; }

                        if (dgvr.Cells["Status"].Value.ToString() == "RS")
                        {
                            string support = "support@eform2290.com";//System.Configuration.ConfigurationSettings.AppSettings["Support"];
                            string error = "";
                            bool mailSent = dbOperation.SendSubmissionMailAcceptance
                                (Convert.ToString(dgvr.Cells["From"].Value).Substring(Convert.ToString(dgvr.Cells["From"].Value).Length - 4, 4),
                                  Convert.ToString(dgvr.Cells["FirstName"].Value), Convert.ToString(dgvr.Cells["Email"].Value),
                                  Convert.ToString(dgvr.Cells["ReferenceNumber"].Value), support,out error);
                            if (error.Length > 0)
                                MessageBox.Show(error);
                              
                          if (mailSent)
                          {
                              string error1 = "";
                              bool statusUpdated = dbOperation.Update_MailSentStatusAfterSubmission(Convert.ToString(dgvr.Cells["ReferenceNumber"].Value),out error1);

                              if (error1.Length > 0)
                                  MessageBox.Show(error1);

                              if (statusUpdated)
                              {
                                  dgvMailingList.Rows.Remove(dgvr);
                                  i--;
                              }
                          
                          }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

       
    }
}
