using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using eFormFaxSender;

namespace Form8849Sub
{
    public partial class frmSubmissionFAX : Form
    {
        eForm8849Operations dbOperation = new eForm8849Operations();
        static DataTable ldtSch1 = new DataTable();
        public frmSubmissionFAX()
        {
            InitializeComponent();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                getPendingFAXList();
                Cursor.Current = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void getPendingFAXList()
        {
            try
            {
                DataSet ldsResultSet = dbOperation.GetPendingFAXList();
                DataTable ldt = ldsResultSet.Tables[0];

               

                if (ldt.Rows.Count > 0)
                {
                    bindGrid(ldt);
                    lblToalPending.Text = ldt.Rows.Count.ToString();
                }
                else
                {
                    dgvFAXList.DataSource = null;
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblToalPending.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void bindGrid(DataTable dt)
        {
            dgvFAXList.AutoGenerateColumns = false;
            dgvFAXList.DataSource = dt;
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvFAXList.Rows)
                    dgvr.Cells[0].Value = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvFAXList.Rows)
                    dgvr.Cells[0].Value = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                frmFormDetails obj_frmFormDetails = frmFormDetails.CurrentInstance;

                foreach (DataGridViewRow dgvr in dgvFAXList.Rows)
                {
                    if (dgvr.Cells[0].Value == null)
                        continue;
                    if ((bool)dgvr.Cells[0].Value == false)
                        continue;
                    else
                    {
                        obj_frmFormDetails.FormId = dgvr.Cells["FormId"].Value.ToString();
                        obj_frmFormDetails.UserId = dgvr.Cells["UserId"].Value.ToString();
                        obj_frmFormDetails.TaxId = dgvr.Cells["TaxId"].Value.ToString();
                        break;
                    }
                }

                obj_frmFormDetails.Parent = this.Parent;
                obj_frmFormDetails.ShowDialog();
            }
            catch { }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvFAXList.Rows.Count == 0)
                {
                    MessageBox.Show("No records found", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (MessageBox.Show("Do you want to send fax?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < dgvFAXList.Rows.Count; i++)
                    {
                        DataGridViewRow dgvr = dgvFAXList.Rows[i];
                        try
                        {
                            if ((bool)dgvr.Cells[0].Value == false)
                                continue;
                        }
                        catch { continue; }

                        if (dgvr.Cells["Status"].Value.ToString() == "RS")
                        {

                            byte[] Schedule1 = null;
                            DataTable dt = new DataTable();
                            DBOperations.DBStorage db = new DBOperations.DBStorage();
                            dt = db.SelectQuery(
                                "select S.irs_submission_id, F.stamped_sch_1 FROM C_Submissions S LEFT " +
                                "JOIN [2290_Form_Binary] F ON S.CFK_FD_form_key=F.FK_2290FD_form_key WHERE " +
                                "S.reference_no='" + dgvr.Cells["ReferenceNumber"].Value + "'").Tables[0];

                            if (dt.Rows.Count > 0)
                            {
                                Schedule1 = ((byte[])dt.Rows[0][1]);

                                string tran_id =
                                 FAXService.SendFaxDocument(Convert.ToString(dgvr.Cells["Fax"].Value), Schedule1, "pdf");

                                if (tran_id.Length > 0)
                                {
                                    bool statusUpdated = dbOperation.Update_FaxSentStatusAfterSubmission
                                        (Convert.ToString(dgvr.Cells["ReferenceNumber"].Value), tran_id);
                                    if (statusUpdated)
                                    {
                                        dgvFAXList.Rows.Remove(dgvr);
                                        i--;
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
            catch { }
        }

        public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                MessageBox.Show("Exception caught in process: {0}", _Exception.ToString());
            }

            return false;
        }
    }
}
