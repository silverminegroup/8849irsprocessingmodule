﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using InterfaxOut;
using System.Security.Authentication;
using System.Net;

namespace eFormFaxSender
{
    public class FAXService
    {
        public static string mUID = Settings.Default.faxUID;
        public static string mPwd = Settings.Default.faxPWD;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFaxNo"></param>
        /// <param name="pFilePath"></param>
        /// <param name="pFileType">hardcoded to pdf</param>
        /// <param name="pPageHeader"></param>
        /// <param name="pSubject"></param>
        /// <param name="pPageSize">hardcoded to A4</param>
        /// <param name="pPageOrientation">hardcoded to Portrait</param>
        /// <param name="pIsHighResolution"></param>
        /// <param name="pIsHighRendering"></param>
        /// <returns></returns>
        public static string SendFaxDocument(string pFaxNo, string pFilePath, string pFileType, string pPageHeader, string pSubject, 
            string pPageSize, string pPageOrientation, bool pIsHighResolution, bool pIsHighRendering)
        {
            string returnVal = "";
            try
            {

                const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
                const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
                ServicePointManager.SecurityProtocol = Tls12;

                pFileType = "pdf";
                pPageSize = "A4";
                pPageOrientation = "Portrait";

                FileStream fs = new FileStream(pFilePath, FileMode.Open, FileAccess.Read);

                //FaxWS.InterFaxSoapClient ws = new FaxWS.InterFaxSoapClient();
                InterFax ws = new InterFax();
                
                string sessionID = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ffff");
                ws.StartFileUpload(mUID, mPwd, ref sessionID);
               
                // loop until all file data is read
                bool isLast = false;
                int readSoFar = 0;
                while (!isLast)
                {
                    byte[] B = new byte[fs.Length];	//this is max buffer size
                    int read = fs.Read(B, 0, B.Length);
                    readSoFar += read;
                    isLast = (readSoFar == fs.Length);
                    if (read < B.Length)	// a partial buffer was read => trim B
                    {
                        byte[] B1 = new byte[read];	//this is max buffer size
                        for (int i = 0; i < read; i++) B1[i] = B[i];
                        ws.UploadFileChunk(sessionID, B1, isLast);
                    }
                    else
                        ws.UploadFileChunk(sessionID, B, isLast);
                }

                long Result = ws.SendfaxEx_2(mUID, mPwd, pFaxNo, "", null, pFileType, 
                    readSoFar.ToString() + "/sessionID=" + sessionID, DateTime.MinValue, 1, 
                    "CSID", pPageHeader, "", pSubject, "", pPageSize, pPageOrientation, 
                    pIsHighResolution, pIsHighRendering);
                
                returnVal = Result.ToString();
            }
            catch (Exception)
            {
            }

            return returnVal;
        }

        public static string SendFaxDocument(string pFaxNo, byte[] pSchedule1, string pFileType)
        {
           // return "";

            string returnVal = "";
            try
            {
                pFileType = "pdf";
             
                InterFax ws = new InterFax();
                
                long Result = ws.Sendfax(mUID, mPwd, pFaxNo, pSchedule1, pFileType);

                returnVal = Result.ToString();
            }
            catch (Exception)
            {
            }

            return returnVal;
        }
    }
}
