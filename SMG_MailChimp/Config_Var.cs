﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMG_MailChimp
{
    public class Config_Var
    {
        public string G_MODE = string.Empty;

        public string G_SERVER = string.Empty;

        public int G_SERVER_PORT = 0;

        public string G_SENDER = string.Empty;

        public string G_SENDER_DISPLAY_NAME = string.Empty;

        public string G_SENDER_KEY = string.Empty;

        public string G_SUBJECT = string.Empty;

        public string G_USERNAME = string.Empty;

        public string G_SENDER_REF = string.Empty;
        public string G_SENDER_REFNAME = string.Empty;

        public Config_Var(bool isTestMode)
        {
            if (isTestMode)
            {
                G_SERVER = SMG_Chimp.Default.SMTP;
                G_SERVER_PORT = SMG_Chimp.Default.Port;
                G_SENDER = SMG_Chimp.Default.Sender;
                G_SENDER_DISPLAY_NAME = SMG_Chimp.Default.DisplayName;
                G_SENDER_KEY = SMG_Chimp.Default.Chimp_Mandrill_Key;
                G_USERNAME = SMG_Chimp.Default.Chimp_User;

                G_SENDER_REF = SMG_Chimp.Default.Sender_Ref;
                G_SENDER_REFNAME = SMG_Chimp.Default.Sender_RefName;
                
            }
            else
            {
                G_SERVER = SMG_Chimp.Default.SMTP;
                G_SERVER_PORT = SMG_Chimp.Default.Port;
                G_SENDER = SMG_Chimp.Default.Sender;
                G_SENDER_DISPLAY_NAME = SMG_Chimp.Default.DisplayName;
                G_SENDER_KEY = SMG_Chimp.Default.Chimp_Mandrill_Key;
                G_USERNAME = SMG_Chimp.Default.Chimp_User;

                G_SENDER_REF = SMG_Chimp.Default.Sender_Ref;
                G_SENDER_REFNAME = SMG_Chimp.Default.Sender_RefName;
            }
        }

    }
}
