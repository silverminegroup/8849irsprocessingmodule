﻿using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using API;
using System.Diagnostics;

namespace SMG_MailChimp
{
    public class SMG_Mailer
    {

        public static bool SendEmail(string pTo, string pCC, string bCC, string pSubject, string pBody, string pAttachment)
        {
            try {
                if (pTo.Trim() != "" && !pTo.Contains("@"))
                {
                    //Log.Write("eForm2290 Email Sending: Invalid Email address Found" , EventLogEntryType.Error);
                    return false;
                }
                bool pTestMode = SMG_Chimp.Default.ProdMode;

                Config_Var objConfigVars = new Config_Var((bool)pTestMode);
                MailAddress mailSndr = new MailAddress(objConfigVars.G_SENDER, objConfigVars.G_SENDER_DISPLAY_NAME);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage();
                myMail.From = mailSndr;
                myMail.To.Add((string)pTo);

                myMail.Subject = pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = pBody;

                if (pCC != "") { myMail.CC.Add(pCC); }
                if (bCC != "") { myMail.Bcc.Add(bCC); }

                if (pAttachment != "")
                    myMail.Attachments.Add(new System.Net.Mail.Attachment(pAttachment));

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);
                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                /*
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(senderServer, senderPort);
                client.Credentials = new System.Net.NetworkCredential(senderEmail, senderPwd);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                */

                client.Send(myMail);

                myMail.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                Log.Write("eForm2290 Email Sending: " + ex.Message, EventLogEntryType.Error);
                return false;
            }
        }

        public static bool SendEmailWithSchedule1(string pTo, string pCC, string bCC, string pSubject, string pBody, byte[] pSchedule1, string pSubmissionID)
        {
            try
            {
                if (pTo.Trim() != "" && !pTo.Contains("@"))
                {
                    //Log.Write("eForm2290 Email Sending: Invalid Email address Found", EventLogEntryType.Error);
                    return false;
                }
               
                bool pTestMode = SMG_Chimp.Default.ProdMode;
                Config_Var objConfigVars = new Config_Var((bool)pTestMode);
                MailAddress mailSndr = new MailAddress(objConfigVars.G_SENDER, objConfigVars.G_SENDER_DISPLAY_NAME);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage();
                myMail.From = mailSndr;
                myMail.To.Add((string)pTo);

                myMail.Subject = pSubject;
                myMail.BodyEncoding = System.Text.Encoding.ASCII;
                myMail.IsBodyHtml = true;
                myMail.Body = pBody;

                if (pCC != "") { myMail.CC.Add(pCC); }
                if (bCC != "") { myMail.Bcc.Add(bCC); }



                string fileName = @"C:\Temp\" + pSubmissionID + ".pdf";


                using (System.IO.BinaryWriter binWriter =
                    new System.IO.BinaryWriter(System.IO.File.Open(fileName, System.IO.FileMode.Create)))
                {
                    binWriter.Write(pSchedule1);
                }

                System.Net.Mail.Attachment attachement = new System.Net.Mail.Attachment(fileName);

                myMail.Attachments.Add(attachement);

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);
                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                /*
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(senderServer, senderPort);
                client.Credentials = new System.Net.NetworkCredential(senderEmail, senderPwd);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                */

                client.Send(myMail);
                myMail.Dispose();
                System.IO.File.Delete(@"C:\Temp\" + pSubmissionID + ".pdf");
                return true;
            }
            catch (Exception ex)
            {
                Log.Write("eForm2290 Email Sending: " + ex.Message + " EMailto=" + pTo + "  EMIAL CC=" + pCC + "  EMAIL BCC=" + bCC, EventLogEntryType.Error);
                return false;
            }
        }

        #region COMMNETBELOW OLD NOT USED

        [SqlProcedure]
        public static void SendEmail(SqlString pTo, SqlString pCC, SqlString bCC, SqlString pSubject,
            SqlString pBody, SqlString pAttachment, SqlBoolean pTestMode, out SqlBoolean isEmailSent)
        {
            /*
            //pTo = "vishwanathpm@silverminegroup.com";// "eformtest@datacloudsystems.com";
            isEmailSent = false;
            try
            {
                Config_Var objConfigVars = new Config_Var((bool)pTestMode);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(objConfigVars.G_SENDER, pTo.ToString());
                myMail.Subject = (string)pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = (string)pBody;

                if (pCC != "") { myMail.CC.Add((string)pCC); }
                if (bCC != "") { myMail.Bcc.Add((string)bCC); }

                if (pAttachment != "")
                    myMail.Attachments.Add(new System.Net.Mail.Attachment((string)pAttachment));

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);

                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);

                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.Send(myMail);

                myMail.Dispose();

                isEmailSent = true;
            }
            catch (Exception ex)
            {

            } 
            */
            // over writing below 
            try
            {
                Config_Var objConfigVars = new Config_Var((bool)pTestMode);
                MailAddress mailSndr = new MailAddress(objConfigVars.G_SENDER, objConfigVars.G_SENDER_DISPLAY_NAME);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage();
                myMail.From = mailSndr;
                myMail.To.Add((string)pTo);

                myMail.Subject = (string)pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = (string)pBody;
                if (pCC != "") { myMail.CC.Add((string)pCC); }
                if (bCC != "") { myMail.Bcc.Add((string)bCC); }
                if (pAttachment != "")
                    myMail.Attachments.Add(new System.Net.Mail.Attachment((string)pAttachment));
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);
                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.Send(myMail);
                myMail.Dispose();
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
        }

        public static void SendEmail_M(string pTo, string pCC, string bCC, string pSubject,
            string pBody, string pAttachment, bool pTestMode, out bool isEmailSent)
        {
            try
            {
                Config_Var objConfigVars = new Config_Var((bool)pTestMode);
                MailAddress mailSndr = new MailAddress(objConfigVars.G_SENDER, objConfigVars.G_SENDER_DISPLAY_NAME);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage();
                myMail.From = mailSndr;
                myMail.To.Add(pTo);

                myMail.Subject = (string)pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = (string)pBody;
                if (pCC != "") { myMail.CC.Add((string)pCC); }
                if (bCC != "") { myMail.Bcc.Add((string)bCC); }
                if (pAttachment != "")
                    myMail.Attachments.Add(new System.Net.Mail.Attachment((string)pAttachment));
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);
                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.Send(myMail);
                myMail.Dispose();
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
        }
        public static void SendEmail_Ref(string pTo, string pCC, string bCC, string pSubject,
            string pBody, string pAttachment, bool pTestMode, out bool isEmailSent)
        {
            try
            {

                Config_Var objConfigVars = new Config_Var((bool)pTestMode);
                MailAddress mailSndr = new MailAddress(objConfigVars.G_SENDER_REF, objConfigVars.G_SENDER_REFNAME);

                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage();
                myMail.From = mailSndr;
                myMail.To.Add(pTo);
                myMail.Subject = (string)pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = (string)pBody;
                if (pCC != "") { myMail.CC.Add((string)pCC); }
                if (bCC != "") { myMail.Bcc.Add((string)bCC); }
                if (pAttachment != "")
                    myMail.Attachments.Add(new System.Net.Mail.Attachment((string)pAttachment));
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(objConfigVars.G_SERVER, objConfigVars.G_SERVER_PORT);
                client.Credentials = new System.Net.NetworkCredential(objConfigVars.G_SENDER, objConfigVars.G_SENDER_KEY);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.Send(myMail);
                myMail.Dispose();
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
        }
        #endregion 

    }
}
