﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace SilverMine.IRSLibrary
{
    public interface IMEFWrapper
    {
        string GetAck(string submissionID, string outputFilePath);
        string GetNewAcks(string[] submissionIdList, string outputFilePath);
        string GetSchedule1(string submissionID, string outputFilePath);
        string GetSubmissionStatus(string submissionID, string etin, string appSysId);
        string Login();
        string Logout();
        string SendSubmissions(string bulkFileName, string[] submissionIds, string outputFilePath);
    }
}
