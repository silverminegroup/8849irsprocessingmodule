﻿using System;
using System.Collections.Generic;
using MeF.Client;
using MeF.Client.Services.MSIServices;
using MeF.Client.Services.ETECTransmitterServices;
using MeF.Client.Services.TransmitterServices;
using MeF.Client.Services.InputComposition;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace SilverMine.IRSLibrary
{
    public class MEFWrapper : IMEFWrapper
    {
        private static ServiceContext _context = null;
        private string _etin = string.Empty;
        private string _appSysId = string.Empty;
        private string _testCd = string.Empty;

        public MEFWrapper(string etin, string appSysId, string testCd)
        {
            _etin = etin;
            _appSysId = appSysId;
            _testCd = testCd;

        }

        public ServiceContext Context
        {
            get
            {
                if (_context == null)
                {
                    if (_testCd == "T")
                    {
                        _context = new ServiceContext(new ClientInfo(_etin, _appSysId, WCFClient.TestCdType.T));
                    }
                    else
                    {
                        _context = new ServiceContext(new ClientInfo(_etin, _appSysId, WCFClient.TestCdType.P));
                    }
                }
                return _context;
            }
        }

        public bool LoggedInStatus { get; set; }

        /// <summary>
        /// Returns the acknowledgement for a specific submissionID
        /// </summary>
        /// <param name="submissionID"></param>
        /// <returns></returns>
        public string GetAck(string submissionID, string outputFilePath)
        {
            GetAckClient client = null;
            GetAckResult result = null;

            try
            {
                client = new GetAckClient(outputFilePath);
                result = client.Invoke(Context, submissionID);

                return result.AttachmentFilePath;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns the acknowledgement for a list of submissionIDs
        /// </summary>
        /// <param name="submissionIdList"></param>
        /// <returns></returns>
        public string GetNewAcks(string[] submissionIdList, string outputFilePath)
        {
            GetAcksClient client = null;
            GetAcksResult result = null;

            try
            {
                client = new GetAcksClient(outputFilePath);
                result = client.Invoke(Context, submissionIdList);

                return result.AttachmentFilePath;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns the schedule for the specified submissionID
        /// </summary>
        /// <param name="submissionID"></param>
        /// <returns></returns>
        public string GetSchedule1(string submissionID, string outputFilePath)
        {
            Get2290Schedule1Client client = null;
            Get2290Schedule1Result result = null;

            try
            {
                client = new Get2290Schedule1Client(outputFilePath);
                result = client.Invoke(Context, submissionID);

                return result.AttachmentFilePath;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetSubmissionStatus(string submissionID, string etin, string appSysId)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Creates a session with MEF
        /// </summary>
        /// <returns></returns>
        public string Login()
        {
            LoginClient client = null;
            LoginResult result = null;

            try
            {
                client = new LoginClient();
                result = client.Invoke(Context);
                LoggedInStatus = true;
            }
            catch (Exception ex)
            {
                //result.StatusTxt = ex.Message;
                LoggedInStatus = false;
                throw;
            }

            return result.StatusTxt;
        }

        /// <summary>
        /// Logs out of MEF
        /// </summary>
        /// <returns></returns>
        public string Logout()
        {
            LogoutClient client = null;
            LogoutResult result = null;

            try
            {
                client = new LogoutClient();
                result = client.Invoke(Context);
                LoggedInStatus = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result.StatusTxt;
        }

        #region Send Submissions Function
        /// <summary>
        /// Send Submissions for the specified bulk file for the submission Ids containing it
        /// </summary>
        /// <param name="bulkFileName"></param>
        /// <param name="submissionIds"></param>
        /// <param name="outputFilePath"></param>
        /// <returns></returns>
        public string SendSubmissions(string bulkFileName, string[] submissionIds, string outputFilePath)
        {
            try
            {
                SubmissionBuilder builder = new SubmissionBuilder();
                List<PostmarkedSubmissionArchive> postMarkSubmissionList = new List<PostmarkedSubmissionArchive>();

                //Extract the zip folder
                string extractZipFolder = ExtractZip(bulkFileName);
                string[] files = Directory.GetFiles(extractZipFolder, "*.zip");

                if (files.Length > 0)
                {

                    if (files.Length != submissionIds.Length)
                    {
                        throw new Exception("The number of files in zip file doesn't match the number of submissionIds supplied");
                    }

                    foreach (string s in submissionIds)
                    {
                        SubmissionArchive arch = builder.CreateIRSSubmissionArchive(extractZipFolder + "\\" + s + ".zip");
                        arch.submissionId = s;
                        PostmarkedSubmissionArchive postMarkSubmission = new PostmarkedSubmissionArchive(arch, DateTime.Now);
                        postMarkSubmissionList.Add(postMarkSubmission);
                    }

                    SubmissionContainer container = new SubmissionContainer(postMarkSubmissionList);

                    SendSubmissionsClient SendSubclient = new SendSubmissionsClient(outputFilePath);
                    SendSubmissionsResult Sendresult = SendSubclient.Invoke(Context, container);

                    //remove the extracted folder and the files
                    try
                    {
                        foreach (string file in files)
                        {
                            File.Delete(file);
                        }
                        Directory.Delete(extractZipFolder);
                    }
                    catch (Exception)
                    {
                        //Do nothing as the files may not get deleted due to access voilation or any other error
                    }

                    return Sendresult.AttachmentFilePath;

                }
                else
                    throw new Exception("No submissions within the specified zip file");

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Functions

        private string ExtractZip(string fileToBeExtracted)
        {
            FastZip zip = new FastZip();
            string extractZipFolder = Path.GetDirectoryName(fileToBeExtracted) + Path.GetFileNameWithoutExtension(fileToBeExtracted);
            zip.ExtractZip(fileToBeExtracted, extractZipFolder, string.Empty);

            return extractZipFolder;
        }

        #endregion
    }
}
