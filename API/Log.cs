using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace API
{
    public class Log
    {
        private static EventLog log = null;
        public static void Write(string Message, EventLogEntryType Type)
        {
            try
            {
                if (log == null)
                {
                    System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
                    string source = asm.GetName().Name;
                    log = new EventLog("Application", ".", source);
                }
                if (Message == null)
                    log.WriteEntry("No Message", Type);
                else if (Message.Length > 32766)
                    log.WriteEntry(Message.Substring(0, 32766), Type);
                else
                    log.WriteEntry(Message, Type);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
