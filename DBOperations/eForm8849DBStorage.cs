using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using API;

namespace DBOperations
{
    public class DBStorage
    {
        public SqlConnection con = new SqlConnection(DBSettings.Default.dbConStr);
        public DataTable odt;

        private static MemoryStream oStream;
        private static DataTable oMemberCompanys;
        private static string oCommonPaymentAmount;
        private static MemoryStream oPdfWithoutWatermark;

        #region Methods
        public static DataTable OMemberCompanys
        {
            get { return DBStorage.oMemberCompanys; }
            set { DBStorage.oMemberCompanys = value; }
        }
        public static MemoryStream OStream
        {
            get { return DBStorage.oStream; }
            set { DBStorage.oStream = value; }
        }
        public static MemoryStream OPdfWithoutWatermark
        {
            get { return DBStorage.oPdfWithoutWatermark; }
            set { DBStorage.oPdfWithoutWatermark = value; }
        }
        public static string OCommonPaymentAmount
        {
            get { return DBStorage.oCommonPaymentAmount; }
            set { DBStorage.oCommonPaymentAmount = value; }
        }
        #endregion


        public DBStorage()
        {
        }

        public DataSet SelectQuery(string query)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                if (con.State != ConnectionState.Open)
                    con.Open();

                da.Fill(ds);
            }
            catch { }
            finally { con.Close(); }
            return ds;

        }

        //Return Datatable
        public DataTable getDatatable(string query, string TableName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dt.TableName = TableName;
            return dt;
        }

        // Update PDF to Table
        public int UpdatePdf(string query, byte[] content)
        {
            int i;

            try
            {
                con.Open();
                SqlCommand command = new SqlCommand(query, con);
                SqlParameter parameter = new SqlParameter();
                command.CommandText = query;
                command.CommandType = System.Data.CommandType.Text;

                parameter = new System.Data.SqlClient.SqlParameter("@Data", System.Data.SqlDbType.VarBinary);
                parameter.Value = content;
                command.Parameters.Add(parameter);

                i = command.ExecuteNonQuery();
                con.Close();
                return i;

            }
            catch
            {
                return 0;
            }

        }

        public DataSet SelectProcedureData(string ProcedureName, string paramName, string paramValue)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter(command);

                command.Connection = con;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;

                if (paramName != "")
                {
                    String[] paramname = paramName.Split(',');
                    String[] paramvalue = paramValue.Split(',');

                    if (paramname.Length != paramvalue.Length)
                    {
                        //throw "Invalid Parameter";
                    }
                    for (int i = 0; i < paramname.Length; i++)
                    {
                        command.Parameters.Add(new SqlParameter(paramname[i], SqlDbType.VarChar)).Value = paramvalue[i];
                    }
                    //command.Parameters.Add(new SqlParameter("@Table", SqlDbType.VarChar)).Value = TableName;
                    //command.Parameters.Add(new SqlParameter("@Column", SqlDbType.VarChar)).Value = Fields;
                    //command.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar)).Value = Values;   
                }
                da.Fill(ds);
                //dt.TableName = ProcedureName;
                con.Close();
                return ds;
            }
            catch (Exception exception)
            {
                string str = exception.Message.ToString();
                return null;
            }
        }

        public int UpdateProcedureData(string ProcedureName, string paramName, string paramValue)
        {
            try
            {
                //DataTable dt = new DataTable();
                SqlCommand command = new SqlCommand();
                //SqlDataAdapter da = new SqlDataAdapter(command);

                command.Connection = con;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = ProcedureName;

                if (paramName != "")
                {
                    String[] paramname = paramName.Split(',');
                    String[] paramvalue = paramValue.Split(',');

                    if (paramname.Length != paramvalue.Length)
                    {
                        //throw "Invalid Parameter";
                    }
                    for (int i = 0; i < paramname.Length; i++)
                    {
                        command.Parameters.Add(new SqlParameter(paramname[i], SqlDbType.VarChar)).Value = paramvalue[i];
                    }
                    //command.Parameters.Add(new SqlParameter("@Table", SqlDbType.VarChar)).Value = TableName;
                    //command.Parameters.Add(new SqlParameter("@Column", SqlDbType.VarChar)).Value = Fields;
                    //command.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar)).Value = Values;   
                }

                int Result = command.ExecuteNonQuery();
                //da.Updat
                //dt.TableName = ProcedureName;
                con.Close();
                return Result;
            }
            catch (Exception exception)
            {
                string str = exception.Message.ToString();
                return 0;
            }
        }

        public int UpdateSchedule1(string reference_no, byte[] schedule1)
        {
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = con;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "usp_Sub8849_UpdateSchedule1";

                command.Parameters.Add(new SqlParameter("@sd_ref_no", SqlDbType.VarChar)).Value = reference_no;
                command.Parameters.Add(new SqlParameter("@Schedule6", SqlDbType.VarBinary)).Value = schedule1;

                int Result = command.ExecuteNonQuery();
                con.Close();
                return Result;
            }
            catch (Exception exception)
            {
                string str = exception.Message.ToString();
                return 0;
            }
        }

        // To insert and update.
        public int InsertUpdateDelete(string query)
        {
            int i;
            try
            {
                //if (con.State==ConnectionState.Closed)
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                i = cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
            return i;
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { }
            return false;
        }

        ////For Inserting details into tables
        //public bool InsertTable(string TableName, string Fields, string Values)
        //{
        //    try
        //    {
        //        SqlCommand command = new SqlCommand();
        //        //command.Transaction = this.trans;
        //        command.Connection = con;
        //        if (con.State != ConnectionState.Open)
        //        {
        //            con.Open();
        //        }

        //        command.CommandType = CommandType.StoredProcedure;
        //        command.CommandText = "Insert_Table";
        //        command.Parameters.Add(new SqlParameter("@Table", SqlDbType.VarChar)).Value = TableName;
        //        command.Parameters.Add(new SqlParameter("@Column", SqlDbType.VarChar)).Value = Fields;
        //        command.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar)).Value = Values;
        //        command.ExecuteNonQuery();
        //        con.Close();
        //        return true;
        //    }
        //    catch (Exception exception)
        //    {
        //        string str = exception.Message.ToString();
        //        return false;
        //    }
        //}

        ////For Updating Table
        //public bool UpdateTable(string TableName, string FieldAndValue, string Condition)
        //{
        //    try
        //    {
        //        SqlCommand command = new SqlCommand();
        //        //command.Transaction = this.trans;
        //        command.Connection = con;
        //        if (con.State != ConnectionState.Open)
        //        {
        //            con.Open();
        //        }
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.CommandText = "Update_Table";
        //        command.Parameters.Add(new SqlParameter("@Table", SqlDbType.VarChar)).Value = TableName;
        //        command.Parameters.Add(new SqlParameter("@FieldAndValue", SqlDbType.VarChar)).Value = FieldAndValue;
        //        command.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar)).Value = Condition;
        //        command.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception exception)
        //    {
        //        string str = exception.Message.ToString();
        //        return false;
        //    }
        //}

        ////For Deleting Table
        //public bool DeleteTable(string TableName, string Condition)
        //{
        //    try
        //    {
        //        SqlCommand command = new SqlCommand();
        //        command.Connection = con;
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.CommandText = "Delete_Table";
        //        command.Parameters.Add(new SqlParameter("@Table", SqlDbType.VarChar)).Value = TableName;
        //        command.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar)).Value = Condition;
        //        command.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception exception)
        //    {
        //        string str = exception.Message.ToString();
        //        return false;
        //    }
        //}

        public static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append('.');
            }
            return builder.ToString();
        }

        public int UpdateMailSentStatus(string ref_no)
        {
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = con;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "usp_Sub8849_Update_Email8849Accepted";

                command.Parameters.Add(new SqlParameter("@sd_ref_no", SqlDbType.BigInt)).Value = ref_no;

                int Result = command.ExecuteNonQuery();
                con.Close();
                return Result;
            }
            catch (Exception exception)
            {
                string str = exception.Message.ToString();
                return 0;
            }
        }

        internal int UpdateFaxSentStatus(string ref_no, string tran_id)
        {
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = con;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "usp_Sub8849_Update_Fax8849Accepted";

                command.Parameters.Add(new SqlParameter("@sd_ref_no", SqlDbType.VarChar)).Value = ref_no;
                command.Parameters.Add(new SqlParameter("@tran_id", SqlDbType.VarChar)).Value = tran_id;

                int Result = command.ExecuteNonQuery();
                con.Close();
                return Result;
            }
            catch (Exception exception)
            {
                string str = exception.Message.ToString();
                return 0;
            }
        }
    }
}