using System;
using System.Data;
using API;
using eFormEmailSender;
using DBOperations;
using System.Configuration;
using SMG_MailChimp;

public class eForm8849Operations 
{
    DBStorage db = new DBStorage();

    public eForm8849Operations()
    {
    }
  
    public DataSet GetPendingSubmissionList(string flag)
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName="",ParamName="",ParamValue="";

            if(flag!="")
            {
                ParamName = "@flag";
                ParamValue = flag;
                ProcedureName = "usp_Sub8849_Get_SubmissionList";
            }
            else
            {
                ParamName = "";
                ParamValue = "";
                ProcedureName = "usp_Sub8849_Get_PendingSubmission";
            }
            ds=db.SelectProcedureData(ProcedureName,ParamName,ParamValue);
            
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxId"] = Crypt.Decrypt(dt.Rows[i]["TaxId"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }
   
    public DataSet GetPendingMailingList()
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName = "";
            ProcedureName = "usp_Sub8849_Get_EmailListAccept";
            ds = db.SelectProcedureData(ProcedureName, "", "");

            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxID"] = Crypt.Decrypt(dt.Rows[i]["TaxID"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet GetPendingFAXList()
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName = "";
            ProcedureName = "usp_Sub8849_Get_FaxListAccept";
            ds = db.SelectProcedureData(ProcedureName, "", "");

            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxID"] = Crypt.Decrypt(dt.Rows[i]["TaxID"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet GetAllSubmissionData(string FormId, string UserId, string TaxId)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectProcedureData("usp_Sub8849_Get_AllSubmissionRecords", "@FormId,@UserId,@TaxId", FormId + "," + UserId + "," + Crypt.Encrypt(TaxId));

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["ur_sec_chars"] = Crypt.Decrypt(ds.Tables[0].Rows[i]["ur_sec_chars"].ToString());
                }
                ds.Tables[0].AcceptChanges();
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    ds.Tables[1].Rows[i]["pd_tax_id"] = Crypt.Decrypt(ds.Tables[1].Rows[i]["pd_tax_id"].ToString());
                    ds.Tables[1].Rows[i]["pd_sig_pin"] = Crypt.Decrypt(ds.Tables[1].Rows[i]["pd_sig_pin"].ToString());
                }
                ds.Tables[1].AcceptChanges();
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    string ttd_tax_id = Crypt.Decrypt(ds.Tables[2].Rows[i]["ttd_tax_id"].ToString());
                    if (ttd_tax_id.Length == 0)
                        ds.Tables[2].Rows[i]["ttd_tax_id"] = 0;
                    else
                        ds.Tables[2].Rows[i]["ttd_tax_id"] = ttd_tax_id;
                }
                ds.Tables[2].AcceptChanges();
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                {
                    ds.Tables[4].Rows[i]["ip_tax_id"] = ds.Tables[4].Rows[i]["ip_tax_id"].ToString();
                    ds.Tables[4].Rows[i]["ip_bank_routing_no"] = Crypt.Decrypt(ds.Tables[4].Rows[i]["ip_bank_routing_no"].ToString());
                    ds.Tables[4].Rows[i]["ip_bank_account_no"] = Crypt.Decrypt(ds.Tables[4].Rows[i]["ip_bank_account_no"].ToString());
                }
                ds.Tables[4].AcceptChanges();
            }
            
            if (ds.Tables[5].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                {
                    if(ds.Tables[5].Rows[i]["ppd_EIN"]!=null)
                        ds.Tables[5].Rows[i]["ppd_EIN"] = Crypt.Decrypt(ds.Tables[5].Rows[i]["ppd_EIN"].ToString());
                    ds.Tables[5].Rows[i]["ppd_SSN_PTIN"] = Crypt.Decrypt(ds.Tables[5].Rows[i]["ppd_SSN_PTIN"].ToString());
                }
                ds.Tables[5].AcceptChanges();
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
                {
                    ds.Tables[6].Rows[i]["tpd_tax_id"] = Crypt.Decrypt(ds.Tables[6].Rows[i]["tpd_tax_id"].ToString());
                    ds.Tables[6].Rows[i]["tpd_designee_PIN"] = Crypt.Decrypt(ds.Tables[6].Rows[i]["tpd_designee_PIN"].ToString());
                }
                ds.Tables[6].AcceptChanges();
            }

            return ds;
        }
        catch
        {
            return null;
        }
    }
    
    public int UpdateSubmissionRecords(string TaxId,string FormId,string SubmissionId,string TaxBegindate,
        string TaxEndDate,string masterStatus,string SubmissionStatus,string RejectionErrorCodes,string TimeStamp)
    {
        try
        {
            string ParamNames, ParamValues = "";
            TaxId = Crypt.Encrypt(TaxId);

            ParamNames = "@sd_tax_ID,@sd_form_id,@sd_submission_ID,@sd_tax_begin_date,@sd_tax_end_date,@sd_submission_status,@sd_rejection_err_codes,@sd_timestamp,@master_status";
            ParamValues = TaxId + "," + FormId + "," + SubmissionId + "," + TaxBegindate + "," + TaxEndDate + "," + SubmissionStatus + "," + RejectionErrorCodes + "," + TimeStamp +","+ masterStatus;
            int i = db.UpdateProcedureData("usp_Sub8849_Update_SubmissionResult", ParamNames, ParamValues);
            return 1;
        }
        catch
        {
            return 0;
        }
    }
    
    public DataSet Select_Data(string ProcedureName, string ParamName, string ParamValue)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectProcedureData(ProcedureName, ParamName, ParamValue);
            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet Select_Data(string qry)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectQuery(qry);
            return ds;
        }
        catch
        {
            return null;
        }
    }

    public int Update_Data(string ProcedureName, string ParamName, string ParamValue)
    {
        try
        {
            int i;
            i = db.UpdateProcedureData(ProcedureName, ParamName, ParamValue);
            return i;
        }
        catch
        {
            return 0;
        }
    }
   
    public int Update_Schedule1(string reference_no, byte[] schedule1)
    {
        try
        {
            int i;
            i = db.UpdateSchedule1(reference_no,schedule1);
            return i;
        }
        catch
        {
            return 0;
        }
    }

    public bool Update_MailSentStatusAfterSubmission(string reference_no,out string error)
    {
        error = "";
        try
        {
            int i;
            i = db.UpdateMailSentStatus(reference_no);
            return Convert.ToBoolean(i);
        }
        catch(Exception ex)
        {
            error = ex.Message + ex.StackTrace;
            return false;
        }
    }

    public bool Update_MailSentStatusAfterSubmission(string reference_no)
    {
        try
        {
            int i;
            i = db.UpdateMailSentStatus(reference_no);
            return Convert.ToBoolean(i);
        }
        catch
        {
            return false;
        }
    }

    public bool SendSubmissionMailAcceptance(string BeginYear, string Name, string Email, string ReferenceID, string support, out string error)
    {
        error = "";
        try
        {
            Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
            byte[] Schedule1;
            string SubmissionID;
            string FK_CD;
            FK_CD = "";
            DataTable dt = new DataTable();

            dt = db.SelectQuery("select S.submission_id, F.schedule6, FK_CM_SS_cd FROM C_Submissions8849 S LEFT JOIN [8849_Form_Binary] F ON S.FK_8849F_key=F.FK_8849F_key WHERE S.ref_no='" + ReferenceID + "'").Tables[0];
            if (dt.Rows.Count == 0)
                return false;
            else
            {
                Schedule1 = null;
                SubmissionID = Convert.ToString(dt.Rows[0][0]);
                FK_CD = Convert.ToString(dt.Rows[0][2]);
                if (SubmissionID.Length > 0 && (FK_CD == "IA" || FK_CD == "RS") && FK_CD.Length > 0)
                {
                    return SendMail_Acceptance(BeginYear, Name, Email, Schedule1, SubmissionID, support, ReferenceID);
                }
                return false;
            }
        }
        catch(Exception ex)
        {
            error = ex.Message;
            return false;
        }

    }
    

    public bool SendSubmissionMailRejection(string pTo, string pCC, string bCC, string pSubject, string pBody)
    {
        try
        {
            //OLD EMAIL
            //return eMailService.SendEmail(pTo, pCC, bCC, pSubject, pBody, "");

            //NEW SMG_MAILCHIMP
            return SMG_Mailer.SendEmail(pTo, pCC, bCC, pSubject, pBody, "");
        }
        catch 
        {
            return false;
        }
    }

    private bool SendMail_Acceptance(string BeginYear, string Name, string Email, byte[] Schedule1, string SubmissionID, string support, string REF_NO)
    {
        try
        {
            string subject = "Congratulations! 8849 Tax Return Accepted by IRS";
            //subject += BeginYear + " - " + (Convert.ToInt16(BeginYear) + 1);

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
            Name = textInfo.ToTitleCase(Name.ToLower());

            /* New below */
            string ReviewTxt = "<table style='border:1px solid #000;font-family: calibri; font-size: 16px;'><tr style='border-bottom:1px solid #000'><td><p  style='text-align:justify'>If you are happy with our recent service, we ask that you <b>leave an online review</b> for our business helping us share our great service with others.</p></td></tr><tr style='text-align:center'><td><a href='https://www.facebook.com/eform2290/reviews/' target='_blank'><img src='https://www.eform2290.com/Images/r-fb.png' style='width:250px;' /></a></td></tr><tr><td><hr /><p  style='text-align:justify'>If we did not meet your expectations in any way, please leave us your feedback so we can address it with you and improve.</p></td></tr><tr style='text-align:center'><td><a href='https://goo.gl/KXVGU0' target='_blank'><img src='https://www.eform2290.com/Images/gf.png' style='width:250px' /></a></td></tr></table>" +
          "<p style='font-family: calibri; font-size: 16px'>We look forward to seeing you again soon.</p>";

            string body = "<p style='font-family: calibri; font-size: 16px'>" +
                "Dear " + Name + ", <br /><br />" +
                "Greeting from eForm2290.com. <br /><br />" +
                "Congratulations! Your Tax return (Form 8849), is accepted by the IRS.<br />" +
                "Please keep the Reference : <span style='background-color:#FFFF55;'>" + REF_NO + "</span>  and Submission ID : <span style='background-color:#FFFF55;'>" + SubmissionID + " </span>, of Schedule 6 for future communications.<br />" +    
                "You can download the reference copy anytime from our website <a href='https://www.eform2290.com/'>www.eForm2290.com</a> .<br />" +
                "If you have questions or need further assistance, please contact our support team. <br/><br /> ";

            //Ref_Copy.aspx?Ref_no=<REFNO>
            //schedule8849_6.aspx?Ref_no=<REFNO>
            //"Please click here to download a copy of <a href='https://www.eForm2290.com/Efile8849/schedule8849_6.aspx?Ref_no=" + REF_NO + "'>Schedule 6</a> <br/>" +
            body = body + "Thank you for your business.<br/></p>";

            body = body + ReviewTxt;

            body = body + "<p style='font-family: calibri; font-size: 16px'>" +
                " Sincerely, <br />" +
                " eForm2290 Team <br /> " +
                " Email: <a href='mailto:support@eform2290.com'>support@eform2290.com</a> <br /> " +
                " <a href='https://www.eform2290.com/'>www.eForm2290.com</a>" +
                " </p>";

            /* ADP */
            /*string ADP = "<a href='http://www.adp.com/silvermine' target='_blank' style='display: inline-flex; text-decoration: none;'>" +
                    "<img src='https://www.eform2290.com/Images/ADP_New.jpg' style='height: 90px; border: 1px solid; border-right: none;' />" +
                    "<span style='background: #DF1E33; color: #fff; font-size: 13px; padding: 5px; text-align: justify; border: 1px solid #000; border-left: none;width:27%'>" +
                    "Through our partnership with ADP, we offer payroll, tax and HR solutions that can help improve the management of your people and your risk, so you can get" +
                    " back to focusing on your business.<br /></span>" +
                    "</a><br />";
             */
            string ADP = "<p style='border: 1px solid;text-align: center;padding: 3px;margin-bottom: -1.3%;width: auto;font-family:tahoma'></p><p style='border: 1px solid;padding: 5px;text-align: center;width: auto;'>" +
                    "<a href='https://www.adp.com/silvermine' target='_blank' style='display: inline-flex; text-decoration: none;height:100px;max-width:600px;width:100%;margin-top: 3px;margin-bottom: 3px;'>" +
                    "<img src='https://www.eform2290.com/Images/ADP_New.jpg' style='border: 1px solid; border-right: none' />" +
                    "<span style='background: #DF1E33; color: #fff; font-size: 13px; padding: 5px; text-align: justify; border: 1px solid #000; border-left: none;font-family:tahoma'>" +
                    "ADP is our preferred partner for payroll, tax and HR solutions.  " +
                    "This collaboration is our step towards a better eco-system for our clients and customers.  " +
                    "<br />Our partnership program empowers our customers to receive exclusive benefits towards services offered by ADP. </span>" +
                    "</a></p>";
            try
            {
                if (ConfigurationSettings.AppSettings["ADPDisplay"].ToString() == "1")
                {
                    body = body + ADP;
                }
            }
            catch (Exception e)
            { }

            /* RIGHauler Advertisment display */
            string RigHDisplay = "<p style='border: 1px solid;text-align: center;padding: 3px;margin-bottom: -1.3%;width: auto;font-family:tahoma'></p><p style='border: 1px solid;padding: 5px;text-align: center;width: auto;'>" +
                    "<a href='https://www.righauler.com' target='_blank' style='display: inline-flex; text-decoration: none;height:100px;max-width:600px;width:100%;margin-top: 3px;margin-bottom: 3px;'>" +
                    "<img src='https://www.eform2290.com/EmailImg/RHAd.gif' style='border: 1px solid; border-right: none' />" +
                    "</a></p>";
            try
            {
                if (ConfigurationSettings.AppSettings["RIGHaulerDisplay"].ToString() == "1")
                {
                    body = body + RigHDisplay;
                }
            }
            catch (Exception e)
            { }



            body = body + "<div style='color: gray'> " +
                " <hr /><p style='font-family: calibri; font-size: 10px;'>This is a computer-generated email. If you have any problems, questions or requests " +
                " regarding this message. Please email to <a style='font-size: 10px;' href='mailto:support@eform2290.com'>support@eform2290.com</a> and Support team will  " +
                " contact you at the earliest.</p> </div>";

            //OLD EMAIL Service
            //return eFormEmailSender.eMailService.SendEmailWithSchedule1(Email, "", support, subject, body, Schedule1, SubmissionID);

            //NEW EMAIL SMG_MAILCHIMP
            //return SMG_Mailer.SendEmailWithSchedule1(Email, "", support, subject, body, Schedule1, SubmissionID);

            //Send with out SCH1
            return SMG_Mailer.SendEmail(Email, "", support, subject, body,"");
        }
        catch
        {
            return false;
        }
    }

    private bool SendMail_Rejected(string BeginYear, string Name, string Email, string business, string reason, string SubmissionID, string support)
    {
        try
        {
            string subject = "8849 Tax Return Rejected for year ";
            subject += BeginYear + " - " + (Convert.ToInt16(BeginYear) + 1);

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
            Name = textInfo.ToTitleCase(Name.ToLower());

            string body = "Dear " + Name + ":<br/><br/>";
            body += "Your heavy vehicle use tax return for ";
            body += business.ToUpper() + "has been rejected by the IRS due to following reason: ";

            body += "<ul><li>";
            body += reason;
            body+="</li></ul>";

            body += "You can contact our support team for further assistance.";
            
            body += "You can even download schedule 6 anytime from ";
            body += "<a href='https://www.eform2290.com'>eForm2290</a> website." + "<br/><br/>";
            body += "If you have questions or need further assistance, ";
            body += "please contact our support team." + "<br/><br/>";
            body += "Thank you for your business." + "<br/><br/>";
            body += "--<br/>With Regards,<br/>";
            body += "eForm2290 Support Team<br/>";
            body += "www.eForm2290.com<br/>";

            return false;
        }
        catch
        {
            return false;
        }
    }

    public DateTime GetServerDate()
    {
        DateTime dt = DateTime.Now;
        try
        {
            dt =Convert.ToDateTime(db.SelectQuery("select getdate()").Tables[0].Rows[0][0]);
         
        }
        catch {  }
        return dt;
    }

    public bool Update_FaxSentStatusAfterSubmission(string pRef_no, string tran_id)
    {
        try
        {
            int i;
            i = db.UpdateFaxSentStatus(pRef_no, tran_id);
            return Convert.ToBoolean(i);
        }
        catch
        {
            return false;
        }
    }
}
