﻿using System;
using System.Web;
using System.IO;

namespace eFormSMSSender
{
    public class Logger
    {

        public static void Write(string pUser, string pForm, string pMsg)
        {
            try
            {
                string path = "~/Log/" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt";

                if (!File.Exists(path))
                    File.Create(path).Close();

                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine("{0}", DateTime.Now.ToString());

                    string strMsg = "Form: " + pForm + "\r\n" +
                                    "Time: " + DateTime.Now + "\r\n" +
                                    "User: " + pUser + "\r\n" +
                                    "Message:" + pMsg + "\r\n";

                    w.WriteLine(strMsg);
                    w.WriteLine("===========================\n\n");
                    w.Flush();
                    w.Close();
                }
            }
            catch
            {

            }

        }


    }
}