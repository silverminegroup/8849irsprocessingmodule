﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Services;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using Microsoft.Web.Services3.Mime;
using ICSharpCode.SharpZipLib.Zip;
using IRSLibrary.MefMsiServices;
using IRSLibrary.Utilities;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;

namespace IRSLibrary
{
    public class Return8849Attachment
    {
        static clsVariables Variables;

        public static string SubmissionType = "8849";
        public static string submissionID;

        static string path2CreateFile_4SendSubmission = @"C:\";
        static string path2CreateZipFile = @"C:\";

        public static byte[] schedule6;

        public Return8849Attachment(string taxyear)
        {
            Variables = new clsVariables(taxyear);
        }

        public string Create_Attachment_For_f8849(string TaxYear, string Filer_EIN)
        {
            try
            {

                int temp_TaxYr = 0;
                temp_TaxYr = Convert.ToInt32(TaxYear);
                DateTime TaxPeriodEndDate = Convert.ToDateTime("06-30-" + TaxYear); ;
                if (temp_TaxYr > 0)
                {
                    temp_TaxYr = temp_TaxYr + 1;
                    TaxPeriodEndDate = Convert.ToDateTime("06-30-" + temp_TaxYr.ToString());
                }

                DateTime TaxPeriodBeginDate = Convert.ToDateTime("07-01-" + TaxYear);


                createManifestXMLFile(TaxYear, TaxPeriodEndDate, TaxPeriodBeginDate, Filer_EIN, "8849");

                createZip();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public void createReturnXml_8849_2020(string Tax_Year_Used_Month, string TaxYear, string Disaster_ReliefTxt, string ISP_Num,
            bool IsPreparerSelfEmployed, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, decimal RefundAmt, System.DateTime ClaimDate, string LossType,
                string VehVin, string VehicleExplanation, string CRN, System.DateTime LatestClaimDt,
                string SplContdDesc
            )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(ReturnF8849_2020.Return));
                #region RETRUN_HEADER

                ReturnF8849_2020.ReturnHeaderType returnHeaderType = new ReturnF8849_2020.ReturnHeaderType();
                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
                returnHeaderType.TaxYearEndMonthNum = Tax_Year_Used_Month;
                
                if (Disaster_ReliefTxt.Length > 0 )
                    returnHeaderType.DisasterReliefTxt = Disaster_ReliefTxt;    
                if(ISP_Num.Length > 0)
                    returnHeaderType.ISPNum = ISP_Num;
                    
                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = ReturnF8849_2020.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = ReturnF8849_2020.ReturnHeaderTypeReturnTypeCd.Item8849;
                returnHeaderType.SignatureOptionCd = ReturnF8849_2020.ReturnHeaderTypeSignatureOptionCd.PINNumber;
               // returnHeaderType.SignatureOptionCd = ReturnF8849_2020.ReturnHeaderTypeSignatureOptionCd.BinaryAttachment8453SignatureDocument;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                ReturnF8849_2020.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new ReturnF8849_2020.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                ReturnF8849_2020.ReturnHeaderTypeOriginatorGrp originator = new ReturnF8849_2020.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = ReturnF8849_2020.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion
                #region FILER
                ReturnF8849_2020.ReturnHeaderTypeFiler objFiler = new ReturnF8849_2020.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                //objFiler.SSN = Filer_EIN; // Optional
                //objFiler.BusinessName = Filer_Name; //max_length=60

                ReturnF8849_2020.BusinessNameType FilerBusinessName = new ReturnF8849_2020.BusinessNameType();
                FilerBusinessName.BusinessNameLine1Txt = Filer_Name;
                objFiler.EIN = Filer_EIN;
                objFiler.BusinessName = FilerBusinessName;
				
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    ReturnF8849_2020.USAddressType usAdd_Filer = new ReturnF8849_2020.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = ReturnF8849_2020.StateType.AA; break;
                        case "AE": usAdd_Filer.State = ReturnF8849_2020.StateType.AE; break;
                        case "AK": usAdd_Filer.State = ReturnF8849_2020.StateType.AK; break;
                        case "AL": usAdd_Filer.State = ReturnF8849_2020.StateType.AL; break;
                        case "AP": usAdd_Filer.State = ReturnF8849_2020.StateType.AP; break;
                        case "AR": usAdd_Filer.State = ReturnF8849_2020.StateType.AR; break;
                        case "AS": usAdd_Filer.State = ReturnF8849_2020.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = ReturnF8849_2020.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = ReturnF8849_2020.StateType.CA; break;
                        case "CO": usAdd_Filer.State = ReturnF8849_2020.StateType.CO; break;
                        case "CT": usAdd_Filer.State = ReturnF8849_2020.StateType.CT; break;
                        case "DC": usAdd_Filer.State = ReturnF8849_2020.StateType.DC; break;
                        case "DE": usAdd_Filer.State = ReturnF8849_2020.StateType.DE; break;
                        case "FL": usAdd_Filer.State = ReturnF8849_2020.StateType.FL; break;
                        case "FM": usAdd_Filer.State = ReturnF8849_2020.StateType.FM; break;
                        case "GA": usAdd_Filer.State = ReturnF8849_2020.StateType.GA; break;
                        case "GU": usAdd_Filer.State = ReturnF8849_2020.StateType.GU; break;
                        case "HI": usAdd_Filer.State = ReturnF8849_2020.StateType.HI; break;
                        case "IA": usAdd_Filer.State = ReturnF8849_2020.StateType.IA; break;
                        case "ID": usAdd_Filer.State = ReturnF8849_2020.StateType.ID; break;
                        case "IL": usAdd_Filer.State = ReturnF8849_2020.StateType.IL; break;
                        case "IN": usAdd_Filer.State = ReturnF8849_2020.StateType.IN; break;
                        case "KS": usAdd_Filer.State = ReturnF8849_2020.StateType.KS; break;
                        case "KY": usAdd_Filer.State = ReturnF8849_2020.StateType.KY; break;
                        case "LA": usAdd_Filer.State = ReturnF8849_2020.StateType.LA; break;
                        case "MA": usAdd_Filer.State = ReturnF8849_2020.StateType.MA; break;
                        case "MD": usAdd_Filer.State = ReturnF8849_2020.StateType.MD; break;
                        case "ME": usAdd_Filer.State = ReturnF8849_2020.StateType.ME; break;
                        case "MH": usAdd_Filer.State = ReturnF8849_2020.StateType.MH; break;
                        case "MI": usAdd_Filer.State = ReturnF8849_2020.StateType.MI; break;
                        case "MN": usAdd_Filer.State = ReturnF8849_2020.StateType.MN; break;
                        case "MO": usAdd_Filer.State = ReturnF8849_2020.StateType.MO; break;
                        case "MP": usAdd_Filer.State = ReturnF8849_2020.StateType.MP; break;
                        case "MS": usAdd_Filer.State = ReturnF8849_2020.StateType.MS; break;
                        case "MT": usAdd_Filer.State = ReturnF8849_2020.StateType.MT; break;
                        case "NC": usAdd_Filer.State = ReturnF8849_2020.StateType.NC; break;
                        case "ND": usAdd_Filer.State = ReturnF8849_2020.StateType.ND; break;
                        case "NE": usAdd_Filer.State = ReturnF8849_2020.StateType.NE; break;
                        case "NH": usAdd_Filer.State = ReturnF8849_2020.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = ReturnF8849_2020.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = ReturnF8849_2020.StateType.NM; break;
                        case "NV": usAdd_Filer.State = ReturnF8849_2020.StateType.NV; break;
                        case "NY": usAdd_Filer.State = ReturnF8849_2020.StateType.NY; break;
                        case "OH": usAdd_Filer.State = ReturnF8849_2020.StateType.OH; break;
                        case "OK": usAdd_Filer.State = ReturnF8849_2020.StateType.OK; break;
                        case "OR": usAdd_Filer.State = ReturnF8849_2020.StateType.OR; break;
                        case "PA": usAdd_Filer.State = ReturnF8849_2020.StateType.PA; break;
                        case "PR": usAdd_Filer.State = ReturnF8849_2020.StateType.PR; break;
                        case "PW": usAdd_Filer.State = ReturnF8849_2020.StateType.PW; break;
                        case "RI": usAdd_Filer.State = ReturnF8849_2020.StateType.RI; break;
                        case "SC": usAdd_Filer.State = ReturnF8849_2020.StateType.SC; break;
                        case "SD": usAdd_Filer.State = ReturnF8849_2020.StateType.SD; break;
                        case "TN": usAdd_Filer.State = ReturnF8849_2020.StateType.TN; break;
                        case "TX": usAdd_Filer.State = ReturnF8849_2020.StateType.TX; break;
                        case "UT": usAdd_Filer.State = ReturnF8849_2020.StateType.UT; break;
                        case "VA": usAdd_Filer.State = ReturnF8849_2020.StateType.VA; break;
                        case "VI": usAdd_Filer.State = ReturnF8849_2020.StateType.VI; break;
                        case "VT": usAdd_Filer.State = ReturnF8849_2020.StateType.VT; break;
                        case "WA": usAdd_Filer.State = ReturnF8849_2020.StateType.WA; break;
                        case "WI": usAdd_Filer.State = ReturnF8849_2020.StateType.WI; break;
                        case "WV": usAdd_Filer.State = ReturnF8849_2020.StateType.WV; break;
                        case "WY": usAdd_Filer.State = ReturnF8849_2020.StateType.WY; break;
                            #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    ReturnF8849_2020.ForeignAddressType foreignAdd_Filer = new ReturnF8849_2020.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = ReturnF8849_2020.CountryType.ZI; break;
                            #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion
                #region OFFICER
                if (Officer_Name != "")
                {
                    ReturnF8849_2020.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new ReturnF8849_2020.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = ReturnF8849_2020.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = ReturnF8849_2020.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion
                #region PREPARER

                if (Preparer_Name != "")
                {
                    ReturnF8849_2020.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new ReturnF8849_2020.ReturnHeaderTypePreparerFirmGrp();
                    ReturnF8849_2020.ReturnHeaderTypePreparerPersonGrp objPreparer = new ReturnF8849_2020.ReturnHeaderTypePreparerPersonGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = ReturnF8849_2020.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = ReturnF8849_2020.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = ReturnF8849_2020.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }
                //2019 Preparer_Address not empty check 
                if (!IsPreparerSelfEmployed && Preparer_Name != "" && Preparer_Address != "")
                {
                    ReturnF8849_2020.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new ReturnF8849_2020.ReturnHeaderTypePreparerFirmGrp();
                    ReturnF8849_2020.BusinessNameType PreparerBusinessName = new ReturnF8849_2020.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        ReturnF8849_2020.USAddressType usAdd_Preparer = new ReturnF8849_2020.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = ReturnF8849_2020.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = ReturnF8849_2020.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = ReturnF8849_2020.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = ReturnF8849_2020.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = ReturnF8849_2020.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = ReturnF8849_2020.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = ReturnF8849_2020.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = ReturnF8849_2020.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = ReturnF8849_2020.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = ReturnF8849_2020.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = ReturnF8849_2020.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = ReturnF8849_2020.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = ReturnF8849_2020.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = ReturnF8849_2020.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = ReturnF8849_2020.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = ReturnF8849_2020.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = ReturnF8849_2020.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = ReturnF8849_2020.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = ReturnF8849_2020.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = ReturnF8849_2020.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = ReturnF8849_2020.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = ReturnF8849_2020.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = ReturnF8849_2020.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = ReturnF8849_2020.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = ReturnF8849_2020.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = ReturnF8849_2020.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = ReturnF8849_2020.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = ReturnF8849_2020.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = ReturnF8849_2020.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = ReturnF8849_2020.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = ReturnF8849_2020.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = ReturnF8849_2020.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = ReturnF8849_2020.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = ReturnF8849_2020.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = ReturnF8849_2020.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = ReturnF8849_2020.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = ReturnF8849_2020.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = ReturnF8849_2020.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = ReturnF8849_2020.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = ReturnF8849_2020.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = ReturnF8849_2020.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = ReturnF8849_2020.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = ReturnF8849_2020.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = ReturnF8849_2020.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = ReturnF8849_2020.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = ReturnF8849_2020.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = ReturnF8849_2020.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = ReturnF8849_2020.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = ReturnF8849_2020.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = ReturnF8849_2020.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = ReturnF8849_2020.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = ReturnF8849_2020.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = ReturnF8849_2020.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = ReturnF8849_2020.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = ReturnF8849_2020.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = ReturnF8849_2020.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = ReturnF8849_2020.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = ReturnF8849_2020.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = ReturnF8849_2020.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = ReturnF8849_2020.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = ReturnF8849_2020.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = ReturnF8849_2020.StateType.WY; break;
                                #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        ReturnF8849_2020.ForeignAddressType foreignAdd_Preparer = new ReturnF8849_2020.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = ReturnF8849_2020.CountryType.ZI; break;
                                #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion
                // returnHeaderType.TaxYr = TaxYear; //Not used for now
                returnHeaderType.binaryAttachmentCnt = "0";
                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion
                #region RETURNDATA
                ReturnF8849_2020.ReturnData returnData = new ReturnF8849_2020.ReturnData();


                #region IRS8849

                ReturnF8849_2020.IRS8849 objIRS8849 = new ReturnF8849_2020.IRS8849();
                //objIRS8849.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));
                //if (objIRS8849.BalanceDueAmt < 0)   objIRS8849.BalanceDueAmt = 0.00M;
                documentCount += 1;
                
                objIRS8849.softwareId = Variables.SoftwareID;
                objIRS8849.softwareVersion = Variables.SoftwareVersion;
                objIRS8849.documentId = "IRS8849"; //documentCount.ToString(); 
                objIRS8849.documentName = "IRS8849";
                objIRS8849.referenceDocumentId = new string[] { Convert.ToString(documentCount) } ;//new string[] { "1" };
                if (SplContdDesc.Trim().Length > 0) {
                    objIRS8849.SpecialConditionDesc = SplContdDesc;//"Special Condition Description" ;
                }
                objIRS8849.Schedule6AttachedInd = ReturnF8849_2020.CheckboxType.X;

                returnData.IRS8849 = objIRS8849;
                #endregion

                #region IRS 8849 SCHEDULE6

                ReturnF8849_2020.IRS8849Schedule6 irs8849Schedule6 = new ReturnF8849_2020.IRS8849Schedule6();
                documentCount += 1;
                irs8849Schedule6.documentId = "IRS8849Schedule6";//documentCount.ToString();
                irs8849Schedule6.documentName = "IRS8849Schedule6";
                irs8849Schedule6.softwareId = Variables.SoftwareID;
                irs8849Schedule6.softwareVersion = Variables.SoftwareVersion;

                irs8849Schedule6.TotalRefundAmt = Convert.ToDecimal(RefundAmt.ToString("N2"));
                irs8849Schedule6.EarliestClaimDt = ClaimDate;//DateTime.Now.ToUniversalTime(); //ClaimDate;
                irs8849Schedule6.LatestClaimDt = LatestClaimDt;//DateTime.Now.ToUniversalTime();
                //OtherTax Claim
                ReturnF8849_2020.OtherClaimNotRptOnOthFormsGrp otherClaimNotRptOnOthFormsGrp = new ReturnF8849_2020.OtherClaimNotRptOnOthFormsGrp();


                ReturnF8849_2020.OtherClaimNotRptGrp otherClaimNotRptGrp = new ReturnF8849_2020.OtherClaimNotRptGrp();

                //otherClaimNotRptOnOthFormsGrp.referenceDocumentId = new string[] { "1" };

                ReturnF8849_2020.OtherTaxClaimGrp otherTaxClaim = new ReturnF8849_2020.OtherTaxClaimGrp();
                otherTaxClaim.TaxTypeDesc = LossType;
                otherTaxClaim.VIN = VehVin;
                otherClaimNotRptGrp.OtherTaxClaimGrp = otherTaxClaim;

                otherClaimNotRptGrp.referenceDocumentId = new string[] { Convert.ToString(documentCount) };//new string[] { "1" };
                otherClaimNotRptGrp.Amt = Convert.ToDecimal(RefundAmt.ToString("N2"));
                otherClaimNotRptGrp.CreditReferenceNum = CRN; //Credit REF# TextType - 3 Maxlen

                otherClaimNotRptOnOthFormsGrp.OtherClaimNotRptGrp = otherClaimNotRptGrp;
                irs8849Schedule6.OtherClaimNotRptOnOthFormsGrp = otherClaimNotRptOnOthFormsGrp;
                returnData.IRS8849Schedule6 = irs8849Schedule6;
                
                #endregion

                #region Other Claims Explanation
                if (VehicleExplanation.Trim().Length > 0)
                {
                    documentCount += 1;
                    ReturnF8849_2020.OtherClaimsExplanationStmt otherClaimsExplanationStmt = new ReturnF8849_2020.OtherClaimsExplanationStmt();
                    otherClaimsExplanationStmt.documentId = documentCount.ToString(); //"OtherClaimsExplanationStatement";
                    otherClaimsExplanationStmt.documentName = "OtherClaimsExplanationStatement";
                    otherClaimsExplanationStmt.ExplanationTxt = VehicleExplanation;
                    returnData.OtherClaimsExplanationStmt = otherClaimsExplanationStmt;
                    
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                ReturnF8849_2020.Return return8849 = new ReturnF8849_2020.Return();
                return8849.ReturnData = returnData;
                return8849.ReturnHeader = returnHeaderType;
                return8849.returnVersion = Variables.SoftwareVersion;

                    #endregion
                #region Create XML file

                    IFormatter ft = new BinaryFormatter();

                    ft.Serialize(mStream, return8849);

                    mStream.Position = 0;
                
                  ReturnF8849_2020.Return irsForm2 = (ReturnF8849_2020.Return)ft.Deserialize(mStream);

                    s.Serialize(fStream2, irsForm2);

                    if (mStream != null) mStream.Close();

                    if (fStream2 != null) fStream2.Close();

                    if (fStream != null) fStream.Close();

                    try
                    {
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.PreserveWhitespace = false;
                        xdoc.Load("C:\\xml\\submission.xml");
                        xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                        XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                        xdoc.Save(xmlWriter);
                        xmlWriter.Flush();
                        xmlWriter.Close();
                    }
                    catch { }

                    #endregion
                
            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
           
        }

        #region TY2021
        public void createReturnXml_8849_2021(string Tax_Year_Used_Month, string TaxYear, string Disaster_ReliefTxt, string ISP_Num,
             bool IsPreparerSelfEmployed, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                 string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                 string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                 string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                 string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                 string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                 string Officer_Title, string Officer_email, decimal RefundAmt, System.DateTime ClaimDate, string LossType,
                 string VehVin, string VehicleExplanation, string CRN, System.DateTime LatestClaimDt,
                 string SplContdDesc
             )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(ReturnF8849_2021.Return));
                #region RETRUN_HEADER

                ReturnF8849_2021.ReturnHeaderType returnHeaderType = new ReturnF8849_2021.ReturnHeaderType();
                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
                returnHeaderType.TaxYearEndMonthNum = Tax_Year_Used_Month;

                if (Disaster_ReliefTxt.Length > 0)
                    returnHeaderType.DisasterReliefTxt = Disaster_ReliefTxt;
                if (ISP_Num.Length > 0)
                    returnHeaderType.ISPNum = ISP_Num;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = ReturnF8849_2021.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = ReturnF8849_2021.ReturnHeaderTypeReturnTypeCd.Item8849;
                returnHeaderType.SignatureOptionCd = ReturnF8849_2021.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                // returnHeaderType.SignatureOptionCd = ReturnF8849_2021.ReturnHeaderTypeSignatureOptionCd.BinaryAttachment8453SignatureDocument;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                ReturnF8849_2021.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new ReturnF8849_2021.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                ReturnF8849_2021.ReturnHeaderTypeOriginatorGrp originator = new ReturnF8849_2021.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = ReturnF8849_2021.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion
                #region FILER
                ReturnF8849_2021.ReturnHeaderTypeFiler objFiler = new ReturnF8849_2021.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                //objFiler.SSN = Filer_EIN; // Optional
                //objFiler.BusinessName = Filer_Name; //max_length=60

                ReturnF8849_2021.BusinessNameType FilerBusinessName = new ReturnF8849_2021.BusinessNameType();
                FilerBusinessName.BusinessNameLine1Txt = Filer_Name;
                objFiler.EIN = Filer_EIN;
                objFiler.BusinessName = FilerBusinessName;

                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    ReturnF8849_2021.USAddressType usAdd_Filer = new ReturnF8849_2021.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = ReturnF8849_2021.StateType.AA; break;
                        case "AE": usAdd_Filer.State = ReturnF8849_2021.StateType.AE; break;
                        case "AK": usAdd_Filer.State = ReturnF8849_2021.StateType.AK; break;
                        case "AL": usAdd_Filer.State = ReturnF8849_2021.StateType.AL; break;
                        case "AP": usAdd_Filer.State = ReturnF8849_2021.StateType.AP; break;
                        case "AR": usAdd_Filer.State = ReturnF8849_2021.StateType.AR; break;
                        case "AS": usAdd_Filer.State = ReturnF8849_2021.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = ReturnF8849_2021.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = ReturnF8849_2021.StateType.CA; break;
                        case "CO": usAdd_Filer.State = ReturnF8849_2021.StateType.CO; break;
                        case "CT": usAdd_Filer.State = ReturnF8849_2021.StateType.CT; break;
                        case "DC": usAdd_Filer.State = ReturnF8849_2021.StateType.DC; break;
                        case "DE": usAdd_Filer.State = ReturnF8849_2021.StateType.DE; break;
                        case "FL": usAdd_Filer.State = ReturnF8849_2021.StateType.FL; break;
                        case "FM": usAdd_Filer.State = ReturnF8849_2021.StateType.FM; break;
                        case "GA": usAdd_Filer.State = ReturnF8849_2021.StateType.GA; break;
                        case "GU": usAdd_Filer.State = ReturnF8849_2021.StateType.GU; break;
                        case "HI": usAdd_Filer.State = ReturnF8849_2021.StateType.HI; break;
                        case "IA": usAdd_Filer.State = ReturnF8849_2021.StateType.IA; break;
                        case "ID": usAdd_Filer.State = ReturnF8849_2021.StateType.ID; break;
                        case "IL": usAdd_Filer.State = ReturnF8849_2021.StateType.IL; break;
                        case "IN": usAdd_Filer.State = ReturnF8849_2021.StateType.IN; break;
                        case "KS": usAdd_Filer.State = ReturnF8849_2021.StateType.KS; break;
                        case "KY": usAdd_Filer.State = ReturnF8849_2021.StateType.KY; break;
                        case "LA": usAdd_Filer.State = ReturnF8849_2021.StateType.LA; break;
                        case "MA": usAdd_Filer.State = ReturnF8849_2021.StateType.MA; break;
                        case "MD": usAdd_Filer.State = ReturnF8849_2021.StateType.MD; break;
                        case "ME": usAdd_Filer.State = ReturnF8849_2021.StateType.ME; break;
                        case "MH": usAdd_Filer.State = ReturnF8849_2021.StateType.MH; break;
                        case "MI": usAdd_Filer.State = ReturnF8849_2021.StateType.MI; break;
                        case "MN": usAdd_Filer.State = ReturnF8849_2021.StateType.MN; break;
                        case "MO": usAdd_Filer.State = ReturnF8849_2021.StateType.MO; break;
                        case "MP": usAdd_Filer.State = ReturnF8849_2021.StateType.MP; break;
                        case "MS": usAdd_Filer.State = ReturnF8849_2021.StateType.MS; break;
                        case "MT": usAdd_Filer.State = ReturnF8849_2021.StateType.MT; break;
                        case "NC": usAdd_Filer.State = ReturnF8849_2021.StateType.NC; break;
                        case "ND": usAdd_Filer.State = ReturnF8849_2021.StateType.ND; break;
                        case "NE": usAdd_Filer.State = ReturnF8849_2021.StateType.NE; break;
                        case "NH": usAdd_Filer.State = ReturnF8849_2021.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = ReturnF8849_2021.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = ReturnF8849_2021.StateType.NM; break;
                        case "NV": usAdd_Filer.State = ReturnF8849_2021.StateType.NV; break;
                        case "NY": usAdd_Filer.State = ReturnF8849_2021.StateType.NY; break;
                        case "OH": usAdd_Filer.State = ReturnF8849_2021.StateType.OH; break;
                        case "OK": usAdd_Filer.State = ReturnF8849_2021.StateType.OK; break;
                        case "OR": usAdd_Filer.State = ReturnF8849_2021.StateType.OR; break;
                        case "PA": usAdd_Filer.State = ReturnF8849_2021.StateType.PA; break;
                        case "PR": usAdd_Filer.State = ReturnF8849_2021.StateType.PR; break;
                        case "PW": usAdd_Filer.State = ReturnF8849_2021.StateType.PW; break;
                        case "RI": usAdd_Filer.State = ReturnF8849_2021.StateType.RI; break;
                        case "SC": usAdd_Filer.State = ReturnF8849_2021.StateType.SC; break;
                        case "SD": usAdd_Filer.State = ReturnF8849_2021.StateType.SD; break;
                        case "TN": usAdd_Filer.State = ReturnF8849_2021.StateType.TN; break;
                        case "TX": usAdd_Filer.State = ReturnF8849_2021.StateType.TX; break;
                        case "UT": usAdd_Filer.State = ReturnF8849_2021.StateType.UT; break;
                        case "VA": usAdd_Filer.State = ReturnF8849_2021.StateType.VA; break;
                        case "VI": usAdd_Filer.State = ReturnF8849_2021.StateType.VI; break;
                        case "VT": usAdd_Filer.State = ReturnF8849_2021.StateType.VT; break;
                        case "WA": usAdd_Filer.State = ReturnF8849_2021.StateType.WA; break;
                        case "WI": usAdd_Filer.State = ReturnF8849_2021.StateType.WI; break;
                        case "WV": usAdd_Filer.State = ReturnF8849_2021.StateType.WV; break;
                        case "WY": usAdd_Filer.State = ReturnF8849_2021.StateType.WY; break;
                            #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    ReturnF8849_2021.ForeignAddressType foreignAdd_Filer = new ReturnF8849_2021.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = ReturnF8849_2021.CountryType.ZI; break;
                            #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion
                #region OFFICER
                if (Officer_Name != "")
                {
                    ReturnF8849_2021.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new ReturnF8849_2021.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = ReturnF8849_2021.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = ReturnF8849_2021.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion
                #region PREPARER

                if (Preparer_Name != "")
                {
                    ReturnF8849_2021.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new ReturnF8849_2021.ReturnHeaderTypePreparerFirmGrp();
                    ReturnF8849_2021.ReturnHeaderTypePreparerPersonGrp objPreparer = new ReturnF8849_2021.ReturnHeaderTypePreparerPersonGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = ReturnF8849_2021.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = ReturnF8849_2021.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = ReturnF8849_2021.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }
                //2019 Preparer_Address not empty check 
                if (!IsPreparerSelfEmployed && Preparer_Name != "" && Preparer_Address != "")
                {
                    ReturnF8849_2021.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new ReturnF8849_2021.ReturnHeaderTypePreparerFirmGrp();
                    ReturnF8849_2021.BusinessNameType PreparerBusinessName = new ReturnF8849_2021.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        ReturnF8849_2021.USAddressType usAdd_Preparer = new ReturnF8849_2021.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = ReturnF8849_2021.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = ReturnF8849_2021.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = ReturnF8849_2021.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = ReturnF8849_2021.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = ReturnF8849_2021.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = ReturnF8849_2021.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = ReturnF8849_2021.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = ReturnF8849_2021.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = ReturnF8849_2021.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = ReturnF8849_2021.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = ReturnF8849_2021.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = ReturnF8849_2021.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = ReturnF8849_2021.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = ReturnF8849_2021.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = ReturnF8849_2021.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = ReturnF8849_2021.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = ReturnF8849_2021.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = ReturnF8849_2021.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = ReturnF8849_2021.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = ReturnF8849_2021.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = ReturnF8849_2021.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = ReturnF8849_2021.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = ReturnF8849_2021.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = ReturnF8849_2021.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = ReturnF8849_2021.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = ReturnF8849_2021.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = ReturnF8849_2021.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = ReturnF8849_2021.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = ReturnF8849_2021.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = ReturnF8849_2021.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = ReturnF8849_2021.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = ReturnF8849_2021.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = ReturnF8849_2021.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = ReturnF8849_2021.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = ReturnF8849_2021.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = ReturnF8849_2021.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = ReturnF8849_2021.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = ReturnF8849_2021.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = ReturnF8849_2021.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = ReturnF8849_2021.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = ReturnF8849_2021.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = ReturnF8849_2021.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = ReturnF8849_2021.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = ReturnF8849_2021.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = ReturnF8849_2021.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = ReturnF8849_2021.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = ReturnF8849_2021.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = ReturnF8849_2021.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = ReturnF8849_2021.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = ReturnF8849_2021.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = ReturnF8849_2021.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = ReturnF8849_2021.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = ReturnF8849_2021.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = ReturnF8849_2021.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = ReturnF8849_2021.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = ReturnF8849_2021.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = ReturnF8849_2021.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = ReturnF8849_2021.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = ReturnF8849_2021.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = ReturnF8849_2021.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = ReturnF8849_2021.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = ReturnF8849_2021.StateType.WY; break;
                                #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        ReturnF8849_2021.ForeignAddressType foreignAdd_Preparer = new ReturnF8849_2021.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = ReturnF8849_2021.CountryType.ZI; break;
                                #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion
                // returnHeaderType.TaxYr = TaxYear; //Not used for now
                returnHeaderType.binaryAttachmentCnt = "0";
                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion
                #region RETURNDATA
                ReturnF8849_2021.ReturnData returnData = new ReturnF8849_2021.ReturnData();


                #region IRS8849

                ReturnF8849_2021.IRS8849 objIRS8849 = new ReturnF8849_2021.IRS8849();
                //objIRS8849.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));
                //if (objIRS8849.BalanceDueAmt < 0)   objIRS8849.BalanceDueAmt = 0.00M;
                documentCount += 1;

                objIRS8849.softwareId = Variables.SoftwareID;
                objIRS8849.softwareVersion = Variables.SoftwareVersion;
                objIRS8849.documentId = "IRS8849"; //documentCount.ToString(); 
                objIRS8849.documentName = "IRS8849";
                objIRS8849.referenceDocumentId = new string[] { Convert.ToString(documentCount) };//new string[] { "1" };
                if (SplContdDesc.Trim().Length > 0)
                {
                    objIRS8849.SpecialConditionDesc = SplContdDesc;//"Special Condition Description" ;
                }
                objIRS8849.Schedule6AttachedInd = ReturnF8849_2021.CheckboxType.X;

                returnData.IRS8849 = objIRS8849;
                #endregion

                #region IRS 8849 SCHEDULE6

                ReturnF8849_2021.IRS8849Schedule6 irs8849Schedule6 = new ReturnF8849_2021.IRS8849Schedule6();
                documentCount += 1;
                irs8849Schedule6.documentId = "IRS8849Schedule6";//documentCount.ToString();
                irs8849Schedule6.documentName = "IRS8849Schedule6";
                irs8849Schedule6.softwareId = Variables.SoftwareID;
                irs8849Schedule6.softwareVersion = Variables.SoftwareVersion;

                irs8849Schedule6.TotalRefundAmt = Convert.ToDecimal(RefundAmt.ToString("N2"));
                irs8849Schedule6.EarliestClaimDt = ClaimDate;//DateTime.Now.ToUniversalTime(); //ClaimDate;
                irs8849Schedule6.LatestClaimDt = LatestClaimDt;//DateTime.Now.ToUniversalTime();
                //OtherTax Claim
                ReturnF8849_2021.OtherClaimNotRptOnOthFormsGrp otherClaimNotRptOnOthFormsGrp = new ReturnF8849_2021.OtherClaimNotRptOnOthFormsGrp();


                ReturnF8849_2021.OtherClaimNotRptGrp otherClaimNotRptGrp = new ReturnF8849_2021.OtherClaimNotRptGrp();

                //otherClaimNotRptOnOthFormsGrp.referenceDocumentId = new string[] { "1" };

                ReturnF8849_2021.OtherTaxClaimGrp otherTaxClaim = new ReturnF8849_2021.OtherTaxClaimGrp();
                otherTaxClaim.TaxTypeDesc = LossType;
                otherTaxClaim.VIN = VehVin;
                otherClaimNotRptGrp.OtherTaxClaimGrp = otherTaxClaim;

                otherClaimNotRptGrp.referenceDocumentId = new string[] { Convert.ToString(documentCount) };//new string[] { "1" };
                otherClaimNotRptGrp.Amt = Convert.ToDecimal(RefundAmt.ToString("N2"));
                otherClaimNotRptGrp.CreditReferenceNum = CRN; //Credit REF# TextType - 3 Maxlen

                otherClaimNotRptOnOthFormsGrp.OtherClaimNotRptGrp = otherClaimNotRptGrp;
                irs8849Schedule6.OtherClaimNotRptOnOthFormsGrp = otherClaimNotRptOnOthFormsGrp;
                returnData.IRS8849Schedule6 = irs8849Schedule6;

                #endregion

                #region Other Claims Explanation
                if (VehicleExplanation.Trim().Length > 0)
                {
                    documentCount += 1;
                    ReturnF8849_2021.OtherClaimsExplanationStmt otherClaimsExplanationStmt = new ReturnF8849_2021.OtherClaimsExplanationStmt();
                    otherClaimsExplanationStmt.documentId = documentCount.ToString(); //"OtherClaimsExplanationStatement";
                    otherClaimsExplanationStmt.documentName = "OtherClaimsExplanationStatement";
                    otherClaimsExplanationStmt.ExplanationTxt = VehicleExplanation;
                    returnData.OtherClaimsExplanationStmt = otherClaimsExplanationStmt;

                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                ReturnF8849_2021.Return return8849 = new ReturnF8849_2021.Return();
                return8849.ReturnData = returnData;
                return8849.ReturnHeader = returnHeaderType;
                return8849.returnVersion = Variables.SoftwareVersion;

                #endregion
                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return8849);

                mStream.Position = 0;

                ReturnF8849_2021.Return irsForm2 = (ReturnF8849_2021.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }

        }
        #endregion

        #region Create Manifest XML

        private void createManifestXML(string TaxYear, DateTime TaxPeriodEndDate, DateTime TaxPeriodBeginDate, string EIN, string SubmissionType)
        {
            if (!Directory.Exists("C:\\" + @"\manifest"))
                Directory.CreateDirectory("C:\\" + @"\manifest");

            XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\manifest\manifest.xml", new UTF8Encoding(false));

            xmlWriter.WriteStartDocument(true);

            xmlWriter.WriteStartElement("IRSSubmissionManifest");

            xmlWriter.WriteAttributeString("xmlns", "http://www.irs.gov/efile");

            xmlWriter.WriteAttributeString("xmlns:efile", "http://www.irs.gov/efile");

            //------------

            xmlWriter.WriteStartElement("SubmissionId");

            submissionID = "";

            BaseMethods BM = new BaseMethods();
            submissionID = Variables.EFIN + DateTime.Now.Year.ToString() + BM.getJulianDayOfYear() + BM.CreateRandomCode(7);

            xmlWriter.WriteString(submissionID);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("EFIN");

            xmlWriter.WriteString(Variables.EFIN);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TaxYr");

            xmlWriter.WriteString(TaxYear);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("GovernmentCd");

            xmlWriter.WriteString("IRS");

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("FederalSubmissionTypeCd");

            xmlWriter.WriteString(SubmissionType);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TaxPeriodBeginDt");
            xmlWriter.WriteString(TaxPeriodBeginDate.ToString("yyyy-MM-dd"));

            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodEndDt");
            xmlWriter.WriteString(TaxPeriodEndDate.ToString("yyyy-MM-dd"));

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TIN");

            xmlWriter.WriteString(EIN);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndDocument();

            xmlWriter.Flush();

            xmlWriter.Close();


        }

        public string createManifestXMLFile(string TaxYear, DateTime TaxPeriodEndDate, DateTime TaxPeriodBeginDate, string EIN, string SubmissionType)
        {
            string temp_submission_id = "";

            if (!Directory.Exists("C:\\" + @"\manifest"))
                Directory.CreateDirectory("C:\\" + @"\manifest");

            XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\manifest\manifest.xml", new UTF8Encoding(false));
            xmlWriter.WriteStartDocument(true);
            xmlWriter.WriteStartElement("IRSSubmissionManifest");
            xmlWriter.WriteAttributeString("xmlns", "http://www.irs.gov/efile");
            xmlWriter.WriteAttributeString("xmlns:efile", "http://www.irs.gov/efile");

            xmlWriter.WriteStartElement("SubmissionId");

            BaseMethods BM = new BaseMethods();
            temp_submission_id = Variables.EFIN + DateTime.Now.Year.ToString() + BM.getJulianDayOfYear() + BM.CreateRandomCode(7);

            xmlWriter.WriteString(temp_submission_id);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("EFIN");
            xmlWriter.WriteString(Variables.EFIN);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxYr");
            xmlWriter.WriteString(TaxYear);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("GovernmentCd");
            xmlWriter.WriteString("IRS");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("FederalSubmissionTypeCd");
            xmlWriter.WriteString(SubmissionType);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodBeginDt");
            xmlWriter.WriteString(TaxPeriodBeginDate.ToString("yyyy-MM-dd"));
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodEndDt");
            xmlWriter.WriteString(TaxPeriodEndDate.ToString("yyyy-MM-dd"));
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TIN");
            xmlWriter.WriteString(EIN);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Flush();
            xmlWriter.Close();

            return temp_submission_id;
        }

        #endregion

        static string createZip()
        {
            try
            {
                string SavePath = @"C:\";

                ZipFile TransmissionZip = ZipFile.Create(SavePath + "SendSubmission" + submissionID + ".zip");//name verification pending
                ZipFile SubmissionZip = ZipFile.Create(SavePath + submissionID + ".zip");
                SubmissionZip.BeginUpdate();
                SubmissionZip.Add(SavePath + "\\manifest\\manifest.xml");
                SubmissionZip.Add(SavePath + "\\xml\\Submission.xml");
                SubmissionZip.CommitUpdate();
                SubmissionZip.Close();

                File.Delete(path2CreateFile_4SendSubmission + @"\manifest\manifest.xml");
                File.Delete(path2CreateFile_4SendSubmission + @"\xml\Submission.xml");
                Directory.Delete(path2CreateFile_4SendSubmission + @"\manifest");
                Directory.Delete(path2CreateFile_4SendSubmission + @"\xml");
                TransmissionZip.BeginUpdate();
                TransmissionZip.Add(path2CreateFile_4SendSubmission + submissionID + ".zip", CompressionMethod.Stored);
                TransmissionZip.CommitUpdate();
                TransmissionZip.Close();
                File.Delete(path2CreateFile_4SendSubmission + submissionID + ".zip");

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public void MoveFiles(string SubmissionId, string ReferenceNumber)
        {
            try
            {
                if (!Directory.Exists("C:\\Live Submissions\\8849\\" + ReferenceNumber))
                    Directory.CreateDirectory("C:\\Live Submissions\\8849\\" + ReferenceNumber);

                string SubmissionFileName = "", ReturnSubmissionFileName = "", GetAckFileName = "", GetSchedule1FileName = "",
                    PathToSave = "C:\\Live Submissions\\8849\\" + ReferenceNumber + "\\";

                SubmissionFileName = "SendSubmission" + SubmissionId + ".zip";
                ReturnSubmissionFileName = "RetrunSubmission" + SubmissionId + ".zip";
                GetAckFileName = SubmissionId + "-Ack.zip";
                GetSchedule1FileName = SubmissionId + "-Sch1.zip";

                if (File.Exists("C:\\" + SubmissionFileName)
                    && File.Exists("C:\\" + ReturnSubmissionFileName)
                    && File.Exists("C:\\" + GetAckFileName)
                    && File.Exists("C:\\" + GetSchedule1FileName)
                    && File.Exists("C:\\" + SubmissionId + ".pdf"))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                    File.Move("C:\\" + GetAckFileName, PathToSave + GetAckFileName);
                    File.Move("C:\\" + GetSchedule1FileName, PathToSave + GetSchedule1FileName);
                    File.Move("C:\\" + SubmissionId + ".pdf", PathToSave + SubmissionId + ".pdf");
                }
                else if (File.Exists("C:\\" + SubmissionFileName)
                    && File.Exists("C:\\" + ReturnSubmissionFileName)
                    && File.Exists("C:\\" + GetAckFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                    File.Move("C:\\" + GetAckFileName, PathToSave + GetAckFileName);
                }
                else if (File.Exists("C:\\" + GetSchedule1FileName) && File.Exists("C:\\" + SubmissionId + ".pdf"))
                {
                    File.Move("C:\\" + GetSchedule1FileName, PathToSave + GetSchedule1FileName);
                    File.Move("C:\\" + SubmissionId + ".pdf", PathToSave + SubmissionId + ".pdf");
                }
                else if (File.Exists("C:\\" + SubmissionFileName) && File.Exists("C:\\" + ReturnSubmissionFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                }
                else if (File.Exists("C:\\" + SubmissionFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                }

            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
            }
        }

    }
}