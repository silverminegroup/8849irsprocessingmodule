using System.Collections;

using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;

using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using Microsoft.Web.Services3.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using IRSLibrary.Utilities;
using System;
using System.ServiceModel;
using Microsoft.Web.Services3.Security.Cryptography;
using System.Security.Cryptography;
using System.Xml;

public class RSAPKCS1SHA256SignatureDescription : SignatureDescription
{
    /// <summary>
    /// Registers the http://www.w3.org/2001/04/xmldsig-more#rsa-sha256 algorithm
    /// with the .NET CrytoConfig registry. This needs to be called once per
    /// appdomain before attempting to validate SHA256 signatures.
    /// </summary>
    public static void Register()
    {
        string[] algoupd = { @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha384" };
        CryptoConfig.AddAlgorithm(
            typeof(RSAPKCS1SHA256SignatureDescription), algoupd);

        CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription),
                            "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

        //CryptoConfig.AddAlgorithm(
        //    typeof(RSAPKCS1SHA256SignatureDescription),
        //    @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
    }

    /// <summary>
    /// .NET calls this parameterless ctor
    /// </summary>
    public RSAPKCS1SHA256SignatureDescription()
    {
        KeyAlgorithm = "System.Security.Cryptography.RSACryptoServiceProvider";
        DigestAlgorithm = "System.Security.Cryptography.SHA256Managed";
        FormatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureFormatter";
        DeformatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureDeformatter";
    }

    public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
    {
        var asymmetricSignatureDeformatter =
            (AsymmetricSignatureDeformatter)CryptoConfig.CreateFromName(DeformatterAlgorithm);
        asymmetricSignatureDeformatter.SetKey(key);
        asymmetricSignatureDeformatter.SetHashAlgorithm("SHA256");
        return asymmetricSignatureDeformatter;
    }

    public override System.Security.Cryptography.AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
    {
        var asymmetricSignatureFormatter =
            (System.Security.Cryptography.AsymmetricSignatureFormatter)CryptoConfig.CreateFromName(FormatterAlgorithm);
        asymmetricSignatureFormatter.SetKey(key);
        asymmetricSignatureFormatter.SetHashAlgorithm("SHA256");
        return asymmetricSignatureFormatter;
    }
}


public class FS_Msg_Signer
{
	public FS_Msg_Signer()
	{
        RSAPKCS1SHA256SignatureDescription.Register();
    }

    private static X509SecurityToken GetSigningToken_SubName(string keyIdentifier)
    {
        X509SecurityToken token = null;
        X509Store store = new X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation.LocalMachine);
        try
        {

            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            X509Certificate2Collection certs = X509Util.FindCertificateBySubjectName(keyIdentifier, StoreLocation.LocalMachine, "MY");

            IEnumerator certEnum;
            certEnum = certs.GetEnumerator();
            certEnum.MoveNext();

            if (certs.Count > 0)
            {
                token = new X509SecurityToken(certs[0]);

            }
        }
        finally
        {
            if (store != null)
                store.Close();
        }
        return token;
    } 

    private static X509SecurityToken GetSigningToken(string keyIdentifier)
    {
        X509SecurityToken token =null;
        X509Store store=new X509Store(System.Security.Cryptography.X509Certificates.StoreName.My,StoreLocation.LocalMachine);
        
        try
        {
        
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
            RSAPKCS1SHA256SignatureDescription.Register();
            X509Certificate2Collection certs = X509Util.FindCertificateByKeyIdentifier(System.Convert.FromBase64String(keyIdentifier), StoreLocation.LocalMachine, "My");

		    IEnumerator certEnum ;
            certEnum = certs.GetEnumerator();
            certEnum.MoveNext();

            if (certs.Count > 0)
            { 
                token = new X509SecurityToken(certs[0]);			
            }
        }
        finally
        {
            if (store != null)
                store.Close();
        }
        return token;
    } 


    private static byte[] ConvertHexToByteArray(string hexString)
    {
        int i = 0;
        int j = 0;
        byte[] bytes = new byte[(hexString.Length / 2)];
        string hex = null;
        char[] hexCharArray = null;

        hexCharArray = hexString.ToCharArray();
        j = 0;
        for (i = 0; i <= (bytes.Length - 1); i++)
        {
            hex = new string(new char[] { hexCharArray[j], hexCharArray[j + 1] });
            bytes[i] = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            j = j + 2;
        }
        return bytes;
    }

    public static void Sign2222(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {

        CustomBinding cbinding = new CustomBinding();
        var sec = (AsymmetricSecurityBindingElement)SecurityBindingElement.CreateMutualCertificateBindingElement
        (System.ServiceModel.MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10);

        sec.EndpointSupportingTokenParameters.Signed.Add(new X509SecurityTokenParameters());
        sec.MessageSecurityVersion = System.ServiceModel.MessageSecurityVersion.WSSecurity10WSTrust13WSSecureConversation13WSSecurityPolicy12BasicSecurityProfile10;
        sec.EnableUnsecuredResponse = true;
        sec.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
        sec.IncludeTimestamp = false;
        cbinding.Elements.Add(sec);
    }

    public static void Sign(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            AsymmetricSecurityBindingElement asbe = null;
            asbe = SecurityBindingElement.CreateMutualCertificateBindingElement(MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10, true) as AsymmetricSecurityBindingElement;
            asbe.DefaultAlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic128Sha256Rsa15;

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }

    public static void Sign113(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;

            AsymmetricSecurityBindingElement asbe = null;
            asbe = SecurityBindingElement.CreateMutualCertificateBindingElement(MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10, true) as AsymmetricSecurityBindingElement;
            asbe.DefaultAlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic128Sha256Rsa15;

            SignatureReference soapRef = new SignatureReference(sigRef);

            //XmlNameTable namTab;
            //XmlNamespaceManager namesp = new XmlNamespaceManager(namTab);
            //namesp.AddNamespace("Algorithm", "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
           // soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigXPathTransform("SignatureMethod", namesp));
            soapRef.DigestMethod = XmlEncryption.AlgorithmURI.SHA256;
            

            string signatureRSAMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

            token = GetSigningToken(KeyIdentifier);
            //((System.Security.Cryptography.RSACryptoServiceProvider)((Microsoft.Web.Services3.Security.Cryptography.RSA)token.Key)._key).SignatureAlgorithm = signatureRSAMethod;
            RSAPKCS1SHA256SignatureDescription.Register();
            proxy.RequestSoapContext.Security.Tokens.Add(token);
            RSAPKCS1SHA256SignatureDescription.Register();
            sig = new MessageSignature(token);

            sig.SignedInfo.SignatureMethod = signatureRSAMethod;
            //sig.SignatureMethod = signatureRSAMethod;

            //((System.Security.Cryptography.RSACryptoServiceProvider)((Microsoft.Web.Services3.Security.Cryptography.RSASHA1SignatureFormatter)((Microsoft.Web.Services3.Security.Cryptography.RSA)token.Key).SignatureFormatter).Key).SignatureAlgorithm =
            //Microsoft.Web.Services3.Security.Cryptography.KeyAlgorithm.VerifyValidAlgorithm(signatureRSAMethod);
            //sig.SigningKey.KeyExchangeFormatter.AlgorithmURI = signatureRSAMethod;

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }

    public static void Sign_12Test(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;

            //RSAPKCS1SHA256SignatureDescription.Register();

            SignatureReference soapRef = new SignatureReference(sigRef);

            ///RSAPKCS1SHA256SignatureDescription soapRef = new RSAPKCS1SHA256SignatureDescription();
            ///RSAPKCS1SHA256SignatureDescription.Register();

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            soapRef.DigestMethod = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
            

            token = GetSigningToken(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            //sig.SignatureOptions =  Basic256SecurityAlgorithmSuite.Basic192Sha256Rsa15;

          
            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }


    public static void Sign_NEWTRY(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            /*
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);

            */

            CustomBinding cbinding = new CustomBinding();
            var sec = (AsymmetricSecurityBindingElement)SecurityBindingElement.CreateMutualCertificateBindingElement
            (System.ServiceModel.MessageSecurityVersion.WSSecurity10WSTrust13WSSecureConversation13WSSecurityPolicy12BasicSecurityProfile10, true);

            sec.EndpointSupportingTokenParameters.Signed.Add(new X509SecurityTokenParameters());
            sec.MessageSecurityVersion = System.ServiceModel.MessageSecurityVersion.WSSecurity10WSTrust13WSSecureConversation13WSSecurityPolicy12BasicSecurityProfile10;
            sec.EnableUnsecuredResponse = true;
            sec.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
            sec.IncludeTimestamp = false;
            cbinding.Elements.Add(sec);

            var tme = new TextMessageEncodingBindingElement(MessageVersion.Soap11, System.Text.Encoding.UTF8);
            cbinding.Elements.Add(tme);

            //start communication
            EndpointAddress address = new EndpointAddress(
            new Uri(IRSLibrary.ENDPOINT.endpoint_address + "Login"));

            var https = new HttpsTransportBindingElement();
            https.RequireClientCertificate = true;
            cbinding.Elements.Add(https);

            ChannelFactory<MessageEncoderFactory> factory = new ChannelFactory<MessageEncoderFactory>(cbinding, address);


            //X509Store store = new X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation.LocalMachine);
            //store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
            //X509Certificate2Collection certs = X509Util.FindCertificateByKeyIdentifier(System.Convert.FromBase64String(KeyIdentifier), StoreLocation.LocalMachine, "My");

            factory.Credentials.ClientCertificate.SetCertificate(StoreLocation.LocalMachine, StoreName.My,
            X509FindType.FindBySubjectKeyIdentifier, System.Convert.FromBase64String(KeyIdentifier));

            factory.Credentials.ServiceCertificate.SetDefaultCertificate(StoreLocation.LocalMachine,
                                                    StoreName.My,
                                                    X509FindType.FindBySubjectKeyIdentifier,
                                                    System.Convert.FromBase64String(KeyIdentifier));


        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }

    public static void Sign111(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }

    public static void Sign_SubName(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken_SubName(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    } 

}
