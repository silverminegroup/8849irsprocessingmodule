using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using Microsoft.VisualBasic;



namespace IRSLibrary
{
    public class DBStorage
    {
        public SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["smConnectionString"]);
        private static MemoryStream oStream;

        #region Methods
        public static MemoryStream OStream
        {
            get { return DBStorage.oStream; }
            set { DBStorage.oStream = value; }
        }
        #endregion

        public DBStorage()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public DataSet SelectQuery(string query)
        {
            
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                if(con.State!=ConnectionState.Open)
                    con.Open();
                da.Fill(ds);
            }
            catch { }
            finally { con.Close(); }
            return ds;

        }
        public DataTable getDatatable(string query, string TableName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dt.TableName = TableName;
            return dt;
        }
        public int UpdatePdf(string query, byte[] content)
        {
            int i;

            try
            {
                con.Open();
                SqlCommand command = new SqlCommand(query, con);
                SqlParameter parameter = new SqlParameter();
                command.CommandText = query;
                command.CommandType = System.Data.CommandType.Text;

                parameter = new System.Data.SqlClient.SqlParameter("@Data", System.Data.SqlDbType.VarBinary);
                parameter.Value = content;
                command.Parameters.Add(parameter);

                i = command.ExecuteNonQuery();
                return i;

            }
            catch
            {
                return 0;
            }

        }

        public byte[] SelectPdf(string query)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            byte[] buffer = (byte[])cmd.ExecuteScalar();
            con.Close();
            return buffer;
        }

        public int InsertUpdateDelete(string query)
        {
            int i;
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);
                i = cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { }
            return false;
        }
        public DataTable GetDataTable(SqlCommand cmd, string TableName)
        {
            DataTable table;
            try
            {
                cmd.Connection = con;
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable(TableName);
                adapter.Fill(dataTable);
                adapter.Dispose();
                cmd.Dispose();
                con.Close();
                // con = null;
                table = dataTable;
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                if (con != null)
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                }
                throw exception;
            }
            return table;
        }
        public DataSet GetDataSet(SqlCommand cmd, string dsName)
        {
            DataSet table;
            try
            {
                cmd.Connection = con;
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet(dsName);
                adapter.Fill(ds);
                adapter.Dispose();
                cmd.Dispose();
                con.Close();
                table = ds;
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                if (con != null)
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                }
                throw exception;
            }
            return table;
        }

        public string ExecuteScalar(SqlCommand cmd)
        {
            string objectValue;
            try
            {
                cmd.Connection = con;
                con.Open();
                objectValue = Convert.ToString(cmd.ExecuteScalar());
                cmd.Dispose();
                con.Close();
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                if (con != null)
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                }
                throw exception;
            }
            return objectValue;
        }

        public int ExecuteNonQuery(SqlCommand cmd)
        {
            int num;
            try
            {
                cmd.Connection = this.con;
                num = cmd.ExecuteNonQuery();
                cmd.Dispose();
                this.con.Close();
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                if (this.con != null)
                {
                    if (this.con.State != ConnectionState.Closed)
                    {
                        this.con.Close();
                    }
                    this.con = null;
                }
                throw exception;
            }
            return num;
        }

    }
}