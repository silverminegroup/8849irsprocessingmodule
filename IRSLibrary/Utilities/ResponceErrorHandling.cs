using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace IRSLibrary
{
    public class LoginErrHandling : MefMsiServices.Login
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class LogoutErrorHandling : MefMsiServices.Logout
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class GetAckErrorHandling : MeFTransmitterServiceWse.GetAck
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class GetAcksErrorHandling : MeFTransmitterServiceWse.GetAcks
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class GetNewAcksErrorHandling : MeFTransmitterServiceWse.GetNewAcks
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }

    public class GetNewSubmissionsStatusErrorHandling : MeFTransmitterServiceWse.GetNewSubmissionsStatus
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class SubmissionErrorHandling : MeFTransmitterServiceWse.SendSubmissions
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class Schedule1ErrorHandling : ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
    public class Schedule1sErrorHandling : ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1s
    {
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            if (response.Headers[HttpResponseHeader.ContentType].ToLower().StartsWith("text/xml"))
            {
                this.RequireMtom = false;
            }

            return response;
        }
    }
}
