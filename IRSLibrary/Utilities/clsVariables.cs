using System;
using System.Configuration;


public class clsVariables
{
    public string TestIndicator;
    public string ETIN;
    public string PIN;
    public string MeF_AppSysID;
    public string EFIN;
    public string SoftwareID;
    public string SoftwareVersion;

    public string path2CreateZipFile;

    public clsVariables(string taxYear)
    {
        TestIndicator = ConfigurationSettings.AppSettings["TestIndicator"].ToString();

        MeF_AppSysID = "06434601";
        EFIN = "064346";
        path2CreateZipFile = @"C:\";

        if (TestIndicator == "T")
        {
            ETIN = "24665";
            PIN = "24665";
        }
        else if (TestIndicator == "P")
        {
            ETIN = "25093";
            PIN = "25093";
        }

        if(taxYear == "2021")
        {
            SoftwareID = "21011439";
            SoftwareVersion = "2021v2.0";
        }
        if(taxYear == "2020")
        {
            SoftwareID = "20010824";
            SoftwareVersion = "2020v1.1";
        }
        if (taxYear == "2019")
        {
            SoftwareID = "20010824";
            SoftwareVersion = "2020v1.1";
        }
        if (taxYear == "2018")
        {
            SoftwareID = "20010824";
            SoftwareVersion = "2020v1.1";
        }

    }
}