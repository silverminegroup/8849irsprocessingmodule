 
#pragma warning disable 1591

namespace IRSLibrary.MefMsiServices
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
  
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "Logout", Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices")]
    public partial class Logout : Microsoft.Web.Services3.WebServicesClientProtocol
    {

        private MeFHeaderType meFField;

        private System.Threading.SendOrPostCallback CallLogoutOperationCompleted;

        private bool useDefaultCredentialsSetExplicitly;

        /// <remarks/>
        public Logout()
        {
            //this.Url = "https://la.www4.irs.gov/a2a/mef/Logout";
            this.Url = ENDPOINT.endpoint_address+ "/Logout";
            if ((this.IsLocalFileSystemWebService(this.Url) == true))
            {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else
            {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        public MeFHeaderType MeFHeader
        {
            get
            {
                return this.meFField;
            }
            set
            {
                this.meFField = value;
            }
        }

        public new string Url
        {
            get
            {
                return base.Url;
            }
            set
            {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true)
                            && (this.useDefaultCredentialsSetExplicitly == false))
                            && (this.IsLocalFileSystemWebService(value) == false)))
                {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }

        public new bool UseDefaultCredentials
        {
            get
            {
                return base.UseDefaultCredentials;
            }
            set
            {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        /// <remarks/>
        public event CallLogoutCompletedEventHandler CallLogoutCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MeFHeader", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.irs.gov/a2a/mef/Logout", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("LogoutResponse", Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
        public LogoutResponseType CallLogout([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")] LogoutRequestType LogoutRequest)
        {
            object[] results = this.Invoke("CallLogout", new object[] {
                        LogoutRequest});
            return ((LogoutResponseType)(results[0]));
        }

        /// <remarks/>
        public void CallLogoutAsync(LogoutRequestType LogoutRequest)
        {
            this.CallLogoutAsync(LogoutRequest, null);
        }

        /// <remarks/>
        public void CallLogoutAsync(LogoutRequestType LogoutRequest, object userState)
        {
            if ((this.CallLogoutOperationCompleted == null))
            {
                this.CallLogoutOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCallLogoutOperationCompleted);
            }
            this.InvokeAsync("CallLogout", new object[] {
                        LogoutRequest}, this.CallLogoutOperationCompleted, userState);
        }

        private void OnCallLogoutOperationCompleted(object arg)
        {
            if ((this.CallLogoutCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CallLogoutCompleted(this, new CallLogoutCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        private bool IsLocalFileSystemWebService(string url)
        {
            if (((url == null)
                        || (url == string.Empty)))
            {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024)
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0)))
            {
                return true;
            }
            return false;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "Login", Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices")]
    public partial class Login : Microsoft.Web.Services3.WebServicesClientProtocol
    {

        private MeFHeaderType meFField;

        private System.Threading.SendOrPostCallback CallLoginOperationCompleted;

        private bool useDefaultCredentialsSetExplicitly;

        /// <remarks/>
        public Login()
        {
            //this.Url = "https://la.www4.irs.gov/a2a/mef/Login";
            this.Url = ENDPOINT.endpoint_address+ "/Login";
            if ((this.IsLocalFileSystemWebService(this.Url) == true))
            {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else
            {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        public MeFHeaderType MeFHeader //MeF
        {
            get
            {
                return this.meFField;
            }
            set
            {
                this.meFField = value;
            }
        }

        public new string Url
        {
            get
            {
                return base.Url;
            }
            set
            {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true)
                            && (this.useDefaultCredentialsSetExplicitly == false))
                            && (this.IsLocalFileSystemWebService(value) == false)))
                {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }

        public new bool UseDefaultCredentials
        {
            get
            {
                return base.UseDefaultCredentials;
            }
            set
            {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        /// <remarks/>
        public event CallLoginCompletedEventHandler CallLoginCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MeFHeader", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.irs.gov/a2a/mef/Login", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("LoginResponse", Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
        public LoginResponseType CallLogin([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")] LoginRequestType LoginRequest)
        {
            object[] results = this.Invoke("CallLogin", new object[] {
                        LoginRequest});
            return ((LoginResponseType)(results[0]));
        }

        /// <remarks/>
        public void CallLoginAsync(LoginRequestType LoginRequest)
        {
            this.CallLoginAsync(LoginRequest, null);
        }

        /// <remarks/>
        public void CallLoginAsync(LoginRequestType LoginRequest, object userState)
        {
            if ((this.CallLoginOperationCompleted == null))
            {
                this.CallLoginOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCallLoginOperationCompleted);
            }
            this.InvokeAsync("CallLogin", new object[] {
                        LoginRequest}, this.CallLoginOperationCompleted, userState);
        }

        private void OnCallLoginOperationCompleted(object arg)
        {
            if ((this.CallLoginCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CallLoginCompleted(this, new CallLoginCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        private bool IsLocalFileSystemWebService(string url)
        {
            if (((url == null)
                        || (url == string.Empty)))
            {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024)
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0)))
            {
                return true;
            }
            return false;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFHeader.xsd")]
    [System.Xml.Serialization.XmlRootAttribute("MeFHeader", Namespace = "http://www.irs.gov/a2a/mef/MeFHeader.xsd", IsNullable = false)]
    public partial class MeFHeaderType : System.Web.Services.Protocols.SoapHeader
    {        
        private string messageIDField;

        private string relatesToField;

        private string actionField;

        private System.DateTime timestampField;

        private string eTINField;

        /*9.5*/
        private string clientSoftwareTxt;
        
        /*9.5*/
        private SessionKeyCdType sessionIndicatorField;

        private bool sessionIndicatorFieldSpecified;

        /*9.5*/
        private TestCdType testIndicatorField;

        private bool testIndicatorFieldSpecified;

        private MeFNotificationResponse notificationResponseField;

        private string appSysIDField;

        //-----
        //new MeF changes in MeFHeader.xsd v9.0
        private string wsdlVersionField;
        //-----

        private string idField;

        private System.Xml.XmlAttribute[] anyAttrField;

        /// <remarks/>
        public string MessageID
        {
            get
            {
                return this.messageIDField;
            }
            set
            {
                this.messageIDField = value;
            }
        }

        /// <remarks/>
        public string RelatesTo
        {
            get
            {
                return this.relatesToField;
            }
            set
            {
                this.relatesToField = value;
            }
        }

        /// <remarks/>
        public string Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /*new wsdl change in MeFHeader.xsd 9.5*/
        /// <remarks/>
        public System.DateTime MessageTs /*Timestamp*/
        {
            get
            {
                return this.timestampField;
            }
            set
            {
                this.timestampField = value;
            }
        }

        /// <remarks/>
        public string ETIN
        {
            get
            {
                return this.eTINField;
            }
            set
            {
                this.eTINField = value;
            }
        }

        /*new wsdl change in MeFHeader.xsd 9.5*/
        /// <remarks/>
        public SessionKeyCdType SessionKeyCd/* SessionIndicatorType SessionIndicator*/
        {
            get
            {
                return this.sessionIndicatorField;
            }
            set
            {
                this.sessionIndicatorField = value;
            }
        }
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SessionIndicatorSpecified
        {
            get
            {
                return this.sessionIndicatorFieldSpecified;
            }
            set
            {
                this.sessionIndicatorFieldSpecified = value;
            }
        }*/

        ///9.5 <remarks/>
        public TestCdType  TestCd
        {
            get
            {
                return this.testIndicatorField;
            }
            set
            {
                this.testIndicatorField = value;
            }
        }

        ///9.5 <remarks/>
        /*
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TestIndicatorSpecified
        {
            get
            {
                return this.testIndicatorFieldSpecified;
            }
            set
            {
                this.testIndicatorFieldSpecified = value;
            }
        }*/

        /// <remarks/>
        public MeFNotificationResponse NotificationResponse
        {
            get
            {
                return this.notificationResponseField;
            }
            set
            {
                this.notificationResponseField = value;
            }
        }

        /// <remarks/>
        public string AppSysID
        {
            get
            {
                return this.appSysIDField;
            }
            set
            {
                this.appSysIDField = value;
            }
        }

        //-----
        //new MeF changes in MeFHeader.xsd v9.0
        /*9.5*/
        public string WSDLVersionNum
        {
            get
            {
                return this.wsdlVersionField;
            }
            set
            {
                this.wsdlVersionField = value;
            }
        }
        //-----

        /*9.5*/
        public string ClientSoftwareTxt
        {
            get
            {
                return this.clientSoftwareTxt;
            }
            set
            {
                this.clientSoftwareTxt = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr
        {
            get
            {
                return this.anyAttrField;
            }
            set
            {
                this.anyAttrField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFHeader.xsd")]
    public enum SessionKeyCdType/*SessionIndicatorType*/
    {

        /// <remarks/>
        Y,

        /// <remarks/>
        N,
    }

    ///9.5 <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFHeader.xsd")]
    public enum TestCdType/*TestIndicatorType*/
    {

        /// <remarks/>
        T,

        /// <remarks/>
        P,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFHeader.xsd")]
    public partial class MeFNotificationResponse
    {

        private string notificationTypeField;

        private System.DateTime applicableDateField;

        /// <remarks/>
        public string NotificationType
        {
            get
            {
                return this.notificationTypeField;
            }
            set
            {
                this.notificationTypeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ApplicableDate
        {
            get
            {
                return this.applicableDateField;
            }
            set
            {
                this.applicableDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
    public partial class LoginResponseType
    {

        private string statusField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
    public partial class LoginRequestType
    {
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
    public partial class LogoutResponseType
    {

        private string statusField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
    public partial class LogoutRequestType
    {
    }

   
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.5420")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(TypeName = "TestIndicatorType", Namespace = "http://www.irs.gov/a2a/mef/MeFMSIServices.xsd")]
    public enum TestIndicatorType1
    {

        /// <remarks/>
        T,

        /// <remarks/>
        P,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    public delegate void CallLogoutCompletedEventHandler(object sender, CallLogoutCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CallLogoutCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal CallLogoutCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public LogoutResponseType Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((LogoutResponseType)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    public delegate void CallLoginCompletedEventHandler(object sender, CallLoginCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.5420")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CallLoginCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal CallLoginCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public LoginResponseType Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((LoginResponseType)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591