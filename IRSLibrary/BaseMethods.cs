using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Mime;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using ICSharpCode.SharpZipLib.Zip;
using IRSLibrary.MeFTransmitterServiceWse;
using IRSLibrary.MefMsiServices;
using IRSLibrary.ETECTransmitterServiceMTOM_SeparateServices;
using IRSLibrary.Utilities;
using System.IO;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Collections;
using System.Linq;

using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

using MeF.Client;
using MeF.Client.Services.MSIServices;
using MeF.Client.Services.ETECTransmitterServices;
using MeF.Client.Services.StateServices;
using MeF.Client.Services.TransmitterServices;
using MeF.Client.Services.InputComposition;
using SilverMine.IRSLibrary;

namespace IRSLibrary
{

    public class BaseMethods
    {
        static string path2CreateFile_4GetAck = "C:\\";

        static string pathResponseFile = "C:\\Live Submissions\\ResponseExtractFolder\\";

        private bool LoggedInstatus;

        private static ServiceContext context = new ServiceContext();

        private static IMEFWrapper _baseWrapper = null;

        public string ackFile_Status = "";

        private static IMEFWrapper baseWrapper
        {
            get
            {
                if (_baseWrapper == null)
                    _baseWrapper = new MEFWrapper(Variables.ETIN, Variables.MeF_AppSysID, Variables.TestIndicator);

                return _baseWrapper;
            }

        }

        public bool LoggedInStatus
        {
            get { return LoggedInstatus; }
            set { LoggedInstatus = value; }
        }

        private static IRSLibrary.LoginErrHandling req_Login;

        public static IRSLibrary.LoginErrHandling Req_Login
        {
            get { return BaseMethods.req_Login; }
            set { BaseMethods.req_Login = value; }
        }

        public string getMessageID()
        {
            string abc = (Variables.ETIN + DateTime.Now.Year + getJulianDayOfYear() + CreateRandomCode(8));
            return abc;
        }

        public string CreateRandomCode(int codeCount)
        {
            System.Byte[] seedBuffer = new System.Byte[4];
            using (var rngCryptoServiceProvider = new System.Security.Cryptography.RNGCryptoServiceProvider())
            {
                rngCryptoServiceProvider.GetBytes(seedBuffer);
                System.String chars = "0123456789";//abcdefghijklmnopqrstuvwxyz";
                System.Random random = new System.Random(System.BitConverter.ToInt32(seedBuffer, 0));
                return new System.String(Enumerable.Repeat(chars, codeCount).Select(s => s[random.Next(s.Length)]).ToArray());

            }
        }

        public string getJulianDayOfYear()
        {
            int dt = 0;

            for (int i = 1; i < DateTime.Now.Month; i++)
                dt += DateTime.DaysInMonth(DateTime.Now.Year, i);

            dt += DateTime.Now.Day;

            if (Convert.ToString(dt).Length == 2)
                return ("0" + dt.ToString());
            else if (Convert.ToString(dt).Length == 1)
                return ("00" + dt.ToString());
            else
                return (dt.ToString());
        }

        public string _Login()
        {
            try
            {
                string result = baseWrapper.Login();

                SecToken.isSessionPresent = true;

                LoggedInStatus = true;
                return "Success";

            }
            catch (Exception ex)
            {
                SecToken.isSessionPresent = false;

                LoggedInStatus = false;
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                LoggedInStatus = false;
                return "Exception";
            }
            
        }

        public string _Login_MEFWCF()
        { 
            try
            { 
                context = new ServiceContext(new ClientInfo(Variables.ETIN, Variables.MeF_AppSysID));

                var client = new LoginClient();
                var result = client.Invoke(context);

                SecToken.isSessionPresent = true;

                LoggedInStatus = true;

                return "Success";
            }
            catch (Exception ex)
            {
                SecToken.isSessionPresent = false;

                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                LoggedInStatus = false;
                return "Exception";
            }

        }
       
        public string _Login_WSE3()
        {
            try
            {

                Req_Login = new LoginErrHandling();

                LoginRequestType reqType_Login = new LoginRequestType();

                LoginResponseType resType_Login = new LoginResponseType();

                MefMsiServices.MeFHeaderType MeFHeader = new MefMsiServices.MeFHeaderType();
                
                MeFHeader.Id = "myTestMeF";
                MeFHeader.MessageID = getMessageID();
                MeFHeader.WSDLVersionNum = Variables.WSDLVerNum;
                MeFHeader.Action = "Login";
                MeFHeader.MessageTs = DateTime.Now;
                MeFHeader.ETIN = Variables.ETIN;

                //MeF.SessionIndicator = MefMsiServices.SessionIndicatorType.N;
                
                MeFHeader.SessionKeyCd = MefMsiServices.SessionKeyCdType.Y;

                if(Variables.TestIndicator=="T")
                {
                    MeFHeader.TestCd = MefMsiServices.TestCdType.T;
                }
                else
                {
                    MeFHeader.TestCd = MefMsiServices.TestCdType.P;
                }
                MeFHeader.AppSysID =Variables.MeF_AppSysID;

                req_Login.MeFHeader = MeFHeader;

                Microsoft.Web.Services3.WebServicesClientProtocol proxy = req_Login;

                FS_Msg_Signer.Sign(ref proxy, "#myTestMeF", Variables.CertificateKey);

                /* Vishwa 2016 New Changes Vishwa 2016 Cert Added this, if we can get by subjectName as well */
                //FS_Msg_Signer.Sign_SubName(ref proxy, "#myTestMeF", Variables.CertificateKey_CN);

                AsymmetricSecurityBindingElement asbe = null;
                asbe = SecurityBindingElement.CreateMutualCertificateBindingElement(MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10, true) as AsymmetricSecurityBindingElement;
                asbe.DefaultAlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Basic128Sha256Rsa15;

                resType_Login = req_Login.CallLogin(reqType_Login);

                Req_Login.ResponseSoapContext.Security.Tokens.CopyTo(SecToken.stLogin, 0);
                SecToken.isSessionPresent = true;

                LoggedInStatus = true;
                return "Success";
            }
            catch (Exception ex)
            {
                SecToken.isSessionPresent = false;

                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                LoggedInStatus = false;
                return "Exception";
            }
        }

        public string _Logout()
        {
            try 
            {
                if (SecToken.isSessionPresent)
                {
                    string result = baseWrapper.Logout();
                }
                

                return "Success";
            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Logout Failed: " + ex.Message;
            }
        }

        public string _Logout_WSE30()
        {
            try
            {
                LogoutErrorHandling req_Logout = new LogoutErrorHandling();

                LogoutRequestType reqType_Logout = new LogoutRequestType();

                LogoutResponseType resType_Logout = new LogoutResponseType();

                MefMsiServices.MeFHeaderType MeF = new MefMsiServices.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                MeF.MessageID = getMessageID();

                MeF.Action = "Logout";

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = Variables.ETIN;

                MeF.SessionKeyCd = MefMsiServices.SessionKeyCdType.Y;

                if (Variables.TestIndicator == "T")
                {
                    MeF.TestCd = MefMsiServices.TestCdType.T;
                }
                else
                {
                    MeF.TestCd = MefMsiServices.TestCdType.P;
                }

                MeF.AppSysID = Variables.MeF_AppSysID;

                req_Logout.MeFHeader = MeF;

                
                if (!SecToken.isSessionPresent)
                {
                    return "Login Not Present";
                }
                else
                    try 
                    {
                        for (int i = 0; i < SecToken.stLogin.Length; i++)
                            req_Logout.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                    }
                    catch { }


                resType_Logout = req_Logout.CallLogout(reqType_Logout);
                LoggedInStatus = false;

                SecToken.isSessionPresent = false;

                return "Success";
            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Logout Failed: " + ex.Message;
            }
        }

        public string _Logout1()
        {
            try
            {
                LogoutErrorHandling req_Logout = new LogoutErrorHandling();
                LogoutRequestType reqType_Logout = new LogoutRequestType();
                LogoutResponseType resType_Logout = new LogoutResponseType();

                MefMsiServices.MeFHeaderType MeF = new MefMsiServices.MeFHeaderType();
                MeF.Id = "myTestMeF";
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                MeF.MessageID = getMessageID();
                MeF.Action = "Logout";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;

                MeF.SessionKeyCd = MefMsiServices.SessionKeyCdType.Y;
                
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MefMsiServices.TestCdType.T;
                else
                    MeF.TestCd = MefMsiServices.TestCdType.P;

                MeF.AppSysID = Variables.MeF_AppSysID;
                req_Logout.MeFHeader = MeF;

                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_Logout.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }


                resType_Logout = req_Logout.CallLogout(reqType_Logout);
                LoggedInStatus = false;

                SecToken.isSessionPresent = false;
                SecToken.stLogin = null;

                return "Success";
            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Logout Failed: " + ex.Message;
            }
        }

        public string _SendSubmissions(string SubmissionId)
        {
            try
            {
                string result = "";
                if(!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                SubmissionErrorHandling req = new SubmissionErrorHandling();

                req.RequireMtom = true;

                MeFTransmitterServiceWse.SendSubmissionsRequestType reqType = new MeFTransmitterServiceWse.SendSubmissionsRequestType();

                MeFTransmitterServiceWse.SendSubmissionsResponseType resType = new MeFTransmitterServiceWse.SendSubmissionsResponseType();

                # region MeF

                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();

                MeF.MessageID = getMessageID();
                MeF.Action = "SendSubmissions";
                MeF.MessageTs= DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd= MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd= MeFTransmitterServiceWse.TestCdType.P;

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req.MeFHeader = MeF;

                #endregion

                #region securityTokens

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                #endregion


                MeFTransmitterServiceWse.SubmissionDataType[] sdt = new MeFTransmitterServiceWse.SubmissionDataType[1];
                MeFTransmitterServiceWse.SubmissionDataType sdt1 = new MeFTransmitterServiceWse.SubmissionDataType();
                sdt1.SubmissionId = SubmissionId;
                //sdt1.SubmissionIdType = SubmissionId; // Vishwa 9.6
                
                sdt1.ElectronicPostmarkTs = DateTime.Now.ToUniversalTime();
                sdt[0] = sdt1;
             

                reqType.SubmissionDataList = sdt; 

                #region Attachment

                byte[] attachment_bytes = File.ReadAllBytes("C:\\SendSubmission" + SubmissionId + ".zip");

                MeFTransmitterServiceWse.base64Binary attachment_post = new MeFTransmitterServiceWse.base64Binary();
                attachment_post.Value = attachment_bytes;

                reqType.SubmissionsAttMTOM = attachment_post;

                
                resType = req.CallSendSubmissions(reqType);

                MeFTransmitterServiceWse.base64Binary attachment_get = new MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType.SubmissionRcptListAttMTOM;

                FileStream stream = new FileStream("C:\\RetrunSubmission" + SubmissionId + ".zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                #endregion

                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Exception";
            }
        }

        public string BulkTransmission(string bulkFileName, Stack submissionIds)
        {
            try
            {
                string[] subIds = new string[submissionIds.Count];
                string result = "";
                int loopCount = 0;
                while (submissionIds.Count > 0)
                {
                    subIds[loopCount] =  submissionIds.Pop().ToString();
                    loopCount++;
                }
                if (!bulkFileName.EndsWith(".zip"))
                    bulkFileName = bulkFileName + ".zip";

                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                string fileSubmission = baseWrapper.SendSubmissions(bulkFileName, subIds, pathResponseFile);



                return "Success";
            }
            catch (Exception ex)
            {

                if (ex.Message.Contains("IDP Rule 'R8-MeF Process Error IDP Rule' aborted processing"))
                    SecToken.isSessionPresent = false;

                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                _Logout();
                return "Exception";
            }
        }

        public string BulkTransmission_MeFSDK(string bulkFileName, Stack submissionIds)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                SubmissionErrorHandling req = new SubmissionErrorHandling();

                req.RequireMtom = true;
                MeFTransmitterServiceWse.SendSubmissionsRequestType reqType = new MeFTransmitterServiceWse.SendSubmissionsRequestType();
                MeFTransmitterServiceWse.SendSubmissionsResponseType resType = new MeFTransmitterServiceWse.SendSubmissionsResponseType();

                # region MeF

                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();
                MeF.MessageID = getMessageID();
                MeF.Action = "SendSubmissions";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                req.MeFHeader = MeF;

                #endregion

                #region securityTokens

                if (!SecToken.isSessionPresent || context == null)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }


                try
                {
                    //for (int i = 0; i < SecToken.stLogin.Length; i++)
                    /// existng -- req.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }


                #endregion


                MeFTransmitterServiceWse.SubmissionDataType[] sdt = new MeFTransmitterServiceWse.SubmissionDataType[submissionIds.Count];//1

                /*int count = 0;
                while (submissionIds.Count > 0)
                {
                    MeFTransmitterServiceWse.SubmissionDataType sdt1 = new MeFTransmitterServiceWse.SubmissionDataType();
                    sdt1.SubmissionId = submissionIds.Pop().ToString();
                    //sdt1.SubmissionIdType = submissionIds.Pop().ToString(); // Vishwa 9.6
                    sdt1.ElectronicPostmarkTs = DateTime.Now.ToUniversalTime();
                    sdt[count++] = sdt1;
                }
                */

                reqType.SubmissionDataList = sdt;

                #region Attachment

                if (!bulkFileName.EndsWith(".zip"))
                    bulkFileName = bulkFileName + ".zip";

                byte[] attachment_bytes = File.ReadAllBytes(bulkFileName);

                MeFTransmitterServiceWse.base64Binary attachment_post = new MeFTransmitterServiceWse.base64Binary();
                attachment_post.Value = attachment_bytes;

                reqType.SubmissionsAttMTOM = attachment_post;

                // New Vishwa 

                try
                {
                    SubmissionBuilder builder = new SubmissionBuilder();
                    List<PostmarkedSubmissionArchive> postMarkSubmissionList = new List<PostmarkedSubmissionArchive>();

                    foreach (string s in submissionIds)
                    {
                        SubmissionArchive arch = builder.CreateIRSSubmissionArchive(bulkFileName);
                        arch.submissionId = s;
                        PostmarkedSubmissionArchive postMarkSubmission = new PostmarkedSubmissionArchive(arch, DateTime.Now);
                        postMarkSubmissionList.Add(postMarkSubmission);
                    }

                    SubmissionContainer container = new SubmissionContainer(postMarkSubmissionList);

                    SendSubmissionsClient SendSubclient = new SendSubmissionsClient(pathResponseFile);
                    SendSubmissionsResult Sendresult = SendSubclient.Invoke( context , container);

                    return Sendresult.AttachmentFilePath;
                }
                catch (Exception ex)
                {
                    throw;
                }


                //resType = req.CallSendSubmissions(reqType);

                MeFTransmitterServiceWse.base64Binary attachment_get = new MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType.SubmissionRcptListAttMTOM;

                FileStream stream = new FileStream(bulkFileName.Replace(".zip", "") + "-Return.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                #endregion

                return "Success";
            }
            catch (Exception ex)
            {

                if (ex.Message.Contains("IDP Rule 'R8-MeF Process Error IDP Rule' aborted processing"))
                    SecToken.isSessionPresent = false;

                WriteLog.Log(ex.Message, EventLogEntryType.Error);

                _Logout();
                return "Exception";
            }
        }
        public string BulkTransmission_BKK(string bulkFileName, Stack submissionIds)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                SubmissionErrorHandling req = new SubmissionErrorHandling();

                req.RequireMtom = true;
                MeFTransmitterServiceWse.SendSubmissionsRequestType reqType = new MeFTransmitterServiceWse.SendSubmissionsRequestType();
                MeFTransmitterServiceWse.SendSubmissionsResponseType resType = new MeFTransmitterServiceWse.SendSubmissionsResponseType();

                # region MeF

                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();
                MeF.MessageID = getMessageID();
                MeF.Action = "SendSubmissions";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                req.MeFHeader = MeF;

                #endregion

                #region securityTokens

                if (!SecToken.isSessionPresent || context == null)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }

                   
                try
                {
                    //for (int i = 0; i < SecToken.stLogin.Length; i++)
                        /// existng -- req.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }
                

                #endregion

                
                MeFTransmitterServiceWse.SubmissionDataType[] sdt = new MeFTransmitterServiceWse.SubmissionDataType[submissionIds.Count];//1
                
                /*int count = 0;
                while (submissionIds.Count > 0)
                {
                    MeFTransmitterServiceWse.SubmissionDataType sdt1 = new MeFTransmitterServiceWse.SubmissionDataType();
                    sdt1.SubmissionId = submissionIds.Pop().ToString();
                    //sdt1.SubmissionIdType = submissionIds.Pop().ToString(); // Vishwa 9.6
                    sdt1.ElectronicPostmarkTs = DateTime.Now.ToUniversalTime();
                    sdt[count++] = sdt1;
                }
                */

                reqType.SubmissionDataList = sdt;

                #region Attachment

                if (!bulkFileName.EndsWith(".zip"))
                    bulkFileName = bulkFileName + ".zip";

                byte[] attachment_bytes = File.ReadAllBytes(bulkFileName);

                MeFTransmitterServiceWse.base64Binary attachment_post = new MeFTransmitterServiceWse.base64Binary();
                attachment_post.Value = attachment_bytes;

                reqType.SubmissionsAttMTOM = attachment_post;

                // New Vishwa 
                FileStream currentManifestFilePath = new FileStream("C:\\temp\\manifest\\manifest.xml", FileMode.Open);
                //SubmissionManifest currentManifestFilePath = new SubmissionManifest(fsman);
                
                FileStream currentXmlFilePath = new FileStream("C:\\temp\\xml\\submission.xml", FileMode.Open);
                //SubmissionXml currentXmlFilePath = new SubmissionXml(fsm);

                SubmissionBuilder builder = new SubmissionBuilder();
                var currentSubmissionId = submissionIds.Pop().ToString();
                //var manifest = new SubmissionManifest(currentManifestFilePath);
                //var xml = new SubmissionXml(currentXmlFilePath);
                //var attachments = new List<BinaryAttachment>();

                //SubmissionArchive arch = builder.CreateIRSSubmissionArchive(currentSubmissionId,
                //manifest, xml, attachments, "C:\\Temp\\");

                SubmissionArchive arch = builder.CreateIRSSubmissionArchive(bulkFileName);
                arch.submissionId = currentSubmissionId;

                PostmarkedSubmissionArchive Postsub1 = new PostmarkedSubmissionArchive(arch, DateTime.Now);
                List<PostmarkedSubmissionArchive> Postsub = new List<PostmarkedSubmissionArchive>();
                Postsub.Add(Postsub1);

                SubmissionContainer ContainerPostsub = new SubmissionContainer(Postsub);
                //ContainerPostsub.ZipFilename = bulkFileName;

                SendSubmissionsClient SendSubclient = new SendSubmissionsClient("C:\\Live Submissions\\ResponseExtractFolder");
                 //See previous example for building SubmissionContainer objects.
                SendSubmissionsResult Sendresult = SendSubclient.Invoke( context , ContainerPostsub);

                currentXmlFilePath.Close();
                currentManifestFilePath.Close();

                string stringExtractFolder = "C:\\Live Submissions\\";

                //Get the Ack to Update back on Status
                GetAckClient Ackclient = new GetAckClient(stringExtractFolder);
 
                GetAckResult Ackresult = Ackclient.Invoke(context, currentSubmissionId);

                // Get the SCH1 If Available 
                Get2290Schedule1Client Sch1client = new Get2290Schedule1Client(stringExtractFolder);
 
                Get2290Schedule1Result Sch1result = Sch1client.Invoke(context, currentSubmissionId);

                //resType = req.CallSendSubmissions(reqType);

                MeFTransmitterServiceWse.base64Binary attachment_get = new MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType.SubmissionRcptListAttMTOM;

                FileStream stream = new FileStream(bulkFileName.Replace(".zip", "") + "-Return.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                #endregion

                return "Success";
            }
            catch (Exception ex)
            {
               
                if (ex.Message.Contains("IDP Rule 'R8-MeF Process Error IDP Rule' aborted processing"))
                    SecToken.isSessionPresent = false;

                WriteLog.Log(ex.Message, EventLogEntryType.Error);

                _Logout();
                return "Exception";
            }
        }

        public string Get_BulkAcks(Stack submissionIds, string bulkFileName)
        {
            try
            {
                #region LOGIN
                string result = "";
                if (!SecToken.isSessionPresent)
                    result = _Login();
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;
                #endregion

                GetAcksErrorHandling req_GetAcks = new GetAcksErrorHandling();
                req_GetAcks.RequireMtom = true;

                MeFTransmitterServiceWse.GetAcksRequestType reqType_GetAcks = new MeFTransmitterServiceWse.GetAcksRequestType();
                MeFTransmitterServiceWse.GetAcksResponseType resType_GetAcks = new MeFTransmitterServiceWse.GetAcksResponseType();
                #region MeF

                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();
                MeF.Id = "MyMeF";
                MeF.MessageID = getMessageID();
                MeF.Action = "GetAcks";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;

                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetAcks.MeFHeader = MeF;
                #endregion
                #region security
                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAcks.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }
                #endregion

                string[] subidlist = (from o in submissionIds.ToArray() select o.ToString()).ToArray();
                reqType_GetAcks.SubmissionIdList = subidlist;
                //reqType_GetAcks.SubmissionIdTypeList = subidlist; // Vishwa 9.6
                resType_GetAcks = req_GetAcks.CallGetAcks(reqType_GetAcks);

                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetAcks.AcknowledgementListAttMTOM;

                FileStream stream = new FileStream(bulkFileName + "-Ack.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                _Logout();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Exception";
            }
        }

        public string Get_BulkSchedule1s(Stack submissionIds, string bulkFileName)
        {
            try
            {
                #region LOGIN
                string result = "";
                if (!SecToken.isSessionPresent)
                    result = _Login();
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;
                #endregion

                Schedule1sErrorHandling req_GetAcks = new Schedule1sErrorHandling();
                req_GetAcks.RequireMtom = true;

                Get2290Schedule1sRequestType reqType_GetSch1s = new Get2290Schedule1sRequestType();
                Get2290Schedule1sResponseType resType_GetSch1s = new Get2290Schedule1sResponseType();

                #region MeF

                ETECTransmitterServiceMTOM_SeparateServices.MeFHeaderType MeF = new ETECTransmitterServiceMTOM_SeparateServices.MeFHeaderType();
                MeF.Id = "MyMeF";
                
                MeF.MessageID = getMessageID();
                MeF.Action = "Get2290Schedule1s";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd= ETECTransmitterServiceMTOM_SeparateServices.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd= ETECTransmitterServiceMTOM_SeparateServices.TestCdType.T;
                else
                    MeF.TestCd= ETECTransmitterServiceMTOM_SeparateServices.TestCdType.P;

                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                req_GetAcks.MeFHeader = MeF;
                #endregion
                #region security
                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAcks.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }
                #endregion

                string[] subidlist = (from o in submissionIds.ToArray() select o.ToString()).ToArray();
                reqType_GetSch1s.SubmissionIdList = subidlist;
                //reqType_GetSch1s.SubmissionIdTypeList = subidlist; // Vishwa 9.6
                resType_GetSch1s = req_GetAcks.CallGet2290Schedule1s(reqType_GetSch1s);

                IRSLibrary.ETECTransmitterServiceMTOM_SeparateServices.base64Binary attachment_get = new IRSLibrary.ETECTransmitterServiceMTOM_SeparateServices.base64Binary();
                attachment_get = resType_GetSch1s.Schedule1AttMTOM;

                FileStream stream = new FileStream("C:\\" + bulkFileName + "-Sch1.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                _Logout();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Exception";
            }
        }

        public string _GetAck(ref DataRow ldr)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;


                GetAckErrorHandling req_GetAck = new GetAckErrorHandling();

                req_GetAck.RequireMtom = true;

                MeFTransmitterServiceWse.GetAckRequestType reqType_GetAck = new MeFTransmitterServiceWse.GetAckRequestType();

                MeFTransmitterServiceWse.GetAckResponseType resType_GetAck = new MeFTransmitterServiceWse.GetAckResponseType();


                MeFTransmitterServiceWse.MeFHeaderType MeF = new IRSLibrary.MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();

                MeF.Action = "GetAck";

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = Variables.ETIN;

                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;

                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetAck.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAck.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType_GetAck.SubmissionId =Convert.ToString(ldr["SubmissionID"]);
                //reqType_GetAck.SubmissionIdType = Convert.ToString(ldr["SubmissionID"]); // Vishwa 9.6


                resType_GetAck = req_GetAck.CallGetAck(reqType_GetAck);


                string GetAckFileName = "GetAck" + Convert.ToString(ldr["SubmissionID"]) + ".zip";

                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetAck.AcknowledgementAttMTOM;

                FileStream stream = new FileStream("C:\\"+ GetAckFileName, FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();


                DataSet ds = UnzipFiles(GetAckFileName);
                string ErrorList = "";
                string node = "";
                if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                {
                    node = "AcceptanceStatusTxt";
                    ldr["StatusDate"] = ds.Tables["Acknowledgement"].Rows[0]["StatusDate"].ToString();
                }
                else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                {
                    node = "FilingStatus";
                    ldr["StatusDate"] = ds.Tables["Acknowledgement"].Rows[0]["StatusDate"].ToString();
                }
                
                if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                {
                    return "Accepted";
                }
                else
                {
                    if (ds.Tables.Contains("Error"))
                    {
                        foreach (DataRow dr in ds.Tables["Error"].Rows)
                        {
                            ErrorList += dr["RuleNumber"].ToString() + "~";
                        }
                        return "Rejected*" + ErrorList.Trim();
                    }
                    else
                    {
                        return "Failed";
                    }
                }

            }
            catch
            {
                return "Exception";
            }

            return "Success";
        }

        public string _GetAck(string SubmissionID)
        {
            try
            {
                string result; 

                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                string fileSubmission = baseWrapper.GetAck(SubmissionID, pathResponseFile);

                // This file has the ACK status with details.
                ackFile_Status = fileSubmission;

                DataSet ds = readXMLFiles(fileSubmission);
                string ErrorList = "";
                string node = "";
                if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                {
                    node = "AcceptanceStatusTxt";
                }
                else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                {
                    node = "FilingStatus";
                }

                if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                {
                    return "Success";
                }
                else
                {
                    if (ds.Tables.Contains("Error"))
                    {
                        foreach (DataRow dr in ds.Tables["Error"].Rows)
                        {
                            if (ErrorList.Length > 0)
                                ErrorList += "," + dr["RuleNumber"].ToString();
                            else
                                ErrorList += dr["RuleNumber"].ToString();
                        }
                        return ErrorList.Trim();
                    }
                    else
                    {
                        return SubmissionID + " Acknowledgement Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                if (ex.Message == "gov.irs.efile.ef.a2a.common.MeFExceptionType")
                {
                    return SubmissionID + " Acknowledgement Failed :::: Unable to Retrieve Acknowledgement";
                }
                else
                    return SubmissionID + " Acknowledgement Failed :::: " + ex.Message;

                _Logout();
            }
            return "Success";
        }
        public string _GetAck_WSE30(string SubmissionID)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;


                GetAckErrorHandling req_GetAck = new GetAckErrorHandling();

                req_GetAck.RequireMtom = true;

                MeFTransmitterServiceWse.GetAckRequestType reqType_GetAck = new MeFTransmitterServiceWse.GetAckRequestType();

                MeFTransmitterServiceWse.GetAckResponseType resType_GetAck = new MeFTransmitterServiceWse.GetAckResponseType();

                MeFTransmitterServiceWse.MeFHeaderType MeF = new IRSLibrary.MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();

                MeF.Action = "GetAck";                

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = Variables.ETIN;

                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;

                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                {
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                }
                else
                {
                    MeF.TestCd= MeFTransmitterServiceWse.TestCdType.P;
                }

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetAck.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAck.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType_GetAck.SubmissionId = SubmissionID;
                //reqType_GetAck.SubmissionIdType = SubmissionID; // Vishwa 9.6

                resType_GetAck = req_GetAck.CallGetAck(reqType_GetAck);
                
                string GetAckFileName = SubmissionID + "-Ack.zip";

                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetAck.AcknowledgementAttMTOM;

                FileStream stream = new FileStream("C:\\" + GetAckFileName, FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                DataSet ds = UnzipFiles(GetAckFileName);
                string ErrorList = "";
                string node = "";
                if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                {
                    node = "AcceptanceStatusTxt";
                }
                else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                {
                    node = "FilingStatus";
                }

                if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                {
                    return "Success";
                }
                else
                {
                    if (ds.Tables.Contains("Error"))
                    {
                        foreach (DataRow dr in ds.Tables["Error"].Rows)
                        {
                            if (ErrorList.Length > 0)
                                ErrorList += "," + dr["RuleNumber"].ToString();
                            else
                                ErrorList += dr["RuleNumber"].ToString();
                        }
                        return ErrorList.Trim();
                    }
                    else
                    {
                        return SubmissionID + " Acknowledgement Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                if (ex.Message == "gov.irs.efile.ef.a2a.common.MeFExceptionType")
                {
                    return SubmissionID + " Acknowledgement Failed :::: Unable to Retrieve Acknowledgement";
                }
                else
                    return SubmissionID + " Acknowledgement Failed :::: " + ex.Message;

                _Logout();
            }
            return "Success";
        }

        public string _GetNewAcks(ref string SubmissionCount)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;
                while (true)
                {
                    GetNewAcksErrorHandling req_GetNewAcks = new GetNewAcksErrorHandling();

                    req_GetNewAcks.RequireMtom = true;

                    MeFTransmitterServiceWse.GetNewAcksRequestType reqType_GetNewAcks = new MeFTransmitterServiceWse.GetNewAcksRequestType();

                    MeFTransmitterServiceWse.GetNewAcksResponseType resType_GetNewAcks = new MeFTransmitterServiceWse.GetNewAcksResponseType();

                    MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();

                    MeF.Id = "MyMeF";
                   
                    MeF.MessageID = getMessageID();
                    MeF.Action = "GetNewAcks";
                    MeF.MessageTs = DateTime.Now;
                    MeF.ETIN = Variables.ETIN;
                    MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                    MeF.SessionIndicatorSpecified = true;
                    if (Variables.TestIndicator == "T")
                        MeF.TestCd= MeFTransmitterServiceWse.TestCdType.T;
                    else
                        MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;

                    MeF.TestIndicatorSpecified = true;
                    MeF.AppSysID = Variables.MeF_AppSysID;
                    MeF.WSDLVersionNum = Variables.WSDLVerNum;
                    req_GetNewAcks.MeFHeader = MeF;

                 
                    if (!SecToken.isSessionPresent)
                    {
                        if (_Login().ToUpper() != "SUCCESS")
                            return "Exception";
                    }
                    try
                    {
                        for (int i = 0; i < SecToken.stLogin.Length; i++)
                            req_GetNewAcks.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                    }
                    catch { }

                    reqType_GetNewAcks.GovernmentAgencyTypeCd = GovernmentAgencyTypeCdType.Federal;

                    reqType_GetNewAcks.AgencySpecified = true;

                    reqType_GetNewAcks.ExtndAcknowledgementCategoryCd = MeFTransmitterServiceWse.ExtndAcknowledgementCategoryCdType.EXCISE;

                    reqType_GetNewAcks.CategorySpecified = true;

                    reqType_GetNewAcks.MaxResultCnt = SubmissionCount;

                    resType_GetNewAcks = req_GetNewAcks.CallGetNewAcks(reqType_GetNewAcks);


                    IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                    attachment_get = resType_GetNewAcks.AcknowledgementListAttMTOM;

                    FileStream stream = new FileStream("C:\\GetNewAcks" + DateTime.Now.ToString("yyMMddHHmmssFFF") + ".zip", FileMode.Create);
                    byte[] buffer = attachment_get.Value;
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Close();

                    if (!resType_GetNewAcks.MoreAvailableInd)
                        break;
                }
                _Logout();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Exception";
            }
        }

        public string _GetAcksByMessageID(string MsgID, string ETIN, string MeF_AppSysID)
        {
            try
            {
                MeFTransmitterServiceWse.GetAcksByMsgID req_GetAcksByMsgID = new MeFTransmitterServiceWse.GetAcksByMsgID();

                MeFTransmitterServiceWse.GetAcksByMsgIDRequestType reqType_GetAcksByMsgID = new MeFTransmitterServiceWse.GetAcksByMsgIDRequestType();

                MeFTransmitterServiceWse.GetAcksByMsgIDResponseType resType_GetAcksByMsgID = new MeFTransmitterServiceWse.GetAcksByMsgIDResponseType();

                MeFTransmitterServiceWse.MeFHeaderType MeF = new IRSLibrary.MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();

                MeF.Action = "GetAcksByMsgID";

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = ETIN;

                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;

                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                {
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                }
                else
                {
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                }

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetAcksByMsgID.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAcksByMsgID.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType_GetAcksByMsgID.MessageId = MsgID;

                resType_GetAcksByMsgID = req_GetAcksByMsgID.CallGetAcksByMsgID(reqType_GetAcksByMsgID);


                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetAcksByMsgID.AcknowledgementListAttMTOM;

                FileStream stream = new FileStream("C:\\GetAcksByMessageID.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string _GetSubmissionStatus(string submissionID, string ETIN, string MeF_AppSysID)
        {
            try
            {
                MeFTransmitterServiceWse.GetSubmissionStatus req_GetSubmissionStatus = new MeFTransmitterServiceWse.GetSubmissionStatus();

                MeFTransmitterServiceWse.GetSubmissionStatusRequestType reqType_GetSubmissionStatus = new MeFTransmitterServiceWse.GetSubmissionStatusRequestType();

                MeFTransmitterServiceWse.GetSubmissionStatusResponseType resType_GetSubmissionStatus = new MeFTransmitterServiceWse.GetSubmissionStatusResponseType();

                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();

                MeF.Action = "GetSubmissionStatus";

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = ETIN;

                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;

                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                {
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                }
                else
                {
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                }

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetSubmissionStatus.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetSubmissionStatus.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType_GetSubmissionStatus.SubmissionId = submissionID;
                //reqType_GetSubmissionStatus.SubmissionIdType = submissionID; // Vishwa 9.6

                resType_GetSubmissionStatus = req_GetSubmissionStatus.CallGetSubmissionStatus(reqType_GetSubmissionStatus);

                //--------------------------------------------------------------------

                string SubmissionStatusFileName = "SubmissionStatus" + submissionID + ".zip";


                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetSubmissionStatus.StatusRecordListAttMTOM;

                FileStream stream = new FileStream(path2CreateFile_4GetAck + SubmissionStatusFileName, FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);


                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string _GetNewSubmissionStatus(string ETIN, string MeF_AppSysID)
        {
            try
            {
                MeFTransmitterServiceWse.GetNewSubmissionsStatus req_GetNewSubmissionStatus = new MeFTransmitterServiceWse.GetNewSubmissionsStatus();
                MeFTransmitterServiceWse.GetNewSubmissionsStatusRequestType reqType_GetNewSubmissionsStatus = new MeFTransmitterServiceWse.GetNewSubmissionsStatusRequestType();
                MeFTransmitterServiceWse.GetNewSubmissionsStatusResponseType resType_GetNewSubmissionsStatus = new MeFTransmitterServiceWse.GetNewSubmissionsStatusResponseType();
                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();
                MeF.Action = "GetNewSubmissionsStatus";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetNewSubmissionStatus.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetNewSubmissionStatus.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType_GetNewSubmissionsStatus.MaxResultCnt = "50";
                resType_GetNewSubmissionsStatus = req_GetNewSubmissionStatus.CallGetNewSubmissionsStatus(reqType_GetNewSubmissionsStatus);
                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetNewSubmissionsStatus.StatusRecordListAttMTOM;

                FileStream stream = new FileStream(path2CreateFile_4GetAck + "GetNewSubmissionsStatus.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);

                _Logout();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                return ex.Message;
            }
        }

        public string _GetNewSubmissionStatus(string count)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;
                while (true)
                {

                GetNewSubmissionsStatusErrorHandling req_GetNewSubStatus = new GetNewSubmissionsStatusErrorHandling();

                req_GetNewSubStatus.RequireMtom = true;

                GetNewSubmissionsStatusRequestType reqType_GetNewSubsStatus = new GetNewSubmissionsStatusRequestType();
                GetNewSubmissionsStatusResponseType resType_GetNewSubsStatus = new GetNewSubmissionsStatusResponseType();
                MeFTransmitterServiceWse.MeFHeaderType MeF = new MeFTransmitterServiceWse.MeFHeaderType();

                MeF.Id = "MyMeF";
                MeF.MessageID = getMessageID();
                MeF.Action = "GetNewSubmissionsStatus";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd = MeFTransmitterServiceWse.TestCdType.P;
                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req_GetNewSubStatus.MeFHeader = MeF;

                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetNewSubStatus.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }
               
                    reqType_GetNewSubsStatus.MaxResultCnt = count;
                    resType_GetNewSubsStatus = req_GetNewSubStatus.CallGetNewSubmissionsStatus(reqType_GetNewSubsStatus);

                    IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                    attachment_get = resType_GetNewSubsStatus.StatusRecordListAttMTOM;

                    FileStream stream = new FileStream("C:\\GetNewSubmissionsStatus" + DateTime.Now.ToString("yyMMddHHmmssFFF") + ".zip", FileMode.Create);
                    byte[] buffer = attachment_get.Value;
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Close();

                    if (!resType_GetNewSubsStatus.MoreAvailableInd)
                        break;
                }
                _Logout();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                return "Exception";
            }
        }

        public DataSet readXMLFiles(string xFileName)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml(xFileName);
                //TODO - Vishwa - delete this at right time
                //File.Delete(xFileName);
                return ds;
            }
            catch (Exception ex) { return null; }
        }
        public DataSet UnzipFiles(string ZipFileName)
        {
            try
            {
                string targetDirectory = @"C:\" + ZipFileName.Substring(0, ZipFileName.IndexOf(".")) + "\\";

                FastZip fz = new FastZip();
                fz.ExtractZip("c:\\" + ZipFileName, targetDirectory, "");
                string[] Filenames = Directory.GetFiles(targetDirectory, "*.xml");
                DataSet ds = new DataSet();
                ds.ReadXml(Filenames[0].ToString());

                File.Delete(Filenames[0].ToString());
                Directory.Delete(targetDirectory);
                return ds;

            }
            catch (Exception ex) { return null; }
        }

        public string createZip(string BatchNo,DataTable dtSubmissionIDs)
        {
            try
            {
                ZipFile TransmissionZip = ZipFile.Create("C:\\SendSubmission" + BatchNo + ".zip");//name verification pending


                foreach (DataRow dr in dtSubmissionIDs.Rows)
                {

                    if (Convert.ToString(dr["SubmissionID"]).Length != 20)
                        continue;

                    TransmissionZip.BeginUpdate();

                    TransmissionZip.Add("C:\\" +Convert.ToString(dr["SubmissionID"]) + ".zip", CompressionMethod.Stored);

                    TransmissionZip.CommitUpdate();
                    
                    File.Delete("C:\\" + Convert.ToString(dr["SubmissionID"]) + ".zip");
                }
                TransmissionZip.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }
        public string createZip(string SubmissionID)
        {
            try
            {
                ZipFile TransmissionZip = ZipFile.Create("C:\\SendSubmission" + SubmissionID + ".zip");//name verification pending

                TransmissionZip.BeginUpdate();

                TransmissionZip.Add("C:\\" + SubmissionID + ".zip", CompressionMethod.Stored);

                TransmissionZip.CommitUpdate();

                File.Delete("C:\\" + SubmissionID + ".zip");

                TransmissionZip.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }
        public static void CreateSingleSubmissionZip(string SubmissionId)
        {
            try
            {

                ZipFile SubmissionZip = ZipFile.Create("C:\\" + SubmissionId + ".zip");

                SubmissionZip.BeginUpdate();

                SubmissionZip.Add("C:\\" + "\\manifest\\manifest.xml");
                SubmissionZip.Add("C:\\" + "\\xml\\Submission.xml");

                SubmissionZip.CommitUpdate();
                SubmissionZip.Close();
                //success!

                File.Delete("C:\\manifest\\manifest.xml");
                File.Delete("C:\\" + @"\xml\Submission.xml");
                Directory.Delete("C:\\" + @"\manifest");
                Directory.Delete("C:\\" + @"\xml");
            }
            catch (Exception ex)
            {
            }
        }

        public string MonthToDate(string month)
        {
            string lmonth = "";
            switch (month)
            {                         
                case "Jan": lmonth = "01"; break;
                case "Feb": lmonth = "02"; break;
                case "Mar": lmonth = "03"; break;
                case "Apr": lmonth = "04"; break;
                case "May": lmonth = "05"; break;
                case "Jun": lmonth = "06"; break;
                case "Jul": lmonth = "07"; break;
                case "Aug": lmonth = "08"; break;
                case "Sep": lmonth = "09"; break;
                case "Oct": lmonth = "10"; break;
                case "Nov": lmonth = "11"; break;
                case "Dec": lmonth = "12"; break;
            }
            return lmonth;
        }

        public int GetMonths(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart) + 1;
        }

        public static string GetNameControl(string corpname)
        {
            int i = 0;
            char[] name = new char[4];

            if (corpname.StartsWith("The "))
                corpname = corpname.Remove(0, 4);
            foreach (char c in corpname)
            {
                if (c != ' ')
                {
                    name[i] = c;

                    if (++i == 4)
                        break;
                }
            }
            return new string(name).ToUpper();
            
        }

        public static int WordCount(string s)
        {
            int c = 0;
            for (int i = 1; i < s.Length; i++)
            {
                if (char.IsWhiteSpace(s[i - 1]) == true)
                {
                    if (char.IsLetterOrDigit(s[i]) == true || char.IsPunctuation(s[i]))
                    {
                        c++;
                    }
                }
            }
            if (s.Length > 2)
            {
                c++;
            }
            return c;
        }
        
        public string convet2Base64String(string FileName)
        {
            try
            {
                FileStream inFile = new FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);//"C:\\SendSubmission.zip"
                byte[] binaryData = new Byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();
                string base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                return base64String;
            }
            catch
            {
                return "";
            }
        }

        public static bool SendMail(string pTo, string pCC, string bCC, string pSubject, string pBody)
        {
            try
            {
                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(Variables.AuthenticationEmail, pTo);
                myMail.Subject = pSubject;
                myMail.IsBodyHtml = true;
                myMail.Body = pBody;

                if (pCC != "") { myMail.CC.Add(pCC); }
                if (bCC != "") { myMail.Bcc.Add(bCC); }

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(Variables.MailServer,Variables.MailPort);

                client.Credentials = new System.Net.NetworkCredential(Variables.AuthenticationEmail, Variables.MailPassword);

                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.Send(myMail);

                myMail.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
            }
        }

        public string _GetSchedule1(string SubmissionID)
        {
            try
            {
                string result = "";
                if (!SecToken.isSessionPresent)
                {
                    result = _Login();
                }
                else
                    result = "Success";

                if (!result.Contains("Success") && result != "")
                    return result;

                Schedule1ErrorHandling req = new Schedule1ErrorHandling();
                req.RequireMtom = true;

                ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1RequestType reqType =
                    new ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1RequestType();

                ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1ResponseType resType =
                    new ETECTransmitterServiceMTOM_SeparateServices.Get2290Schedule1ResponseType();

                ETECTransmitterServiceMTOM_SeparateServices.MeFHeaderType MeF = new ETECTransmitterServiceMTOM_SeparateServices.MeFHeaderType();

                MeF.Id = "myTestMeF";
                MeF.MessageID = getMessageID();

                MeF.Action = "Get2290Schedule1";

                MeF.MessageTs = DateTime.Now;

                MeF.ETIN = Variables.ETIN;

                MeF.SessionKeyCd= ETECTransmitterServiceMTOM_SeparateServices.SessionKeyCdType.Y;

                MeF.SessionIndicatorSpecified = true;

                if (Variables.TestIndicator == "T")
                    MeF.TestCd = ETECTransmitterServiceMTOM_SeparateServices.TestCdType.T;
                else
                    MeF.TestCd = ETECTransmitterServiceMTOM_SeparateServices.TestCdType.P;

                MeF.TestIndicatorSpecified = true;

                MeF.AppSysID = Variables.MeF_AppSysID;
                MeF.WSDLVersionNum = Variables.WSDLVerNum;

                req.MeFHeader = MeF;

            
                if (!SecToken.isSessionPresent)
                {
                    if (_Login().ToUpper() != "SUCCESS")
                        return "Exception";
                }
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }

                reqType.SubmissionId = SubmissionID;
                //reqType.SubmissionIdType = SubmissionID; // Vishwa 9.6

                resType = req.CallGet2290Schedule1(reqType);
                ETECTransmitterServiceMTOM_SeparateServices.base64Binary attachment_get = new ETECTransmitterServiceMTOM_SeparateServices.base64Binary();
                attachment_get = resType.Schedule1AttMTOM;

                FileStream stream = new FileStream("C:\\" + SubmissionID + "-Sch1.zip", FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                FastZip fz = new FastZip();
                fz.ExtractZip("c:\\" + SubmissionID + "-Sch1.zip", @"C:\", "");

                FileStream fstr = new FileStream(@"C:\" + SubmissionID + ".pdf", FileMode.Open, FileAccess.Read);

                IRSLibrary.Return8849Attachment.schedule6 = new byte[fstr.Length];
                fstr.Read(IRSLibrary.Return8849Attachment.schedule6, 0, (int)fstr.Length);

                fstr.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                _Logout();
                return ex.ToString();
            }
        }

        public string _GetAckSingle(string SubmissionID)
        {
            string result = "";
            try
            {
                result = _Login();

                if (!result.Contains("Success") && result != "")
                    return "Login Failed";
                result = "";

                GetAckErrorHandling req_GetAck = new GetAckErrorHandling();
                req_GetAck.RequireMtom = true;

                MeFTransmitterServiceWse.GetAckRequestType reqType_GetAck = new MeFTransmitterServiceWse.GetAckRequestType();
                MeFTransmitterServiceWse.GetAckResponseType resType_GetAck = new MeFTransmitterServiceWse.GetAckResponseType();
                MeFTransmitterServiceWse.MeFHeaderType MeF = new IRSLibrary.MeFTransmitterServiceWse.MeFHeaderType();
                MeF.Id = "myTestMeF";
                MeF.WSDLVersionNum = Variables.WSDLVerNum;
                MeF.MessageID = getMessageID();
                MeF.Action = "GetAck";
                MeF.MessageTs = DateTime.Now;
                MeF.ETIN = Variables.ETIN;
                MeF.SessionKeyCd = MeFTransmitterServiceWse.SessionKeyCdType.Y;
                MeF.SessionIndicatorSpecified = true;
                if (Variables.TestIndicator == "T")
                    MeF.TestCd= MeFTransmitterServiceWse.TestCdType.T;
                else
                    MeF.TestCd= MeFTransmitterServiceWse.TestCdType.P;

                MeF.TestIndicatorSpecified = true;
                MeF.AppSysID = Variables.MeF_AppSysID;
                req_GetAck.MeFHeader = MeF;

                //--------------------------------------------------------------------
               
                try
                {
                    for (int i = 0; i < SecToken.stLogin.Length; i++)
                        req_GetAck.RequestSoapContext.Security.Tokens.Add(SecToken.stLogin[i]);
                }
                catch { }





                reqType_GetAck.SubmissionId = SubmissionID;
                //reqType_GetAck.SubmissionIdType = SubmissionID; // Vishwa 9.6

                resType_GetAck = req_GetAck.CallGetAck(reqType_GetAck);

                string GetAckFileName = SubmissionID + "-Ack.zip";

                IRSLibrary.MeFTransmitterServiceWse.base64Binary attachment_get = new IRSLibrary.MeFTransmitterServiceWse.base64Binary();
                attachment_get = resType_GetAck.AcknowledgementAttMTOM;

                FileStream stream = new FileStream("C:\\" + GetAckFileName, FileMode.Create);
                byte[] buffer = attachment_get.Value;
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();

                DataSet ds = UnzipFiles(GetAckFileName);
                string ErrorList = "";
                string node = "";
                if (ds.Tables["Acknowledgement"].Columns.Contains("AcceptanceStatusTxt"))
                    node = "AcceptanceStatusTxt";
                else if (ds.Tables["Acknowledgement"].Columns.Contains("FilingStatus"))
                    node = "FilingStatus";

                if (ds.Tables["Acknowledgement"].Rows[0][node].ToString() == "Accepted")
                {
                    result= SubmissionID+ " : Accepted";
                }
                else
                {
                    if (ds.Tables.Contains("Error"))
                    {
                        foreach (DataRow dr in ds.Tables["Error"].Rows)
                        {
                            if (ErrorList.Length > 0)
                                ErrorList += "," + dr["RuleNumber"].ToString();
                            else
                                ErrorList += dr["RuleNumber"].ToString();
                        }
                        result= ErrorList.Trim();
                        result = SubmissionID + " : " + result;
                    }
                    else
                    {
                        result= SubmissionID + " : Acknowledgement Failed";			
                    }
                }

            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
                if (ex.Message == "gov.irs.efile.ef.a2a.common.MeFExceptionType")
                {
                    result= SubmissionID + " Acknowledgement Failed :::: Unable to Retrieve Acknowledgement";
                }
                else
                    result = SubmissionID + " Acknowledgement Failed :::: " + ex.Message;
            }

            _Logout();
            return result;
        }
    }
}



