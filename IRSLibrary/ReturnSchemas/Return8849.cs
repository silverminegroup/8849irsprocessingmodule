
using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace IRS_Return_8849
{

	public struct Declarations
	{
		public const string SchemaVersion = "http://www.irs.gov/efile";
	}

	[Serializable]
	public enum DocumentType
	{
		[XmlEnum(Name="PDF")] PDF
	}

	[Serializable]
	public enum PartnersPageFilingType
	{
		[XmlEnum(Name="Partners Page English")] Partners_Page_English,
		[XmlEnum(Name="Partners Page Spanish")] Partners_Page_Spanish
	}

	[Serializable]
	public enum CountryType
	{
		[XmlEnum(Name="AF")] AF,
		[XmlEnum(Name="AX")] AX,
		[XmlEnum(Name="XI")] XI,
		[XmlEnum(Name="AL")] AL,
		[XmlEnum(Name="AG")] AG,
		[XmlEnum(Name="AQ")] AQ,
		[XmlEnum(Name="AN")] AN,
		[XmlEnum(Name="AO")] AO,
		[XmlEnum(Name="AV")] AV,
		[XmlEnum(Name="AY")] AY,
		[XmlEnum(Name="AC")] AC,
		[XmlEnum(Name="AR")] AR,
		[XmlEnum(Name="AM")] AM,
		[XmlEnum(Name="AA")] AA,
		[XmlEnum(Name="XA")] XA,
		[XmlEnum(Name="AT")] AT,
		[XmlEnum(Name="AS")] @AS,
		[XmlEnum(Name="AU")] AU,
		[XmlEnum(Name="AJ")] AJ,
		[XmlEnum(Name="XZ")] XZ,
		[XmlEnum(Name="BF")] BF,
		[XmlEnum(Name="BA")] BA,
		[XmlEnum(Name="FQ")] FQ,
		[XmlEnum(Name="BG")] BG,
		[XmlEnum(Name="BB")] BB,
		[XmlEnum(Name="BO")] BO,
		[XmlEnum(Name="BE")] BE,
		[XmlEnum(Name="BH")] BH,
		[XmlEnum(Name="BN")] BN,
		[XmlEnum(Name="BD")] BD,
		[XmlEnum(Name="BT")] BT,
		[XmlEnum(Name="BL")] BL,
		[XmlEnum(Name="BK")] BK,
		[XmlEnum(Name="BC")] BC,
		[XmlEnum(Name="BV")] BV,
		[XmlEnum(Name="BR")] BR,
		[XmlEnum(Name="XB")] XB,
		[XmlEnum(Name="IO")] IO,
		[XmlEnum(Name="VI")] VI,
		[XmlEnum(Name="BX")] BX,
		[XmlEnum(Name="BU")] BU,
		[XmlEnum(Name="UV")] UV,
		[XmlEnum(Name="BM")] BM,
		[XmlEnum(Name="BY")] BY,
		[XmlEnum(Name="CB")] CB,
		[XmlEnum(Name="CM")] CM,
		[XmlEnum(Name="CA")] CA,
		[XmlEnum(Name="CV")] CV,
		[XmlEnum(Name="CJ")] CJ,
		[XmlEnum(Name="CT")] CT,
		[XmlEnum(Name="CD")] CD,
		[XmlEnum(Name="CI")] CI,
		[XmlEnum(Name="CH")] CH,
		[XmlEnum(Name="KT")] KT,
		[XmlEnum(Name="IP")] IP,
		[XmlEnum(Name="CK")] CK,
		[XmlEnum(Name="CO")] CO,
		[XmlEnum(Name="CN")] CN,
		[XmlEnum(Name="CF")] CF,
		[XmlEnum(Name="CG")] CG,
		[XmlEnum(Name="CW")] CW,
		[XmlEnum(Name="CR")] CR,
		[XmlEnum(Name="VP")] VP,
		[XmlEnum(Name="CS")] CS,
		[XmlEnum(Name="IV")] IV,
		[XmlEnum(Name="HR")] HR,
		[XmlEnum(Name="CU")] CU,
		[XmlEnum(Name="CY")] CY,
		[XmlEnum(Name="EZ")] EZ,
		[XmlEnum(Name="DA")] DA,
		[XmlEnum(Name="DX")] DX,
		[XmlEnum(Name="DJ")] DJ,
		[XmlEnum(Name="DO")] @DO,
		[XmlEnum(Name="DR")] DR,
		[XmlEnum(Name="TT")] TT,
		[XmlEnum(Name="EC")] EC,
		[XmlEnum(Name="EG")] EG,
		[XmlEnum(Name="ES")] ES,
		[XmlEnum(Name="EK")] EK,
		[XmlEnum(Name="ER")] ER,
		[XmlEnum(Name="EN")] EN,
		[XmlEnum(Name="ET")] ET,
		[XmlEnum(Name="FK")] FK,
		[XmlEnum(Name="FO")] FO,
		[XmlEnum(Name="FM")] FM,
		[XmlEnum(Name="FJ")] FJ,
		[XmlEnum(Name="FI")] FI,
		[XmlEnum(Name="FR")] FR,
		[XmlEnum(Name="FP")] FP,
		[XmlEnum(Name="FS")] FS,
		[XmlEnum(Name="GB")] GB,
		[XmlEnum(Name="GA")] GA,
		[XmlEnum(Name="GG")] GG,
		[XmlEnum(Name="GM")] GM,
		[XmlEnum(Name="GH")] GH,
		[XmlEnum(Name="GI")] GI,
		[XmlEnum(Name="GR")] GR,
		[XmlEnum(Name="GL")] GL,
		[XmlEnum(Name="GJ")] GJ,
		[XmlEnum(Name="VC")] VC,
		[XmlEnum(Name="GP")] GP,
		[XmlEnum(Name="GQ")] GQ,
		[XmlEnum(Name="GT")] GT,
		[XmlEnum(Name="GK")] GK,
		[XmlEnum(Name="GV")] GV,
		[XmlEnum(Name="PU")] PU,
		[XmlEnum(Name="GY")] GY,
		[XmlEnum(Name="HA")] HA,
		[XmlEnum(Name="HM")] HM,
		[XmlEnum(Name="VT")] VT,
		[XmlEnum(Name="HO")] HO,
		[XmlEnum(Name="HK")] HK,
		[XmlEnum(Name="HQ")] HQ,
		[XmlEnum(Name="HU")] HU,
		[XmlEnum(Name="IC")] IC,
		[XmlEnum(Name="IN")] @IN,
		[XmlEnum(Name="ID")] ID,
		[XmlEnum(Name="IR")] IR,
		[XmlEnum(Name="IZ")] IZ,
		[XmlEnum(Name="EI")] EI,
		[XmlEnum(Name="IS")] @IS,
		[XmlEnum(Name="IT")] IT,
		[XmlEnum(Name="JM")] JM,
		[XmlEnum(Name="JN")] JN,
		[XmlEnum(Name="JA")] JA,
		[XmlEnum(Name="DQ")] DQ,
		[XmlEnum(Name="JE")] JE,
		[XmlEnum(Name="JQ")] JQ,
		[XmlEnum(Name="JO")] JO,
		[XmlEnum(Name="KZ")] KZ,
		[XmlEnum(Name="KE")] KE,
		[XmlEnum(Name="KQ")] KQ,
		[XmlEnum(Name="KR")] KR,
		[XmlEnum(Name="KN")] KN,
		[XmlEnum(Name="KS")] KS,
		[XmlEnum(Name="KU")] KU,
		[XmlEnum(Name="KG")] KG,
		[XmlEnum(Name="LA")] LA,
		[XmlEnum(Name="LG")] LG,
		[XmlEnum(Name="LE")] LE,
		[XmlEnum(Name="LT")] LT,
		[XmlEnum(Name="LI")] LI,
		[XmlEnum(Name="LY")] LY,
		[XmlEnum(Name="LS")] LS,
		[XmlEnum(Name="LH")] LH,
		[XmlEnum(Name="LU")] LU,
		[XmlEnum(Name="MC")] MC,
		[XmlEnum(Name="MK")] MK,
		[XmlEnum(Name="MA")] MA,
		[XmlEnum(Name="MI")] MI,
		[XmlEnum(Name="MY")] MY,
		[XmlEnum(Name="MV")] MV,
		[XmlEnum(Name="ML")] ML,
		[XmlEnum(Name="MT")] MT,
		[XmlEnum(Name="IM")] IM,
		[XmlEnum(Name="RM")] RM,
		[XmlEnum(Name="MR")] MR,
		[XmlEnum(Name="MP")] MP,
		[XmlEnum(Name="MF")] MF,
		[XmlEnum(Name="MX")] MX,
		[XmlEnum(Name="MQ")] MQ,
		[XmlEnum(Name="MD")] MD,
		[XmlEnum(Name="MN")] MN,
		[XmlEnum(Name="MG")] MG,
		[XmlEnum(Name="MJ")] MJ,
		[XmlEnum(Name="MH")] MH,
		[XmlEnum(Name="MO")] MO,
		[XmlEnum(Name="MZ")] MZ,
		[XmlEnum(Name="WA")] WA,
		[XmlEnum(Name="NR")] NR,
		[XmlEnum(Name="BQ")] BQ,
		[XmlEnum(Name="NP")] NP,
		[XmlEnum(Name="NL")] NL,
		[XmlEnum(Name="NT")] NT,
		[XmlEnum(Name="NC")] NC,
		[XmlEnum(Name="NZ")] NZ,
		[XmlEnum(Name="NU")] NU,
		[XmlEnum(Name="NG")] NG,
		[XmlEnum(Name="NI")] NI,
		[XmlEnum(Name="NE")] NE,
		[XmlEnum(Name="NF")] NF,
		[XmlEnum(Name="CQ")] CQ,
		[XmlEnum(Name="NO")] NO,
		[XmlEnum(Name="MU")] MU,
		[XmlEnum(Name="OC")] OC,
		[XmlEnum(Name="PK")] PK,
		[XmlEnum(Name="LQ")] LQ,
		[XmlEnum(Name="PS")] PS,
		[XmlEnum(Name="PM")] PM,
		[XmlEnum(Name="PP")] PP,
		[XmlEnum(Name="PF")] PF,
		[XmlEnum(Name="PA")] PA,
		[XmlEnum(Name="PE")] PE,
		[XmlEnum(Name="RP")] RP,
		[XmlEnum(Name="PC")] PC,
		[XmlEnum(Name="PL")] PL,
		[XmlEnum(Name="PO")] PO,
		[XmlEnum(Name="RQ")] RQ,
		[XmlEnum(Name="QA")] QA,
		[XmlEnum(Name="RO")] RO,
		[XmlEnum(Name="RS")] RS,
		[XmlEnum(Name="RW")] RW,
		[XmlEnum(Name="WS")] WS,
		[XmlEnum(Name="SM")] SM,
		[XmlEnum(Name="TP")] TP,
		[XmlEnum(Name="SA")] SA,
		[XmlEnum(Name="SG")] SG,
		[XmlEnum(Name="RI")] RI,
		[XmlEnum(Name="SE")] SE,
		[XmlEnum(Name="SL")] SL,
		[XmlEnum(Name="SN")] SN,
		[XmlEnum(Name="LO")] LO,
		[XmlEnum(Name="SI")] SI,
		[XmlEnum(Name="BP")] BP,
		[XmlEnum(Name="SO")] SO,
		[XmlEnum(Name="SF")] SF,
		[XmlEnum(Name="SX")] SX,
		[XmlEnum(Name="SP")] SP,
		[XmlEnum(Name="PG")] PG,
		[XmlEnum(Name="CE")] CE,
		[XmlEnum(Name="SH")] SH,
		[XmlEnum(Name="SC")] SC,
		[XmlEnum(Name="ST")] ST,
		[XmlEnum(Name="SB")] SB,
		[XmlEnum(Name="SU")] SU,
		[XmlEnum(Name="NS")] NS,
		[XmlEnum(Name="SV")] SV,
		[XmlEnum(Name="WZ")] WZ,
		[XmlEnum(Name="SW")] SW,
		[XmlEnum(Name="SZ")] SZ,
		[XmlEnum(Name="SY")] SY,
		[XmlEnum(Name="TW")] TW,
		[XmlEnum(Name="TI")] TI,
		[XmlEnum(Name="TZ")] TZ,
		[XmlEnum(Name="TH")] TH,
		[XmlEnum(Name="TO")] TO,
		[XmlEnum(Name="TL")] TL,
		[XmlEnum(Name="TN")] TN,
		[XmlEnum(Name="TD")] TD,
		[XmlEnum(Name="TS")] TS,
		[XmlEnum(Name="TU")] TU,
		[XmlEnum(Name="TX")] TX,
		[XmlEnum(Name="TK")] TK,
		[XmlEnum(Name="TV")] TV,
		[XmlEnum(Name="UG")] UG,
		[XmlEnum(Name="UP")] UP,
		[XmlEnum(Name="AE")] AE,
		[XmlEnum(Name="UK")] UK,
		[XmlEnum(Name="UY")] UY,
		[XmlEnum(Name="UZ")] UZ,
		[XmlEnum(Name="NH")] NH,
		[XmlEnum(Name="VE")] VE,
		[XmlEnum(Name="VM")] VM,
		[XmlEnum(Name="VQ")] VQ,
		[XmlEnum(Name="WQ")] WQ,
		[XmlEnum(Name="WF")] WF,
		[XmlEnum(Name="WI")] WI,
		[XmlEnum(Name="YM")] YM,
		[XmlEnum(Name="YI")] YI,
		[XmlEnum(Name="ZA")] ZA,
		[XmlEnum(Name="ZI")] ZI
	}

	[Serializable]
	public enum OriginatorType
	{
		[XmlEnum(Name="ERO")] ERO,
		[XmlEnum(Name="OnlineFiler")] OnlineFiler,
		[XmlEnum(Name="ReportingAgent")] ReportingAgent,
		[XmlEnum(Name="IRSAgent")] IRSAgent,
		[XmlEnum(Name="FinancialAgent")] FinancialAgent,
		[XmlEnum(Name="LargeTaxpayer")] LargeTaxpayer
	}

	[Serializable]
	public enum SignatureOption
	{
		[XmlEnum(Name="PIN Number")] PIN_Number,
		[XmlEnum(Name="Binary Attachment 8453 Signature Document")] Binary_Attachment_8453_Signature_Document
	}

	[Serializable]
	public enum BankAccountType
	{
		[XmlEnum(Name="1")] _1,
		[XmlEnum(Name="2")] _2
	}

	[Serializable]
	public enum DepreciationMethodType
	{
		[XmlEnum(Name="200 DB")] _200_DB,
		[XmlEnum(Name="150 DB")] _150_DB,
		[XmlEnum(Name="DB")] DB,
		[XmlEnum(Name="S/L")] S_L
	}

	[Serializable]
	public enum SignatureOption2
	{
		[XmlEnum(Name="PIN Number")] PIN_Number,
		[XmlEnum(Name="Binary Attachment VIN Disclosure Statement")] Binary_Attachment_VIN_Disclosure_Statement
	}

	[Serializable]
	public enum DepreciationConventionType
	{
		[XmlEnum(Name="HY")] HY,
		[XmlEnum(Name="MQ")] MQ,
		[XmlEnum(Name="MM")] MM,
		[XmlEnum(Name="S/L")] S_L
	}

	[Serializable]
	public enum GovernmentCodeType
	{
		[XmlEnum(Name="IRS")] IRS,
		[XmlEnum(Name="ALST")] ALST,
		[XmlEnum(Name="AKST")] AKST,
		[XmlEnum(Name="AZST")] AZST,
		[XmlEnum(Name="ARST")] ARST,
		[XmlEnum(Name="CAST")] CAST,
		[XmlEnum(Name="COST")] COST,
		[XmlEnum(Name="CTST")] CTST,
		[XmlEnum(Name="DEST")] DEST,
		[XmlEnum(Name="DCST")] DCST,
		[XmlEnum(Name="FLST")] FLST,
		[XmlEnum(Name="GAST")] GAST,
		[XmlEnum(Name="HIST")] HIST,
		[XmlEnum(Name="IDST")] IDST,
		[XmlEnum(Name="ILST")] ILST,
		[XmlEnum(Name="INST")] INST,
		[XmlEnum(Name="IAST")] IAST,
		[XmlEnum(Name="KSST")] KSST,
		[XmlEnum(Name="KYST")] KYST,
		[XmlEnum(Name="LAST")] LAST,
		[XmlEnum(Name="MEST")] MEST,
		[XmlEnum(Name="MDST")] MDST,
		[XmlEnum(Name="MAST")] MAST,
		[XmlEnum(Name="MIST")] MIST,
		[XmlEnum(Name="MNST")] MNST,
		[XmlEnum(Name="MSST")] MSST,
		[XmlEnum(Name="MOST")] MOST,
		[XmlEnum(Name="MTST")] MTST,
		[XmlEnum(Name="NEST")] NEST,
		[XmlEnum(Name="NVST")] NVST,
		[XmlEnum(Name="NHST")] NHST,
		[XmlEnum(Name="NJST")] NJST,
		[XmlEnum(Name="NMST")] NMST,
		[XmlEnum(Name="NYST")] NYST,
		[XmlEnum(Name="NCST")] NCST,
		[XmlEnum(Name="NDST")] NDST,
		[XmlEnum(Name="OHST")] OHST,
		[XmlEnum(Name="OKST")] OKST,
		[XmlEnum(Name="ORST")] ORST,
		[XmlEnum(Name="PAST")] PAST,
		[XmlEnum(Name="RIST")] RIST,
		[XmlEnum(Name="SCST")] SCST,
		[XmlEnum(Name="SDST")] SDST,
		[XmlEnum(Name="TNST")] TNST,
		[XmlEnum(Name="TXST")] TXST,
		[XmlEnum(Name="UTST")] UTST,
		[XmlEnum(Name="VTST")] VTST,
		[XmlEnum(Name="VAST")] VAST,
		[XmlEnum(Name="WAST")] WAST,
		[XmlEnum(Name="WVST")] WVST,
		[XmlEnum(Name="WIST")] WIST,
		[XmlEnum(Name="WYST")] WYST,
		[XmlEnum(Name="NYCT")] NYCT
	}

	[Serializable]
	public enum StateType
	{
		[XmlEnum(Name="AL")] AL,
		[XmlEnum(Name="AK")] AK,
		[XmlEnum(Name="AS")] @AS,
		[XmlEnum(Name="AZ")] AZ,
		[XmlEnum(Name="AR")] AR,
		[XmlEnum(Name="CA")] CA,
		[XmlEnum(Name="CO")] CO,
		[XmlEnum(Name="MP")] MP,
		[XmlEnum(Name="CT")] CT,
		[XmlEnum(Name="DE")] DE,
		[XmlEnum(Name="DC")] DC,
		[XmlEnum(Name="FM")] FM,
		[XmlEnum(Name="FL")] FL,
		[XmlEnum(Name="GA")] GA,
		[XmlEnum(Name="GU")] GU,
		[XmlEnum(Name="HI")] HI,
		[XmlEnum(Name="ID")] ID,
		[XmlEnum(Name="IL")] IL,
		[XmlEnum(Name="IN")] @IN,
		[XmlEnum(Name="IA")] IA,
		[XmlEnum(Name="KS")] KS,
		[XmlEnum(Name="KY")] KY,
		[XmlEnum(Name="LA")] LA,
		[XmlEnum(Name="ME")] ME,
		[XmlEnum(Name="MH")] MH,
		[XmlEnum(Name="MD")] MD,
		[XmlEnum(Name="MA")] MA,
		[XmlEnum(Name="MI")] MI,
		[XmlEnum(Name="MN")] MN,
		[XmlEnum(Name="MS")] MS,
		[XmlEnum(Name="MO")] MO,
		[XmlEnum(Name="MT")] MT,
		[XmlEnum(Name="NE")] NE,
		[XmlEnum(Name="NV")] NV,
		[XmlEnum(Name="NH")] NH,
		[XmlEnum(Name="NJ")] NJ,
		[XmlEnum(Name="NM")] NM,
		[XmlEnum(Name="NY")] NY,
		[XmlEnum(Name="NC")] NC,
		[XmlEnum(Name="ND")] ND,
		[XmlEnum(Name="OH")] OH,
		[XmlEnum(Name="OK")] OK,
		[XmlEnum(Name="OR")] OR,
		[XmlEnum(Name="PW")] PW,
		[XmlEnum(Name="PA")] PA,
		[XmlEnum(Name="PR")] PR,
		[XmlEnum(Name="RI")] RI,
		[XmlEnum(Name="SC")] SC,
		[XmlEnum(Name="SD")] SD,
		[XmlEnum(Name="TN")] TN,
		[XmlEnum(Name="TX")] TX,
		[XmlEnum(Name="VI")] VI,
		[XmlEnum(Name="UT")] UT,
		[XmlEnum(Name="VT")] VT,
		[XmlEnum(Name="VA")] VA,
		[XmlEnum(Name="WA")] WA,
		[XmlEnum(Name="WV")] WV,
		[XmlEnum(Name="WI")] WI,
		[XmlEnum(Name="WY")] WY,
		[XmlEnum(Name="AA")] AA,
		[XmlEnum(Name="AE")] AE,
		[XmlEnum(Name="AP")] AP
	}

	[Serializable]
	public enum TimezoneType
	{
		[XmlEnum(Name="US")] US,
		[XmlEnum(Name="ES")] ES,
		[XmlEnum(Name="ED")] ED,
		[XmlEnum(Name="CS")] CS,
		[XmlEnum(Name="CD")] CD,
		[XmlEnum(Name="MS")] MS,
		[XmlEnum(Name="MD")] MD,
		[XmlEnum(Name="PS")] PS,
		[XmlEnum(Name="PD")] PD,
		[XmlEnum(Name="AS")] @AS,
		[XmlEnum(Name="AD")] AD,
		[XmlEnum(Name="HS")] HS,
		[XmlEnum(Name="HD")] HD
	}

	[Serializable]
	public enum PINEnteredBy
	{
		[XmlEnum(Name="Taxpayer")] Taxpayer,
		[XmlEnum(Name="ERO")] ERO
	}

	[Serializable]
	public enum MethodOfAccountingType
	{
		[XmlEnum(Name="cash")] cash,
		[XmlEnum(Name="accrual")] accrual,
		[XmlEnum(Name="hybrid")] hybrid
	}

	[Serializable]
	public enum PINCodeType
	{
		[XmlEnum(Name="Practitioner")] Practitioner,
		[XmlEnum(Name="Self-Select Practitioner")] Self_Select_Practitioner,
		[XmlEnum(Name="Self-Select On-Line")] Self_Select_On_Line
	}

	[Serializable]
	public enum ReturnType
	{
		[XmlEnum(Name="2290")] _2290
	}

	[Serializable]
	public enum CheckboxType
	{
		[XmlEnum(Name="X")] X
	}

	[Serializable]
	public enum PINEnteredByType
	{
		[XmlEnum(Name="Taxpayer")] Taxpayer,
		[XmlEnum(Name="ERO")] ERO
	}

	[Serializable]
	public enum StringVARIOUSType
	{
		[XmlEnum(Name="VARIOUS")] VARIOUS
	}

	[Serializable]
	public enum ConsortiumType
	{
		[XmlEnum(Name="English Free-File")] English_Free_File,
		[XmlEnum(Name="Spanish Free-File")] Spanish_Free_File,
		[XmlEnum(Name="Free Fillable Forms")] Free_Fillable_Forms
	}

	[Serializable]
	public enum MissingEINReason
	{
		[XmlEnum(Name="APPLD FOR")] APPLD_FOR,
		[XmlEnum(Name="FOREIGNUS")] FOREIGNUS
	}


	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class IRSPayment2Collection : ArrayList
	{
		public IRS_Return_8849.IRSPayment2 Add(IRS_Return_8849.IRSPayment2 obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.IRSPayment2 Add()
		{
			return Add(new IRS_Return_8849.IRSPayment2());
		}

		public void Insert(int index, IRS_Return_8849.IRSPayment2 obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.IRSPayment2 obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.IRSPayment2 this[int index]
		{
			get { return (IRS_Return_8849.IRSPayment2) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class StmtInSupportOfSuspensionInfoTypeCollection : ArrayList
	{
		public IRS_Return_8849.StmtInSupportOfSuspensionInfoType Add(IRS_Return_8849.StmtInSupportOfSuspensionInfoType obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.StmtInSupportOfSuspensionInfoType Add()
		{
			return Add(new IRS_Return_8849.StmtInSupportOfSuspensionInfoType());
		}

		public void Insert(int index, IRS_Return_8849.StmtInSupportOfSuspensionInfoType obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.StmtInSupportOfSuspensionInfoType obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.StmtInSupportOfSuspensionInfoType this[int index]
		{
			get { return (IRS_Return_8849.StmtInSupportOfSuspensionInfoType) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SuspendedVINStatementCollection : ArrayList
	{
		public IRS_Return_8849.SuspendedVINStatement Add(IRS_Return_8849.SuspendedVINStatement obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.SuspendedVINStatement Add()
		{
			return Add(new IRS_Return_8849.SuspendedVINStatement());
		}

		public void Insert(int index, IRS_Return_8849.SuspendedVINStatement obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.SuspendedVINStatement obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.SuspendedVINStatement this[int index]
		{
			get { return (IRS_Return_8849.SuspendedVINStatement) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class DisposalReportingItemCollection : ArrayList
	{
		public DisposalReportingItem Add(DisposalReportingItem obj)
		{
			base.Add(obj);
			return obj;
		}

		public DisposalReportingItem Add()
		{
			return Add(new DisposalReportingItem());
		}

		public void Insert(int index, DisposalReportingItem obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(DisposalReportingItem obj)
		{
			base.Remove(obj);
		}

		new public DisposalReportingItem this[int index]
		{
			get { return (DisposalReportingItem) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SpecialConditionDescriptionCollection : ArrayList
	{
		public string Add(string obj)
		{
			base.Add(obj);
			return obj;
		}

		public void Insert(int index, string obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(string obj)
		{
			base.Remove(obj);
		}

		new public string this[int index]
		{
			get { return (string) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TaxComputationCollection : ArrayList
	{
		public TaxComputation Add(TaxComputation obj)
		{
			base.Add(obj);
			return obj;
		}

		public TaxComputation Add()
		{
			return Add(new TaxComputation());
		}

		public void Insert(int index, TaxComputation obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(TaxComputation obj)
		{
			base.Remove(obj);
		}

		new public TaxComputation this[int index]
		{
			get { return (TaxComputation) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SuspendedVINInfoTypeItemCollection : ArrayList
	{
		public SuspendedVINInfoTypeItem Add(SuspendedVINInfoTypeItem obj)
		{
			base.Add(obj);
			return obj;
		}

		public SuspendedVINInfoTypeItem Add()
		{
			return Add(new SuspendedVINInfoTypeItem());
		}

		public void Insert(int index, SuspendedVINInfoTypeItem obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(SuspendedVINInfoTypeItem obj)
		{
			base.Remove(obj);
		}

		new public SuspendedVINInfoTypeItem this[int index]
		{
			get { return (SuspendedVINInfoTypeItem) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ItemCollection : ArrayList
	{
		public Item Add(Item obj)
		{
			base.Add(obj);
			return obj;
		}

		public Item Add()
		{
			return Add(new Item());
		}

		public void Insert(int index, Item obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(Item obj)
		{
			base.Remove(obj);
		}

		new public Item this[int index]
		{
			get { return (Item) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class StmtInSupportOfSuspensionCollection : ArrayList
	{
		public IRS_Return_8849.StmtInSupportOfSuspension Add(IRS_Return_8849.StmtInSupportOfSuspension obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.StmtInSupportOfSuspension Add()
		{
			return Add(new IRS_Return_8849.StmtInSupportOfSuspension());
		}

		public void Insert(int index, IRS_Return_8849.StmtInSupportOfSuspension obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.StmtInSupportOfSuspension obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.StmtInSupportOfSuspension this[int index]
		{
			get { return (IRS_Return_8849.StmtInSupportOfSuspension) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class CreditsAmountInfoTypeCollection : ArrayList
	{
		public IRS_Return_8849.CreditsAmountInfoType Add(IRS_Return_8849.CreditsAmountInfoType obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.CreditsAmountInfoType Add()
		{
			return Add(new IRS_Return_8849.CreditsAmountInfoType());
		}

		public void Insert(int index, IRS_Return_8849.CreditsAmountInfoType obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.CreditsAmountInfoType obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.CreditsAmountInfoType this[int index]
		{
			get { return (IRS_Return_8849.CreditsAmountInfoType) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TGWIncreaseWorksheetCollection : ArrayList
	{
		public IRS_Return_8849.TGWIncreaseWorksheet Add(IRS_Return_8849.TGWIncreaseWorksheet obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.TGWIncreaseWorksheet Add()
		{
			return Add(new IRS_Return_8849.TGWIncreaseWorksheet());
		}

		public void Insert(int index, IRS_Return_8849.TGWIncreaseWorksheet obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.TGWIncreaseWorksheet obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.TGWIncreaseWorksheet this[int index]
		{
			get { return (IRS_Return_8849.TGWIncreaseWorksheet) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ReasonableCauseExplnCollection : ArrayList
	{
		public IRS_Return_8849.ReasonableCauseExpln Add(IRS_Return_8849.ReasonableCauseExpln obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.ReasonableCauseExpln Add()
		{
			return Add(new IRS_Return_8849.ReasonableCauseExpln());
		}

		public void Insert(int index, IRS_Return_8849.ReasonableCauseExpln obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.ReasonableCauseExpln obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.ReasonableCauseExpln this[int index]
		{
			get { return (IRS_Return_8849.ReasonableCauseExpln) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SuspendedVINInfoTypeCollection : ArrayList
	{
		public IRS_Return_8849.SuspendedVINInfoType Add(IRS_Return_8849.SuspendedVINInfoType obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.SuspendedVINInfoType Add()
		{
			return Add(new IRS_Return_8849.SuspendedVINInfoType());
		}

		public void Insert(int index, IRS_Return_8849.SuspendedVINInfoType obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.SuspendedVINInfoType obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.SuspendedVINInfoType this[int index]
		{
			get { return (IRS_Return_8849.SuspendedVINInfoType) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class BinaryAttachmentCollection : ArrayList
	{
		public IRS_Return_8849.BinaryAttachment Add(IRS_Return_8849.BinaryAttachment obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.BinaryAttachment Add()
		{
			return Add(new IRS_Return_8849.BinaryAttachment());
		}

		public void Insert(int index, IRS_Return_8849.BinaryAttachment obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.BinaryAttachment obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.BinaryAttachment this[int index]
		{
			get { return (IRS_Return_8849.BinaryAttachment) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TGWIncreaseInfoTypeCollection : ArrayList
	{
		public IRS_Return_8849.TGWIncreaseInfoType Add(IRS_Return_8849.TGWIncreaseInfoType obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.TGWIncreaseInfoType Add()
		{
			return Add(new IRS_Return_8849.TGWIncreaseInfoType());
		}

		public void Insert(int index, IRS_Return_8849.TGWIncreaseInfoType obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.TGWIncreaseInfoType obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.TGWIncreaseInfoType this[int index]
		{
			get { return (IRS_Return_8849.TGWIncreaseInfoType) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class VehicleReportTaxItemCollection : ArrayList
	{
		public VehicleReportTaxItem Add(VehicleReportTaxItem obj)
		{
			base.Add(obj);
			return obj;
		}

		public VehicleReportTaxItem Add()
		{
			return Add(new VehicleReportTaxItem());
		}

		public void Insert(int index, VehicleReportTaxItem obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(VehicleReportTaxItem obj)
		{
			base.Remove(obj);
		}

		new public VehicleReportTaxItem this[int index]
		{
			get { return (VehicleReportTaxItem) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class CreditsAmountStatementCollection : ArrayList
	{
		public IRS_Return_8849.CreditsAmountStatement Add(IRS_Return_8849.CreditsAmountStatement obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.CreditsAmountStatement Add()
		{
			return Add(new IRS_Return_8849.CreditsAmountStatement());
		}

		public void Insert(int index, IRS_Return_8849.CreditsAmountStatement obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.CreditsAmountStatement obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.CreditsAmountStatement this[int index]
		{
			get { return (IRS_Return_8849.CreditsAmountStatement) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class VehicleSuspendedTaxItemCollection : ArrayList
	{
		public VehicleSuspendedTaxItem Add(VehicleSuspendedTaxItem obj)
		{
			base.Add(obj);
			return obj;
		}

		public VehicleSuspendedTaxItem Add()
		{
			return Add(new VehicleSuspendedTaxItem());
		}

		public void Insert(int index, VehicleSuspendedTaxItem obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(VehicleSuspendedTaxItem obj)
		{
			base.Remove(obj);
		}

		new public VehicleSuspendedTaxItem this[int index]
		{
			get { return (VehicleSuspendedTaxItem) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class GeneralDependencyMediumCollection : ArrayList
	{
		public IRS_Return_8849.GeneralDependencyMedium Add(IRS_Return_8849.GeneralDependencyMedium obj)
		{
			base.Add(obj);
			return obj;
		}

		public IRS_Return_8849.GeneralDependencyMedium Add()
		{
			return Add(new IRS_Return_8849.GeneralDependencyMedium());
		}

		public void Insert(int index, IRS_Return_8849.GeneralDependencyMedium obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(IRS_Return_8849.GeneralDependencyMedium obj)
		{
			base.Remove(obj);
		}

		new public IRS_Return_8849.GeneralDependencyMedium this[int index]
		{
			get { return (IRS_Return_8849.GeneralDependencyMedium) base[index]; }
			set { base[index] = value; }
		}
	}



	[XmlType(TypeName="USItemizedEntryType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class USItemizedEntryType
	{

		[XmlElement(ElementName="Description",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Description;
		
		[XmlIgnore]
		public string Description
		{ 
			get { return __Description; }
			set { __Description = value; }
		}

		[XmlElement(ElementName="Amount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="integer",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Amount;
		
		[XmlIgnore]
		public string Amount
		{ 
			get { return __Amount; }
			set { __Amount = value; }
		}

		public USItemizedEntryType()
		{
		}
	}


	[XmlType(TypeName="ForeignItemizedEntryType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ForeignItemizedEntryType
	{

		[XmlElement(ElementName="Description",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Description;
		
		[XmlIgnore]
		public string Description
		{ 
			get { return __Description; }
			set { __Description = value; }
		}

		[XmlElement(ElementName="Amount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="integer",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Amount;
		
		[XmlIgnore]
		public string Amount
		{ 
			get { return __Amount; }
			set { __Amount = value; }
		}

		public ForeignItemizedEntryType()
		{
		}
	}


	[XmlType(TypeName="USAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class USAddressType
	{

		[XmlElement(ElementName="AddressLine1",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine1;
		
		[XmlIgnore]
		public string AddressLine1
		{ 
			get { return __AddressLine1; }
			set { __AddressLine1 = value; }
		}

		[XmlElement(ElementName="AddressLine2",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine2;
		
		[XmlIgnore]
		public string AddressLine2
		{ 
			get { return __AddressLine2; }
			set { __AddressLine2 = value; }
		}

		[XmlElement(ElementName="City",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __City;
		
		[XmlIgnore]
		public string City
		{ 
			get { return __City; }
			set { __City = value; }
		}

		[XmlElement(ElementName="State",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.StateType __State;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __StateSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.StateType State
		{ 
			get { return __State; }
			set { __State = value; __StateSpecified = true; }
		}

		[XmlElement(ElementName="ZIPCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ZIPCode;
		
		[XmlIgnore]
		public string ZIPCode
		{ 
			get { return __ZIPCode; }
			set { __ZIPCode = value; }
		}

		public USAddressType()
		{
		}
	}


	[XmlType(TypeName="OtherUSAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class OtherUSAddressType
	{

		[XmlElement(ElementName="AddressLine1",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine1;
		
		[XmlIgnore]
		public string AddressLine1
		{ 
			get { return __AddressLine1; }
			set { __AddressLine1 = value; }
		}

		[XmlElement(ElementName="AddressLine2",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine2;
		
		[XmlIgnore]
		public string AddressLine2
		{ 
			get { return __AddressLine2; }
			set { __AddressLine2 = value; }
		}

		[XmlElement(ElementName="City",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __City;
		
		[XmlIgnore]
		public string City
		{ 
			get { return __City; }
			set { __City = value; }
		}

		[XmlElement(ElementName="State",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.StateType __State;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __StateSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.StateType State
		{ 
			get { return __State; }
			set { __State = value; __StateSpecified = true; }
		}

		[XmlElement(ElementName="ZIPCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ZIPCode;
		
		[XmlIgnore]
		public string ZIPCode
		{ 
			get { return __ZIPCode; }
			set { __ZIPCode = value; }
		}

		public OtherUSAddressType()
		{
		}
	}


	[XmlType(TypeName="ForeignAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ForeignAddressType
	{

		[XmlElement(ElementName="AddressLine1",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine1;
		
		[XmlIgnore]
		public string AddressLine1
		{ 
			get { return __AddressLine1; }
			set { __AddressLine1 = value; }
		}

		[XmlElement(ElementName="AddressLine2",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine2;
		
		[XmlIgnore]
		public string AddressLine2
		{ 
			get { return __AddressLine2; }
			set { __AddressLine2 = value; }
		}

		[XmlElement(ElementName="City",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __City;
		
		[XmlIgnore]
		public string City
		{ 
			get { return __City; }
			set { __City = value; }
		}

		[XmlElement(ElementName="ProvinceOrState",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ProvinceOrState;
		
		[XmlIgnore]
		public string ProvinceOrState
		{ 
			get { return __ProvinceOrState; }
			set { __ProvinceOrState = value; }
		}

		[XmlElement(ElementName="Country",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CountryType __Country;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __CountrySpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CountryType Country
		{ 
			get { return __Country; }
			set { __Country = value; __CountrySpecified = true; }
		}

		[XmlElement(ElementName="PostalCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PostalCode;
		
		[XmlIgnore]
		public string PostalCode
		{ 
			get { return __PostalCode; }
			set { __PostalCode = value; }
		}

		public ForeignAddressType()
		{
		}
	}


	[XmlType(TypeName="OtherForeignAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class OtherForeignAddressType
	{

		[XmlElement(ElementName="AddressLine1",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine1;
		
		[XmlIgnore]
		public string AddressLine1
		{ 
			get { return __AddressLine1; }
			set { __AddressLine1 = value; }
		}

		[XmlElement(ElementName="AddressLine2",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AddressLine2;
		
		[XmlIgnore]
		public string AddressLine2
		{ 
			get { return __AddressLine2; }
			set { __AddressLine2 = value; }
		}

		[XmlElement(ElementName="City",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __City;
		
		[XmlIgnore]
		public string City
		{ 
			get { return __City; }
			set { __City = value; }
		}

		[XmlElement(ElementName="ProvinceOrState",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ProvinceOrState;
		
		[XmlIgnore]
		public string ProvinceOrState
		{ 
			get { return __ProvinceOrState; }
			set { __ProvinceOrState = value; }
		}

		[XmlElement(ElementName="Country",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CountryType __Country;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __CountrySpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CountryType Country
		{ 
			get { return __Country; }
			set { __Country = value; __CountrySpecified = true; }
		}

		[XmlElement(ElementName="PostalCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PostalCode;
		
		[XmlIgnore]
		public string PostalCode
		{ 
			get { return __PostalCode; }
			set { __PostalCode = value; }
		}

		public OtherForeignAddressType()
		{
		}
	}


	[XmlType(TypeName="BusinessNameType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class BusinessNameType
	{

		[XmlElement(ElementName="BusinessNameLine1",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __BusinessNameLine1;
		
		[XmlIgnore]
		public string BusinessNameLine1
		{ 
			get { return __BusinessNameLine1; }
			set { __BusinessNameLine1 = value; }
		}

		[XmlElement(ElementName="BusinessNameLine2",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __BusinessNameLine2;
		
		[XmlIgnore]
		public string BusinessNameLine2
		{ 
			get { return __BusinessNameLine2; }
			set { __BusinessNameLine2 = value; }
		}

		public BusinessNameType()
		{
		}
	}


	[XmlType(TypeName="NameAndAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class NameAndAddressType
	{

		[XmlElement(ElementName="PersonName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PersonName;
		
		[XmlIgnore]
		public string PersonName
		{ 
			get { return __PersonName; }
			set { __PersonName = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.BusinessNameType),ElementName="BusinessName",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BusinessNameType __BusinessName;
		
		[XmlIgnore]
		public IRS_Return_8849.BusinessNameType BusinessName
		{
			get
			{
				if (__BusinessName == null) __BusinessName = new IRS_Return_8849.BusinessNameType();		
				return __BusinessName;
			}
			set {__BusinessName = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.USAddressType),ElementName="AddressInUS",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.USAddressType __AddressInUS;
		
		[XmlIgnore]
		public IRS_Return_8849.USAddressType AddressInUS
		{
			get
			{
				if (__AddressInUS == null) __AddressInUS = new IRS_Return_8849.USAddressType();		
				return __AddressInUS;
			}
			set {__AddressInUS = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ForeignAddressType),ElementName="AddressInForeign",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.ForeignAddressType __AddressInForeign;
		
		[XmlIgnore]
		public IRS_Return_8849.ForeignAddressType AddressInForeign
		{
			get
			{
				if (__AddressInForeign == null) __AddressInForeign = new IRS_Return_8849.ForeignAddressType();		
				return __AddressInForeign;
			}
			set {__AddressInForeign = value;}
		}

		public NameAndAddressType()
		{
		}
	}


	[XmlType(TypeName="PersonFullNameType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class PersonFullNameType
	{

		[XmlElement(ElementName="PersonFirstName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PersonFirstName;
		
		[XmlIgnore]
		public string PersonFirstName
		{ 
			get { return __PersonFirstName; }
			set { __PersonFirstName = value; }
		}

		[XmlElement(ElementName="PersonLastName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PersonLastName;
		
		[XmlIgnore]
		public string PersonLastName
		{ 
			get { return __PersonLastName; }
			set { __PersonLastName = value; }
		}

		public PersonFullNameType()
		{
		}
	}


	[XmlType(TypeName="IPAddressType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class IPAddressType
	{

		[XmlElement(ElementName="IPv4Address",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __IPv4Address;
		
		[XmlIgnore]
		public string IPv4Address
		{ 
			get { return __IPv4Address; }
			set { __IPv4Address = value; }
		}

		[XmlElement(ElementName="IPv6Address",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __IPv6Address;
		
		[XmlIgnore]
		public string IPv6Address
		{ 
			get { return __IPv6Address; }
			set { __IPv6Address = value; }
		}

		public IPAddressType()
		{
		}
	}


	[XmlType(TypeName="VehicleDescriptionType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class VehicleDescriptionType
	{

		[XmlElement(ElementName="VehicleYear",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="gYear",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VehicleYear;
		
		[XmlIgnore]
		public string VehicleYear
		{ 
			get { return __VehicleYear; }
			set { __VehicleYear = value; }
		}

		[XmlElement(ElementName="VehicleMake",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VehicleMake;
		
		[XmlIgnore]
		public string VehicleMake
		{ 
			get { return __VehicleMake; }
			set { __VehicleMake = value; }
		}

		[XmlElement(ElementName="VehicleModel",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VehicleModel;
		
		[XmlIgnore]
		public string VehicleModel
		{ 
			get { return __VehicleModel; }
			set { __VehicleModel = value; }
		}

		public VehicleDescriptionType()
		{
		}
	}


	[XmlType(TypeName="ReturnHeaderType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ReturnHeaderType
	{

		[XmlAttribute(AttributeName="binaryAttachmentCount",Form=XmlSchemaForm.Unqualified,DataType="nonNegativeInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __binaryAttachmentCount;
		
		[XmlIgnore]
		public string binaryAttachmentCount
		{ 
			get { return __binaryAttachmentCount; }
			set { __binaryAttachmentCount = value; }
		}

		[XmlElement(ElementName="Timestamp",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Timestamp;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TimestampSpecified;
		
		[XmlIgnore]
		public string Timestamp
		{ 
			get { return __Timestamp; }
			set { __Timestamp = value; __TimestampSpecified = true; }
		}
		
		[XmlIgnore]
		public string TimestampUtc
		{ 
			get { return  Convert.ToDateTime(__Timestamp).ToUniversalTime().ToString(); }
			set { __Timestamp = Convert.ToDateTime(value).ToLocalTime().ToString(); __TimestampSpecified = true; }

		}

		[XmlElement(ElementName="FirstUsedDate",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="gYearMonth",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __FirstUsedDate;
		
		[XmlIgnore]
		public string FirstUsedDate
		{ 
			get { return __FirstUsedDate; }
			set { __FirstUsedDate = value; }
		}

		[XmlElement(ElementName="DisasterRelief",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DisasterRelief;
		
		[XmlIgnore]
		public string DisasterRelief
		{ 
			get { return __DisasterRelief; }
			set { __DisasterRelief = value; }
		}

		[XmlElement(ElementName="ISPNumber",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ISPNumber;
		
		[XmlIgnore]
		public string ISPNumber
		{ 
			get { return __ISPNumber; }
			set { __ISPNumber = value; }
		}

		[XmlElement(Type=typeof(PreparerFirm),ElementName="PreparerFirm",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PreparerFirm __PreparerFirm;
		
		[XmlIgnore]
		public PreparerFirm PreparerFirm
		{
			get
			{
				if (__PreparerFirm == null) __PreparerFirm = new PreparerFirm();		
				return __PreparerFirm;
			}
			set {__PreparerFirm = value;}
		}

		[XmlElement(ElementName="SoftwareId",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __SoftwareId;
		
		[XmlIgnore]
		public string SoftwareId
		{ 
			get { return __SoftwareId; }
			set { __SoftwareId = value; }
		}
        /* Vishwa 2015 */
        /*
		[XmlElement(ElementName="SoftwareVersion",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
         
		public string __SoftwareVersion;
		
		[XmlIgnore]
		public string SoftwareVersion
		{ 
			get { return __SoftwareVersion; }
			set { __SoftwareVersion = value; }
		}
        * */
		[XmlElement(ElementName="MultipleSoftwarePackagesUsed",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="boolean",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __MultipleSoftwarePackagesUsed;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __MultipleSoftwarePackagesUsedSpecified;
		
		[XmlIgnore]
		public bool MultipleSoftwarePackagesUsed
		{ 
			get { return __MultipleSoftwarePackagesUsed; }
			set { __MultipleSoftwarePackagesUsed = value; __MultipleSoftwarePackagesUsedSpecified = true; }
		}

		[XmlElement(Type=typeof(Originator),ElementName="Originator",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Originator __Originator;
		
		[XmlIgnore]
		public Originator Originator
		{
			get
			{
				if (__Originator == null) __Originator = new Originator();		
				return __Originator;
			}
			set {__Originator = value;}
		}

		[XmlElement(ElementName="PINEnteredBy",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PINEnteredBy __PINEnteredBy;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __PINEnteredBySpecified;
		
		[XmlIgnore]
		public PINEnteredBy PINEnteredBy
		{ 
			get { return __PINEnteredBy; }
			set { __PINEnteredBy = value; __PINEnteredBySpecified = true; }
		}

		[XmlElement(ElementName="SignatureOption",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SignatureOption __SignatureOption;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __SignatureOptionSpecified;
		
		[XmlIgnore]
		public SignatureOption SignatureOption
		{ 
			get { return __SignatureOption; }
			set { __SignatureOption = value; __SignatureOptionSpecified = true; }
		}

		[XmlElement(Type=typeof(ThirdPartyDesignee),ElementName="ThirdPartyDesignee",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ThirdPartyDesignee __ThirdPartyDesignee;
		
		[XmlIgnore]
		public ThirdPartyDesignee ThirdPartyDesignee
		{
			get
			{
				if (__ThirdPartyDesignee == null) __ThirdPartyDesignee = new ThirdPartyDesignee();		
				return __ThirdPartyDesignee;
			}
			set {__ThirdPartyDesignee = value;}
		}

		[XmlElement(ElementName="ReturnType",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ReturnType __ReturnType;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __ReturnTypeSpecified;
		
		[XmlIgnore]
		public ReturnType ReturnType
		{ 
			get { return __ReturnType; }
			set { __ReturnType = value; __ReturnTypeSpecified = true; }
		}

		[XmlElement(Type=typeof(Filer),ElementName="Filer",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Filer __Filer;
		
		[XmlIgnore]
		public Filer Filer
		{
			get
			{
				if (__Filer == null) __Filer = new Filer();		
				return __Filer;
			}
			set {__Filer = value;}
		}

		[XmlElement(Type=typeof(Officer),ElementName="Officer",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Officer __Officer;
		
		[XmlIgnore]
		public Officer Officer
		{
			get
			{
				if (__Officer == null) __Officer = new Officer();		
				return __Officer;
			}
			set {__Officer = value;}
		}

		[XmlElement(Type=typeof(Preparer),ElementName="Preparer",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Preparer __Preparer;
		
		[XmlIgnore]
		public Preparer Preparer
		{
			get
			{
				if (__Preparer == null) __Preparer = new Preparer();		
				return __Preparer;
			}
			set {__Preparer = value;}
		}

		[XmlElement(Type=typeof(ConsentToVINDataDisclosure),ElementName="ConsentToVINDataDisclosure",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ConsentToVINDataDisclosure __ConsentToVINDataDisclosure;
		
		[XmlIgnore]
		public ConsentToVINDataDisclosure ConsentToVINDataDisclosure
		{
			get
			{
				if (__ConsentToVINDataDisclosure == null) __ConsentToVINDataDisclosure = new ConsentToVINDataDisclosure();		
				return __ConsentToVINDataDisclosure;
			}
			set {__ConsentToVINDataDisclosure = value;}
		}

		[XmlElement(ElementName="TaxYear",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="gYear",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TaxYear;
		
		[XmlIgnore]
		public string TaxYear
		{ 
			get { return __TaxYear; }
			set { __TaxYear = value; }
		}

		public ReturnHeaderType()
		{
			__Timestamp = DateTime.Now.ToString();
		}
	}


	[XmlType(TypeName="PreparerFirm",Namespace=Declarations.SchemaVersion),Serializable]
	public class PreparerFirm
	{

		[XmlElement(ElementName="EIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EIN;
		
		[XmlIgnore]
		public string EIN
		{ 
			get { return __EIN; }
			set { __EIN = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.BusinessNameType),ElementName="PreparerFirmBusinessName",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BusinessNameType __PreparerFirmBusinessName;
		
		[XmlIgnore]
		public IRS_Return_8849.BusinessNameType PreparerFirmBusinessName
		{
			get
			{
				if (__PreparerFirmBusinessName == null) __PreparerFirmBusinessName = new IRS_Return_8849.BusinessNameType();		
				return __PreparerFirmBusinessName;
			}
			set {__PreparerFirmBusinessName = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.USAddressType),ElementName="PreparerFirmUSAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.USAddressType __PreparerFirmUSAddress;
		
		[XmlIgnore]
		public IRS_Return_8849.USAddressType PreparerFirmUSAddress
		{
			get
			{
				if (__PreparerFirmUSAddress == null) __PreparerFirmUSAddress = new IRS_Return_8849.USAddressType();		
				return __PreparerFirmUSAddress;
			}
			set {__PreparerFirmUSAddress = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ForeignAddressType),ElementName="PreparerFirmForeignAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.ForeignAddressType __PreparerFirmForeignAddress;
		
		[XmlIgnore]
		public IRS_Return_8849.ForeignAddressType PreparerFirmForeignAddress
		{
			get
			{
				if (__PreparerFirmForeignAddress == null) __PreparerFirmForeignAddress = new IRS_Return_8849.ForeignAddressType();		
				return __PreparerFirmForeignAddress;
			}
			set {__PreparerFirmForeignAddress = value;}
		}

		public PreparerFirm()
		{
		}
	}


	[XmlType(TypeName="Originator",Namespace=Declarations.SchemaVersion),Serializable]
	public class Originator
	{

		[XmlElement(ElementName="EFIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EFIN;
		
		[XmlIgnore]
		public string EFIN
		{ 
			get { return __EFIN; }
			set { __EFIN = value; }
		}

		[XmlElement(ElementName="Type",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.OriginatorType __Type;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TypeSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.OriginatorType Type
		{ 
			get { return __Type; }
			set { __Type = value; __TypeSpecified = true; }
		}

		[XmlElement(Type=typeof(PractitionerPIN),ElementName="PractitionerPIN",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PractitionerPIN __PractitionerPIN;
		
		[XmlIgnore]
		public PractitionerPIN PractitionerPIN
		{
			get
			{
				if (__PractitionerPIN == null) __PractitionerPIN = new PractitionerPIN();		
				return __PractitionerPIN;
			}
			set {__PractitionerPIN = value;}
		}

		public Originator()
		{
		}
	}


	[XmlType(TypeName="PractitionerPIN",Namespace=Declarations.SchemaVersion),Serializable]
	public class PractitionerPIN
	{

		[XmlElement(ElementName="EFIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EFIN;
		
		[XmlIgnore]
		public string EFIN
		{ 
			get { return __EFIN; }
			set { __EFIN = value; }
		}

		[XmlElement(ElementName="PIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PIN;
		
		[XmlIgnore]
		public string PIN
		{ 
			get { return __PIN; }
			set { __PIN = value; }
		}

		public PractitionerPIN()
		{
		}
	}


	[XmlType(TypeName="ThirdPartyDesignee",Namespace=Declarations.SchemaVersion),Serializable]
	public class ThirdPartyDesignee
	{

		[XmlElement(ElementName="DiscussWithThirdPartyYes",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __DiscussWithThirdPartyYes;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DiscussWithThirdPartyYesSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType DiscussWithThirdPartyYes
		{ 
			get { return __DiscussWithThirdPartyYes; }
			set { __DiscussWithThirdPartyYes = value; __DiscussWithThirdPartyYesSpecified = true; }
		}

		[XmlElement(ElementName="DesigneeName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DesigneeName;
		
		[XmlIgnore]
		public string DesigneeName
		{ 
			get { return __DesigneeName; }
			set { __DesigneeName = value; }
		}

		[XmlElement(ElementName="DesigneePhone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DesigneePhone;
		
		[XmlIgnore]
		public string DesigneePhone
		{ 
			get { return __DesigneePhone; }
			set { __DesigneePhone = value; }
		}

		[XmlElement(ElementName="DesigneePIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DesigneePIN;
		
		[XmlIgnore]
		public string DesigneePIN
		{ 
			get { return __DesigneePIN; }
			set { __DesigneePIN = value; }
		}

		[XmlElement(ElementName="DiscussWithThirdPartyNo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __DiscussWithThirdPartyNo;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DiscussWithThirdPartyNoSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType DiscussWithThirdPartyNo
		{ 
			get { return __DiscussWithThirdPartyNo; }
			set { __DiscussWithThirdPartyNo = value; __DiscussWithThirdPartyNoSpecified = true; }
		}

		public ThirdPartyDesignee()
		{
		}
	}


	[XmlType(TypeName="Filer",Namespace=Declarations.SchemaVersion),Serializable]
	public class Filer
	{

		[XmlElement(ElementName="EIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EIN;
		
		[XmlIgnore]
		public string EIN
		{ 
			get { return __EIN; }
			set { __EIN = value; }
		}

		[XmlElement(ElementName="Name",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Name;
		
		[XmlIgnore]
		public string Name
		{ 
			get { return __Name; }
			set { __Name = value; }
		}

		[XmlElement(ElementName="InCareOfName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __InCareOfName;
		
		[XmlIgnore]
		public string InCareOfName
		{ 
			get { return __InCareOfName; }
			set { __InCareOfName = value; }
		}

		[XmlElement(ElementName="NameControl",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NameControl;
		
		[XmlIgnore]
		public string NameControl
		{ 
			get { return __NameControl; }
			set { __NameControl = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.USAddressType),ElementName="USAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.USAddressType __USAddress;
		
		[XmlIgnore]
		public IRS_Return_8849.USAddressType USAddress
		{
			get
			{
				if (__USAddress == null) __USAddress = new IRS_Return_8849.USAddressType();		
				return __USAddress;
			}
			set {__USAddress = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ForeignAddressType),ElementName="ForeignAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.ForeignAddressType __ForeignAddress;
		
		[XmlIgnore]
		public IRS_Return_8849.ForeignAddressType ForeignAddress
		{
			get
			{
				if (__ForeignAddress == null) __ForeignAddress = new IRS_Return_8849.ForeignAddressType();		
				return __ForeignAddress;
			}
			set {__ForeignAddress = value;}
		}

		public Filer()
		{
		}
	}


	[XmlType(TypeName="Officer",Namespace=Declarations.SchemaVersion),Serializable]
	public class Officer
	{

		[XmlElement(ElementName="Name",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Name;
		
		[XmlIgnore]
		public string Name
		{ 
			get { return __Name; }
			set { __Name = value; }
		}

		[XmlElement(ElementName="Title",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Title;
		
		[XmlIgnore]
		public string Title
		{ 
			get { return __Title; }
			set { __Title = value; }
		}

		[XmlElement(ElementName="TaxpayerPIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TaxpayerPIN;
		
		[XmlIgnore]
		public string TaxpayerPIN
		{ 
			get { return __TaxpayerPIN; }
			set { __TaxpayerPIN = value; }
		}

		[XmlElement(ElementName="Phone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Phone;
		
		[XmlIgnore]
		public string Phone
		{ 
			get { return __Phone; }
			set { __Phone = value; }
		}

		[XmlElement(ElementName="ForeignPhone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ForeignPhone;
		
		[XmlIgnore]
		public string ForeignPhone
		{ 
			get { return __ForeignPhone; }
			set { __ForeignPhone = value; }
		}

		[XmlElement(ElementName="EmailAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EmailAddress;
		
		[XmlIgnore]
		public string EmailAddress
		{ 
			get { return __EmailAddress; }
			set { __EmailAddress = value; }
		}

		[XmlElement(ElementName="DateSigned",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __DateSigned;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DateSignedSpecified;
		
		[XmlIgnore]
		public DateTime DateSigned
		{ 
			get { return __DateSigned; }
			set { __DateSigned = value; __DateSignedSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime DateSignedUtc
		{ 
			get { return __DateSigned.ToUniversalTime(); }
			set { __DateSigned = value.ToLocalTime(); __DateSignedSpecified = true; }
		}

		public Officer()
		{
			__DateSigned = DateTime.Now;
		}
	}


	[XmlType(TypeName="Preparer",Namespace=Declarations.SchemaVersion),Serializable]
	public class Preparer
	{

		[XmlElement(ElementName="Name",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Name;
		
		[XmlIgnore]
		public string Name
		{ 
			get { return __Name; }
			set { __Name = value; }
		}

		[XmlElement(ElementName="SSN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __SSN;
		
		[XmlIgnore]
		public string SSN
		{ 
			get { return __SSN; }
			set { __SSN = value; }
		}

		[XmlElement(ElementName="PTIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PTIN;
		
		[XmlIgnore]
		public string PTIN
		{ 
			get { return __PTIN; }
			set { __PTIN = value; }
		}

		[XmlElement(ElementName="Phone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Phone;
		
		[XmlIgnore]
		public string Phone
		{ 
			get { return __Phone; }
			set { __Phone = value; }
		}

		[XmlElement(ElementName="ForeignPhone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ForeignPhone;
		
		[XmlIgnore]
		public string ForeignPhone
		{ 
			get { return __ForeignPhone; }
			set { __ForeignPhone = value; }
		}

		[XmlElement(ElementName="EmailAddress",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EmailAddress;
		
		[XmlIgnore]
		public string EmailAddress
		{ 
			get { return __EmailAddress; }
			set { __EmailAddress = value; }
		}

		[XmlElement(ElementName="DatePrepared",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __DatePrepared;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DatePreparedSpecified;
		
		[XmlIgnore]
		public DateTime DatePrepared
		{ 
			get { return __DatePrepared; }
			set { __DatePrepared = value; __DatePreparedSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime DatePreparedUtc
		{ 
			get { return __DatePrepared.ToUniversalTime(); }
			set { __DatePrepared = value.ToLocalTime(); __DatePreparedSpecified = true; }
		}

		[XmlElement(ElementName="SelfEmployed",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __SelfEmployed;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __SelfEmployedSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType SelfEmployed
		{ 
			get { return __SelfEmployed; }
			set { __SelfEmployed = value; __SelfEmployedSpecified = true; }
		}

		public Preparer()
		{
			__DatePrepared = DateTime.Now;
		}
	}


	[XmlType(TypeName="ConsentToVINDataDisclosure",Namespace=Declarations.SchemaVersion),Serializable]
	public class ConsentToVINDataDisclosure
	{

		[XmlElement(ElementName="ConsentToDiscloseYes",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __ConsentToDiscloseYes;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __ConsentToDiscloseYesSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType ConsentToDiscloseYes
		{ 
			get { return __ConsentToDiscloseYes; }
			set { __ConsentToDiscloseYes = value; __ConsentToDiscloseYesSpecified = true; }
		}

		[XmlElement(Type=typeof(DisclosureFormSignatureInfo),ElementName="DisclosureFormSignatureInfo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DisclosureFormSignatureInfo __DisclosureFormSignatureInfo;
		
		[XmlIgnore]
		public DisclosureFormSignatureInfo DisclosureFormSignatureInfo
		{
			get
			{
				if (__DisclosureFormSignatureInfo == null) __DisclosureFormSignatureInfo = new DisclosureFormSignatureInfo();		
				return __DisclosureFormSignatureInfo;
			}
			set {__DisclosureFormSignatureInfo = value;}
		}

		[XmlElement(ElementName="SignatureOption",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SignatureOption __SignatureOption;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __SignatureOptionSpecified;
		
		[XmlIgnore]
		public SignatureOption SignatureOption
		{ 
			get { return __SignatureOption; }
			set { __SignatureOption = value; __SignatureOptionSpecified = true; }
		}

		[XmlElement(ElementName="PIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PIN;
		
		[XmlIgnore]
		public string PIN
		{ 
			get { return __PIN; }
			set { __PIN = value; }
		}

		[XmlElement(ElementName="ConsentToDiscloseNo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __ConsentToDiscloseNo;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __ConsentToDiscloseNoSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType ConsentToDiscloseNo
		{ 
			get { return __ConsentToDiscloseNo; }
			set { __ConsentToDiscloseNo = value; __ConsentToDiscloseNoSpecified = true; }
		}

		public ConsentToVINDataDisclosure()
		{
		}
	}


	[XmlType(TypeName="DisclosureFormSignatureInfo",Namespace=Declarations.SchemaVersion),Serializable]
	public class DisclosureFormSignatureInfo
	{

		[XmlElement(ElementName="EIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EIN;
		
		[XmlIgnore]
		public string EIN
		{ 
			get { return __EIN; }
			set { __EIN = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.BusinessNameType),ElementName="Name",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BusinessNameType __Name;
		
		[XmlIgnore]
		public IRS_Return_8849.BusinessNameType Name
		{
			get
			{
				if (__Name == null) __Name = new IRS_Return_8849.BusinessNameType();		
				return __Name;
			}
			set {__Name = value;}
		}

		[XmlElement(ElementName="DateSigned",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __DateSigned;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DateSignedSpecified;
		
		[XmlIgnore]
		public DateTime DateSigned
		{ 
			get { return __DateSigned; }
			set { __DateSigned = value; __DateSignedSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime DateSignedUtc
		{ 
			get { return __DateSigned.ToUniversalTime(); }
			set { __DateSigned = value.ToLocalTime(); __DateSignedSpecified = true; }
		}

		public DisclosureFormSignatureInfo()
		{
			__DateSigned = DateTime.Now;
		}
	}


	[XmlType(TypeName="BinaryAttachmentType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class BinaryAttachmentType
	{

		[XmlElement(ElementName="DocumentType",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DocumentType __DocumentType;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DocumentTypeSpecified;
		
		[XmlIgnore]
		public DocumentType DocumentType
		{ 
			get { return __DocumentType; }
			set { __DocumentType = value; __DocumentTypeSpecified = true; }
		}

		[XmlElement(ElementName="Description",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Description;
		
		[XmlIgnore]
		public string Description
		{ 
			get { return __Description; }
			set { __Description = value; }
		}

		[XmlElement(ElementName="AttachmentLocation",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AttachmentLocation;
		
		[XmlIgnore]
		public string AttachmentLocation
		{ 
			get { return __AttachmentLocation; }
			set { __AttachmentLocation = value; }
		}

		public BinaryAttachmentType()
		{
		}
	}


	[XmlType(TypeName="GeneralDependencyMediumType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class GeneralDependencyMediumType
	{

		[XmlElement(Type=typeof(IRS_Return_8849.BusinessNameType),ElementName="BusinessName",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BusinessNameType __BusinessName;
		
		[XmlIgnore]
		public IRS_Return_8849.BusinessNameType BusinessName
		{
			get
			{
				if (__BusinessName == null) __BusinessName = new IRS_Return_8849.BusinessNameType();		
				return __BusinessName;
			}
			set {__BusinessName = value;}
		}

		[XmlElement(ElementName="PersonName",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PersonName;
		
		[XmlIgnore]
		public string PersonName
		{ 
			get { return __PersonName; }
			set { __PersonName = value; }
		}

		[XmlElement(ElementName="SSN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __SSN;
		
		[XmlIgnore]
		public string SSN
		{ 
			get { return __SSN; }
			set { __SSN = value; }
		}

		[XmlElement(ElementName="EIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __EIN;
		
		[XmlIgnore]
		public string EIN
		{ 
			get { return __EIN; }
			set { __EIN = value; }
		}

		[XmlElement(ElementName="MissingEINReason",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public MissingEINReason __MissingEINReason;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __MissingEINReasonSpecified;
		
		[XmlIgnore]
		public MissingEINReason MissingEINReason
		{ 
			get { return __MissingEINReason; }
			set { __MissingEINReason = value; __MissingEINReasonSpecified = true; }
		}

		[XmlElement(ElementName="FormLineOrInstructionReference",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __FormLineOrInstructionReference;
		
		[XmlIgnore]
		public string FormLineOrInstructionReference
		{ 
			get { return __FormLineOrInstructionReference; }
			set { __FormLineOrInstructionReference = value; }
		}

		[XmlElement(ElementName="RegulationsReference",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __RegulationsReference;
		
		[XmlIgnore]
		public string RegulationsReference
		{ 
			get { return __RegulationsReference; }
			set { __RegulationsReference = value; }
		}

		[XmlElement(ElementName="Description",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Description;
		
		[XmlIgnore]
		public string Description
		{ 
			get { return __Description; }
			set { __Description = value; }
		}

		[XmlElement(ElementName="AttachmentInformation",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AttachmentInformation;
		
		[XmlIgnore]
		public string AttachmentInformation
		{ 
			get { return __AttachmentInformation; }
			set { __AttachmentInformation = value; }
		}

		public GeneralDependencyMediumType()
		{
		}
	}


	[XmlType(TypeName="IRS2290Type",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class IRS2290Type
	{

		[XmlElement(Type=typeof(string),ElementName="SpecialConditionDescription",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SpecialConditionDescriptionCollection __SpecialConditionDescriptionCollection;
		
		[XmlIgnore]
		public SpecialConditionDescriptionCollection SpecialConditionDescriptionCollection
		{
			get
			{
				if (__SpecialConditionDescriptionCollection == null) __SpecialConditionDescriptionCollection = new SpecialConditionDescriptionCollection();
				return __SpecialConditionDescriptionCollection;
			}
			set {__SpecialConditionDescriptionCollection = value;}
		}

		[XmlElement(ElementName="AddressChange",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __AddressChange;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __AddressChangeSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType AddressChange
		{ 
			get { return __AddressChange; }
			set { __AddressChange = value; __AddressChangeSpecified = true; }
		}

		[XmlElement(ElementName="AmendedReturn",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __AmendedReturn;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __AmendedReturnSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType AmendedReturn
		{ 
			get { return __AmendedReturn; }
			set { __AmendedReturn = value; __AmendedReturnSpecified = true; }
		}

		[XmlElement(ElementName="AmendedMonth",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="gMonth",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __AmendedMonth;
		
		[XmlIgnore]
		public string AmendedMonth
		{ 
			get { return __AmendedMonth; }
			set { __AmendedMonth = value; }
		}

		[XmlElement(ElementName="VINCorrection",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __VINCorrection;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __VINCorrectionSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType VINCorrection
		{ 
			get { return __VINCorrection; }
			set { __VINCorrection = value; __VINCorrectionSpecified = true; }
		}

		[XmlElement(ElementName="FinalReturn",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __FinalReturn;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __FinalReturnSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType FinalReturn
		{ 
			get { return __FinalReturn; }
			set { __FinalReturn = value; __FinalReturnSpecified = true; }
		}

		[XmlElement(ElementName="TaxFromTaxComputation",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __TaxFromTaxComputation;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TaxFromTaxComputationSpecified;
		
		[XmlIgnore]
		public decimal TaxFromTaxComputation
		{ 
			get { return __TaxFromTaxComputation; }
			set { __TaxFromTaxComputation = value; __TaxFromTaxComputationSpecified = true; }
		}

		[XmlElement(Type=typeof(AdditionalTax),ElementName="AdditionalTax",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public AdditionalTax __AdditionalTax;
		
		[XmlIgnore]
		public AdditionalTax AdditionalTax
		{
			get
			{
				if (__AdditionalTax == null) __AdditionalTax = new AdditionalTax();		
				return __AdditionalTax;
			}
			set {__AdditionalTax = value;}
		}

		[XmlElement(ElementName="TotalTax",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __TotalTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TotalTaxSpecified;
		
		[XmlIgnore]
		public decimal TotalTax
		{ 
			get { return __TotalTax; }
			set { __TotalTax = value; __TotalTaxSpecified = true; }
		}

		[XmlElement(Type=typeof(CreditsAmount),ElementName="CreditsAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public CreditsAmount __CreditsAmount;
		
		[XmlIgnore]
		public CreditsAmount CreditsAmount
		{
			get
			{
				if (__CreditsAmount == null) __CreditsAmount = new CreditsAmount();		
				return __CreditsAmount;
			}
			set {__CreditsAmount = value;}
		}

		[XmlElement(ElementName="BalanceDue",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __BalanceDue;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __BalanceDueSpecified;
		
		[XmlIgnore]
		public decimal BalanceDue
		{ 
			get { return __BalanceDue; }
			set { __BalanceDue = value; __BalanceDueSpecified = true; }
		}

		[XmlElement(ElementName="EFTPSPayment",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __EFTPSPayment;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __EFTPSPaymentSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType EFTPSPayment
		{ 
			get { return __EFTPSPayment; }
			set { __EFTPSPayment = value; __EFTPSPaymentSpecified = true; }
		}

		[XmlElement(ElementName="Checkbox5000Miles",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __Checkbox5000Miles;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __Checkbox5000MilesSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType Checkbox5000Miles
		{ 
			get { return __Checkbox5000Miles; }
			set { __Checkbox5000Miles = value; __Checkbox5000MilesSpecified = true; }
		}

		[XmlElement(ElementName="Checkbox7500Miles",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __Checkbox7500Miles;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __Checkbox7500MilesSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType Checkbox7500Miles
		{ 
			get { return __Checkbox7500Miles; }
			set { __Checkbox7500Miles = value; __Checkbox7500MilesSpecified = true; }
		}

		[XmlElement(ElementName="NotSubjectToTax",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.CheckboxType __NotSubjectToTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __NotSubjectToTaxSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.CheckboxType NotSubjectToTax
		{ 
			get { return __NotSubjectToTax; }
			set { __NotSubjectToTax = value; __NotSubjectToTaxSpecified = true; }
		}

		[XmlElement(Type=typeof(SuspendedVIN),ElementName="SuspendedVIN",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SuspendedVIN __SuspendedVIN;
		
		[XmlIgnore]
		public SuspendedVIN SuspendedVIN
		{
			get
			{
				if (__SuspendedVIN == null) __SuspendedVIN = new SuspendedVIN();		
				return __SuspendedVIN;
			}
			set {__SuspendedVIN = value;}
		}

		[XmlElement(Type=typeof(TransferredSuspendedVIN),ElementName="TransferredSuspendedVIN",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TransferredSuspendedVIN __TransferredSuspendedVIN;
		
		[XmlIgnore]
		public TransferredSuspendedVIN TransferredSuspendedVIN
		{
			get
			{
				if (__TransferredSuspendedVIN == null) __TransferredSuspendedVIN = new TransferredSuspendedVIN();		
				return __TransferredSuspendedVIN;
			}
			set {__TransferredSuspendedVIN = value;}
		}

		[XmlElement(Type=typeof(TaxComputation),ElementName="TaxComputation",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TaxComputationCollection __TaxComputationCollection;
		
		[XmlIgnore]
		public TaxComputationCollection TaxComputationCollection
		{
			get
			{
				if (__TaxComputationCollection == null) __TaxComputationCollection = new TaxComputationCollection();
				return __TaxComputationCollection;
			}
			set {__TaxComputationCollection = value;}
		}

		[XmlElement(ElementName="TotalNumberOfVehicles",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TotalNumberOfVehicles;
		
		[XmlIgnore]
		public string TotalNumberOfVehicles
		{ 
			get { return __TotalNumberOfVehicles; }
			set { __TotalNumberOfVehicles = value; }
		}

		[XmlElement(ElementName="TotalAmountOfTax",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __TotalAmountOfTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TotalAmountOfTaxSpecified;
		
		[XmlIgnore]
		public decimal TotalAmountOfTax
		{ 
			get { return __TotalAmountOfTax; }
			set { __TotalAmountOfTax = value; __TotalAmountOfTaxSpecified = true; }
		}

		[XmlElement(ElementName="NumberTaxSuspendedLoggingVeh",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NumberTaxSuspendedLoggingVeh;
		
		[XmlIgnore]
		public string NumberTaxSuspendedLoggingVeh
		{ 
			get { return __NumberTaxSuspendedLoggingVeh; }
			set { __NumberTaxSuspendedLoggingVeh = value; }
		}

		[XmlElement(ElementName="NumTaxSuspendedNonLoggingVeh",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NumTaxSuspendedNonLoggingVeh;
		
		[XmlIgnore]
		public string NumTaxSuspendedNonLoggingVeh
		{ 
			get { return __NumTaxSuspendedNonLoggingVeh; }
			set { __NumTaxSuspendedNonLoggingVeh = value; }
		}

		public IRS2290Type()
		{
		}
	}


	[XmlType(TypeName="AdditionalTax",Namespace=Declarations.SchemaVersion),Serializable]
	public class AdditionalTax
	{

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		[XmlText(DataType="decimal")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __MixedValue;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __MixedValueSpecified;
		
		[XmlIgnore]
		public decimal MixedValue
		{ 
			get { return __MixedValue; }
			set { __MixedValue = value; __MixedValueSpecified = true; }
		}

		public AdditionalTax()
		{
		}
	}


	[XmlType(TypeName="CreditsAmount",Namespace=Declarations.SchemaVersion),Serializable]
	public class CreditsAmount
	{

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		[XmlText(DataType="decimal")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __MixedValue;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __MixedValueSpecified;
		
		[XmlIgnore]
		public decimal MixedValue
		{ 
			get { return __MixedValue; }
			set { __MixedValue = value; __MixedValueSpecified = true; }
		}

		public CreditsAmount()
		{
		}
	}


	[XmlType(TypeName="SuspendedVIN",Namespace=Declarations.SchemaVersion),Serializable]
	public class SuspendedVIN
	{

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		public SuspendedVIN()
		{
		}
	}


	[XmlType(TypeName="TransferredSuspendedVIN",Namespace=Declarations.SchemaVersion),Serializable]
	public class TransferredSuspendedVIN
	{

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		public TransferredSuspendedVIN()
		{
		}
	}


	[XmlType(TypeName="TaxComputation",Namespace=Declarations.SchemaVersion),Serializable]
	public class TaxComputation
	{

		[XmlElement(ElementName="Category",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Category;
		
		[XmlIgnore]
		public string Category
		{ 
			get { return __Category; }
			set { __Category = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.TaxComputationColumnType),ElementName="ComputationColumns",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.TaxComputationColumnType __ComputationColumns;
		
		[XmlIgnore]
		public IRS_Return_8849.TaxComputationColumnType ComputationColumns
		{
			get
			{
				if (__ComputationColumns == null) __ComputationColumns = new IRS_Return_8849.TaxComputationColumnType();		
				return __ComputationColumns;
			}
			set {__ComputationColumns = value;}
		}

		public TaxComputation()
		{
		}
	}


	[XmlType(TypeName="TaxComputationColumnType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TaxComputationColumnType
	{

		[XmlElement(ElementName="NonLoggingPartialTax",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __NonLoggingPartialTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __NonLoggingPartialTaxSpecified;
		
		[XmlIgnore]
		public decimal NonLoggingPartialTax
		{ 
			get { return __NonLoggingPartialTax; }
			set { __NonLoggingPartialTax = value; __NonLoggingPartialTaxSpecified = true; }
		}

		[XmlElement(ElementName="LoggingPartialTax",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __LoggingPartialTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __LoggingPartialTaxSpecified;
		
		[XmlIgnore]
		public decimal LoggingPartialTax
		{ 
			get { return __LoggingPartialTax; }
			set { __LoggingPartialTax = value; __LoggingPartialTaxSpecified = true; }
		}

		[XmlElement(ElementName="NumberVehicleNonLogging",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NumberVehicleNonLogging;
		
		[XmlIgnore]
		public string NumberVehicleNonLogging
		{ 
			get { return __NumberVehicleNonLogging; }
			set { __NumberVehicleNonLogging = value; }
		}

		[XmlElement(ElementName="NumberVehicleLogging",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NumberVehicleLogging;
		
		[XmlIgnore]
		public string NumberVehicleLogging
		{ 
			get { return __NumberVehicleLogging; }
			set { __NumberVehicleLogging = value; }
		}

		[XmlElement(ElementName="AmountOfTax",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __AmountOfTax;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __AmountOfTaxSpecified;
		
		[XmlIgnore]
		public decimal AmountOfTax
		{ 
			get { return __AmountOfTax; }
			set { __AmountOfTax = value; __AmountOfTaxSpecified = true; }
		}

		public TaxComputationColumnType()
		{
		}
	}


	[XmlType(TypeName="IRS2290Schedule1Type",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class IRS2290Schedule1Type
	{

		[XmlElement(Type=typeof(VehicleReportTaxItem),ElementName="VehicleReportTaxItem",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public VehicleReportTaxItemCollection __VehicleReportTaxItemCollection;
		
		[XmlIgnore]
		public VehicleReportTaxItemCollection VehicleReportTaxItemCollection
		{
			get
			{
				if (__VehicleReportTaxItemCollection == null) __VehicleReportTaxItemCollection = new VehicleReportTaxItemCollection();
				return __VehicleReportTaxItemCollection;
			}
			set {__VehicleReportTaxItemCollection = value;}
		}

		[XmlElement(Type=typeof(VehicleSuspendedTaxItem),ElementName="VehicleSuspendedTaxItem",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public VehicleSuspendedTaxItemCollection __VehicleSuspendedTaxItemCollection;
		
		[XmlIgnore]
		public VehicleSuspendedTaxItemCollection VehicleSuspendedTaxItemCollection
		{
			get
			{
				if (__VehicleSuspendedTaxItemCollection == null) __VehicleSuspendedTaxItemCollection = new VehicleSuspendedTaxItemCollection();
				return __VehicleSuspendedTaxItemCollection;
			}
			set {__VehicleSuspendedTaxItemCollection = value;}
		}

		[XmlElement(ElementName="NumberOfTaxableVehicles",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __NumberOfTaxableVehicles;
		
		[XmlIgnore]
		public string NumberOfTaxableVehicles
		{ 
			get { return __NumberOfTaxableVehicles; }
			set { __NumberOfTaxableVehicles = value; }
		}

		[XmlElement(ElementName="TotalNumberSuspendedVehicles",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TotalNumberSuspendedVehicles;
		
		[XmlIgnore]
		public string TotalNumberSuspendedVehicles
		{ 
			get { return __TotalNumberSuspendedVehicles; }
			set { __TotalNumberSuspendedVehicles = value; }
		}

		public IRS2290Schedule1Type()
		{
		}
	}


	[XmlType(TypeName="VehicleReportTaxItem",Namespace=Declarations.SchemaVersion),Serializable]
	public class VehicleReportTaxItem
	{

		[XmlElement(ElementName="VIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VIN;
		
		[XmlIgnore]
		public string VIN
		{ 
			get { return __VIN; }
			set { __VIN = value; }
		}

		[XmlElement(ElementName="VehicleCategoryCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VehicleCategoryCode;
		
		[XmlIgnore]
		public string VehicleCategoryCode
		{ 
			get { return __VehicleCategoryCode; }
			set { __VehicleCategoryCode = value; }
		}

		public VehicleReportTaxItem()
		{
		}
	}


	[XmlType(TypeName="VehicleSuspendedTaxItem",Namespace=Declarations.SchemaVersion),Serializable]
	public class VehicleSuspendedTaxItem
	{

		[XmlElement(ElementName="VIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VIN;
		
		[XmlIgnore]
		public string VIN
		{ 
			get { return __VIN; }
			set { __VIN = value; }
		}

		[XmlElement(ElementName="VehicleCategoryCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VehicleCategoryCode;
		
		[XmlIgnore]
		public string VehicleCategoryCode
		{ 
			get { return __VehicleCategoryCode; }
			set { __VehicleCategoryCode = value; }
		}

		public VehicleSuspendedTaxItem()
		{
		}
	}


	[XmlType(TypeName="CreditsAmountStatementType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class CreditsAmountStatementType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return CreditsAmountInfoCollection.GetEnumerator();
		}

		public IRS_Return_8849.CreditsAmountInfoType Add(IRS_Return_8849.CreditsAmountInfoType obj)
		{
			return CreditsAmountInfoCollection.Add(obj);
		}

		[XmlIgnore]
		public IRS_Return_8849.CreditsAmountInfoType this[int index]
		{
			get { return (IRS_Return_8849.CreditsAmountInfoType) CreditsAmountInfoCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return CreditsAmountInfoCollection.Count; }
        }

        public void Clear()
		{
			CreditsAmountInfoCollection.Clear();
        }

		public IRS_Return_8849.CreditsAmountInfoType Remove(int index) 
		{ 
            IRS_Return_8849.CreditsAmountInfoType obj = CreditsAmountInfoCollection[index];
            CreditsAmountInfoCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            CreditsAmountInfoCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(IRS_Return_8849.CreditsAmountInfoType),ElementName="CreditsAmountInfo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public CreditsAmountInfoTypeCollection __CreditsAmountInfoCollection;
		
		[XmlIgnore]
		public CreditsAmountInfoTypeCollection CreditsAmountInfoCollection
		{
			get
			{
				if (__CreditsAmountInfoCollection == null) __CreditsAmountInfoCollection = new CreditsAmountInfoTypeCollection();
				return __CreditsAmountInfoCollection;
			}
			set {__CreditsAmountInfoCollection = value;}
		}

		public CreditsAmountStatementType()
		{
		}
	}


	[XmlType(TypeName="CreditsAmountInfoType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class CreditsAmountInfoType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return DisposalReportingItemCollection.GetEnumerator();
		}

		public DisposalReportingItem Add(DisposalReportingItem obj)
		{
			return DisposalReportingItemCollection.Add(obj);
		}

		[XmlIgnore]
		public DisposalReportingItem this[int index]
		{
			get { return (DisposalReportingItem) DisposalReportingItemCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return DisposalReportingItemCollection.Count; }
        }

        public void Clear()
		{
			DisposalReportingItemCollection.Clear();
        }

		public DisposalReportingItem Remove(int index) 
		{ 
            DisposalReportingItem obj = DisposalReportingItemCollection[index];
            DisposalReportingItemCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            DisposalReportingItemCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(DisposalReportingItem),ElementName="DisposalReportingItem",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DisposalReportingItemCollection __DisposalReportingItemCollection;
		
		[XmlIgnore]
		public DisposalReportingItemCollection DisposalReportingItemCollection
		{
			get
			{
				if (__DisposalReportingItemCollection == null) __DisposalReportingItemCollection = new DisposalReportingItemCollection();
				return __DisposalReportingItemCollection;
			}
			set {__DisposalReportingItemCollection = value;}
		}

		public CreditsAmountInfoType()
		{
		}
	}


	[XmlType(TypeName="DisposalReportingItem",Namespace=Declarations.SchemaVersion),Serializable]
	public class DisposalReportingItem
	{

		[XmlElement(ElementName="CreditsAmountExplanation",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __CreditsAmountExplanation;
		
		[XmlIgnore]
		public string CreditsAmountExplanation
		{ 
			get { return __CreditsAmountExplanation; }
			set { __CreditsAmountExplanation = value; }
		}

		[XmlElement(ElementName="DisposalReportingVIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DisposalReportingVIN;
		
		[XmlIgnore]
		public string DisposalReportingVIN
		{ 
			get { return __DisposalReportingVIN; }
			set { __DisposalReportingVIN = value; }
		}

		[XmlElement(ElementName="DisposalReportingDate",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __DisposalReportingDate;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DisposalReportingDateSpecified;
		
		[XmlIgnore]
		public DateTime DisposalReportingDate
		{ 
			get { return __DisposalReportingDate; }
			set { __DisposalReportingDate = value; __DisposalReportingDateSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime DisposalReportingDateUtc
		{ 
			get { return __DisposalReportingDate.ToUniversalTime(); }
			set { __DisposalReportingDate = value.ToLocalTime(); __DisposalReportingDateSpecified = true; }
		}

		[XmlElement(ElementName="DisposalReportingAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="nonNegativeInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __DisposalReportingAmount;
		
		[XmlIgnore]
		public string DisposalReportingAmount
		{ 
			get { return __DisposalReportingAmount; }
			set { __DisposalReportingAmount = value; }
		}

		public DisposalReportingItem()
		{
			__DisposalReportingDate = DateTime.Now;
		}
	}


	[XmlType(TypeName="IRSPaymentType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class IRSPaymentType
	{

		[XmlElement(ElementName="RoutingTransitNumber",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __RoutingTransitNumber;
		
		[XmlIgnore]
		public string RoutingTransitNumber
		{ 
			get { return __RoutingTransitNumber; }
			set { __RoutingTransitNumber = value; }
		}

		[XmlElement(ElementName="BankAccountNumber",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __BankAccountNumber;
		
		[XmlIgnore]
		public string BankAccountNumber
		{ 
			get { return __BankAccountNumber; }
			set { __BankAccountNumber = value; }
		}

		[XmlElement(ElementName="AccountType",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BankAccountType __AccountType;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __AccountTypeSpecified;
		
		[XmlIgnore]
		public IRS_Return_8849.BankAccountType AccountType
		{ 
			get { return __AccountType; }
			set { __AccountType = value; __AccountTypeSpecified = true; }
		}

		[XmlElement(ElementName="PaymentAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __PaymentAmount;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __PaymentAmountSpecified;
		
		[XmlIgnore]
		public decimal PaymentAmount
		{ 
			get { return __PaymentAmount; }
			set { __PaymentAmount = value; __PaymentAmountSpecified = true; }
		}

		[XmlElement(ElementName="RequestedPaymentDate",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __RequestedPaymentDate;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __RequestedPaymentDateSpecified;
		
		[XmlIgnore]
		public DateTime RequestedPaymentDate
		{ 
			get { return __RequestedPaymentDate; }
			set { __RequestedPaymentDate = value; __RequestedPaymentDateSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime RequestedPaymentDateUtc
		{ 
			get { return __RequestedPaymentDate.ToUniversalTime(); }
			set { __RequestedPaymentDate = value.ToLocalTime(); __RequestedPaymentDateSpecified = true; }
		}

		[XmlElement(ElementName="TaxpayerDaytimePhone",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TaxpayerDaytimePhone;
		
		[XmlIgnore]
		public string TaxpayerDaytimePhone
		{ 
			get { return __TaxpayerDaytimePhone; }
			set { __TaxpayerDaytimePhone = value; }
		}

		public IRSPaymentType()
		{
			__RequestedPaymentDate = DateTime.Now;
		}
	}


	[XmlType(TypeName="ReasonableCauseExplnTyp",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class ReasonableCauseExplnTyp
	{

		[XmlElement(ElementName="Explanation",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Explanation;
		
		[XmlIgnore]
		public string Explanation
		{ 
			get { return __Explanation; }
			set { __Explanation = value; }
		}

		public ReasonableCauseExplnTyp()
		{
		}
	}


	[XmlType(TypeName="StmtInSupportOfSuspensionType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class StmtInSupportOfSuspensionType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return StmtInSupportOfSuspensionInfoCollection.GetEnumerator();
		}

		public IRS_Return_8849.StmtInSupportOfSuspensionInfoType Add(IRS_Return_8849.StmtInSupportOfSuspensionInfoType obj)
		{
			return StmtInSupportOfSuspensionInfoCollection.Add(obj);
		}

		[XmlIgnore]
		public IRS_Return_8849.StmtInSupportOfSuspensionInfoType this[int index]
		{
			get { return (IRS_Return_8849.StmtInSupportOfSuspensionInfoType) StmtInSupportOfSuspensionInfoCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return StmtInSupportOfSuspensionInfoCollection.Count; }
        }

        public void Clear()
		{
			StmtInSupportOfSuspensionInfoCollection.Clear();
        }

		public IRS_Return_8849.StmtInSupportOfSuspensionInfoType Remove(int index) 
		{ 
            IRS_Return_8849.StmtInSupportOfSuspensionInfoType obj = StmtInSupportOfSuspensionInfoCollection[index];
            StmtInSupportOfSuspensionInfoCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            StmtInSupportOfSuspensionInfoCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(IRS_Return_8849.StmtInSupportOfSuspensionInfoType),ElementName="StmtInSupportOfSuspensionInfo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public StmtInSupportOfSuspensionInfoTypeCollection __StmtInSupportOfSuspensionInfoCollection;
		
		[XmlIgnore]
		public StmtInSupportOfSuspensionInfoTypeCollection StmtInSupportOfSuspensionInfoCollection
		{
			get
			{
				if (__StmtInSupportOfSuspensionInfoCollection == null) __StmtInSupportOfSuspensionInfoCollection = new StmtInSupportOfSuspensionInfoTypeCollection();
				return __StmtInSupportOfSuspensionInfoCollection;
			}
			set {__StmtInSupportOfSuspensionInfoCollection = value;}
		}

		public StmtInSupportOfSuspensionType()
		{
		}
	}


	[XmlType(TypeName="StmtInSupportOfSuspensionInfoType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class StmtInSupportOfSuspensionInfoType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return ItemCollection.GetEnumerator();
		}

		public Item Add(Item obj)
		{
			return ItemCollection.Add(obj);
		}

		[XmlIgnore]
		public Item this[int index]
		{
			get { return (Item) ItemCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return ItemCollection.Count; }
        }

        public void Clear()
		{
			ItemCollection.Clear();
        }

		public Item Remove(int index) 
		{ 
            Item obj = ItemCollection[index];
            ItemCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            ItemCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(Item),ElementName="Item",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ItemCollection __ItemCollection;
		
		[XmlIgnore]
		public ItemCollection ItemCollection
		{
			get
			{
				if (__ItemCollection == null) __ItemCollection = new ItemCollection();
				return __ItemCollection;
			}
			set {__ItemCollection = value;}
		}

		public StmtInSupportOfSuspensionInfoType()
		{
		}
	}


	[XmlType(TypeName="Item",Namespace=Declarations.SchemaVersion),Serializable]
	public class Item
	{

		[XmlElement(ElementName="VIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VIN;
		
		[XmlIgnore]
		public string VIN
		{ 
			get { return __VIN; }
			set { __VIN = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.BusinessNameType),ElementName="Name",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.BusinessNameType __Name;
		
		[XmlIgnore]
		public IRS_Return_8849.BusinessNameType Name
		{
			get
			{
				if (__Name == null) __Name = new IRS_Return_8849.BusinessNameType();		
				return __Name;
			}
			set {__Name = value;}
		}

		[XmlElement(ElementName="Date",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="date",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public DateTime __Date;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __DateSpecified;
		
		[XmlIgnore]
		public DateTime Date
		{ 
			get { return __Date; }
			set { __Date = value; __DateSpecified = true; }
		}
		
		[XmlIgnore]
		public DateTime DateUtc
		{ 
			get { return __Date.ToUniversalTime(); }
			set { __Date = value.ToLocalTime(); __DateSpecified = true; }
		}

		public Item()
		{
			__Date = DateTime.Now;
		}
	}


	[XmlType(TypeName="SuspendedVINStatementType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SuspendedVINStatementType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return SuspendedVINInfoCollection.GetEnumerator();
		}

		public IRS_Return_8849.SuspendedVINInfoType Add(IRS_Return_8849.SuspendedVINInfoType obj)
		{
			return SuspendedVINInfoCollection.Add(obj);
		}

		[XmlIgnore]
		public IRS_Return_8849.SuspendedVINInfoType this[int index]
		{
			get { return (IRS_Return_8849.SuspendedVINInfoType) SuspendedVINInfoCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return SuspendedVINInfoCollection.Count; }
        }

        public void Clear()
		{
			SuspendedVINInfoCollection.Clear();
        }

		public IRS_Return_8849.SuspendedVINInfoType Remove(int index) 
		{ 
            IRS_Return_8849.SuspendedVINInfoType obj = SuspendedVINInfoCollection[index];
            SuspendedVINInfoCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            SuspendedVINInfoCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(IRS_Return_8849.SuspendedVINInfoType),ElementName="SuspendedVINInfo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SuspendedVINInfoTypeCollection __SuspendedVINInfoCollection;
		
		[XmlIgnore]
		public SuspendedVINInfoTypeCollection SuspendedVINInfoCollection
		{
			get
			{
				if (__SuspendedVINInfoCollection == null) __SuspendedVINInfoCollection = new SuspendedVINInfoTypeCollection();
				return __SuspendedVINInfoCollection;
			}
			set {__SuspendedVINInfoCollection = value;}
		}

		public SuspendedVINStatementType()
		{
		}
	}


	[XmlType(TypeName="SuspendedVINInfoType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class SuspendedVINInfoType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return ItemCollection.GetEnumerator();
		}

		public SuspendedVINInfoTypeItem Add(SuspendedVINInfoTypeItem obj)
		{
			return ItemCollection.Add(obj);
		}

		[XmlIgnore]
		public SuspendedVINInfoTypeItem this[int index]
		{
			get { return (SuspendedVINInfoTypeItem) ItemCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return ItemCollection.Count; }
        }

        public void Clear()
		{
			ItemCollection.Clear();
        }

		public SuspendedVINInfoTypeItem Remove(int index) 
		{ 
            SuspendedVINInfoTypeItem obj = ItemCollection[index];
            ItemCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            ItemCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(SuspendedVINInfoTypeItem),ElementName="Item",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SuspendedVINInfoTypeItemCollection __ItemCollection;
		
		[XmlIgnore]
		public SuspendedVINInfoTypeItemCollection ItemCollection
		{
			get
			{
				if (__ItemCollection == null) __ItemCollection = new SuspendedVINInfoTypeItemCollection();
				return __ItemCollection;
			}
			set {__ItemCollection = value;}
		}

		public SuspendedVINInfoType()
		{
		}
	}


	[XmlType(TypeName="SuspendedVINInfoTypeItem",Namespace=Declarations.SchemaVersion),Serializable]
	public class SuspendedVINInfoTypeItem
	{

		[XmlElement(ElementName="VIN",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __VIN;
		
		[XmlIgnore]
		public string VIN
		{ 
			get { return __VIN; }
			set { __VIN = value; }
		}

		public SuspendedVINInfoTypeItem()
		{
		}
	}


	[XmlType(TypeName="TGWIncreaseWorksheetType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TGWIncreaseWorksheetType
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return TGWIncreaseInfoCollection.GetEnumerator();
		}

		public IRS_Return_8849.TGWIncreaseInfoType Add(IRS_Return_8849.TGWIncreaseInfoType obj)
		{
			return TGWIncreaseInfoCollection.Add(obj);
		}

		[XmlIgnore]
		public IRS_Return_8849.TGWIncreaseInfoType this[int index]
		{
			get { return (IRS_Return_8849.TGWIncreaseInfoType) TGWIncreaseInfoCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return TGWIncreaseInfoCollection.Count; }
        }

        public void Clear()
		{
			TGWIncreaseInfoCollection.Clear();
        }

		public IRS_Return_8849.TGWIncreaseInfoType Remove(int index) 
		{ 
            IRS_Return_8849.TGWIncreaseInfoType obj = TGWIncreaseInfoCollection[index];
            TGWIncreaseInfoCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            TGWIncreaseInfoCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(IRS_Return_8849.TGWIncreaseInfoType),ElementName="TGWIncreaseInfo",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TGWIncreaseInfoTypeCollection __TGWIncreaseInfoCollection;
		
		[XmlIgnore]
		public TGWIncreaseInfoTypeCollection TGWIncreaseInfoCollection
		{
			get
			{
				if (__TGWIncreaseInfoCollection == null) __TGWIncreaseInfoCollection = new TGWIncreaseInfoTypeCollection();
				return __TGWIncreaseInfoCollection;
			}
			set {__TGWIncreaseInfoCollection = value;}
		}

		public TGWIncreaseWorksheetType()
		{
		}
	}


	[XmlType(TypeName="TGWIncreaseInfoType",Namespace=Declarations.SchemaVersion),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public class TGWIncreaseInfoType
	{

		[XmlElement(ElementName="MonthOfTGWIncrease",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="gMonth",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __MonthOfTGWIncrease;
		
		[XmlIgnore]
		public string MonthOfTGWIncrease
		{ 
			get { return __MonthOfTGWIncrease; }
			set { __MonthOfTGWIncrease = value; }
		}

		[XmlElement(ElementName="TGWCategoryCode",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TGWCategoryCode;
		
		[XmlIgnore]
		public string TGWCategoryCode
		{ 
			get { return __TGWCategoryCode; }
			set { __TGWCategoryCode = value; }
		}

		[XmlElement(ElementName="NewTaxAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __NewTaxAmount;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __NewTaxAmountSpecified;
		
		[XmlIgnore]
		public decimal NewTaxAmount
		{ 
			get { return __NewTaxAmount; }
			set { __NewTaxAmount = value; __NewTaxAmountSpecified = true; }
		}

		[XmlElement(ElementName="PreviousTaxAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __PreviousTaxAmount;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __PreviousTaxAmountSpecified;
		
		[XmlIgnore]
		public decimal PreviousTaxAmount
		{ 
			get { return __PreviousTaxAmount; }
			set { __PreviousTaxAmount = value; __PreviousTaxAmountSpecified = true; }
		}

		[XmlElement(ElementName="AdditionalTaxAmount",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="decimal",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public decimal __AdditionalTaxAmount;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __AdditionalTaxAmountSpecified;
		
		[XmlIgnore]
		public decimal AdditionalTaxAmount
		{ 
			get { return __AdditionalTaxAmount; }
			set { __AdditionalTaxAmount = value; __AdditionalTaxAmountSpecified = true; }
		}

		public TGWIncreaseInfoType()
		{
		}
	}


	[XmlRoot(ElementName="BinaryAttachment",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class BinaryAttachment : IRS_Return_8849.BinaryAttachmentType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}
        /* Vishwa 2015 */
        /*
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}

		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}
        */
		public BinaryAttachment() : base()
		{
		}
	}


	[XmlRoot(ElementName="GeneralDependencyMedium",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class GeneralDependencyMedium : IRS_Return_8849.GeneralDependencyMediumType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}
        /* Vishwa 2015 */
        /*
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
         
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        */
		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		public GeneralDependencyMedium() : base()
		{
		}
	}


	[XmlRoot(ElementName="IRS2290",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class IRS2290 : IRS_Return_8849.IRS2290Type
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}

		

		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentId",Form=XmlSchemaForm.Unqualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentId;
		
		[XmlIgnore]
		public string referenceDocumentId
		{ 
			get { return __referenceDocumentId; }
			set { __referenceDocumentId = value; }
		}

		[XmlAttribute(AttributeName="referenceDocumentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __referenceDocumentName;
		
		[XmlIgnore]
		public string referenceDocumentName
		{ 
			get { return __referenceDocumentName; }
			set { __referenceDocumentName = value; }
		}

		public IRS2290() : base()
		{
		}
	}


	[XmlRoot(ElementName="IRS2290Schedule1",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class IRS2290Schedule1 : IRS_Return_8849.IRS2290Schedule1Type
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}

		

		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		public IRS2290Schedule1() : base()
		{
		}
	}


	[XmlRoot(ElementName="CreditsAmountStatement",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class CreditsAmountStatement : IRS_Return_8849.CreditsAmountStatementType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}
        /* Vishwa 2015
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]        
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        */
		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		public CreditsAmountStatement() : base()
		{
		}
	}


	[XmlRoot(ElementName="IRSPayment2",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class IRSPayment2 : IRS_Return_8849.IRSPaymentType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}

        /* Vishwa 2015 */
        /*
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
         
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        */
        /* Vishwa 2015 */
        /*
		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]        
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}
        */
		public IRSPayment2() : base()
		{
		}
	}


	[XmlRoot(ElementName="ReasonableCauseExpln",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class ReasonableCauseExpln : IRS_Return_8849.ReasonableCauseExplnTyp
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}
        /* Vishwa 2015 
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
         
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        * */
        [XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		public ReasonableCauseExpln() : base()
		{
		}
	}


	[XmlRoot(ElementName="StmtInSupportOfSuspension",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class StmtInSupportOfSuspension : IRS_Return_8849.StmtInSupportOfSuspensionType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}
        /* Vishwa 2015 
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
         
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        * */
        [XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		public StmtInSupportOfSuspension() : base()
		{
		}
	}


	[XmlRoot(ElementName="SuspendedVINStatement",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class SuspendedVINStatement : IRS_Return_8849.SuspendedVINStatementType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}

        /*
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
        */
		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}

		public SuspendedVINStatement() : base()
		{
		}
	}


	[XmlRoot(ElementName="TGWIncreaseWorksheet",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class TGWIncreaseWorksheet : IRS_Return_8849.TGWIncreaseWorksheetType
	{

		[XmlAttribute(AttributeName="documentId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __documentId;
		
		[XmlIgnore]
		public string documentId
		{ 
			get { return __documentId; }
			set { __documentId = value; }
		}

		[XmlAttribute(AttributeName="softwareId",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __softwareId;
		
		[XmlIgnore]
		public string softwareId
		{ 
			get { return __softwareId; }
			set { __softwareId = value; }
		}

        /* Vishwa 2015 */
        /* 
		[XmlAttribute(AttributeName="softwareVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
        
		public string __softwareVersion;
		
		[XmlIgnore]
		public string softwareVersion
		{ 
			get { return __softwareVersion; }
			set { __softwareVersion = value; }
		}
         *  */
        
		[XmlAttribute(AttributeName="documentName",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
        
		public string __documentName;
		
		[XmlIgnore]
		public string documentName
		{ 
			get { return __documentName; }
			set { __documentName = value; }
		}
               
		public TGWIncreaseWorksheet() : base()
		{
		}
	}


	[XmlRoot(ElementName="ReturnData",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class ReturnData
	{
        /* Vishwa 2015 */
        /*
		[XmlAttribute(AttributeName="documentCount",Form=XmlSchemaForm.Unqualified,DataType="positiveInteger",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]        
		public string __documentCount;
	
		[XmlIgnore]
		public string documentCount
		{ 
			get { return __documentCount; }
			set { __documentCount = value; }
		}
        */

		[XmlElement(Type=typeof(IRS_Return_8849.IRS2290),ElementName="IRS2290",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.IRS2290 __IRS2290;
		
		[XmlIgnore]
		public IRS_Return_8849.IRS2290 IRS2290
		{
			get
			{
				if (__IRS2290 == null) __IRS2290 = new IRS_Return_8849.IRS2290();		
				return __IRS2290;
			}
			set {__IRS2290 = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.IRS2290Schedule1),ElementName="IRS2290Schedule1",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.IRS2290Schedule1 __IRS2290Schedule1;
		
		[XmlIgnore]
		public IRS_Return_8849.IRS2290Schedule1 IRS2290Schedule1
		{
			get
			{
				if (__IRS2290Schedule1 == null) __IRS2290Schedule1 = new IRS_Return_8849.IRS2290Schedule1();		
				return __IRS2290Schedule1;
			}
			set {__IRS2290Schedule1 = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.CreditsAmountStatement),ElementName="CreditsAmountStatement",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public CreditsAmountStatementCollection __CreditsAmountStatementCollection;
		
		[XmlIgnore]
		public CreditsAmountStatementCollection CreditsAmountStatementCollection
		{
			get
			{
				if (__CreditsAmountStatementCollection == null) __CreditsAmountStatementCollection = new CreditsAmountStatementCollection();
				return __CreditsAmountStatementCollection;
			}
			set {__CreditsAmountStatementCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.IRSPayment2),ElementName="IRSPayment2",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRSPayment2Collection __IRSPayment2Collection;
		
		[XmlIgnore]
		public IRSPayment2Collection IRSPayment2Collection
		{
			get
			{
				if (__IRSPayment2Collection == null) __IRSPayment2Collection = new IRSPayment2Collection();
				return __IRSPayment2Collection;
			}
			set {__IRSPayment2Collection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ReasonableCauseExpln),ElementName="ReasonableCauseExpln",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ReasonableCauseExplnCollection __ReasonableCauseExplnCollection;
		
		[XmlIgnore]
		public ReasonableCauseExplnCollection ReasonableCauseExplnCollection
		{
			get
			{
				if (__ReasonableCauseExplnCollection == null) __ReasonableCauseExplnCollection = new ReasonableCauseExplnCollection();
				return __ReasonableCauseExplnCollection;
			}
			set {__ReasonableCauseExplnCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.StmtInSupportOfSuspension),ElementName="StmtInSupportOfSuspension",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public StmtInSupportOfSuspensionCollection __StmtInSupportOfSuspensionCollection;
		
		[XmlIgnore]
		public StmtInSupportOfSuspensionCollection StmtInSupportOfSuspensionCollection
		{
			get
			{
				if (__StmtInSupportOfSuspensionCollection == null) __StmtInSupportOfSuspensionCollection = new StmtInSupportOfSuspensionCollection();
				return __StmtInSupportOfSuspensionCollection;
			}
			set {__StmtInSupportOfSuspensionCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.SuspendedVINStatement),ElementName="SuspendedVINStatement",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SuspendedVINStatementCollection __SuspendedVINStatementCollection;
		
		[XmlIgnore]
		public SuspendedVINStatementCollection SuspendedVINStatementCollection
		{
			get
			{
				if (__SuspendedVINStatementCollection == null) __SuspendedVINStatementCollection = new SuspendedVINStatementCollection();
				return __SuspendedVINStatementCollection;
			}
			set {__SuspendedVINStatementCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.TGWIncreaseWorksheet),ElementName="TGWIncreaseWorksheet",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TGWIncreaseWorksheetCollection __TGWIncreaseWorksheetCollection;
		
		[XmlIgnore]
		public TGWIncreaseWorksheetCollection TGWIncreaseWorksheetCollection
		{
			get
			{
				if (__TGWIncreaseWorksheetCollection == null) __TGWIncreaseWorksheetCollection = new TGWIncreaseWorksheetCollection();
				return __TGWIncreaseWorksheetCollection;
			}
			set {__TGWIncreaseWorksheetCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.GeneralDependencyMedium),ElementName="GeneralDependencyMedium",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public GeneralDependencyMediumCollection __GeneralDependencyMediumCollection;
		
		[XmlIgnore]
		public GeneralDependencyMediumCollection GeneralDependencyMediumCollection
		{
			get
			{
				if (__GeneralDependencyMediumCollection == null) __GeneralDependencyMediumCollection = new GeneralDependencyMediumCollection();
				return __GeneralDependencyMediumCollection;
			}
			set {__GeneralDependencyMediumCollection = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.BinaryAttachment),ElementName="BinaryAttachment",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public BinaryAttachmentCollection __BinaryAttachmentCollection;
		
		[XmlIgnore]
		public BinaryAttachmentCollection BinaryAttachmentCollection
		{
			get
			{
				if (__BinaryAttachmentCollection == null) __BinaryAttachmentCollection = new BinaryAttachmentCollection();
				return __BinaryAttachmentCollection;
			}
			set {__BinaryAttachmentCollection = value;}
		}

		public ReturnData()
		{
		}
	}


	[XmlRoot(ElementName="Return",Namespace=Declarations.SchemaVersion,IsNullable=false),Serializable]
	public class @Return
	{

		[XmlAttribute(AttributeName="returnVersion",Form=XmlSchemaForm.Unqualified,DataType="string",Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __returnVersion;
		
		[XmlIgnore]
		public string returnVersion
		{ 
			get { return __returnVersion; }
			set { __returnVersion = value; }
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ReturnHeaderType),ElementName="ReturnHeader",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.ReturnHeaderType __ReturnHeader;
		
		[XmlIgnore]
		public IRS_Return_8849.ReturnHeaderType ReturnHeader
		{
			get
			{
				if (__ReturnHeader == null) __ReturnHeader = new IRS_Return_8849.ReturnHeaderType();		
				return __ReturnHeader;
			}
			set {__ReturnHeader = value;}
		}

		[XmlElement(Type=typeof(IRS_Return_8849.ReturnData),ElementName="ReturnData",IsNullable=false,Form=XmlSchemaForm.Qualified,Namespace=Declarations.SchemaVersion)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IRS_Return_8849.ReturnData __ReturnData;
		
		[XmlIgnore]
		public IRS_Return_8849.ReturnData ReturnData
		{
			get
			{
				if (__ReturnData == null) __ReturnData = new IRS_Return_8849.ReturnData();		
				return __ReturnData;
			}
			set {__ReturnData = value;}
		}

		public @Return()
		{
		}
	}
}
