﻿//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// Vish 2021 TY changes as per IRS specs

using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace ReturnF8849_2021
{
    using System.Xml.Serialization;

    public struct Declarations
    {
        public const string SchemaVersion = "http://www.irs.gov/efile";
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class BinaryAttachment : BinaryAttachmentType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public BinaryAttachment()
        {
            this.documentNameField = "BinaryAttachment";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015
         * 
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
        */
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }

        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }
        /*
        [XmlAttribute(AttributeName = "documentName", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __documentName;

        [XmlIgnore]
        public string documentName
        {
            get { return __documentName; }
            set { __documentName = value; }
        }
         * */
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class BinaryAttachmentType
    {

        private BinaryAttachmentTypeDocumentType documentTypeField;

        private string descriptionField;

        private string attachmentLocationField;

        /// <remarks/>
        public BinaryAttachmentTypeDocumentType DocumentType
        {
            get
            {
                return this.documentTypeField;
            }
            set
            {
                this.documentTypeField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string AttachmentLocation
        {
            get
            {
                return this.attachmentLocationField;
            }
            set
            {
                this.attachmentLocationField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum BinaryAttachmentTypeDocumentType
    {

        /// <remarks/>
        PDF,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class IPAddressType
    {

        private string itemField;

        private ItemChoiceType2 itemElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IPv4Address", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("IPv6Address", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public string Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType2 ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum ItemChoiceType2
    {

        /// <remarks/>
        IPv4Address,

        /// <remarks/>
        IPv6Address,
    }
    /* Vishwa 2015 */
    [XmlType(TypeName = "ForeignAddressType", Namespace = Declarations.SchemaVersion), Serializable]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public class ForeignAddressType
    {

        [XmlElement(ElementName = "AddressLine1Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __AddressLine1;

        [XmlIgnore]
        public string AddressLine1
        {
            get { return __AddressLine1; }
            set { __AddressLine1 = value; }
        }

        [XmlElement(ElementName = "AddressLine2Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __AddressLine2;

        [XmlIgnore]
        public string AddressLine2
        {
            get { return __AddressLine2; }
            set { __AddressLine2 = value; }
        }

        [XmlElement(ElementName = "CityNm", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __City;

        [XmlIgnore]
        public string City
        {
            get { return __City; }
            set { __City = value; }
        }

        [XmlElement(ElementName = "ProvinceOrStateNm", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __ProvinceOrState;

        [XmlIgnore]
        public string ProvinceOrState
        {
            get { return __ProvinceOrState; }
            set { __ProvinceOrState = value; }
        }

        [XmlElement(ElementName = "CountryCd", IsNullable = false, Form = XmlSchemaForm.Qualified, Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public ReturnF8849_2021.CountryType __Country;

        [XmlIgnore]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool __CountrySpecified;

        [XmlIgnore]
        public ReturnF8849_2021.CountryType Country
        {
            get { return __Country; }
            set { __Country = value; __CountrySpecified = true; }
        }

        [XmlElement(ElementName = "ForeignPostalCd", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __PostalCode;

        [XmlIgnore]
        public string PostalCode
        {
            get { return __PostalCode; }
            set { __PostalCode = value; }
        }

        public ForeignAddressType()
        {
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum CountryType
    {

        /// <remarks/>
        AF,

        /// <remarks/>
        AX,

        /// <remarks/>
        AL,

        /// <remarks/>
        AG,

        /// <remarks/>
        AQ,

        /// <remarks/>
        AN,

        /// <remarks/>
        AO,

        /// <remarks/>
        AV,

        /// <remarks/>
        AY,

        /// <remarks/>
        AC,

        /// <remarks/>
        AR,

        /// <remarks/>
        AM,

        /// <remarks/>
        AA,

        /// <remarks/>
        AT,

        /// <remarks/>
        AS,

        /// <remarks/>
        AU,

        /// <remarks/>
        AJ,

        /// <remarks/>
        BF,

        /// <remarks/>
        BA,

        /// <remarks/>
        FQ,

        /// <remarks/>
        BG,

        /// <remarks/>
        BB,

        /// <remarks/>
        BO,

        /// <remarks/>
        BE,

        /// <remarks/>
        BH,

        /// <remarks/>
        BN,

        /// <remarks/>
        BD,

        /// <remarks/>
        BT,

        /// <remarks/>
        BL,

        /// <remarks/>
        BK,

        /// <remarks/>
        BC,

        /// <remarks/>
        BV,

        /// <remarks/>
        BR,

        /// <remarks/>
        IO,

        /// <remarks/>
        VI,

        /// <remarks/>
        BX,

        /// <remarks/>
        BU,

        /// <remarks/>
        UV,

        /// <remarks/>
        BM,

        /// <remarks/>
        BY,

        /// <remarks/>
        CB,

        /// <remarks/>
        CM,

        /// <remarks/>
        CA,

        /// <remarks/>
        CV,

        /// <remarks/>
        CJ,

        /// <remarks/>
        CT,

        /// <remarks/>
        CD,

        /// <remarks/>
        CI,

        /// <remarks/>
        CH,

        /// <remarks/>
        KT,

        /// <remarks/>
        IP,

        /// <remarks/>
        CK,

        /// <remarks/>
        CO,

        /// <remarks/>
        CN,

        /// <remarks/>
        CF,

        /// <remarks/>
        CG,

        /// <remarks/>
        CW,

        /// <remarks/>
        CR,

        /// <remarks/>
        CS,

        /// <remarks/>
        IV,

        /// <remarks/>
        HR,

        /// <remarks/>
        CU,

        /// <remarks/>
        UC,

        /// <remarks/>
        CY,

        /// <remarks/>
        EZ,

        /// <remarks/>
        DA,

        /// <remarks/>
        DX,

        /// <remarks/>
        DJ,

        /// <remarks/>
        DO,

        /// <remarks/>
        DR,

        /// <remarks/>
        TT,

        /// <remarks/>
        EC,

        /// <remarks/>
        EG,

        /// <remarks/>
        ES,

        /// <remarks/>
        EK,

        /// <remarks/>
        ER,

        /// <remarks/>
        EN,

        /// <remarks/>
        ET,

        /// <remarks/>
        FK,

        /// <remarks/>
        FO,

        /// <remarks/>
        FM,

        /// <remarks/>
        FJ,

        /// <remarks/>
        FI,

        /// <remarks/>
        FR,

        /// <remarks/>
        FP,

        /// <remarks/>
        FS,

        /// <remarks/>
        GB,

        /// <remarks/>
        GA,

        /// <remarks/>
        GG,

        /// <remarks/>
        GM,

        /// <remarks/>
        GH,

        /// <remarks/>
        GI,

        /// <remarks/>
        GR,

        /// <remarks/>
        GL,

        /// <remarks/>
        GJ,

        /// <remarks/>
        GQ,

        /// <remarks/>
        GT,

        /// <remarks/>
        GK,

        /// <remarks/>
        GV,

        /// <remarks/>
        PU,

        /// <remarks/>
        GY,

        /// <remarks/>
        HA,

        /// <remarks/>
        HM,

        /// <remarks/>
        VT,

        /// <remarks/>
        HO,

        /// <remarks/>
        HK,

        /// <remarks/>
        HQ,

        /// <remarks/>
        HU,

        /// <remarks/>
        IC,

        /// <remarks/>
        IN,

        /// <remarks/>
        ID,

        /// <remarks/>
        IR,

        /// <remarks/>
        IZ,

        /// <remarks/>
        EI,

        /// <remarks/>
        IS,

        /// <remarks/>
        IT,

        /// <remarks/>
        JM,

        /// <remarks/>
        JN,

        /// <remarks/>
        JA,

        /// <remarks/>
        DQ,

        /// <remarks/>
        JE,

        /// <remarks/>
        JQ,

        /// <remarks/>
        JO,

        /// <remarks/>
        KZ,

        /// <remarks/>
        KE,

        /// <remarks/>
        KQ,

        /// <remarks/>
        KR,

        /// <remarks/>
        KN,

        /// <remarks/>
        KS,

        /// <remarks/>
        KV,

        /// <remarks/>
        KU,

        /// <remarks/>
        KG,

        /// <remarks/>
        LA,

        /// <remarks/>
        LG,

        /// <remarks/>
        LE,

        /// <remarks/>
        LT,

        /// <remarks/>
        LI,

        /// <remarks/>
        LY,

        /// <remarks/>
        LS,

        /// <remarks/>
        LH,

        /// <remarks/>
        LU,

        /// <remarks/>
        MC,

        /// <remarks/>
        MK,

        /// <remarks/>
        MA,

        /// <remarks/>
        MI,

        /// <remarks/>
        MY,

        /// <remarks/>
        MV,

        /// <remarks/>
        ML,

        /// <remarks/>
        MT,

        /// <remarks/>
        IM,

        /// <remarks/>
        RM,

        /// <remarks/>
        MR,

        /// <remarks/>
        MP,

        /// <remarks/>
        MX,

        /// <remarks/>
        MQ,

        /// <remarks/>
        MD,

        /// <remarks/>
        MN,

        /// <remarks/>
        MG,

        /// <remarks/>
        MJ,

        /// <remarks/>
        MH,

        /// <remarks/>
        MO,

        /// <remarks/>
        MZ,

        /// <remarks/>
        WA,

        /// <remarks/>
        NR,

        /// <remarks/>
        BQ,

        /// <remarks/>
        NP,

        /// <remarks/>
        NL,

        /// <remarks/>
        NC,

        /// <remarks/>
        NZ,

        /// <remarks/>
        NU,

        /// <remarks/>
        NG,

        /// <remarks/>
        NI,

        /// <remarks/>
        NE,

        /// <remarks/>
        NF,

        /// <remarks/>
        CQ,

        /// <remarks/>
        NO,

        /// <remarks/>
        MU,

        /// <remarks/>
        OC,

        /// <remarks/>
        PK,

        /// <remarks/>
        PS,

        /// <remarks/>
        LQ,

        /// <remarks/>
        PM,

        /// <remarks/>
        PP,

        /// <remarks/>
        PF,

        /// <remarks/>
        PA,

        /// <remarks/>
        PE,

        /// <remarks/>
        RP,

        /// <remarks/>
        PC,

        /// <remarks/>
        PL,

        /// <remarks/>
        PO,

        /// <remarks/>
        RQ,

        /// <remarks/>
        QA,

        /// <remarks/>
        RO,

        /// <remarks/>
        RS,

        /// <remarks/>
        RW,

        /// <remarks/>
        TB,

        /// <remarks/>
        RN,

        /// <remarks/>
        WS,

        /// <remarks/>
        SM,

        /// <remarks/>
        TP,

        /// <remarks/>
        SA,

        /// <remarks/>
        SG,

        /// <remarks/>
        RI,

        /// <remarks/>
        SE,

        /// <remarks/>
        SL,

        /// <remarks/>
        SN,

        /// <remarks/>
        NN,

        /// <remarks/>
        LO,

        /// <remarks/>
        SI,

        /// <remarks/>
        BP,

        /// <remarks/>
        SO,

        /// <remarks/>
        SF,

        /// <remarks/>
        SX,

        /// <remarks/>
        OD,

        /// <remarks/>
        SP,

        /// <remarks/>
        PG,

        /// <remarks/>
        CE,

        /// <remarks/>
        SH,

        /// <remarks/>
        SC,

        /// <remarks/>
        ST,

        /// <remarks/>
        SB,

        /// <remarks/>
        VC,

        /// <remarks/>
        SU,

        /// <remarks/>
        NS,

        /// <remarks/>
        SV,

        /// <remarks/>
        WZ,

        /// <remarks/>
        SW,

        /// <remarks/>
        SZ,

        /// <remarks/>
        SY,

        /// <remarks/>
        TW,

        /// <remarks/>
        TI,

        /// <remarks/>
        TZ,

        /// <remarks/>
        TH,

        /// <remarks/>
        TO,

        /// <remarks/>
        TL,

        /// <remarks/>
        TN,

        /// <remarks/>
        TD,

        /// <remarks/>
        TS,

        /// <remarks/>
        TU,

        /// <remarks/>
        TX,

        /// <remarks/>
        TK,

        /// <remarks/>
        TV,

        /// <remarks/>
        UG,

        /// <remarks/>
        UP,

        /// <remarks/>
        AE,

        /// <remarks/>
        UK,

        /// <remarks/>
        UY,

        /// <remarks/>
        UZ,

        /// <remarks/>
        NH,

        /// <remarks/>
        VE,

        /// <remarks/>
        VM,

        /// <remarks/>
        VQ,

        /// <remarks/>
        WQ,

        /// <remarks/>
        WF,

        /// <remarks/>
        WI,

        /// <remarks/>
        YM,

        /// <remarks/>
        ZA,

        /// <remarks/>
        ZI,
    }

    /*  Vishwa 2015 */
    /*
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class USAddressType
    {

        private string addressLine1Field;

        private string addressLine2Field;

        private string cityField;

        private StateType stateField;

        private string zIPCodeField;

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public StateType State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string ZIPCode
        {
            get
            {
                return this.zIPCodeField;
            }
            set
            {
                this.zIPCodeField = value;
            }
        }
    }
    */

    [XmlType(TypeName = "USAddressType", Namespace = Declarations.SchemaVersion), Serializable]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public class USAddressType
    {

        [XmlElement(ElementName = "AddressLine1Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __AddressLine1;

        [XmlIgnore]
        public string AddressLine1
        {
            get { return __AddressLine1; }
            set { __AddressLine1 = value; }
        }

        [XmlElement(ElementName = "AddressLine2Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __AddressLine2;

        [XmlIgnore]
        public string AddressLine2
        {
            get { return __AddressLine2; }
            set { __AddressLine2 = value; }
        }

        [XmlElement(ElementName = "CityNm", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __City;

        [XmlIgnore]
        public string City
        {
            get { return __City; }
            set { __City = value; }
        }

        [XmlElement(ElementName = "StateAbbreviationCd", IsNullable = false, Form = XmlSchemaForm.Qualified, Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public ReturnF8849_2021.StateType __State;

        [XmlIgnore]
        public ReturnF8849_2021.StateType State
        {
            get { return __State; }
            set { __State = value; }
        }

        [XmlElement(ElementName = "ZIPCd", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __ZIPCode;

        [XmlIgnore]
        public string ZIPCode
        {
            get { return __ZIPCode; }
            set { __ZIPCode = value; }
        }

        public USAddressType()
        {
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum StateType
    {

        /// <remarks/>
        AL,

        /// <remarks/>
        AK,

        /// <remarks/>
        AS,

        /// <remarks/>
        AZ,

        /// <remarks/>
        AR,

        /// <remarks/>
        CA,

        /// <remarks/>
        CO,

        /// <remarks/>
        MP,

        /// <remarks/>
        CT,

        /// <remarks/>
        DE,

        /// <remarks/>
        DC,

        /// <remarks/>
        FM,

        /// <remarks/>
        FL,

        /// <remarks/>
        GA,

        /// <remarks/>
        GU,

        /// <remarks/>
        HI,

        /// <remarks/>
        ID,

        /// <remarks/>
        IL,

        /// <remarks/>
        IN,

        /// <remarks/>
        IA,

        /// <remarks/>
        KS,

        /// <remarks/>
        KY,

        /// <remarks/>
        LA,

        /// <remarks/>
        ME,

        /// <remarks/>
        MH,

        /// <remarks/>
        MD,

        /// <remarks/>
        MA,

        /// <remarks/>
        MI,

        /// <remarks/>
        MN,

        /// <remarks/>
        MS,

        /// <remarks/>
        MO,

        /// <remarks/>
        MT,

        /// <remarks/>
        NE,

        /// <remarks/>
        NV,

        /// <remarks/>
        NH,

        /// <remarks/>
        NJ,

        /// <remarks/>
        NM,

        /// <remarks/>
        NY,

        /// <remarks/>
        NC,

        /// <remarks/>
        ND,

        /// <remarks/>
        OH,

        /// <remarks/>
        OK,

        /// <remarks/>
        OR,

        /// <remarks/>
        PW,

        /// <remarks/>
        PA,

        /// <remarks/>
        PR,

        /// <remarks/>
        RI,

        /// <remarks/>
        SC,

        /// <remarks/>
        SD,

        /// <remarks/>
        TN,

        /// <remarks/>
        TX,

        /// <remarks/>
        VI,

        /// <remarks/>
        UT,

        /// <remarks/>
        VT,

        /// <remarks/>
        VA,

        /// <remarks/>
        WA,

        /// <remarks/>
        WV,

        /// <remarks/>
        WI,

        /// <remarks/>
        WY,

        /// <remarks/>
        AA,

        /// <remarks/>
        AE,

        /// <remarks/>
        AP,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderType
    {

        private string returnTsField;

        private string taxyearendMonthField;

        private string disasterReliefTxtField;

        private string iSPNumField;

        private ReturnHeaderTypePreparerFirmGrp preparerFirmGrpField;

        private string softwareIdField;

        private string softwareVersionNumField;

        private bool multSoftwarePackagesUsedIndField;

        private ReturnHeaderTypeOriginatorGrp originatorGrpField;

        private ReturnHeaderTypePINEnteredByCd pINEnteredByCdField;

        private bool pINEnteredByCdFieldSpecified;

        private ReturnHeaderTypeSignatureOptionCd signatureOptionCdField;

        private bool signatureOptionCdFieldSpecified;

        private ReturnHeaderTypeThirdPartyDesignee thirdPartyDesigneeField;

        private ReturnHeaderTypeReturnTypeCd returnTypeCdField;

        private ReturnHeaderTypeFiler filerField;

        private ReturnHeaderTypeBusinessOfficerGrp businessOfficerGrpField;

        private ReturnHeaderTypePreparerPersonGrp preparerPersonGrpField;

        private IPAddressType iPAddressField;

        private System.DateTime iPDtField;

        private bool iPDtFieldSpecified;

        private System.DateTime iPTmField;

        private bool iPTmFieldSpecified;

        private TimezoneType iPTimezoneCdField;

        private bool iPTimezoneCdFieldSpecified;

        private ReturnHeaderTypeConsentToVINDataDisclosureGrp consentToVINDataDisclosureGrpField;

        private string taxYrField;

        private string binaryAttachmentCntField;

        /// <remarks/>
        public string ReturnTs
        {
            get
            {
                return this.returnTsField;
            }
            set
            {
                this.returnTsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gYearMonth")]
       
        public string TaxYearEndMonthNum
        {
            get { return this.taxyearendMonthField; }
            set { this.taxyearendMonthField = value; }
        }
        /// <remarks/>
        public string DisasterReliefTxt
        {
            get
            {
                return this.disasterReliefTxtField;
            }
            set
            {
                this.disasterReliefTxtField = value;
            }
        }

        /// <remarks/>
        public string ISPNum
        {
            get
            {
                return this.iSPNumField;
            }
            set
            {
                this.iSPNumField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypePreparerFirmGrp PreparerFirmGrp
        {
            get
            {
                return this.preparerFirmGrpField;
            }
            set
            {
                this.preparerFirmGrpField = value;
            }
        }

        /// <remarks/>
        public string SoftwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }
        [XmlElement(ElementName = "SoftwareVersionNum", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        /// <remarks/>
        public string SoftwareVersionNum
        {
            get
            {
                return this.softwareVersionNumField;
            }
            set
            {
                this.softwareVersionNumField = value;
            }
        }

        /// <remarks/>
        public bool MultSoftwarePackagesUsedInd
        {
            get
            {
                return this.multSoftwarePackagesUsedIndField;
            }
            set
            {
                this.multSoftwarePackagesUsedIndField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeOriginatorGrp OriginatorGrp
        {
            get
            {
                return this.originatorGrpField;
            }
            set
            {
                this.originatorGrpField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypePINEnteredByCd PINEnteredByCd
        {
            get
            {
                return this.pINEnteredByCdField;
            }
            set
            {
                this.pINEnteredByCdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PINEnteredByCdSpecified
        {
            get
            {
                return this.pINEnteredByCdFieldSpecified;
            }
            set
            {
                this.pINEnteredByCdFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeSignatureOptionCd SignatureOptionCd
        {
            get
            {
                return this.signatureOptionCdField;
            }
            set
            {
                this.signatureOptionCdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignatureOptionCdSpecified
        {
            get
            {
                return this.signatureOptionCdFieldSpecified;
            }
            set
            {
                this.signatureOptionCdFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeThirdPartyDesignee ThirdPartyDesignee
        {
            get
            {
                return this.thirdPartyDesigneeField;
            }
            set
            {
                this.thirdPartyDesigneeField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeReturnTypeCd ReturnTypeCd
        {
            get
            {
                return this.returnTypeCdField;
            }
            set
            {
                this.returnTypeCdField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeFiler Filer
        {
            get
            {
                return this.filerField;
            }
            set
            {
                this.filerField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeBusinessOfficerGrp BusinessOfficerGrp
        {
            get
            {
                return this.businessOfficerGrpField;
            }
            set
            {
                this.businessOfficerGrpField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypePreparerPersonGrp PreparerPersonGrp
        {
            get
            {
                return this.preparerPersonGrpField;
            }
            set
            {
                this.preparerPersonGrpField = value;
            }
        }

        /// <remarks/>
        public IPAddressType IPAddress
        {
            get
            {
                return this.iPAddressField;
            }
            set
            {
                this.iPAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime IPDt
        {
            get
            {
                return this.iPDtField;
            }
            set
            {
                this.iPDtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IPDtSpecified
        {
            get
            {
                return this.iPDtFieldSpecified;
            }
            set
            {
                this.iPDtFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime IPTm
        {
            get
            {
                return this.iPTmField;
            }
            set
            {
                this.iPTmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IPTmSpecified
        {
            get
            {
                return this.iPTmFieldSpecified;
            }
            set
            {
                this.iPTmFieldSpecified = value;
            }
        }

        /// <remarks/>
        public TimezoneType IPTimezoneCd
        {
            get
            {
                return this.iPTimezoneCdField;
            }
            set
            {
                this.iPTimezoneCdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IPTimezoneCdSpecified
        {
            get
            {
                return this.iPTimezoneCdFieldSpecified;
            }
            set
            {
                this.iPTimezoneCdFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeConsentToVINDataDisclosureGrp ConsentToVINDataDisclosureGrp
        {
            get
            {
                return this.consentToVINDataDisclosureGrpField;
            }
            set
            {
                this.consentToVINDataDisclosureGrpField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gYear")]
        public string TaxYr
        {
            get
            {
                return this.taxYrField;
            }
            set
            {
                this.taxYrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string binaryAttachmentCnt
        {
            get
            {
                return this.binaryAttachmentCntField;
            }
            set
            {
                this.binaryAttachmentCntField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypePreparerFirmGrp
    {

        private string preparerFirmEINField;

        private BusinessNameType preparerFirmNameField;

        private object itemField;

        /// <remarks/>
        public string PreparerFirmEIN
        {
            get
            {
                return this.preparerFirmEINField;
            }
            set
            {
                this.preparerFirmEINField = value;
            }
        }

        /// <remarks/>
        public BusinessNameType PreparerFirmName
        {
            get
            {
                return this.preparerFirmNameField;
            }
            set
            {
                this.preparerFirmNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PreparerForeignAddress", typeof(ForeignAddressType))]
        [System.Xml.Serialization.XmlElementAttribute("PreparerUSAddress", typeof(USAddressType))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class BusinessNameType
    {

        private string businessNameLine1Field;

        private string businessNameLine2Field;
        /* Vishwa 2015 */
        [XmlElement(ElementName = "BusinessNameLine1Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        /// <remarks/>
        public string BusinessNameLine1Txt
        {
            get
            {
                return this.businessNameLine1Field;
            }
            set
            {
                this.businessNameLine1Field = value;
            }
        }

        [XmlElement(ElementName = "BusinessNameLine2Txt", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]

        /// <remarks/>
        public string BusinessNameLine2Txt
        {
            get
            {
                return this.businessNameLine2Field;
            }
            set
            {
                this.businessNameLine2Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeOriginatorGrp
    {

        private string eFINField;

        private OriginatorType originatorTypeCdField;

        private ReturnHeaderTypeOriginatorGrpPractitionerPINGrp practitionerPINGrpField;

        /// <remarks/>
        public string EFIN
        {
            get
            {
                return this.eFINField;
            }
            set
            {
                this.eFINField = value;
            }
        }

        /// <remarks/>
        public OriginatorType OriginatorTypeCd
        {
            get
            {
                return this.originatorTypeCdField;
            }
            set
            {
                this.originatorTypeCdField = value;
            }
        }

        /// <remarks/>
        public ReturnHeaderTypeOriginatorGrpPractitionerPINGrp PractitionerPINGrp
        {
            get
            {
                return this.practitionerPINGrpField;
            }
            set
            {
                this.practitionerPINGrpField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum OriginatorType
    {

        /// <remarks/>
        ERO,

        /// <remarks/>
        OnlineFiler,

        /// <remarks/>
        ReportingAgent,

        /// <remarks/>
        IRSAgent,

        /// <remarks/>
        FinancialAgent,

        /// <remarks/>
        LargeTaxpayer,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeOriginatorGrpPractitionerPINGrp
    {

        private string eFINField;

        private string pINField;

        /// <remarks/>
        public string EFIN
        {
            get
            {
                return this.eFINField;
            }
            set
            {
                this.eFINField = value;
            }
        }

        /// <remarks/>
        public string PIN
        {
            get
            {
                return this.pINField;
            }
            set
            {
                this.pINField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum ReturnHeaderTypePINEnteredByCd
    {

        /// <remarks/>
        Taxpayer,

        /// <remarks/>
        ERO,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum ReturnHeaderTypeSignatureOptionCd
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("PIN Number")]
        PINNumber,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Binary Attachment 8453 Signature Document")]
        BinaryAttachment8453SignatureDocument,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeThirdPartyDesignee
    {

        private object[] itemsField;

        private ItemsChoiceType[] itemsElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DiscussWithThirdPartyNoInd", typeof(CheckboxType))]
        [System.Xml.Serialization.XmlElementAttribute("DiscussWithThirdPartyYesInd", typeof(CheckboxType))]
        [System.Xml.Serialization.XmlElementAttribute("ThirdPartyDesigneeNm", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("ThirdPartyDesigneePIN", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("ThirdPartyDesigneePhoneNum", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemsChoiceType[] ItemsElementName
        {
            get
            {
                return this.itemsElementNameField;
            }
            set
            {
                this.itemsElementNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum CheckboxType
    {

        /// <remarks/>
        X,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum ItemsChoiceType
    {

        /// <remarks/>
        DiscussWithThirdPartyNoInd,

        /// <remarks/>
        DiscussWithThirdPartyYesInd,

        /// <remarks/>
        ThirdPartyDesigneeNm,

        /// <remarks/>
        ThirdPartyDesigneePIN,

        /// <remarks/>
        ThirdPartyDesigneePhoneNum,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum ReturnHeaderTypeReturnTypeCd
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("8849")]
        Item8849,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeFiler
    {

        private string eINField;

        //private string businessNameLine1TxtField;
        private BusinessNameType businessNameField;

        private string SSNField;

        private string inCareOfNmField;

        private string businessNameControlTxtField;

        private object itemField;

        /// <remarks/>
        public string EIN
        {
            get
            {
                return this.eINField;
            }
            set
            {
                this.eINField = value;
            }
        }

        /// <remarks/>
        public BusinessNameType BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        ///
        /// <remarks/>
        public string SSN
        {
            get
            {
                return this.SSNField;
            }
            set
            {
                this.SSNField = value;
            }
        }
        /// <remarks/>
        public string InCareOfNm
        {
            get
            {
                return this.inCareOfNmField;
            }
            set
            {
                this.inCareOfNmField = value;
            }
        }

        /// <remarks/>
        public string BusinessNameControlTxt
        {
            get
            {
                return this.businessNameControlTxtField;
            }
            set
            {
                this.businessNameControlTxtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ForeignAddress", typeof(ForeignAddressType))]
        [System.Xml.Serialization.XmlElementAttribute("USAddress", typeof(USAddressType))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeBusinessOfficerGrp
    {

        private string personNmField;

        private string personTitleTxtField;

        private string taxpayerPINField;

        private string itemField;

        private ItemChoiceType itemElementNameField;

        private string emailAddressTxtField;

        private System.DateTime signatureDtField;

        /// <remarks/>
        public string PersonNm
        {
            get
            {
                return this.personNmField;
            }
            set
            {
                this.personNmField = value;
            }
        }

        /// <remarks/>
        public string PersonTitleTxt
        {
            get
            {
                return this.personTitleTxtField;
            }
            set
            {
                this.personTitleTxtField = value;
            }
        }

        /// <remarks/>
        public string TaxpayerPIN
        {
            get
            {
                return this.taxpayerPINField;
            }
            set
            {
                this.taxpayerPINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ForeignPhoneNum", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("PhoneNum", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public string Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }

        /// <remarks/>
        public string EmailAddressTxt
        {
            get
            {
                return this.emailAddressTxtField;
            }
            set
            {
                this.emailAddressTxtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime SignatureDt
        {
            get
            {
                return this.signatureDtField;
            }
            set
            {
                this.signatureDtField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum ItemChoiceType
    {

        /// <remarks/>
        ForeignPhoneNum,

        /// <remarks/>
        PhoneNum,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypePreparerPersonGrp
    {

        private string businessNameField;

        private string pTINField;

        private string itemField;

        private ItemChoiceType1 itemElementNameField;

        private string emailAddressTxtField;

        private System.DateTime preparationDtField;

        private bool preparationDtFieldSpecified;

        private CheckboxType selfEmployedIndField;

        private bool selfEmployedIndFieldSpecified;

        /* Vishwa 2015 Now we dont have BusinessName it is replaced with PreparerPersonNm */

        /*
        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }
        */
        /// <remarks/>
        public string PreparerPersonNm
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string PTIN
        {
            get
            {
                return this.pTINField;
            }
            set
            {
                this.pTINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ForeignPhoneNum", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("PhoneNum", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public string Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType1 ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }

        /// <remarks/>
        public string EmailAddressTxt
        {
            get
            {
                return this.emailAddressTxtField;
            }
            set
            {
                this.emailAddressTxtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime PreparationDt
        {
            get
            {
                return this.preparationDtField;
            }
            set
            {
                this.preparationDtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreparationDtSpecified
        {
            get
            {
                return this.preparationDtFieldSpecified;
            }
            set
            {
                this.preparationDtFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType SelfEmployedInd
        {
            get
            {
                return this.selfEmployedIndField;
            }
            set
            {
                this.selfEmployedIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelfEmployedIndSpecified
        {
            get
            {
                return this.selfEmployedIndFieldSpecified;
            }
            set
            {
                this.selfEmployedIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum ItemChoiceType1
    {

        /// <remarks/>
        ForeignPhoneNum,

        /// <remarks/>
        PhoneNum,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum TimezoneType
    {

        /// <remarks/>
        US,

        /// <remarks/>
        ES,

        /// <remarks/>
        ED,

        /// <remarks/>
        CS,

        /// <remarks/>
        CD,

        /// <remarks/>
        MS,

        /// <remarks/>
        MD,

        /// <remarks/>
        PS,

        /// <remarks/>
        PD,

        /// <remarks/>
        AS,

        /// <remarks/>
        AD,

        /// <remarks/>
        HS,

        /// <remarks/>
        HD,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeConsentToVINDataDisclosureGrp
    {

        private object[] itemsField;

        private ItemsChoiceType1[] itemsElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ConsentToDiscloseNoInd", typeof(CheckboxType))]
        [System.Xml.Serialization.XmlElementAttribute("ConsentToDiscloseYesInd", typeof(CheckboxType))]
        [System.Xml.Serialization.XmlElementAttribute("DisclosureFormSignatureInfo", typeof(ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo))]
        [System.Xml.Serialization.XmlElementAttribute("PIN", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("SignatureOptionCd", typeof(ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemsChoiceType1[] ItemsElementName
        {
            get
            {
                return this.itemsElementNameField;
            }
            set
            {
                this.itemsElementNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo
    {

        private string eINField;

        private BusinessNameType businessNameField;

        private System.DateTime signatureDtField;

        /// <remarks/>
        public string EIN
        {
            get
            {
                return this.eINField;
            }
            set
            {
                this.eINField = value;
            }
        }

        /// <remarks/>
        public BusinessNameType BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime SignatureDt
        {
            get
            {
                return this.signatureDtField;
            }
            set
            {
                this.signatureDtField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("PIN Number")]
        PINNumber,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Binary Attachment VIN Disclosure Statement")]
        BinaryAttachmentVINDisclosureStatement,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum ItemsChoiceType1
    {

        /// <remarks/>
        ConsentToDiscloseNoInd,

        /// <remarks/>
        ConsentToDiscloseYesInd,

        /// <remarks/>
        DisclosureFormSignatureInfo,

        /// <remarks/>
        PIN,

        /// <remarks/>
        SignatureOptionCd,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class TGWIncreaseWorksheetType
    {

        private TGWIncreaseInfoType[] tGWIncreaseInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TGWIncreaseInfo")]
        public TGWIncreaseInfoType[] TGWIncreaseInfo
        {
            get
            {
                return this.tGWIncreaseInfoField;
            }
            set
            {
                this.tGWIncreaseInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class TGWIncreaseInfoType
    {

        private string tGWIncreaseMonthNumField;

        private string tGWCategoryCdField;

        private decimal newTaxAmtField;

        private bool newTaxAmtFieldSpecified;

        private decimal previousTaxAmtField;

        private bool previousTaxAmtFieldSpecified;

        private decimal additionalTaxAmtField;

        private bool additionalTaxAmtFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gMonth")]
        public string TGWIncreaseMonthNum
        {
            get
            {
                return this.tGWIncreaseMonthNumField;
            }
            set
            {
                this.tGWIncreaseMonthNumField = value;
            }
        }

        /// <remarks/>
        public string TGWCategoryCd
        {
            get
            {
                return this.tGWCategoryCdField;
            }
            set
            {
                this.tGWCategoryCdField = value;
            }
        }

        /// <remarks/>
        public decimal NewTaxAmt
        {
            get
            {
                return this.newTaxAmtField;
            }
            set
            {
                this.newTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NewTaxAmtSpecified
        {
            get
            {
                return this.newTaxAmtFieldSpecified;
            }
            set
            {
                this.newTaxAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PreviousTaxAmt
        {
            get
            {
                return this.previousTaxAmtField;
            }
            set
            {
                this.previousTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreviousTaxAmtSpecified
        {
            get
            {
                return this.previousTaxAmtFieldSpecified;
            }
            set
            {
                this.previousTaxAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal AdditionalTaxAmt
        {
            get
            {
                return this.additionalTaxAmtField;
            }
            set
            {
                this.additionalTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AdditionalTaxAmtSpecified
        {
            get
            {
                return this.additionalTaxAmtFieldSpecified;
            }
            set
            {
                this.additionalTaxAmtFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class SuspendedVINStatementType
    {

        private SuspendedVINInfoTypeVINDetail[] suspendedVINInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VINDetail", IsNullable = false)]
        public SuspendedVINInfoTypeVINDetail[] SuspendedVINInfo
        {
            get
            {
                return this.suspendedVINInfoField;
            }
            set
            {
                this.suspendedVINInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class SuspendedVINInfoTypeVINDetail
    {

        private string vINField;

        /// <remarks/>
        public string VIN
        {
            get
            {
                return this.vINField;
            }
            set
            {
                this.vINField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class StmtInSupportOfSuspensionType
    {

        private StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail[] stmtInSupportOfSuspensionInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VehicleSuspensionDetail", typeof(StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail), IsNullable = false)]
        public StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail[] StmtInSupportOfSuspensionInfo
        {
            get
            {
                return this.stmtInSupportOfSuspensionInfoField;
            }
            set
            {
                this.stmtInSupportOfSuspensionInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail
    {

        private string vINField;

        private BusinessNameType businessNameField;

        private System.DateTime dtField;

        /// <remarks/>
        public string VIN
        {
            get
            {
                return this.vINField;
            }
            set
            {
                this.vINField = value;
            }
        }

        /// <remarks/>
        public BusinessNameType BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime Dt
        {
            get
            {
                return this.dtField;
            }
            set
            {
                this.dtField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class ReasonableCauseExplnTyp
    {

        private string explanationTxtField;

        /// <remarks/>
        public string ExplanationTxt
        {
            get
            {
                return this.explanationTxtField;
            }
            set
            {
                this.explanationTxtField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class IRSPaymentType
    {

        private string routingTransitNumField;

        private string bankAccountNumField;

        private BankAccountType bankAccountTypeCdField;

        private decimal paymentAmtField;

        private System.DateTime requestedPaymentDtField;

        private string taxpayerDaytimePhoneNumField;

        /// <remarks/>
        public string RoutingTransitNum
        {
            get
            {
                return this.routingTransitNumField;
            }
            set
            {
                this.routingTransitNumField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNum
        {
            get
            {
                return this.bankAccountNumField;
            }
            set
            {
                this.bankAccountNumField = value;
            }
        }

        /// <remarks/>
        public BankAccountType BankAccountTypeCd
        {
            get
            {
                return this.bankAccountTypeCdField;
            }
            set
            {
                this.bankAccountTypeCdField = value;
            }
        }

        /// <remarks/>
        public decimal PaymentAmt
        {
            get
            {
                return this.paymentAmtField;
            }
            set
            {
                this.paymentAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime RequestedPaymentDt
        {
            get
            {
                return this.requestedPaymentDtField;
            }
            set
            {
                this.requestedPaymentDtField = value;
            }
        }

        /// <remarks/>
        public string TaxpayerDaytimePhoneNum
        {
            get
            {
                return this.taxpayerDaytimePhoneNumField;
            }
            set
            {
                this.taxpayerDaytimePhoneNumField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public enum BankAccountType
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class CreditsAmountStatementType
    {

        private CreditsAmountInfoTypeDisposalReportingItem[] creditsAmountInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("DisposalReportingItem", typeof(CreditsAmountInfoTypeDisposalReportingItem), IsNullable = false)]
        public CreditsAmountInfoTypeDisposalReportingItem[] CreditsAmountInfo
        {
            get
            {
                return this.creditsAmountInfoField;
            }
            set
            {
                this.creditsAmountInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class CreditsAmountInfoTypeDisposalReportingItem
    {

        private string creditsAmountExplanationTxtField;

        private string disposalReportingVINField;

        private System.DateTime disposalReportingDtField;

        private string disposalReportingAmtField;

        /// <remarks/>
        public string CreditsAmountExplanationTxt
        {
            get
            {
                return this.creditsAmountExplanationTxtField;
            }
            set
            {
                this.creditsAmountExplanationTxtField = value;
            }
        }

        /// <remarks/>
        public string DisposalReportingVIN
        {
            get
            {
                return this.disposalReportingVINField;
            }
            set
            {
                this.disposalReportingVINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DisposalReportingDt
        {
            get
            {
                return this.disposalReportingDtField;
            }
            set
            {
                this.disposalReportingDtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "nonNegativeInteger")]
        public string DisposalReportingAmt
        {
            get
            {
                return this.disposalReportingAmtField;
            }
            set
            {
                this.disposalReportingAmtField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class OtherClaimsExplanationStmtType
    {
        private string explanationTxtField;
        ///<remarks/>
        public string ExplanationTxt
        {
            get { return this.explanationTxtField; }
            set { this.explanationTxtField = value; }
        }


    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class IRS8849Schedule6Type
    {
        private OtherClaimNotRptOnOthFormsGrp otherClaimNotRptOnOthFormsField;

        private IRS2290Schedule1TypeVehicleReportTaxItem[] vehicleReportTaxItemField;

        private decimal totalRefundAmtField;

        private System.DateTime earliestClaimDtField;

        private System.DateTime latestClaimDtField;

        private string vehicleCntField;

        private string taxableVehicleCntField;

        private string totalSuspendedVehicleCntField;


        ///<remarks/>
        public decimal TotalRefundAmt
        {
            get { return this.totalRefundAmtField; }
            set {this.totalRefundAmtField = value; }
        }

        ///<remarks/>
        //public System.DateTime EarliestClaimDt
        //{
        //    get { return this.earliestClaimDtField; }
        //    set { this.earliestClaimDtField = value; }
        //}

        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime EarliestClaimDt
        {
            get
            {
                return this.earliestClaimDtField;
            }
            set
            {
                this.earliestClaimDtField = value;
            }
        }

        /////<remarks/>
        //public System.DateTime LatestClaimDt
        //{
        //    get { return this.latestClaimDtField; }
        //    set { this.latestClaimDtField = value; }
        //}

        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LatestClaimDt
        {
            get
            {
                return this.latestClaimDtField;
            }
            set
            {
                this.latestClaimDtField = value;
            }
        }
        

        ///<remarks/>
        public OtherClaimNotRptOnOthFormsGrp OtherClaimNotRptOnOthFormsGrp
         {
             get { return this.otherClaimNotRptOnOthFormsField; }
            set { this.otherClaimNotRptOnOthFormsField = value; }
         }
       
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VehicleReportTaxItem")]
        public IRS2290Schedule1TypeVehicleReportTaxItem[] VehicleReportTaxItem
        {
            get
            {
                return this.vehicleReportTaxItemField;
            }
            set
            {
                this.vehicleReportTaxItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string VehicleCnt
        {
            get
            {
                return this.vehicleCntField;
            }
            set
            {
                this.vehicleCntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string TotalSuspendedVehicleCnt
        {
            get
            {
                return this.totalSuspendedVehicleCntField;
            }
            set
            {
                this.totalSuspendedVehicleCntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string TaxableVehicleCnt
        {
            get
            {
                return this.taxableVehicleCntField;
            }
            set
            {
                this.taxableVehicleCntField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class OtherClaimNotRptOnOthFormsGrp
    {
        private OtherClaimNotRptGrp otherClaimNotRptField;
      
        
        private string referenceDocumentNameField;
        
        ///<remarks/>
        public OtherClaimNotRptGrp OtherClaimNotRptGrp
        {
            get { return this.otherClaimNotRptField; }
            set { this.otherClaimNotRptField = value; }
        }
    }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class OtherClaimNotRptGrp
    {
        private OtherTaxClaimGrp otherTaxClaimField;
        private decimal refundAmount;
        private string creditReference;
        private string[] referenceDocumentIdField;
        private string referenceDocumentNameField;

        public OtherClaimNotRptGrp()
        {
            this.referenceDocumentNameField = "OtherClaimsExplanationStatement";
        }

        ///<remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }


        ///<remarks/>
        public OtherTaxClaimGrp OtherTaxClaimGrp
        {
            get { return this.otherTaxClaimField; }
            set { this.otherTaxClaimField = value; }
        }
        ///<remarks/>
        public decimal Amt
        {
            get { return this.refundAmount; }
            set { this.refundAmount = value; }
        }
        ///<remarks/>
        public string CreditReferenceNum
        {
            get { return this.creditReference; }
            set { this.creditReference = value; }
        }


    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class OtherTaxClaimGrp
    {
        private string taxTypeDesc;
        private string vinType;
        /// <remarks/>
        public string TaxTypeDesc
        {
            get { return this.taxTypeDesc; }
            set { this.taxTypeDesc = value; }
        }
        /// <remarks/>
        public string VIN
        {
            get { return this.vinType; }
            set { this.vinType = value; }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290Schedule1TypeVehicleReportTaxItem
    {

        private string vINField;

        private string vehicleCategoryCdField;

        /// <remarks/>
        public string VIN
        {
            get
            {
                return this.vINField;
            }
            set
            {
                this.vINField = value;
            }
        }

        /// <remarks/>
        public string VehicleCategoryCd
        {
            get
            {
                return this.vehicleCategoryCdField;
            }
            set
            {
                this.vehicleCategoryCdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class HighwayMtrVehTxCmptColumnsGrpType
    {

        private decimal nonLoggingVehPartialTaxAmtField;

        private bool nonLoggingVehPartialTaxAmtFieldSpecified;

        private decimal loggingVehPartialTaxAmtField;

        private bool loggingVehPartialTaxAmtFieldSpecified;

        private string nonLoggingVehicleCntField;

        private string loggingVehicleCntField;

        private decimal taxAmtField;

        private bool taxAmtFieldSpecified;

        /// <remarks/>
        public decimal NonLoggingVehPartialTaxAmt
        {
            get
            {
                return this.nonLoggingVehPartialTaxAmtField;
            }
            set
            {
                this.nonLoggingVehPartialTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NonLoggingVehPartialTaxAmtSpecified
        {
            get
            {
                return this.nonLoggingVehPartialTaxAmtFieldSpecified;
            }
            set
            {
                this.nonLoggingVehPartialTaxAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal LoggingVehPartialTaxAmt
        {
            get
            {
                return this.loggingVehPartialTaxAmtField;
            }
            set
            {
                this.loggingVehPartialTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LoggingVehPartialTaxAmtSpecified
        {
            get
            {
                return this.loggingVehPartialTaxAmtFieldSpecified;
            }
            set
            {
                this.loggingVehPartialTaxAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string NonLoggingVehicleCnt
        {
            get
            {
                return this.nonLoggingVehicleCntField;
            }
            set
            {
                this.nonLoggingVehicleCntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string LoggingVehicleCnt
        {
            get
            {
                return this.loggingVehicleCntField;
            }
            set
            {
                this.loggingVehicleCntField = value;
            }
        }

        /// <remarks/>
        public decimal TaxAmt
        {
            get
            {
                return this.taxAmtField;
            }
            set
            {
                this.taxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TaxAmtSpecified
        {
            get
            {
                return this.taxAmtFieldSpecified;
            }
            set
            {
                this.taxAmtFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class IRS8849Type
    {

        private string specialConditionDescField;

        private CheckboxType schedule6AttachedIndField;

        private CheckboxType addressChangeIndField;

        private bool addressChangeIndFieldSpecified;

        private CheckboxType amendedReturnIndField;

        private bool amendedReturnIndFieldSpecified;

        private string amendedMonthNumField;

        //private CheckboxType vINCorrectionIndField;

        //private bool vINCorrectionIndFieldSpecified;

        private IRS2290TypeVINCorrInd VINCorrectionField;

        private VINCorrectionExplanationStmtType VINCorrectionExplnFld;

        private CheckboxType finalReturnIndField;

        private bool finalReturnIndFieldSpecified;

        private IRS2290TypeAdditionalTaxAmt additionalTaxAmtField;

        private decimal totalTaxAmtField;

        private bool totalTaxAmtFieldSpecified;

        private IRS2290TypeTaxCreditsAmt taxCreditsAmtField;

        //private decimal balanceDueAmtField;

        private CheckboxType eFTPSPaymentIndField;

        private bool eFTPSPaymentIndFieldSpecified;

        private CheckboxType CreditDebitCardPaymentIndField;

        private bool CreditDebitCardPaymentIndFieldSpecified;


        private CheckboxType mileageUsed5000OrLessIndField;

        private bool mileageUsed5000OrLessIndFieldSpecified;

        private CheckboxType agricMileageUsed7500OrLessIndField;

        private bool agricMileageUsed7500OrLessIndFieldSpecified;

        private CheckboxType notSubjectToTaxIndField;

        private bool notSubjectToTaxIndFieldSpecified;

        private IRS2290TypeSuspendedVINReferenceTyp suspendedVINReferenceTypField;

        private IRS2290TypeTrnsfrSuspendedVINReferenceTyp trnsfrSuspendedVINReferenceTypField;

        private IRS2290TypeHighwayMtrVehTxComputationGrp[] highwayMtrVehTxComputationGrpField;

        private string totalVehicleCntField;

        private decimal totalTaxComputationAmtField;

        private bool totalTaxComputationAmtFieldSpecified;

        private string taxSuspendedLoggingVehCntField;

        private string taxSuspendedNonLoggingVehCntField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SpecialConditionDesc")]
        public string SpecialConditionDesc
        {
            get
            {
                return this.specialConditionDescField;
            }
            set
            {
                this.specialConditionDescField = value;
            }
        }

        ///<remarks/>
        public CheckboxType Schedule6AttachedInd
        {
            get
            {
                return this.schedule6AttachedIndField;
            }
            set
            {
                 this.schedule6AttachedIndField = value;
            }

        }

        /// <remarks/>
        public CheckboxType AddressChangeInd
        {
            get
            {
                return this.addressChangeIndField;
            }
            set
            {
                this.addressChangeIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AddressChangeIndSpecified
        {
            get
            {
                return this.addressChangeIndFieldSpecified;
            }
            set
            {
                this.addressChangeIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType AmendedReturnInd
        {
            get
            {
                return this.amendedReturnIndField;
            }
            set
            {
                this.amendedReturnIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmendedReturnIndSpecified
        {
            get
            {
                return this.amendedReturnIndFieldSpecified;
            }
            set
            {
                this.amendedReturnIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gMonth")]
        public string AmendedMonthNum
        {
            get
            {
                return this.amendedMonthNumField;
            }
            set
            {
                this.amendedMonthNumField = value;
            }
        }

        // 2019 VIN Correction Comment Below
        /*
        /// <remarks/>
        public CheckboxType VINCorrectionInd
        {
            get
            {
                return this.vINCorrectionIndField;
            }
            set
            {
                this.vINCorrectionIndField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VINCorrectionIndSpecified
        {
            get
            {
                return this.vINCorrectionIndFieldSpecified;
            }
            set
            {
                this.vINCorrectionIndFieldSpecified = value;
            }
        }
        */
        /// <remarks/>
        public CheckboxType FinalReturnInd
        {
            get
            {
                return this.finalReturnIndField;
            }
            set
            {
                this.finalReturnIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FinalReturnIndSpecified
        {
            get
            {
                return this.finalReturnIndFieldSpecified;
            }
            set
            {
                this.finalReturnIndFieldSpecified = value;
            }
        }

        //2019 VIN Correction
        public IRS2290TypeVINCorrInd VINCorrectionInd
        {
            get
            {
                return this.VINCorrectionField;
            }
            set
            {
                this.VINCorrectionField = value;
            }
        }

        //2019 New VIN Correction Statement
        public VINCorrectionExplanationStmtType VINCorrectionExplanationStmt
        {
            get
            {
                return this.VINCorrectionExplnFld;
            }
            set
            {
                this.VINCorrectionExplnFld = value;
            }
        }


        /// <remarks/>
        public IRS2290TypeAdditionalTaxAmt AdditionalTaxAmt
        {
            get
            {
                return this.additionalTaxAmtField;
            }
            set
            {
                this.additionalTaxAmtField = value;
            }
        }

        /// <remarks/>
        public decimal TotalTaxAmt
        {
            get
            {
                return this.totalTaxAmtField;
            }
            set
            {
                this.totalTaxAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalTaxAmtSpecified
        {
            get
            {
                return this.totalTaxAmtFieldSpecified;
            }
            set
            {
                this.totalTaxAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        public IRS2290TypeTaxCreditsAmt TaxCreditsAmt
        {
            get
            {
                return this.taxCreditsAmtField;
            }
            set
            {
                this.taxCreditsAmtField = value;
            }
        }

        /// <remarks/>
        //public decimal BalanceDueAmt
        //{
        //    get
        //    {
        //        return this.balanceDueAmtField;
        //    }
        //    set
        //    {
        //        this.balanceDueAmtField = value;
        //    }
        //}

        /// <remarks/>
        public CheckboxType CreditDebitCardPaymentInd
        {
            get
            {
                return this.CreditDebitCardPaymentIndField;
            }
            set
            {
                this.CreditDebitCardPaymentIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreditDebitCardPaymentIndSpecified
        {
            get
            {
                return this.CreditDebitCardPaymentIndFieldSpecified;
            }
            set
            {
                this.CreditDebitCardPaymentIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType EFTPSPaymentInd
        {
            get
            {
                return this.eFTPSPaymentIndField;
            }
            set
            {
                this.eFTPSPaymentIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EFTPSPaymentIndSpecified
        {
            get
            {
                return this.eFTPSPaymentIndFieldSpecified;
            }
            set
            {
                this.eFTPSPaymentIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType MileageUsed5000OrLessInd
        {
            get
            {
                return this.mileageUsed5000OrLessIndField;
            }
            set
            {
                this.mileageUsed5000OrLessIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MileageUsed5000OrLessIndSpecified
        {
            get
            {
                return this.mileageUsed5000OrLessIndFieldSpecified;
            }
            set
            {
                this.mileageUsed5000OrLessIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType AgricMileageUsed7500OrLessInd
        {
            get
            {
                return this.agricMileageUsed7500OrLessIndField;
            }
            set
            {
                this.agricMileageUsed7500OrLessIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgricMileageUsed7500OrLessIndSpecified
        {
            get
            {
                return this.agricMileageUsed7500OrLessIndFieldSpecified;
            }
            set
            {
                this.agricMileageUsed7500OrLessIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CheckboxType NotSubjectToTaxInd
        {
            get
            {
                return this.notSubjectToTaxIndField;
            }
            set
            {
                this.notSubjectToTaxIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NotSubjectToTaxIndSpecified
        {
            get
            {
                return this.notSubjectToTaxIndFieldSpecified;
            }
            set
            {
                this.notSubjectToTaxIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        public IRS2290TypeSuspendedVINReferenceTyp SuspendedVINReferenceTyp
        {
            get
            {
                return this.suspendedVINReferenceTypField;
            }
            set
            {
                this.suspendedVINReferenceTypField = value;
            }
        }

        /// <remarks/>
        public IRS2290TypeTrnsfrSuspendedVINReferenceTyp TrnsfrSuspendedVINReferenceTyp
        {
            get
            {
                return this.trnsfrSuspendedVINReferenceTypField;
            }
            set
            {
                this.trnsfrSuspendedVINReferenceTypField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("HighwayMtrVehTxComputationGrp")]
        public IRS2290TypeHighwayMtrVehTxComputationGrp[] HighwayMtrVehTxComputationGrp
        {
            get
            {
                return this.highwayMtrVehTxComputationGrpField;
            }
            set
            {
                this.highwayMtrVehTxComputationGrpField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string TotalVehicleCnt
        {
            get
            {
                return this.totalVehicleCntField;
            }
            set
            {
                this.totalVehicleCntField = value;
            }
        }

        /// <remarks/>
        public decimal TotalTaxComputationAmt
        {
            get
            {
                return this.totalTaxComputationAmtField;
            }
            set
            {
                this.totalTaxComputationAmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalTaxComputationAmtSpecified
        {
            get
            {
                return this.totalTaxComputationAmtFieldSpecified;
            }
            set
            {
                this.totalTaxComputationAmtFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string TaxSuspendedLoggingVehCnt
        {
            get
            {
                return this.taxSuspendedLoggingVehCntField;
            }
            set
            {
                this.taxSuspendedLoggingVehCntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
        public string TaxSuspendedNonLoggingVehCnt
        {
            get
            {
                return this.taxSuspendedNonLoggingVehCntField;
            }
            set
            {
                this.taxSuspendedNonLoggingVehCntField = value;
            }
        }
    }

    //2019 New VINCorrection 
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeVINCorrInd
    {
        private string[] referenceDocumentIdField;
        private string referenceDocumentNameField;
        private string valueField;

        public IRS2290TypeVINCorrInd()
        {
            this.referenceDocumentNameField = "VINCorrectionExplanationStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    //2019 NEW VIN Correction Statement Added

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class VINCorrectionExplanationStmtType
    {

        private string ExplanationTxtField;

        /// <remarks/>
        public string ExplanationTxt
        {
            get
            {
                return this.ExplanationTxtField;
            }
            set
            {
                this.ExplanationTxtField = value;
            }
        }

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        //private string documentNameField;

        public VINCorrectionExplanationStmtType()
        {
            //this.documentNameField = "VINCorrectoinExp";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015 */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }

        ///// <remarks/>
        //[System.Xml.Serialization.XmlAttributeAttribute()]
        //public string documentName
        //{
        //    get
        //    {
        //        return this.documentNameField;
        //    }
        //    set
        //    {
        //        this.documentNameField = value;
        //    }
        //}

    }




    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeAdditionalTaxAmt
    {

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        private decimal valueField;

        public IRS2290TypeAdditionalTaxAmt()
        {
            this.referenceDocumentNameField = "TGWIncreaseWorksheet";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeTaxCreditsAmt
    {

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        private decimal valueField;

        public IRS2290TypeTaxCreditsAmt()
        {
            this.referenceDocumentNameField = "CreditsAmountStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeSuspendedVINReferenceTyp
    {

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        public IRS2290TypeSuspendedVINReferenceTyp()
        {
            this.referenceDocumentNameField = "SuspendedVINStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeTrnsfrSuspendedVINReferenceTyp
    {

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        public IRS2290TypeTrnsfrSuspendedVINReferenceTyp()
        {
            this.referenceDocumentNameField = "StatementInSupportOfSuspension";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public partial class IRS2290TypeHighwayMtrVehTxComputationGrp
    {

        private string vehicleCategoryCdField;

        private HighwayMtrVehTxCmptColumnsGrpType highwayMtrVehTxCmptColumnsGrpField;

        /// <remarks/>
        public string VehicleCategoryCd
        {
            get
            {
                return this.vehicleCategoryCdField;
            }
            set
            {
                this.vehicleCategoryCdField = value;
            }
        }

        /// <remarks/>
        public HighwayMtrVehTxCmptColumnsGrpType HighwayMtrVehTxCmptColumnsGrp
        {
            get
            {
                return this.highwayMtrVehTxCmptColumnsGrpField;
            }
            set
            {
                this.highwayMtrVehTxCmptColumnsGrpField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile")]
    public partial class GeneralDependencyMediumType
    {

        private object itemField;

        private object item1Field;

        private Item1ChoiceType item1ElementNameField;

        private string formLineOrInstructionRefTxtField;

        private string regulationReferenceTxtField;

        private string descField;

        private string attachmentInformationMedDescField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BusinessName", typeof(BusinessNameType))]
        [System.Xml.Serialization.XmlElementAttribute("PersonNm", typeof(string))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EIN", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("MissingEINReasonCd", typeof(GeneralDependencyMediumTypeMissingEINReasonCd))]
        [System.Xml.Serialization.XmlElementAttribute("SSN", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("Item1ElementName")]
        public object Item1
        {
            get
            {
                return this.item1Field;
            }
            set
            {
                this.item1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public Item1ChoiceType Item1ElementName
        {
            get
            {
                return this.item1ElementNameField;
            }
            set
            {
                this.item1ElementNameField = value;
            }
        }

        /// <remarks/>
        public string FormLineOrInstructionRefTxt
        {
            get
            {
                return this.formLineOrInstructionRefTxtField;
            }
            set
            {
                this.formLineOrInstructionRefTxtField = value;
            }
        }

        /// <remarks/>
        public string RegulationReferenceTxt
        {
            get
            {
                return this.regulationReferenceTxtField;
            }
            set
            {
                this.regulationReferenceTxtField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
            }
        }

        /// <remarks/>
        public string AttachmentInformationMedDesc
        {
            get
            {
                return this.attachmentInformationMedDescField;
            }
            set
            {
                this.attachmentInformationMedDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    public enum GeneralDependencyMediumTypeMissingEINReasonCd
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("APPLD FOR")]
        APPLDFOR,

        /// <remarks/>
        FOREIGNUS,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.irs.gov/efile", IncludeInSchema = false)]
    public enum Item1ChoiceType
    {

        /// <remarks/>
        EIN,

        /// <remarks/>
        MissingEINReasonCd,

        /// <remarks/>
        SSN,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class GeneralDependencyMedium : GeneralDependencyMediumType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        public GeneralDependencyMedium()
        {
            this.documentNameField = "GeneralDependencyMedium";
            this.referenceDocumentNameField = "BinaryAttachment";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }


        /* Vishwa 2015 */
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
        */
        [XmlElement(ElementName = "softwareVersionNum", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __SoftwareVersion;

        [XmlIgnore]
        public string SoftwareVersion
        {
            get { return __SoftwareVersion; }
            set { __SoftwareVersion = value; }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class IRS8849 : IRS8849Type
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        private string[] referenceDocumentIdField;

        private string referenceDocumentNameField;

        public IRS8849()
        {
            this.documentNameField = "IRS8849";
            this.referenceDocumentNameField = "GeneralDependencyMedium IRS8849Schedule1 IRS8849Schedule2 IRS8849Schedule3 IRS8849Schedule5 IRS8849Schedule6 IRS8849Schedule8";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }
        /* Vishwa 2015 */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
         * */

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] referenceDocumentId
        {
            get
            {
                return this.referenceDocumentIdField;
            }
            set
            {
                this.referenceDocumentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string referenceDocumentName
        {
            get
            {
                return this.referenceDocumentNameField;
            }
            set
            {
                this.referenceDocumentNameField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class OtherClaimsExplanationStmt : OtherClaimsExplanationStmtType
    {
        private string documentNameField;
        private string documentIdField;
        public OtherClaimsExplanationStmt()
        {
            this.documentNameField = "OtherClaimsExplanationStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class IRS8849Schedule6 : IRS8849Schedule6Type
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public IRS8849Schedule6()
        {
            this.documentNameField = "IRS8849Schedule6";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015 */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion {
            get {
                return this.softwareVersionField;
            }
            set {
                this.softwareVersionField = value;
            }
        }
        */

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class CreditsAmountStatement : CreditsAmountStatementType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public CreditsAmountStatement()
        {
            this.documentNameField = "CreditsAmountStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015 
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
        */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class IRSPayment2 : IRSPaymentType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public IRSPayment2()
        {
            this.documentNameField = "IRSPayment2";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }
        /* Vishwa 2015 */
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
        */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class ReasonableCauseExpln : ReasonableCauseExplnTyp
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public ReasonableCauseExpln()
        {
            this.documentNameField = "ReasonableCauseExplanation2";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015 
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
         * */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]

        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class StmtInSupportOfSuspension : StmtInSupportOfSuspensionType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public StmtInSupportOfSuspension()
        {
            this.documentNameField = "StatementInSupportOfSuspension";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }
        /* Vishwa 2015 
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
         * */
        [XmlAttribute(AttributeName = "softwareVersion", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]

        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class SuspendedVINStatement : SuspendedVINStatementType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public SuspendedVINStatement()
        {
            this.documentNameField = "SuspendedVINStatement";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }
        /* Vishwa 2015 */

        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }
        */
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class TGWIncreaseWorksheet : TGWIncreaseWorksheetType
    {

        private string documentIdField;

        private string softwareIdField;

        private string softwareVersionField;

        private string documentNameField;

        public TGWIncreaseWorksheet()
        {
            this.documentNameField = "TGWIncreaseWorksheet";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentId
        {
            get
            {
                return this.documentIdField;
            }
            set
            {
                this.documentIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareId
        {
            get
            {
                return this.softwareIdField;
            }
            set
            {
                this.softwareIdField = value;
            }
        }

        /* Vishwa 2015 */
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string softwareVersion
        {
            get
            {
                return this.softwareVersionField;
            }
            set
            {
                this.softwareVersionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentName
        {
            get
            {
                return this.documentNameField;
            }
            set
            {
                this.documentNameField = value;
            }
        }
         **/
        [XmlAttribute(AttributeName = "softwareVersionNum", Form = XmlSchemaForm.Unqualified, DataType = "string", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __softwareVersion;

        [XmlIgnore]
        public string softwareVersion
        {
            get { return __softwareVersion; }
            set { __softwareVersion = value; }
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class ReturnData
    {

        private IRS8849 iRS8849Field;

        private IRS8849Schedule6 iRS8849Schedule6Field;

        private VINCorrectionExplanationStmtType VINCorrectionExplnFld;

        private CreditsAmountStatement[] creditsAmountStatementField;

        private IRSPayment2[] iRSPayment2Field;

        private ReasonableCauseExpln[] reasonableCauseExplnField;

        private StmtInSupportOfSuspension[] stmtInSupportOfSuspensionField;

        private SuspendedVINStatement[] suspendedVINStatementField;

        private TGWIncreaseWorksheet[] tGWIncreaseWorksheetField;

        private OtherClaimsExplanationStmt otherClaimsExplanationStmtField;

        private GeneralDependencyMedium[] generalDependencyMediumField;

        private BinaryAttachment[] binaryAttachmentField;

        private string documentCountField;

        /// <remarks/>
        public IRS8849 IRS8849
        {
            get
            {
                return this.iRS8849Field;
            }
            set
            {
                this.iRS8849Field = value;
            }
        }

        /// <remarks/>
        public IRS8849Schedule6 IRS8849Schedule6
        {
            get
            {
                return this.iRS8849Schedule6Field;
            }
            set
            {
                this.iRS8849Schedule6Field = value;
            }
        }
        /// <remarks/>
        public OtherClaimsExplanationStmt OtherClaimsExplanationStmt
        {
            get
            {
                return this.otherClaimsExplanationStmtField;
            }
            set
            {
                this.otherClaimsExplanationStmtField = value;
            }
        }
       

        //2019 New VIN Correction Statement
        public VINCorrectionExplanationStmtType VINCorrectionExplanationStmt
        {
            get
            {
                return this.VINCorrectionExplnFld;
            }
            set
            {
                this.VINCorrectionExplnFld = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CreditsAmountStatement")]
        public CreditsAmountStatement[] CreditsAmountStatement
        {
            get
            {
                return this.creditsAmountStatementField;
            }
            set
            {
                this.creditsAmountStatementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IRSPayment2")]
        public IRSPayment2[] IRSPayment2
        {
            get
            {
                return this.iRSPayment2Field;
            }
            set
            {
                this.iRSPayment2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ReasonableCauseExpln")]
        public ReasonableCauseExpln[] ReasonableCauseExpln
        {
            get
            {
                return this.reasonableCauseExplnField;
            }
            set
            {
                this.reasonableCauseExplnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StmtInSupportOfSuspension")]
        public StmtInSupportOfSuspension[] StmtInSupportOfSuspension
        {
            get
            {
                return this.stmtInSupportOfSuspensionField;
            }
            set
            {
                this.stmtInSupportOfSuspensionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SuspendedVINStatement")]
        public SuspendedVINStatement[] SuspendedVINStatement
        {
            get
            {
                return this.suspendedVINStatementField;
            }
            set
            {
                this.suspendedVINStatementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TGWIncreaseWorksheet")]
        public TGWIncreaseWorksheet[] TGWIncreaseWorksheet
        {
            get
            {
                return this.tGWIncreaseWorksheetField;
            }
            set
            {
                this.tGWIncreaseWorksheetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GeneralDependencyMedium")]
        public GeneralDependencyMedium[] GeneralDependencyMedium
        {
            get
            {
                return this.generalDependencyMediumField;
            }
            set
            {
                this.generalDependencyMediumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BinaryAttachment")]
        public BinaryAttachment[] BinaryAttachment
        {
            get
            {
                return this.binaryAttachmentField;
            }
            set
            {
                this.binaryAttachmentField = value;
            }
        }

        /* Vishwa 2015 */
        [XmlAttribute(AttributeName = "documentCnt", Form = XmlSchemaForm.Unqualified, DataType = "positiveInteger", Namespace = Declarations.SchemaVersion)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __documentCount;

        [XmlIgnore]
        public string documentCount
        {
            get { return __documentCount; }
            set { __documentCount = value; }
        }
        /*
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string documentCount
        {
            get
            {
                return this.documentCountField;
            }
            set
            {
                this.documentCountField = value;
            }
        }
         * */
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.irs.gov/efile")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.irs.gov/efile", IsNullable = false)]
    public partial class Return
    {

        private ReturnHeaderType returnHeaderField;

        private ReturnData returnDataField;

        private string returnVersionField;

        public Return()
        {
            this.returnVersionField = "2015v4.2"; // "2015v4.0"; //Vishwa 4.2 
        }

        /// <remarks/>
        public ReturnHeaderType ReturnHeader
        {
            get
            {
                return this.returnHeaderField;
            }
            set
            {
                this.returnHeaderField = value;
            }
        }

        /// <remarks/>
        public ReturnData ReturnData
        {
            get
            {
                return this.returnDataField;
            }
            set
            {
                this.returnDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string returnVersion
        {
            get
            {
                return this.returnVersionField;
            }
            set
            {
                this.returnVersionField = value;
            }
        }
    }
}
