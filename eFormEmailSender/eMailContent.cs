using System;
using System.Data;
using System.Configuration;

public class eMailContent
{
    public eMailContent()
    {
    }

    public static string ReferralMail_To(string mailTo)
    {
        string mail4ReferredTo = "<html><head></head><body>" +
              "<b>Dear " + mailTo + ":<br/><br/></b>" +
              "Thank you for using our referral service and realize the value of our affordable and efficient service. This great decision saves you money every time when you use our service. Now spread the word and help your friends save too!" +
              "<br/><br/>When you use our service next time, please use the below coupon code at the checkout to get 20% discount.<br/><br/><b>REFERRAL2010<b/><br/><br/>" +
              "--<br/>With Regards," +
              "<br/><a href='https://www.eform22290.com'>eform2290.com</a> Rewards Team" +
              "<br/>www.eform2290.com" +
              "<br/>" +
              "</body></html>";
        return mail4ReferredTo;
    }

    public static string ReferralMail_By(string refBy, string refTo)
    {
        string mail4ReferredBy = "<html><head></head><body>" +
               "<b>Dear " + refBy + ":<br/><br/></b>" +
               refBy + " has used www.eform2290.com  service to file Form 2290 electronically and has referred you to our service to save money and time. You may want to use our introductory offer below when you sign up and file your tax return with us.<br/><br/>" +
               "Please use the below coupon code at the checkout to get 20% discount.<br/><br/><b>REFERRAL2010<b/><br/><br/>" +
               "--<br/>With Regards," +
               "<br/>eform2290 Rewards Team" +
               "<br/>www.eform2290.com" +
               "<br/>" +
               "</body></html>";
        return mail4ReferredBy;
    }

    public static string ReferralMail_Support(string refBy, string refTo)
    {
        string mail4ReferralUpdate = refBy + " has referred " + refTo + " to www.eform2290.com using our referral program.";
        return mail4ReferralUpdate;
    }

    public static string FullServiceMail_Support(string user)
    {
        string mail4FullService = "Premium Service: File submitted by "+ user;
        return mail4FullService;
    }
   
}
